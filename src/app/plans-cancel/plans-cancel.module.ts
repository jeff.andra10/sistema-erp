import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from "angular2-text-mask";
import { FlexLayoutModule } from "@angular/flex-layout";
import { RouterModule, Routes } from '@angular/router'

import { PlansCancelComponent } from "./plans-cancel.component";
import { PlanCancelComponent } from "./plan-cancel/plan-cancel.component";
import { NewPlansCancelComponent } from "./new-plans-cancel/new-plans-cancel.component";
import { EditPlansCancelComponent } from "./edit-plans-cancel/edit-plans-cancel.component";
import { ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../angular-material/material.module";
import { FuseSharedModule } from "@fuse/shared.module";

const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'plans-cancel', component: PlansCancelComponent },
    { path: 'plans-cancel/new', component: NewPlansCancelComponent },
    { path: 'plans-cancel/new/:id', component: PlanCancelComponent },
    { path: 'plans-cancel/:id', component: EditPlansCancelComponent },
  ]);

@NgModule({
    declarations:[
        PlansCancelComponent,
        PlanCancelComponent,
        NewPlansCancelComponent,
        EditPlansCancelComponent
    ],
    imports:[
        TextMaskModule,
        FlexLayoutModule,
        RouterModule,
        ReactiveFormsModule,
        MaterialModule,
        FuseSharedModule,
        authRouting
    ],
    exports:[
        PlansCancelComponent,
        PlanCancelComponent,
        NewPlansCancelComponent,
        EditPlansCancelComponent
    ],
    entryComponents: [
        
    ]
})
export class PlansCancelModule{}