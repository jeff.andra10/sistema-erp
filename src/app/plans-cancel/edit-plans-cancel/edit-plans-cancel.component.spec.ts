import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPlansCancelComponent } from './edit-plans-cancel.component';

describe('EditPlansCancelComponent', () => {
  let component: EditPlansCancelComponent;
  let fixture: ComponentFixture<EditPlansCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPlansCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPlansCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
