import { Component, OnInit, Injector , Pipe, PipeTransform } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { MaskUtils } from '../../core/mask-utils';
import { PlansService } from '../../core/services/plans.service';
import { VehiclesService } from '../../core/services/vehicles.service';
import { fuseAnimations } from '@fuse/animations';
import { BaseComponent } from '../../_base/base-component.component';
import { UserService } from '../../core/services/users.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-edit-plans-cancel',
  templateUrl: './edit-plans-cancel.component.html',
  styleUrls: ['./edit-plans.component.scss'],
  animations: fuseAnimations
})
export class EditPlansCancelComponent extends BaseComponent implements OnInit {

  vehicleForm: FormGroup
  customerForm: FormGroup
  planForm: FormGroup

  plan: any
  phoneMask = MaskUtils.phoneMask
  mobilePhoneMask =  MaskUtils.mobilePhoneMask  
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private router: Router,
    private snackbar: MatSnackBar,
    private plansCancelService: PlansService,
    private vehicleService: VehiclesService,
    private usersService: UserService
  ) {
    super(injector)
   }

  ngOnInit() {

    this.plansCancelService.currentPlan.subscribe(currentPlan => this.plan = currentPlan)
    
    this.initForms()

    if (!this.plan.device) {
      this.router.navigate(['/plans-cancel'])
      return
    } else {
      this.loadData()
    }
  }

  initForms() {

    this.vehicleForm = this.formBuilder.group({
      licensePlate: this.formBuilder.control({value : '', disabled: true}),
      type: this.formBuilder.control({value : '', disabled: true}),
      chassi: this.formBuilder.control({value : '', disabled: true}),
      manufacturer: this.formBuilder.control({value : '', disabled: true}),
      model: this.formBuilder.control({value : '', disabled: true}),
      color: this.formBuilder.control({value : '', disabled: true}),
      manufacturerYear: this.formBuilder.control({value : '', disabled: true}),
      modelYear: this.formBuilder.control({value : '', disabled: true}),
    })

    this.customerForm = this.formBuilder.group({
      name: this.formBuilder.control({value : '', disabled: true})
    })

    this.planForm = this.formBuilder.group({
      name: this.formBuilder.control({value : '', disabled: true}),
      cancelDate: this.formBuilder.control({value : '', disabled: true}),
      cancelUser: this.formBuilder.control({value : '', disabled: true}),
      reactivateDate: this.formBuilder.control({value : '', disabled: true}),
      reactivateUser: this.formBuilder.control({value : '', disabled: true}),      
    })
  }

  loadData() {

    if (this.plan.relationships.user) {
      this.customerForm.patchValue ({
        name: this.plan.relationships.user.name
      })
    }

    if (this.plan.relationships.offer) {
      this.planForm.patchValue ({
        name: this.plan.relationships.offer.name
      })
    }

    if (this.plan.relationships.device 
        && this.plan.relationships.device.car) {

      this.vehicleService.getOne(this.plan.relationships.device.car)
      .subscribe(vehicle => {

        this.vehicleForm.patchValue ({
          licensePlate: vehicle.licensePlate,
          type: vehicle.type,
          chassi: vehicle.chassi,
          manufacturer: vehicle.manufacture,
          model: vehicle.model,
          color: vehicle.color,
          manufacturerYear: vehicle.registrationYear,
          modelYear: vehicle.modelYear
        })
      })
    }

    if (this.plan.relationships.lifecycle) {

      this.planForm.patchValue({
        cancelUser : this.plan.relationships.lifecycle.cancelLoggedUser.login
      })

      var datePipe = new DatePipe(navigator.language)
      this.planForm.patchValue({
        cancelDate : datePipe.transform(new Date(this.plan.lifecycle.cancelDate), 'dd/MM/yyyy HH:mm:ss')
      })        
    }

    if (this.plan.relationships.lifecycle) {

        this.planForm.patchValue({
          reactivateUser : this.plan.relationships.lifecycle.reactivateLoggedUser.login
        })

        var datePipe = new DatePipe(navigator.language)
        this.planForm.patchValue({
          reactivateDate : datePipe.transform(new Date(this.plan.lifecycle.reactivateDate), 'dd/MM/yyyy HH:mm:ss')
        })        
    }    
  }

  cancel() {
    this.router.navigate(['/plans-cancel'])
  }

  reactivatePlan() {
    

    this.plansCancelService.reactivatePlan(this.plan.id, { loggedUser : this.appContext.session.user.id })
    .subscribe(
      result => {
        this.snackbar.open('Plano reativado com sucesso', '', { duration: 8000})
        this.router.navigate(['/plans-cancel'])
      },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000})
      })
  }

}
