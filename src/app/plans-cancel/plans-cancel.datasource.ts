import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import { PlansService } from "../core/services/plans.service";

export class PlansCancelDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private totalElementsSubject = new BehaviorSubject<number>(0)

    public totalElements$ = this.totalElementsSubject.asObservable()

    constructor(
        private plansService: PlansService) {
    }

    loadPlans(filter:string,
                sortDirection:string,
                pageIndex:number,
                pageSize:number) {

        this.plansService.findCanceledPlans(filter, sortDirection,
            pageIndex, pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe(plans => {
                this.totalElementsSubject.next(plans.totalElements)
                this.dataSubject.next(plans.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
