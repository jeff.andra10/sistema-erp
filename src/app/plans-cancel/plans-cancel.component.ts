import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";

import { PlansCancelDataSource } from './plans-cancel.datasource';
import { PlansService } from '../core/services/plans.service';
import { Subject } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';

import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-plans-cancel',
  templateUrl: './plans-cancel.component.html',
  styleUrls: ['./plans-cancel.component.scss'],
  animations: fuseAnimations
})
export class PlansCancelComponent implements OnInit, AfterViewInit {

  displayedColumns = ['userName', 'cpfCnpj', 'offerName', 'cancelDate']
  dataSource: PlansCancelDataSource
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  resultLength: number = 0
  searchSubject = new Subject()
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(
    private router: Router,
    private plansService: PlansService,
  ) { 
  }

  ngOnInit() {

    this.initForms()
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    this.dataSource.totalElements$.subscribe( value => {
      this.resultLength = value
    })

    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadPlansCancel())
    )
    .subscribe()
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }  

  initForms() {

    this.dataSource = new PlansCancelDataSource(this.plansService)
    this.dataSource.loadPlans('', 'asc', 0, 10)

    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadPlansCancel(val)
    })
  }

  loadPlansCancel(terms? : string) {
    this.dataSource.loadPlans(
      terms, 
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }

  selectPlan(plan) {
    this.plansService.setPlan(plan)
    this.router.navigate([`/plans-cancel/${plan.id}`])
  }
}
