import { Component, OnInit, Injector } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Address } from '../../core/models/address.model';
import { MaskUtils } from '../../core/mask-utils';
import { PlansService } from '../../core/services/plans.service';
import { CustomersService } from '../../core/services/customers.service';
import { VehiclesService } from '../../core/services/vehicles.service';
import { UtilsService } from '../../core/utils.service';
import { StateAddress } from '../../core/common-data-domain';
import { BaseComponent } from '../../_base/base-component.component';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'tnm-plan-cancel',
  templateUrl: './plan-cancel.component.html'
})
export class PlanCancelComponent extends BaseComponent implements OnInit {
  
  vehicleForm: FormGroup
  customerForm: FormGroup
  planCancelForm: FormGroup
  addressForm: FormGroup
  plan: any
  customerAddress: Address
  

  licensePlateMask = MaskUtils.licensePlateMask
  phoneMask = MaskUtils.phoneMask
  mobilePhoneMask =  MaskUtils.mobilePhoneMask
  postalCodeMask =  MaskUtils.postalCodeMask
  stateAddressList: any

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private router: Router,
    private snackbar: MatSnackBar,
    private plansService: PlansService,
    private customersService: CustomersService,
    private vehicleService: VehiclesService,
    private utils: UtilsService

  ) { 
    super(injector)
  }

  ngOnInit() {

    this.plansService.currentPlan.subscribe(currentPlan => this.plan = currentPlan)

   
    // if (!this.plan.device) {
    //   this.router.navigate(['/plans-cancel'])
    //   return
    // }

    this.initForms()
    this.loadDomainData()
    this.loadData()
  }

  initForms() {

    this.vehicleForm = this.formBuilder.group({
      licensePlate: this.formBuilder.control('', [Validators.required]),
      type: this.formBuilder.control('', [Validators.required]),
      chassi: this.formBuilder.control('', [Validators.required]),
      manufacture: this.formBuilder.control('', [Validators.required]),
      model: this.formBuilder.control('', [Validators.required]),
      color: this.formBuilder.control('', [Validators.required]),
      registrationYear: this.formBuilder.control('', [Validators.required]),
      modelYear: this.formBuilder.control('', [Validators.required]),
    })

    this.customerForm = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required]),
      mobilePhone: this.formBuilder.control('', [Validators.required]),
      residencialPhone: this.formBuilder.control(''),
      commercialPhone: this.formBuilder.control(''),
      email: this.formBuilder.control('', [Validators.required]),
    })

    this.planCancelForm = this.formBuilder.group({
      name: this.formBuilder.control({value : '', disabled: true}),
    })

    this.addressForm = this.formBuilder.group({
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', [])
    })    
  }

  loadDomainData() {

    this.stateAddressList = this.utils.stringEnumToKeyValue(StateAddress)
  }

  loadData() {

    if (this.plan.relationships.customer) {

      this.customerForm.patchValue ({
        name: this.plan.relationships.customer.name,
        mobilePhone: this.plan.relationships.customer.mobilePhone || null,
        residencialPhone: this.plan.relationships.customer.residencialPhone || null,
        commercialPhone: this.plan.relationships.customer.commercialPhone || null,
        email: this.plan.relationships.customer.email || null
      })
    }

    this.planCancelForm.patchValue ({
      name: this.plan.relationships.offer.name
    })

    if(this.plan.relationships.device  && this.plan.relationships.device.car){

      this.vehicleService.getOne(this.plan.relationships.device.car)
      .subscribe(vehicle => {

        this.vehicleForm.patchValue ({
          licensePlate: vehicle.licensePlate,
          type: vehicle.type,
          chassi: vehicle.chassi,
          manufacture: vehicle.manufacture,
          model: vehicle.model,
          color: vehicle.color,
          registrationYear: vehicle.registrationYear,
          modelYear: vehicle.modelYear
        })
      })

    }else if(this.plan.relationships.vehicle){

      this.vehicleForm.patchValue ({
        licensePlate: this.plan.relationships.vehicle.licensePlate,
        type: this.plan.relationships.vehicle.type,
        chassi: this.plan.relationships.vehicle.chassi,
        manufacture: this.plan.relationships.vehicle.manufacture,
        model: this.plan.relationships.vehicle.model,
        color: this.plan.relationships.vehicle.color,
        registrationYear: this.plan.relationships.vehicle.registrationYear,
        modelYear: this.plan.relationships.vehicle.modelYear
      })

    }

    

    this.customersService.getCustomerAddress(this.plan.user)
    .subscribe(addresses => {

      if (addresses.content != null && addresses.content.length > 0) {
        
        this.customerAddress = addresses.content[0]
        
        this.addressForm.setValue ({
          street: this.customerAddress.street || null,
          number: this.customerAddress.number || null,
          complement: this.customerAddress.complement || null,
          district: this.customerAddress.district || null,
          city: this.customerAddress.city || null,
          state: this.customerAddress.state || null,
          postalCode: this.customerAddress.postalCode || null,
          reference: this.customerAddress.reference || null
        })
      }
    })    
  }

  cancelPlan() {

    if (this.addressForm.invalid) {
      this.snackbar.open('Informe o endereço do Cliente', '', {
        duration: 8000
      })
      return
    }

    var updatesCall = []

    // customer data
    let customerData = []
    customerData.push({'op' : 'add', 'path' : '/name', 'value' : this.customerForm.value.name})
    customerData.push({'op' : 'add', 'path' : '/email', 'value' : this.customerForm.value.email})
    customerData.push({'op' : 'add', 'path' : '/mobilePhone', 'value' : this.utils.stringToNumber(this.customerForm.value.mobilePhone)})
    if (this.customerForm.value.residencialPhone) {
      customerData.push({'op' : 'add', 'path' : '/residencialPhone', 'value' : this.utils.stringToNumber(this.customerForm.value.residencialPhone)})
    }
    if (this.customerForm.value.commercialPhone) {
      customerData.push({'op' : 'add', 'path' : '/commercialPhone', 'value' : this.utils.stringToNumber(this.customerForm.value.commercialPhone)})
    }
    
    updatesCall.push(this.customersService.update(this.plan.relationships.customer.id, customerData).map((res:Response) => res))

    // customer address
    if (this.customerAddress) {
      let addressData = []

      let obj = this.addressForm.value
      obj.postalCode = this.utils.stringToNumber(this.addressForm.value.postalCode)

      for(let item in obj){
        let editItem = {'op': 'add', 'path': '/'+item, 'value': this.addressForm.value[item]};
        addressData.push(editItem);
      }
      updatesCall.push(this.customersService.updateCustomerAddress(this.customerAddress.id, addressData).map((res:Response) => res))
    } else {

      this.customerAddress = new Address()
      this.customerAddress.user = this.plan.user
      this.customerAddress.street = this.addressForm.value.street
      this.customerAddress.number = this.addressForm.value.number
      this.customerAddress.complement = this.addressForm.value.complement
      this.customerAddress.district = this.addressForm.value.district
      this.customerAddress.city = this.addressForm.value.city
      this.customerAddress.state = this.addressForm.value.state
      this.customerAddress.postalCode = this.utils.stringToNumber(this.addressForm.value.postalCode)
      this.customerAddress.reference = this.addressForm.value.reference
      updatesCall.push(this.customersService.createCustomerAddress(this.customerAddress).map((res:Response) => res))
    }

    // vehicle update
    let vehicleData = []
    for(let item in this.vehicleForm.value){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': this.vehicleForm.value[item]};
      vehicleData.push(editItem);
    }    

    if(this.plan.relationships.device  && this.plan.relationships.device.car){
      updatesCall.push(this.vehicleService.update(this.plan.relationships.device.car, vehicleData))
    }else if(this.plan.relationships.vehicle){
      updatesCall.push(this.vehicleService.update(this.plan.relationships.vehicle.id, vehicleData))
    }
    
    // cancel plan
    updatesCall.push(this.plansService.cancelPlan(this.plan.id, { loggedUser : this.appContext.session.user.id }))
    
    // execute calls
    Observable.forkJoin(updatesCall).subscribe(result => {

      this.snackbar.open('Plano cancelado com sucesso', '', {
        duration: 8000
      })

      this.cancel()
    })
  }

  cancel() {
    this.router.navigate(['/contracts'])
  }
}
