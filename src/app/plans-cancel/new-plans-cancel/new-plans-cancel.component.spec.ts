import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPlansCancelComponent } from './new-plans-cancel.component';

describe('NewPlansCancelComponent', () => {
  let component: NewPlansCancelComponent;
  let fixture: ComponentFixture<NewPlansCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPlansCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPlansCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
