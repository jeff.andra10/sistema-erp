import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { PlansService } from '../../core/services/plans.service';
import { Subject } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-new-plans-cancel',
  templateUrl: './new-plans-cancel.component.html',
  styleUrls: ['./new-plans-cancel.component.scss']
})
export class NewPlansCancelComponent implements OnInit {

  plansList: Array<any>
  searchSubject = new Subject()
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(
    private router: Router,
    private plansService: PlansService
  ) { 
  }

  ngOnInit() {

    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadPlans(val)
    })
  }

  selectPlan(plan) {
    this.plansService.setPlan(plan)
    this.router.navigate([`/plans-cancel/new/${plan.id}`])
  }

  loadPlans(value: string) {

    this.plansService.getActivePlansBySearchTerms(value)
     .subscribe( searchResult => {
        this.plansList = searchResult.content
      });
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }    

}
