import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlansCancelComponent } from './plans-cancel.component';

describe('PlansCancelComponent', () => {
  let component: PlansCancelComponent;
  let fixture: ComponentFixture<PlansCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlansCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlansCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
