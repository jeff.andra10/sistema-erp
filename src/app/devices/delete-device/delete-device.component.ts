import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Device } from '../../core/models/device.model';
import { DevicesService } from '../../core/services/devices.service';

@Component({
  selector: 'tnm-delete-device',
  templateUrl: './delete-device.component.html',
  styleUrls: ['./delete-device.component.scss']
})
export class DeleteDeviceComponent implements OnInit {

  device: Device

  constructor(
    private dialogRef: MatDialogRef<DeleteDeviceComponent>,
    private devicesService: DevicesService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit() {
    this.device = this.data
  }

  deleteDevice(){
    this.devicesService.delete(this.device.id)
      .subscribe(response => {
        this.dialogRef.close()
        this.snackbar.open('Rastreador excluído com sucesso', '', {
          duration: 8000
        })

      })
  }


}

