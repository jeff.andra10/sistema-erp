import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DevicesComponent } from "./devices.component";
import { NewDeviceComponent } from './new-device/new-device.component';
import { EditDeviceComponent } from './edit-device/edit-device.component';

const routes: Routes = [
  {
    path: '',
    component: DevicesComponent
  },
  { path: 'details/:id', component: EditDeviceComponent },
  { path: 'new', component: NewDeviceComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DevicesRoutingModule { }
