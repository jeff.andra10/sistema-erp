import { NgModule, ModuleWithProviders } from "@angular/core";
import { NewDeviceComponent } from './new-device/new-device.component';
import { EditDeviceComponent } from './edit-device/edit-device.component';
import { DeleteDeviceComponent } from './delete-device/delete-device.component'
import { TextMaskModule } from "angular2-text-mask";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../angular-material/material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { DevicesComponent } from "./devices.component";
import { RouterModule } from "@angular/router";
import { VehicleDetailsComponent } from "../vehicles/vehicle-details/vehicle-details.component";
import { VehiclesModule } from "../vehicles/vehicles.module";
import { CustomerPersonalComponent } from "../customers/customer-personal/customer-personal.component";
import { CustomerAddressComponent } from "../customers/customer-address/customer-address.component"
import { CustomersModule } from "../customers/customers.module";
import { UsersSearchComponent } from "../users/users-search/users-search.component";
import { UsersModule } from "../users/users.module"
import { DevicesRoutingModule } from "./devices-routing.module"


// const authRouting: ModuleWithProviders = RouterModule.forChild([
//     { path: '', component: DevicesComponent },
//     { path: 'details/:id', component: EditDeviceComponent },
//     { path: 'new', component: NewDeviceComponent }
//   ]);
  
@NgModule({
    imports: [
        //authRouting,
        TextMaskModule,
        FlexLayoutModule,
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        VehiclesModule,
        CustomersModule,
        UsersModule,
        DevicesRoutingModule
    ],
    declarations:[
        DevicesComponent,
        NewDeviceComponent,
        EditDeviceComponent,
        DeleteDeviceComponent
    ],
    entryComponents: [
        CustomerPersonalComponent,
        VehicleDetailsComponent,
        CustomerAddressComponent,
        UsersSearchComponent
    ],
    exports     : [
        DevicesComponent,
        NewDeviceComponent,
        EditDeviceComponent,
        DeleteDeviceComponent
    ]
})
export class DevicesModule{}