import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../_base/base-component.component';
import { MaskUtils } from '../../core/mask-utils';
import { DevicesService } from '../../core/services/devices.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { Router } from '@angular/router';
import { DevicesModel} from '../../core/models/device.model';
import { UtilsService } from '../../core/utils.service';


@Component({
  selector: 'tnm-new-device',
  templateUrl: './new-device.component.html',
  styleUrls: ['./new-device.component.scss']
})
export class NewDeviceComponent extends BaseComponent implements OnInit {

  deviceForm: FormGroup
  activitDeviceForm: FormGroup
  brandList: Array<any>
  deviceModelList : any
  suppliers: Array<object>
  isLoading: boolean = false
  isDisabled: boolean = false

  //TODO - Put on shared file
  phoneMask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  simCardMask = MaskUtils.simCardMask
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private devicesService: DevicesService,
    private snackbar: MatSnackBar,
    private utils: UtilsService,
    private loadingService: FuseSplashScreenService,
    private router: Router
  ) { 
    super(injector)
  }

  ngOnInit() {

    this.isDisabled = false;

    localStorage.setItem('hiddenLoading','true');

    this.brandList = this.appContext.brands
    this.suppliers = JSON.parse(sessionStorage.getItem('suppliers'))
   
    this.deviceForm = this.formBuilder.group({
      imei: this.formBuilder.control('', [Validators.required,Validators.maxLength(15)]),
      number: this.formBuilder.control(''),
      simCard: this.formBuilder.control(''),
      operator: this.formBuilder.control(''),
      identifier: this.formBuilder.control(''),
      hidden: this.formBuilder.control(false),
      model: this.formBuilder.control('', [Validators.required]),
      brand: this.formBuilder.control(this.appContext.loggedUser.brand, [Validators.required]),
      status: this.formBuilder.control('INACTIVE')
    })

    this.activitDeviceForm = this.formBuilder.group({
      apn: this.formBuilder.control('',[Validators.required,this.noWhitespaceValidator]),
      login: this.formBuilder.control(''),
      password: this.formBuilder.control(''),
    })

    this.loadDomainListData()
  }

  trimming_fn(x) {    
    return x ? x.replace(/\s/g,'') : '';    
  };  

  removeSpaces(string) {
    return string.split(' ').join('');
  }
  

  loadDomainListData() {
    this.deviceModelList = this.utils.stringEnumToKeyValue(DevicesModel)
  }

  cancel(){
    this.router.navigate([`/devices`])
  }

  createDevice(body) {
    this.isDisabled = true
    this.loadingService.show()
    body.number = body.number.replace(/\D+/g, '')
    body.simCard = body.simCard.replace(/\D+/g, '')
   this.devicesService.createDevice(body)
      .subscribe(device => {
        
        
          this.snackbar.open('Rastreador criado com sucesso', '', {
            duration: 8000
          })

          this.router.navigate([`/devices`])

          // if(this.activitDeviceForm.value.apn && this.activitDeviceForm.value.login && this.activitDeviceForm.value.password){
          //   this.activeDevice(this.activitDeviceForm.value)
          // }
          //this.loadingService.hide()
          //this.dialogRef.close()

          // this.isLoading = true
          // this.activitDeviceForm.disable()
   
          // setTimeout(() => {
          //   this.snackbar.open('Rastreador ativado com sucesso', '', {
          //     duration: 8000
          //   })
          //   //this.dialogRef.close()
          // },5000);
    
      }, error => {
         this.loadingService.hide()
         this.snackbar.open(error.error ? error.error.message : error.message, '', { duration: 5000 });
         this.isDisabled = false
   })
  }

  activeDevice(body) {

    
    // this.devicesService.createDevice(body)
    //   .subscribe(device => {

    //   });
   
    
  }

  routerGoTo(){
    this.router.navigate([`/devices`])
  }

  public noWhitespaceValidator(control: FormControl) {

    const isWhitespace = (control.value || ' ').trim().length === 0;
    let isValid = !isWhitespace;
    
     if(control.value.indexOf(' ') > -1){
        isValid = false
      }else{
        isValid = true
      }
    return isValid ? null : { 'whitespace': true };
    
  }
}

