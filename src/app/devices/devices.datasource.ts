import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import { DevicesService } from '../core/services/devices.service';
import { Brand } from "../core/models/brand.model";
import { UtilsService } from "app/core/utils.service";

export class DevicesDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private totalElementsSubject = new BehaviorSubject<number>(0)

    public totalElements$ = this.totalElementsSubject.asObservable()

    constructor(
        private devicesService: DevicesService,
        private utilsService : UtilsService,
        private brands: Array<Brand>
     ) {
    }

    load(filter:string,
            brand: number,
            model:string,
            status:string,
            startDate:string,
            endDate:string,
            sortField: string,
            sortDirection:string,
            pageIndex:number,
            pageSize:number) {

        this.devicesService.findDevices(
            filter, 
            brand,
            model,
            status,
            startDate,
            endDate,
            sortField,
            sortDirection,
            pageIndex, 
            pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe(result => {
                if (result.content && this.brands) {
                    result.content.forEach(obj => {
                        obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                    });
                }
               
                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
