//EditDeviceComponent
import { Component, OnInit, Injector } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import { Customer } from '../../core/models/customer.model';
import { DatePipe } from '@angular/common';
import { MaskUtils } from '../../core/mask-utils';
import { CustomersService } from '../../core/services/customers.service';
import { Plan } from '../../core/models/plan.model';
import { DeviceStatus, Device } from '../../core/models/device.model';
import { PlansService } from '../../core/services/plans.service';
import { VehiclesService } from '../../core/services/vehicles.service';
import { PlanStatus } from '../../core/models/plan.model';
import { PaymentType, PaymentCreditCard, ChargeType } from '../../core/models/payment.model';
import { fuseAnimations } from '@fuse/animations';
import { Vehicle } from '../../core/models/vehicle.model';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { DevicesService } from '../../core/services/devices.service'
import { OffersService } from '../../core/services/offers.service'
import { Offer } from 'app/core/models/offer.model';
import { Address } from '../../core/models/address.model';
import { StateAddress } from '../../core/common-data-domain';
import { ConfirmDialogComponent } from 'app/dialogs/confirm-dialog/confirm-dialog.component';
import { VehicleDetailsComponent } from '../../vehicles/vehicle-details/vehicle-details.component';
import { BaseComponent } from 'app/_base/base-component.component';
import { ProductsService } from '../../core/services/products.service';
import { Product } from 'app/core/models/product.model';
import { Service } from 'app/core/models/service.model';
import { CustomerPersonalComponent } from '../../customers/customer-personal/customer-personal.component';
import { CustomerAddressComponent } from '../../customers/customer-address/customer-address.component';

import { UtilsService } from '../../core/utils.service';
import { DocumentsService } from '../../core/services/documents.service';
import { ServiceOrdersService } from '../../core/services/service-orders.service';
import { UserService } from '../../core/services/users.service'
import { DevicesModel } from '../../core/models/device.model';


import { UsersSearchComponent } from "../../users/users-search/users-search.component";
import * as moment from 'moment';

@Component({
  selector: 'tnm-device-edit',
  templateUrl: './edit-device.component.html',
  styleUrls: ['./edit-device.component.scss'],
  animations: fuseAnimations
})

export class EditDeviceComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(VehicleDetailsComponent)
  public vehicleDetailsComponent: VehicleDetailsComponent

  @ViewChild(CustomerPersonalComponent)
  public customerPersonalComponent: CustomerPersonalComponent;

  @ViewChild(CustomerAddressComponent)
  public customerAddressComponent: CustomerAddressComponent


  customer: Customer
  device: Device
  generalForm: FormGroup
  vehicleForm: FormGroup
  deviceForm: FormGroup
  planForm: FormGroup
  searchForm: FormGroup
  deviceEditForm: FormGroup
  activitDeviceForm: FormGroup
  localInstalationForm: FormGroup
  brandList: Array<object>
  planName: String
  status: Boolean = true
  plan: Plan
  statusPlan: String = ''
  offersList: Array<Offer>
  tracknmeOffersList: Array<Offer>
  serviceOrderDocuments: any
  addressForm: FormGroup
  stateAddressList: any
  postalCodeMask = MaskUtils.postalCodeMask
  commoditiesService: Array<Service>
  commoditiesProducts: Array<Product>
  scheduleForm: FormGroup
  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask
  mobilePhoneMask = MaskUtils.mobilePhoneMask
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');
  editColor: string = "mat-accent-bg"
  scheduleTimeList: Array<any>
  planChargesDataSource: any
  planChargeLogsDataSource: any
  random: string = ''
  random_0: string = ''
  random_1: string = ''
  showAddCustomer: boolean = false
  showAddVeicle: boolean = false
  enableBilling: boolean = false
  defaultUrl: string = 'assets/images/ecommerce/product-image-placeholder.png'
  fileToUpload: File = null
  isVehicle: boolean = false;
  isCustomer: boolean = false;
  isEdit: boolean = false;
  deviceModelList: any
  vehicle: Vehicle
  isTracknme : boolean = false;

  phoneMask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  simCardMask = MaskUtils.simCardMask

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customersService: CustomersService,
    private plansService: PlansService,
    private vehicleService: VehiclesService,
    private plansCancelService: PlansService,
    private loadingService: FuseSplashScreenService,
    private devicesService: DevicesService,
    private offersService: OffersService,
    private productsService: ProductsService,
    private serviceOrdersService: ServiceOrdersService,
    private utils: UtilsService,
    private documentsService: DocumentsService,
    private userService: UserService
  ) {
    super(injector)

    if(this.appContext.session.user.brand == this.appContext.brandTracknme){
      this.isTracknme = true
    }else{
      this.isTracknme = false
    }

  }


  ngOnInit() {

    localStorage.setItem('hiddenLoading', 'true');

    this.status = true
    let deviceId = this.activatedRoute.snapshot.params['id']

    this.initForms()
    this.getDevice(deviceId)
    this.getPlanData(deviceId)
    //this.getCommodities()
    this.stateAddressList = this.utilsService.stringEnumToKeyValue(StateAddress)

    setTimeout(() => {
      this.brandList = this.appContext.brands
    })

    this.loadDomainListData()

  }

  loadDomainListData() {
    this.getTracknmeOffers()
    this.deviceModelList = this.utils.stringEnumToKeyValue(DevicesModel)
  }

  getTracknmeOffers(){
    
    this.offersService.getTracknmeOffers().subscribe(offers => {
      if (offers && offers.content) {
        this.tracknmeOffersList = offers.content;
      }
    });
    
  }

  ///api/plans?by=default-search &brand=<TRACKNME> &segment=BUSINESS &agreement=TRACKING &device=???

  formatOffersName(name){
    return name.replace('Plano de ','');
  }

  formatOffersPrice(price){
    return price.toFixed(2);
  }

  formatOffersDescription(description){
    return description.replace(/\d+/g, '');
  }

  formatOffersDescriptionNumber(description){
    let number = description.replace(/[^0-9]/g,'');
    return number > 9 ? number : '0'+number
  }

  getDevice(id) {
    this.devicesService.getDevicesById(id).subscribe(device => {
      if (device) {
        this.device = device
        if (device.user) {
          this.getCustomerByUser(device.user)
        }
        if (device.car) {
          this.getVehicleById(device.car)
        }
        //this.getImageDevice(device.id)
        this.loadDevice(device)
      }
    }, error => {
      console.log(error)
      this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
      this.loadingService.hide()
    })
  }

  getCustomerByUser(userId) {

    this.customersService.getCustomerByUser(userId)
      .subscribe(customer => {
        if (customer && customer.content && customer.content.length > 0) {
          this.customer = customer.content[0]
          this.isCustomer = true
          this.loadCustomerData(this.customer)
          this.loadingService.hide()
        }
      })

  }

  searchOnChange(field) {

    this.dialog.open(UsersSearchComponent,
      { panelClass: 'event-form-dialog',data: {
        width: '600px',
        height: '300px', title: 'Atualização do rastreador', message: 'Deseja fazer essa atualização?'
      }})
      .afterClosed()
      .subscribe(response => {

        if (response && response.user) {
          this.updateLifecycle(field, response.user.id)
        }
      });
  }

  dateOnChange(date, field) {

    let dateFormat = moment(date.value).format("YYYY-MM-DD")
    let dateTime = new Date()


    let hours = '' + dateTime.getHours();
    if (hours.length == 1) {
      hours = '0' + hours
    }

    let minutes = '' + dateTime.getMinutes();
    if (minutes.length == 1) {
      minutes = '0' + minutes
    }

    let _date = date = dateFormat + 'T' + hours + ':' + minutes + ':00'

    this.updateLifecycle(field, _date)

  }

  updateLifecycle(field, data) {

    this.dialog.open(ConfirmDialogComponent, {
      panelClass: 'event-form-dialog', data: {
        width: '600px',
        height: '300px', title: 'Atualização do rastreador', message: 'Deseja fazer essa atualização?'
      }
    })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm == true) {
          this.loadingService.show()

          let editData = []

          let editItem = { 'op': 'add', 'path': '/lifecycle/' + field, 'value': data };

          editData.push(editItem);


          this.devicesService.update(this.device.id, editData)
            .subscribe(device => {
              this.snackbar.open('Rastreador atualizado com sucesso', '', {
                duration: 8000
              })

              if (this.isEdit) {
                this.isEdit = false
              }

              this.getDevice(this.device.id)

             

            }, error => {
              console.log(error)
              this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
              this.loadingService.hide()
            })
        } 
      });
  }

  getVehicleById(id) {

    this.vehicleService.getVehicleById(id)
      .subscribe(vehicle => {
        if (vehicle) {

          this.vehicle = vehicle
          this.loadVehicleData(vehicle)
          this.loadingService.hide()

          this.localInstalationForm.patchValue({
            installationSpot: vehicle.description
          })

          this.localInstalationForm.disable()
        }
      })

  }


  getOffers() {

    let params = { limit: 200, page: 0, segment: 'CUSTOMERS' }

    this.offersService.getOffers(params).subscribe(offers => {
      if (offers && offers.content && offers.content.length > 0) {
        this.offersList = offers.content
      }

    })
  }

  getPlanData(id) {

    this.plansService.getPlansByDevice(id)
      .subscribe(plan => {
        
        if (plan && plan.content && plan.content.length > 0) {
          this.plan = plan.content[0]
          this.loadPlanCharge(plan.id)
          this.loadPlanData(plan.content[0])
          this.loadingService.hide()
        }
      })
  }

  getImageDevice() {
    this.loadingService.show()
    this.documentsService.getDocumentsDevice(this.device.id)
      .subscribe(documents => {
        this.random = '0'
        this.random = '?random=' + new Date().getTime()
        this.serviceOrderDocuments = {};
        this.serviceOrderDocuments = documents
        if( this.serviceOrderDocuments &&  this.serviceOrderDocuments.INSTALLATION_DOCS_TRACKER_TAG){
          this.serviceOrderDocuments.INSTALLATION_DOCS_TRACKER_TAG = this.serviceOrderDocuments.INSTALLATION_DOCS_TRACKER_TAG+'?random=' + new Date().getTime()
        }
        this.loadingService.hide()
      })
  }

  initForms() {

    this.localInstalationForm = this.formBuilder.group({
      installationSpot: this.formBuilder.control(''),
    })

    this.generalForm = this.formBuilder.group({
      brand: this.formBuilder.control('', [Validators.required]),
      name: this.formBuilder.control('', [Validators.required]),
      mobilePhone: this.formBuilder.control('', []),
      residencialPhone: this.formBuilder.control('', [Validators.required]),
      registrationNumber: this.formBuilder.control('', [Validators.required]),
      email: this.formBuilder.control('', [Validators.required])
    })

    this.planForm = this.formBuilder.group({
      brand: this.formBuilder.control('', [Validators.required]),
      id: this.formBuilder.control('', [Validators.required]),
      name: this.formBuilder.control('', [Validators.required]),
      planStatus: this.formBuilder.control('', []),
      paymentStatus: this.formBuilder.control('', [Validators.required]),
      installDate: this.formBuilder.control({ value: '', disabled: true }),
      installUser: this.formBuilder.control({ value: '', disabled: true }),
      cancelDate: this.formBuilder.control({ value: '', disabled: true }),
      cancelUser: this.formBuilder.control({ value: '', disabled: true }),
      reactivateDate: this.formBuilder.control({ value: '', disabled: true }),
      reactivateUser: this.formBuilder.control({ value: '', disabled: true }),
    })

    this.vehicleForm = this.formBuilder.group({
      licensePlate: this.formBuilder.control({ value: '', disabled: true }),
      type: this.formBuilder.control({ value: '', disabled: true }),
      chassi: this.formBuilder.control({ value: '', disabled: true }),
      manufacturer: this.formBuilder.control({ value: '', disabled: true }),
      model: this.formBuilder.control({ value: '', disabled: true }),
      color: this.formBuilder.control({ value: '', disabled: true }),
      manufacturerYear: this.formBuilder.control({ value: '', disabled: true }),
      modelYear: this.formBuilder.control({ value: '', disabled: true }),
    })

    this.deviceForm = this.formBuilder.group({
      id: this.formBuilder.control({ value: '', disabled: true }),
      operator: this.formBuilder.control({ value: '', disabled: true }),
      number: this.formBuilder.control({ value: '', disabled: true }),
      identifier: this.formBuilder.control({ value: '', disabled: true }),
      model: this.formBuilder.control({ value: '', disabled: true }),
      imei: this.formBuilder.control({ value: '', disabled: true }),
      simCard: this.formBuilder.control({ value: '', disabled: true }),
      status: this.formBuilder.control({ value: '', disabled: true }),
      brand: this.formBuilder.control({ value: '', disabled: false }),
      activeDate: this.formBuilder.control({ value: '', disabled: true }),
      inactiveDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedActive: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedInactive: this.formBuilder.control({ value: '', disabled: true }),
      receivedDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedReceived: this.formBuilder.control({ value: '', disabled: true }),
      uninstalledDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedUninstalled: this.formBuilder.control({ value: '', disabled: true }),
      discardedDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedDiscarded: this.formBuilder.control({ value: '', disabled: true }),
      testedDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedTested: this.formBuilder.control({ value: '', disabled: true }),
      underMaintenanceDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedUnderMaintenance: this.formBuilder.control({ value: '', disabled: true }),
      canceledDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedCanceled: this.formBuilder.control({ value: '', disabled: true }),
      returnedDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedReturned: this.formBuilder.control({ value: '', disabled: true }),
      reactivateDate: this.formBuilder.control({ value: '', disabled: true }),
      userLoggedReactivate: this.formBuilder.control({ value: '', disabled: true }),
      hidden: this.formBuilder.control({ value: false, disabled: false })
    })

    this.addressForm = this.formBuilder.group({
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', [])
    })

    // schedule form
    this.scheduleForm = this.formBuilder.group({
      installer: this.formBuilder.control('', []),//this.searchField,
      scheduledDate: this.formBuilder.control({ value: '', disabled: true }, [Validators.required]),
      time: this.formBuilder.control('', [Validators.required]),
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', []),
      check: this.formBuilder.control('', [])
    })

    this.searchForm = this.formBuilder.group({
      brand: this.formBuilder.control('')
    });

    this.deviceEditForm = this.formBuilder.group({
      imei: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control(''),
      simCard: this.formBuilder.control(''),
      operator: this.formBuilder.control(''),
      identifier: this.formBuilder.control(''),
      model: this.formBuilder.control('', [Validators.required]),
      brand: this.formBuilder.control(this.appContext.loggedUser.brand, [Validators.required]),
      hidden: this.formBuilder.control({ value: false, disabled: false })
    })

    this.activitDeviceForm = this.formBuilder.group({
      apn: this.formBuilder.control(''),
      login: this.formBuilder.control(''),
      password: this.formBuilder.control(''),
    })
  }

  public loadData(address: Address) {

    this.addressForm.setValue({
      street: address.street || null,
      number: address.number || null,
      complement: address.complement || null,
      district: address.district || null,
      city: address.city || null,
      state: address.state || null,
      postalCode: address.postalCode || null,
      reference: address.reference || null
    })
  }

  loadVehicleData(vehicle: Vehicle) {

    if (vehicle && vehicle.licensePlate) {

      this.vehicleForm.patchValue({
        licensePlate: vehicle.licensePlate,
        type: vehicle.type,
        chassi: vehicle.chassi,
        manufacturer: vehicle.manufacture,
        model: vehicle.model,
        color: vehicle.color,
        manufacturerYear: vehicle.registrationYear,
        modelYear: vehicle.modelYear
      })

    }


  }

  routerGoTo() {
    this.router.navigate([`/devices`])
  }

  loadCustomerData(customer: Customer) {

    if (customer) {
      this.generalForm.setValue({
        brand: customer.brand || null,
        name: customer.name || null,
        mobilePhone: customer.mobilePhone || null,
        residencialPhone: customer.residencialPhone || null,
        email: customer.email || null,
        registrationNumber: customer.registrationNumber
      })

      this.generalForm.disable()
    }

  }

  showCustomer() {

    if (!this.showAddCustomer) {

      this.showAddCustomer = true
      setTimeout(() => {
        this.customerPersonalComponent.setHideFields()
        this.customerPersonalComponent.setCreatUserAdmin()
        if (this.customer && this.customer.id) {
          this.customerPersonalComponent.setCustomer(this.customer)
        }
        this.customerPersonalComponent.customerSelected
          .subscribe(customer => {
            if (customer) {
              this.customer = customer
            }
          });
      }, 10)
    } else {
      this.showAddCustomer = false
    }

  }

  hideCustomer() {
    this.showAddCustomer = false
  }

  cancelAll() {
    this.showAddCustomer = false
    this.showAddVeicle = false
    this.isEdit = false
  }

  saveAll() {

    if (this.isEdit) {
      this.updateDevice(this.deviceEditForm.value)
    }

    if (this.showAddCustomer) {
      this.createCustomer()
    }

    if (this.showAddVeicle) {
      this.createVehicle()
    }

  }


  showVeicle() {

    if (!this.showAddVeicle) {
      this.showAddVeicle = true
      setTimeout(() => {
        this.vehicleDetailsComponent.setHideFields()
        if (this.vehicle && this.vehicle.id && this.vehicle.chassi != undefined) {
          this.loadingService.show()
          this.vehicleDetailsComponent.loadData(this.vehicle)
        }
      }, 10)
    } else {
      this.showAddVeicle = false
    }

  }

  hideVehicle() {
    this.showAddVeicle = false
  }

  showBilling() {

    if (!this.enableBilling) {
      this.enableBilling = true
    } else {
      this.enableBilling = false
    }

  }

  updateDeviceHiddenCheckBox(isDeviceHidden: boolean) {
    this.deviceForm.patchValue({ hidden: isDeviceHidden })
  }


  loadDevice(device: Device) {

    this.deviceForm.setValue({
      id: device.id || null,
      operator: device.operator || null,
      number: device.number || null,
      identifier: device.identifier || null,
      model: device.model || null,
      imei: device.imei || null,
      hidden: device.hidden,
      brand: device.brand || null,
      simCard: device.simCard || null,
      status: this.deviceActivityToString(device.status) || null,
      activeDate: device['lifecycle'].activeDate ? moment(device['lifecycle'].activeDate).format('DD/MM/YYYY HH:mm:ss') : null,
      inactiveDate: device['lifecycle'].inactiveDate ? moment(device['lifecycle'].inactiveDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedActive: null,
      userLoggedInactive: null,
      receivedDate: device['lifecycle'].receivedDate ? moment(device['lifecycle'].receivedDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedReceived: null,
      uninstalledDate: device['lifecycle'].uninstalledDate ? moment(device['lifecycle'].uninstalledDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedUninstalled: null,
      discardedDate: device['lifecycle'].discardedDate ? moment(device['lifecycle'].discardedDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedDiscarded: null,
      testedDate: device['lifecycle'].testedDate ? moment(device['lifecycle'].testedDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedTested: null,
      underMaintenanceDate: device['lifecycle'].underMaintenanceDate ? moment(device['lifecycle'].underMaintenanceDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedUnderMaintenance: null,
      canceledDate: device['lifecycle'].canceledDate ? moment(device['lifecycle'].canceledDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedCanceled: null,
      returnedDate: device['lifecycle'].returnedDate ? moment(device['lifecycle'].returnedDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedReturned: null,
      reactivateDate: device['lifecycle'].reactivateDate ? moment(device['lifecycle'].reactivateDate).format('DD/MM/YYYY HH:mm:ss') : null,
      userLoggedReactivate: null
    })

    this.searchForm.setValue({
      brand: device.brand || null
    });

    this.deviceForm.controls.brand.disable({ onlySelf: true });

    if (device['lifecycle'].userLoggedActive) {
      this.getUserById(device['lifecycle'].userLoggedActive, 'userLoggedActive')
    }

    if (device['lifecycle'].userLoggedInactive) {
      this.getUserById(device['lifecycle'].userLoggedInactive, 'userLoggedInactive')
    }

    if (device['lifecycle'].userLoggedReceived) {
      this.getUserById(device['lifecycle'].userLoggedReceived, 'userLoggedReceived')
    }

    if (device['lifecycle'].userLoggedUninstalled) {
      this.getUserById(device['lifecycle'].userLoggedUninstalled, 'userLoggedUninstalled')
    }

    if (device['lifecycle'].userLoggedDiscarded) {
      this.getUserById(device['lifecycle'].userLoggedDiscarded, 'userLoggedDiscarded')
    }

    if (device['lifecycle'].userLoggedTested) {
      this.getUserById(device['lifecycle'].userLoggedTested, 'userLoggedTested')
    }

    if (device['lifecycle'].userLoggedUnderMaintenance) {
      this.getUserById(device['lifecycle'].userLoggedUnderMaintenance, 'userLoggedUnderMaintenance')
    }

    if (device['lifecycle'].userLoggedCanceled) {
      this.getUserById(device['lifecycle'].userLoggedCanceled, 'userLoggedCanceled')
    }

    if (device['lifecycle'].userLoggedReturned) {
      this.getUserById(device['lifecycle'].userLoggedReturned, 'userLoggedReturned')
    }

    if (device['lifecycle'].userLoggedReactivate) {
      this.getUserById(device['lifecycle'].userLoggedReactivate, 'userLoggedReactivate')
    }

    this.random = '0'
    this.random = '?random=' + new Date().getTime()

    this.loadingService.hide()

  }

  getUserById(id, param) {

    this.userService.getUserById(id).subscribe(user => {
      if (user) {

        this.deviceForm.patchValue({
          [param]: user.name
        })

      }
    })

  }

  getCommodities() {

    this.commoditiesService = []
    this.commoditiesProducts = []

    this.productsService.getCommodities()
      .subscribe(commodities => {
        commodities.content.forEach(comoditie => {

          if (this.appContext.session.user.brand == this.appContext.brandTracknme) {
            if (comoditie.service) {
              this.commoditiesService.push(comoditie.relationships.service)
            } else {
              this.commoditiesProducts.push(comoditie.relationships.product)
            }
          } else {
            if (comoditie.service && comoditie.status == "ACTIVE") {
              this.commoditiesService.push(comoditie.relationships.service)
            } else if (comoditie.status == "ACTIVE") {
              this.commoditiesProducts.push(comoditie.relationships.product)
            }
          }


        });

      })

  }

  loadPlanCharge(id) {

    this.planChargesDataSource = []
    this.customersService.getCustomerPlanCharges(id)
      .subscribe(charges => {
        if (charges.content != null) {
          let _charges = charges.content

          _charges.forEach(charge => {
            charge.payment = this.plan['relationships'].payment
            this.planChargesDataSource.push(charge)
          });

          this.planChargesDataSource.sort((d1, d2) => new Date(d1['scheduledDateTime']).getTime() - new Date(d2['scheduledDateTime']).getTime());

          if (this.planChargesDataSource.length > 0)
            this.loadChargeLog(this.planChargesDataSource[0])
        }
      })
  }

  deviceActivityToString(status): string {

    if (!status) {
      return 'Inativo'
    }

    if (status == 'RECEIVED') {
      return 'Disponivel'
    }

    return DeviceStatus[status]
  }

  cancelPlan() {
    this.plansService.setPlan(this.plan)
    this.router.navigate([`/contracts/contracts-cancel/new/${this.plan.id}`])
  }

  loadPlanData(plan) {
    var datePipe = new DatePipe(navigator.language)
    //this.status = plan.status

    this.planForm.setValue({
      brand: plan.brand || null,
      id: plan.id || null,
      name: plan.relationships.offer ? plan.relationships.offer.name : null,
      planStatus: this.planStatusEnumToString(plan.status) || null,
      paymentStatus: plan.agreement == "FREE" ? 'Contrato sem Pagamento' : (plan.debt == 'DUE' ? 'Em Dia' : plan.debt == 'OVERDUE' ? 'Em Atraso' : '-') || null,//plan.debt == 'DUE' ? 'Em Dia' : 'Em Atraso' || null,
      cancelDate: plan.lifecycle.cancelDate ? datePipe.transform(new Date(plan.lifecycle.cancelDate), 'dd/MM/yyyy HH:mm:ss') : null,
      cancelUser: plan.relationships.lifecycle.cancelLoggedUser ? plan.relationships.lifecycle.cancelLoggedUser.login : null,
      installDate: plan.lifecycle.installDate ? datePipe.transform(new Date(plan.lifecycle.installDate), 'dd/MM/yyyy HH:mm:ss') : null,
      installUser: plan.relationships.lifecycle.installLoggedUser ? plan.relationships.lifecycle.installLoggedUser.login : null,
      reactivateDate: plan.relationships.reactivateDate ? datePipe.transform(new Date(plan.lifecycle.reactivateDate), 'dd/MM/yyyy HH:mm:ss') : null,
      reactivateUser: plan.relationships.lifecycle.reactivateLoggedUser ? plan.relationships.lifecycle.reactivateLoggedUser.login : null,
    })

    if (plan.status == 'CANCELED') {
      this.statusPlan = 'lightcoral'
      this.status = true
    } else {
      this.status = false
    }


    this.planForm.disable()
  }

  ngAfterViewInit() {

  }

  onPaymentTypeChanged(type: string) {

  }

  planStatusEnumToString(value): string {
    return PlanStatus[value]
  }

  PaymentTypeEnumToString(value): string {
    return PaymentType[value]
  }

  PaymentCreditCardEnumToString(value): string {
    return PaymentCreditCard[value]
  }

  ChargeTypeCardEnumToString(value): string {
    return ChargeType[value]
  }

  downloadBooklet(url) {
    window.open(url, "_blank");
  }

  loadChargeLog(plan) {

    this.planName = plan.type
    this.planChargeLogsDataSource = []
    this.customersService.getCustomerChargeLogs(plan.id)
      .subscribe(log => {
        if (log.content != null) {
          this.planChargeLogsDataSource = log.content
        }
      })
  }

  cancel() {
    this.router.navigate(['/contracts'])
  }

  showEditDevice() {

    this.deviceEditForm.patchValue({
      imei: this.device.imei || null,
      number: this.device.number || null,
      simCard: this.utils.stringToNumber(this.device.simCard) || null,
      operator: this.device.operator || null,
      identifier: this.device.identifier || null,
      model: this.device.model || null,
      brand: this.device.brand || null,
      hidden: this.device.hidden
    })

    this.isEdit = true
  }
  cancelEdit() {
    this.isEdit = false
  }

  onBrandValueChanged() {

    if (this.device.status == 'INACTIVE' && !this.isCustomer && !this.isVehicle) {

      this.dialog.open(ConfirmDialogComponent, {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja mudar a Marca desse Rastreador?'
        }
      })
        .afterClosed()
        .subscribe(response => {
          if (response.data.confirm) {
            this.loadingService.show()
            this.updateDevice({ 'brand': this.searchForm.value.brand })
          } else {

            this.searchForm.patchValue({
              brand: this.device.brand
            })
            this.hideCustomer()
          }

        });
    } else if (this.device.status == 'ACTIVE') {

      this.dialog.open(ConfirmDialogComponent, {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Migração Completa',
          message: 'Deseja fazer a migração completa de marca? Com essa ação o Rastreador, veículo, cliente e todos os contratos pertencentes a esse cliente serão migrados para a nova marca.'
        }
      })
        .afterClosed()
        .subscribe(response => {
          if (response.data.confirm) {

            this.loadingService.show()

            let body = {
              "brand": this.searchForm.value.brand,
              "users": [this.customer.user]
            };

            this.userService.migration(body).subscribe(
              user => {
                this.snackbar.open('Migração feita com sucesso.', '', { duration: 8000 });
                this.router.navigate(['/devices'])
              }, error => {
                console.log(error)
                this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
                this.loadingService.hide()
              })

          } else {

            this.searchForm.patchValue({
              brand: this.device.brand
            })
            this.hideCustomer()
          }

        });
    } else {
      this.snackbar.open('Só é possível migrar rastreadores com o status Ativo', '', { duration: 5000 })
    }

  }

  reactivatePlan() {
    this.plansCancelService.reactivatePlan(this.plan.id, { loggedUser: this.appContext.session.user.id })
      .subscribe(
        result => {
          this.snackbar.open('Plano reativado com sucesso', '', { duration: 5000 })
          this.router.navigate(['/contracts'])
        },
        response => {
          this.snackbar.open(response.error.message, '', { duration: 8000 })
        })
  }

  returnedDevice() {

    this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja retornar esse Rastreador?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.devicesService.returnedDevice(this.device.id)
            .subscribe(device => {
              this.getDevice(this.device.id)
              this.snackbar.open('Rastreador retornando com sucesso', '', {
                duration: 8000
              })
            })
        }

      });

  }

  discardDevice() {

    this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja descartar esse Rastreador?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.devicesService.discardDevices(this.device.id)
            .subscribe(device => {
              this.getDevice(this.device.id)
              this.snackbar.open('Rastreador descartado com sucesso', '', {
                duration: 8000
              })
            })
        }

      });

  }

  createCustomer() {

    let customer = new Customer()
    let personalData = this.customerPersonalComponent.getCustomerData()

    customer.brand = this.device.brand
    customer.personType = personalData.personType
    customer.name = personalData.name
    customer.mobilePhone = this.utils.stringToNumber(personalData.mobilePhone)
    customer.registrationNumber = this.utils.stringToNumber(this.utils.stringOnlyDigits(personalData.registrationNumber))
    customer.email = personalData.email

    if(this.device.status != 'INACTIVE' && this.customer && (this.customer.email != customer.email)){
      this.snackbar.open('Não é possível fazer a troca do cliente, você precisa ir na ação Trocar Cliente', '', { duration: 8000 })
      return;
    }

    if (personalData.password && personalData.password.length > 0) {
      customer.password = personalData.password;

      if (personalData.password.length < 8) {
        this.snackbar.open('A senha deve ter no mínimo 8 caracteres.', '', { duration: 6000 });
        return;
      }

      if (personalData.confirmPassword && personalData.confirmPassword.length > 0) {
        customer.confirmPassword = personalData.confirmPassword;
        customer.birthDate = new Date(personalData.birthDate);

        if (personalData.confirmPassword != personalData.password) {
          this.snackbar.open('As senhas devem ser iguais.', '', { duration: 6000 });
          return;
        }
      } else {
        this.snackbar.open('A confimação de senha deve ser  preenchida.', '', { duration: 6000 });
        return;
      }

    }


    if (customer.personType == 'PERSON') {

      if (!customer.name || !customer.mobilePhone || !customer.registrationNumber || !customer.email) {
        this.validateAllFields(this.customerPersonalComponent.getCustomerForm())
        this.snackbar.open('Todos os dados do cliente precisam ser preenchidos.', '', { duration: 6000 });
        return;
      }

    } else {

      if (!customer.name || !customer.mobilePhone || !customer.registrationNumber || !customer.email) {
        this.validateAllFields(this.customerPersonalComponent.getCustomerForm())
        this.snackbar.open('Todos os dados da empresa precisam ser preenchidos.', '', { duration: 6000 });
        return;
      }
    }

    this.dialog.open(ConfirmDialogComponent, {
      panelClass: 'event-form-dialog',
      data: {
        width: '600px',
        height: '300px',
        title: 'Confirmação',
        message: (this.customer && this.customer.id) ? 'Deseja atualizar esse Cliente?' : 'Deseja cadastrar esse Cliente?'
      }
    })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {

          this.loadingService.show()

          let wrapper = {
            brand: customer.brand,
            customer: customer
          }
          if (!this.customer || this.customer.id == undefined || (this.customer && (this.customer.email != customer.email))) {

          
              this.customersService.registerCustomer(wrapper)
              .subscribe(response => {

                this.customer = response.customer
                this.loadCustomerData(this.customer)
                this.updateDevice({ 'user': response.customer.user })

                
                this.snackbar.open('Cliente vinculado ao Rastreador com sucesso', '', { duration: 8000 })
                 
               
                if (customer.password && customer.password.length > 0) {
                  this.changePassword(customer, response.customer.user);
                }

                this.showAddCustomer = false
                
               
              }, error => {
                console.log(error)
                this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
                this.loadingService.hide()
              }
              )

            

           
          } else {
            this.updateCustormer()
            this.showAddCustomer = false
            this.loadCustomerData(this.customer)
            this.updateDevice({ 'user': this.customer.user })
            if (customer.password && customer.password.length > 0) {
              this.changePassword(customer, this.customer.user);
            }
          }
        } else {
          this.hideCustomer()
        }

      });

  }

  changeCustomer(){

      this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Trocar de Cliente',
          message: 'Para trocar de cliente é necessário desinstalar o rastreador. deseja fazer esse processo agora ?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.loadingService.show()
          this.devicesService.returnedDevice(this.device.id)
            .subscribe(device => {

              if(device){

                this.device =  device.newDevice
                this.customer = null
                this.isCustomer = false
                this.vehicle = null
                this.isVehicle = false;
                this.isEdit  = false;

                this.router.navigate(['/devices/details/', this.device.id])
                
                this.vehicleForm.patchValue({
                  licensePlate: null,
                  type: null,
                  chassi: null,
                  manufacturer: null,
                  model: null,
                  color: null,
                  manufacturerYear: null,
                  modelYear: null,
                })

                this.generalForm.setValue({
                  brand: null,
                  name: null,
                  mobilePhone: null,
                  residencialPhone: null,
                  email: null,
                  registrationNumber: null
                })

              
                this.localInstalationForm.patchValue({
                  installationSpot: null
                })
      
                //this.getImageDevice(this.device.id)
                this.getPlanData(this.device.id)
                this.loadDevice(this.device)
                
              }else{
                this.loadingService.hide()
              }
            
              this.snackbar.open('Processo de desistalação concluído com sucesso!', '', {
                duration: 8000
              })
            }, error => {
              this.loadingService.hide()
              console.log(error)
              this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
              this.loadingService.hide()
            } )
        }

      });

  }

  updateCustormer() {

    let personalData = this.customerPersonalComponent.getCustomerData()

    var customerData = []

    for (let item in personalData) {
      if (item == 'mobilePhone') {
        personalData[item] = this.utils.stringToNumber(personalData[item])
      } else if (item == 'residencialPhone') {
        personalData[item] = this.utils.stringToNumber(personalData[item])
      } else if (item == 'commercialPhone') {
        personalData[item] = this.utils.stringToNumber(personalData[item])
      }
    }

    for (let item in personalData) {
      let editItem = { 'op': 'add', 'path': '/' + item, 'value': item == 'birthDate' ? this.convertDate(personalData[item]) : personalData[item] };
      customerData.push(editItem);
    }

    var updatesCall = [this.customersService.update(this.customer.id, customerData).map((res: Response) => res)]

    Observable.forkJoin(updatesCall).subscribe(result => {

      this.snackbar.open('Cliente atualizado com sucesso', '', {
        duration: 8000
      })

      this.loadingService.hide()
    },
      response => {
        this.snackbar.open(response.error.message, '', { duration: 8000 })
        this.loadingService.hide()
      })

  }

  convertDate(date) {
    if (date) {
      let _date = date.split('/');
      _date = _date[1] + '-' + _date[0] + '-' + _date[2]
      return new Date(_date)
    }
  }

  checkOffersActive(offerId){
    
    if(this.plan && offerId == this.plan.offer){
      return  true
    }

    return false
  }

  planOffersChange(offer){

    let body ={
      "loggedUser": this.appContext.loggedUser.id,
	     "offer": offer.id
    }

    this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Mudança de Plano',
          message: 'Deseja atualizar seu plano para o plano '+this.formatOffersName(offer.name)+ ' ?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.loadingService.show();
          this.plansService.planOffersChange(this.plan.id,body)
            .subscribe(result => {
              this.getPlanData(this.device.id)
              this.snackbar.open('Plano atualizado com sucesso!', '', {
                duration: 8000
              })
            })
        }

      });

  }

  changePassword(customer, userId) {

    let body = {
      "token": '',
      "user": userId,
      "password": customer.password,
      "confirmPassword": customer.confirmPassword,
    }

    this.userService.changePassword(body)
      .subscribe(result => {

      })
  }

  createVehicle() {

    if (this.vehicleDetailsComponent.isInvalid()) {
      this.validateAllFields(this.vehicleDetailsComponent.getDataForm())
      this.snackbar.open('Informe todos os dados do Veículo.', '', { duration: 8000 })
      return
    }

    if(this.device.status != 'INACTIVE' && this.vehicle && ((this.vehicle.model != this.vehicleForm.value.model) && (this.vehicle.licensePlate != this.vehicleForm.value.licensePlate))){
      this.snackbar.open('Não é possível fazer a troca do veículo, você precisa ir na ação Trocar Cliente', '', { duration: 8000 })
      return;
    }

    this.dialog.open(ConfirmDialogComponent, {
      panelClass: 'event-form-dialog',
      data: {
        width: '600px',
        height: '300px',
        title: 'Confirmação',
        message: (this.vehicle && this.vehicle.id) ? 'Deseja atualizar esse Veículo?' : 'Deseja cadastrar esse Veículo?'
      }
    })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {

          this.loadingService.show()

          let vehicle = this.vehicleDetailsComponent.getData()
          vehicle.name = vehicle.licensePlate
          vehicle.brand = this.device.brand

          if (this.vehicle && this.vehicle.id && this.vehicle.chassi != undefined ) {
      
            this.updateVehicle(vehicle)
            this.showAddVeicle = false
           
          } else {
            this.vehicleService.create(vehicle)
              .subscribe(response => {
                this.isVehicle = true;
                this.loadVehicleData(response)
                this.updateDevice({ 'car': response.id })
                this.showAddVeicle = false
                this.snackbar.open('Veículo vinculado ao Rastreador com sucesso', '', { duration: 8000 })
                
              }, error => {
                console.log(error)
                this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
                this.loadingService.hide()
              })
              
          }

        } else {
          this.hideVehicle()
        }
      });

  }

  updateVehicle(editForm) {

    this.loadingService.show()

    let editData = []

    for (let item in editForm) {
      let editItem = { 'op': 'add', 'path': '/' + item, 'value': editForm[item] };
      editData.push(editItem);
    }

    this.vehicleService.update(this.vehicle.id, editData)
      .subscribe(vehicle => {
        this.snackbar.open('Veículo atualizado com sucesso', '', {
          duration: 8000
        })

        this.getVehicleById(this.vehicle.id)

      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        this.loadingService.hide()
      })

  }

  updateDevice(editForm) {
    this.loadingService.show()

    let editData = []

    for (let item in editForm) {
      let editItem = null;

      if (item == 'number' || item == 'simCard') {
        editItem = { 'op': 'add', 'path': '/' + item, 'value': this.utils.stringToNumber(editForm[item]) };
      } else {
        editItem = { 'op': 'add', 'path': '/' + item, 'value': editForm[item] };
      }

      editData.push(editItem);
    }

    this.devicesService.update(this.device.id, editData)
      .subscribe(device => {
        this.snackbar.open('Rastreador atualizado com sucesso', '', {
          duration: 8000
        })

        if (this.isEdit) {
          this.isEdit = false
        }

        this.getDevice(this.device.id)

        this.loadingService.hide()

      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        this.loadingService.hide()
      })
  }

  handleFileInput(files: FileList, type: string, imageType: string) {

    this.fileToUpload = files.item(0);
    let reader = new FileReader();
    reader.readAsDataURL(files.item(0));
    reader.onload = (event) => {
      this.changeImage(type, imageType)
    }
  }

  changeImage(type, imageType) {

    let id = this.device.id
    this.loadingService.show()

    if (this.fileToUpload) {
      const formData = new FormData();
      formData.append('file', this.fileToUpload);

      this.serviceOrdersService.setPictureDevice(formData, type, id).subscribe(
        picture => {
          
          this.fileToUpload = null
          //this.getImageDevice()
        }, error => {
          console.log(error)
          this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
          this.loadingService.hide()
        })
    } else {
      this.loadingService.hide()
    }

  }

  activeDevice() {

    this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja ativar esse Rastreador?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.loadingService.show();
          this.devicesService.activateDevices(this.device.id)
            .subscribe(device => {
              this.getDevice(this.device.id)
              this.snackbar.open('Rastreador ativado com sucesso', '', {
                duration: 8000
              })
            })
        }

      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        this.loadingService.hide()
      })


  }

  reactivateDevice() {

    this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja reativar esse Rastreador?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.loadingService.show();
          this.devicesService.reactivateDevices(this.device.id)
            .subscribe(device => {
              this.getDevice(this.device.id)
              this.snackbar.open('Rastreador ativado com sucesso', '', {
                duration: 8000
              })
            })
        }

      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        this.loadingService.hide()
      })

  }

  cancelDevice() {

    this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja cancelar esse Rastreador?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.loadingService.show();
          this.devicesService.cancelDevices(this.device.id)
            .subscribe(device => {
              this.getDevice(this.device.id)
              this.snackbar.open('Rastreador cancelado com sucesso', '', {
                duration: 8000
              })
            })
        }

      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        this.loadingService.hide()
      })

  }

  unistalDevice() {

    this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja desistalar esse Rastreador?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.loadingService.show();
          this.devicesService.deactivateDevices(this.device.id)
            .subscribe(device => {
              this.getDevice(this.device.id)
              this.snackbar.open('Rastreador desistalado com sucesso', '', {
                duration: 8000
              })
            })
        }

      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        this.loadingService.hide()
      })

  }

  maintenanceDevice() {

    this.dialog.open(ConfirmDialogComponent,
      {
        panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja colocar em manutencão esse Rastreador?'
        }
      })
      .afterClosed()
      .subscribe(response => {

        if (response.data.confirm) {
          this.loadingService.show();
          this.devicesService.maintenanceDevices(this.device.id)
            .subscribe(device => {
              this.getDevice(this.device.id)
              this.snackbar.open('Rastreador em manutenção com sucesso', '', {
                duration: 8000
              })
            })
        }

      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        this.loadingService.hide()
      })

  }
}


