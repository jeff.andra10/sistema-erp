import { Component, OnInit, ViewChild, AfterViewInit, Injector } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Subject } from 'rxjs';
import { EditDeviceComponent } from './edit-device/edit-device.component';
import { DeleteDeviceComponent } from './delete-device/delete-device.component';
import { DevicesService } from '../core/services/devices.service';
import { fuseAnimations } from '@fuse/animations';
import { DevicesDataSource } from './devices.datasource';
import { BaseComponent } from '../_base/base-component.component';
import { DevicesModel, DeviceStatus, DeviceColorStatus, DeviceColorStatusLight, DeviceStatusDate, DeviceStatusLifecycle} from '../core/models/device.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { VehiclesService } from '../core/services/vehicles.service';
import { ConfirmDialogComponent } from 'app/dialogs/confirm-dialog/confirm-dialog.component';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { Router } from '@angular/router';
import { UtilsService } from '../core/utils.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'tnm-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss'],
  animations: fuseAnimations
})
export class DevicesComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  displayedColumns = []
  
  dataSource: DevicesDataSource
  brandList: any
  deviceModelList : any
  searchForm: FormGroup
  statusList: Array<any>
  resultLength: number = 0
  searchSubject = new Subject()
  isAssociation : Boolean = false
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  statusDevice = 'Data da Ativação'
  statusSearchForm = null

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private devicesService: DevicesService,
    private vehicleService: VehiclesService,
    private dialog: MatDialog,
    private utils: UtilsService,
    private snackbar: MatSnackBar,
    private loadingService: FuseSplashScreenService,
    private router: Router
  ) { 
    super(injector)
    
  }

  ngOnInit() {

    this.loadingService.hide()
    localStorage.setItem('hiddenLoading','false');
    
    if(this.appContext.brandTracknme != this.appContext.loggedUser.brand){
      this.isAssociation = true
    }
    
    this.displayedColumns = ['statusColor','space','imei', 'brand', 'status', 'licensePlate', 'model', 'conection', 'lifecycle.activeDate' ]//'maintenance','recovered',  'discard' , 'cancel',


    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadDevices(val)
    })

    this.dataSource = new DevicesDataSource(this.devicesService, this.utilsService,  this.appContext.brands)
    this.dataSource.load('',0,'','','','','id', 'asc', 0, 10)

    this.initForms()
    this.loadDomainListData()
  }

  loadDomainListData() {
    this.statusList = this.utils.stringEnumToKeyValue(DeviceStatus)
    this.deviceModelList = this.utils.stringEnumToKeyValue(DevicesModel)
    this.brandList = this.appContext.brands
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    if(this.dataSource && this.dataSource.totalElements$){
      this.dataSource.totalElements$.subscribe( value => {
        this.resultLength = value
      })
    }

    let startDate = ''
    let endDate = ''
    
    if(this.searchForm.value.startDate){
      startDate =  new Date(this.searchForm.value.startDate).toISOString()
    }

    if(this.searchForm.value.endDate){
      endDate =  new Date(this.searchForm.value.endDate).toISOString()
    }
    
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadDevices('',this.searchForm.value.brand,this.searchForm.value.model,this.searchForm.value.status,startDate.replace('.000Z', ''),endDate.replace('.000Z', '')))
    )
    .subscribe()
  }

  initForms() {

    this.searchForm = this.formBuilder.group({
      brand: this.formBuilder.control(''),
      model: this.formBuilder.control(''),
      status: this.formBuilder.control(''),
      startDate: this.formBuilder.control(''),
      endDate: this.formBuilder.control('')
    });
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  onListValueChanged() {
    let startDate = ''
    let endDate = ''
    
    if(this.searchForm.value.startDate){
      startDate =  new Date(this.searchForm.value.startDate).toISOString()
    }

    if(this.searchForm.value.endDate){
      endDate =  new Date(this.searchForm.value.endDate).toISOString()
    }

  
    this.loadDevices('',this.searchForm.value.brand,this.searchForm.value.model,this.searchForm.value.status,startDate.replace('.000Z', ''),endDate.replace('.000Z', ''))
    if(this.searchForm.value.status != ""){
      this.statusDevice =  DeviceStatusDate[this.searchForm.value.status]
      this.statusSearchForm = this.searchForm.value.status
    }else{
      this.statusDevice = 'Data da Ativação'
      this.statusSearchForm = null
    }
    

  }

  
  loadDevices(filter? : string,brand?: number,model? : string,status? : string,startDate? : string,endDate? : string) {
    
    this.dataSource.load(
      filter, 
      brand,
      model,
      status,
      startDate,
      endDate,
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  } 

  editDeviceDialog(row) {
    this.dialog
      .open(EditDeviceComponent, {
        data: row,
        width: '600px',
      })
      .afterClosed()
        .subscribe(() => this.loadDevices())
  }


  deleteDeviceDialog(row){
    this.dialog
      .open(DeleteDeviceComponent, {
        data: row,
        width: '500px',
      })
      .afterClosed()
        .subscribe(() => this.loadDevices())
  }

  deviceActivityToString(status) : string {

    if(!status){
      return 'Inativo'
    }
    
    if(status == 'RECEIVED'){
      return 'Disponivel'
    }

    return DeviceStatus[status]
  }

  returnedDevice(row) {

    this.dialog.open(ConfirmDialogComponent, 
      { 
        panelClass: 'event-form-dialog',
        data: { 
          width: '600px',
          height: '300px',
          title: 'Confirmação', 
          message: 'Deseja retornar esse rastreador?' 
        }
      })
      .afterClosed()
      .subscribe(response=>{
        if(response.data.confirm){
          this.devicesService.returnedDevice(row.id)
          .subscribe(device => {
            this.loadDevices()
            this.snackbar.open('Rastreador retornando com sucesso', '', {
              duration: 8000
            })
          })
        }
       
      });
    
  }

  discardDevice(row) {

    this.dialog.open(ConfirmDialogComponent, 
      { 
        panelClass: 'event-form-dialog',
        data: { 
          width: '600px',
          height: '300px',
          title: 'Confirmação', 
          message: 'Deseja descartar esse rastreador?' 
        }
      })
      .afterClosed()
      .subscribe(response=>{
        if(response.data.confirm){
          this.devicesService.discardDevices(row.id)
    .subscribe(device => {
      this.loadDevices()
      this.snackbar.open('Rastreador descartado com sucesso', '', {
        duration: 8000
      })
    })
        }
       
      });
    
  }

  deviceColorStatusEnumToString(value) : string {
    return DeviceColorStatus[value]
  }

  deviceColorStatusLightEnumToString(value) : string {
    return DeviceColorStatusLight[value]
  }

  selectDevice(row){
    this.loadingService.show()
    this.router.navigate(['/devices/details/', row.id])
  }

  getLifecycle(element){

    var datePipe = new DatePipe(navigator.language)

    if(element && element.lifecycle){

      if(this.statusSearchForm && element.lifecycle[DeviceStatusLifecycle[this.statusSearchForm]] != undefined){
        return element.lifecycle[DeviceStatusLifecycle[this.statusSearchForm]] ? datePipe.transform(new Date(element.lifecycle[DeviceStatusLifecycle[this.statusSearchForm]]), 'dd/MM/yyyy HH:mm:ss') : '-'
      }

      return element.lifecycle.activeDate ? datePipe.transform(new Date(element.lifecycle.activeDate), 'dd/MM/yyyy HH:mm:ss') : '-'
    }

    return '-'
  
  }
}