import { NgModule, ModuleWithProviders } from "@angular/core";
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../angular-material/material.module";
import { FuseSharedModule } from "@fuse/shared.module";
import { RouterModule } from "@angular/router";
import { ChartsModule } from 'ng2-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TransactionsSelectedService } from './transactions.selected.service';

import { MatFormFieldModule, MatIconModule, MatMenuModule, MatSelectModule, MatTabsModule } from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';


import { TransactionsComponent } from "./transactions.component";
import { TransactionsListComponent } from "./transactions-list/transactions-list.component";
import { TransactionsDetailComponent } from "./transactions-detail/transactions-detail.component";
//import { TransactionsListDataSource } from "./transactions-list/transactions-list.datasource"
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';

const authRouting: ModuleWithProviders = RouterModule.forChild([
    // { path: 'transactions', component: TransactionsComponent },
    // { path: 'transactions/list', component: TransactionsListComponent}

      { path: '', component: TransactionsComponent },
      { path: 'list', component: TransactionsListComponent}
  ]);

@NgModule({
    declarations:[
        TransactionsComponent,
        TransactionsListComponent,
        TransactionsDetailComponent
    ],
    imports: [
        TextMaskModule,
        MaterialModule,
        ReactiveFormsModule,
        CommonModule,
        FuseSharedModule,
        RouterModule,
        authRouting,
        MatFormFieldModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        MatTabsModule,
        ChartsModule,
        NgxChartsModule,
        FuseWidgetModule,
        DateRangePickerModule
    ],
    exports: [
        TransactionsDetailComponent
    ],
    entryComponents: [
        TransactionsDetailComponent
    ],
    providers: [
        TransactionsSelectedService
    ]
   
})
export class TransactionsModule {}