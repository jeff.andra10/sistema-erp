import { Component, OnInit, Injector , Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA} from '@angular/material';
import { BaseComponent } from '../../_base/base-component.component';
import { AppContext } from '../../core/appcontext';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';
import { Accouttings } from '../../core/models/accouttings.modal'
import { Sale } from '../../core/models/sale.model'
import { Payment, CreditCardBrand , PaymentCreditCard} from '../../core/models/payment.model'
import { SalesService } from '../../core/services/sales.service'
import { PaymentsService } from '../../core/services/payments.service'

@Component({
  selector: 'tnm-transactions-detail',
  templateUrl: './transactions-detail.component.html',
  styleUrls: ['../transactions.component.scss'],
})
export class TransactionsDetailComponent  implements OnInit {

  
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');
  
  transaction: Accouttings;
  payment: Payment;
  sale: Sale;

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef< TransactionsDetailComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackbar: MatSnackBar,
    private paymentsService : PaymentsService,
    private salesService : SalesService
  ) {
   }

  ngOnInit() {
  
    this.salesService.getSaleById(this.data.sale)
    .subscribe(sale => { 
      this.sale = sale;
      this.paymentsService.getPaymentById(sale.payment)
       .subscribe(payment => { 
        this.payment = payment
      })
    })
  
    this.transaction = this.data
  }

  CreditCardBrandEnum(value) : string {
    return CreditCardBrand[value]
  }

  PaymentStatusEnum(value) : string {
    return PaymentCreditCard[value]
  }


}
