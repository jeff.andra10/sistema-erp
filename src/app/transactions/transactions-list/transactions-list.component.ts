import { Component, OnInit, ViewChild, AfterViewInit, Injector} from '@angular/core'
import { MatSnackBar, MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material'
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import { Accouttings, AccouttingsOrigin } from '../../core/models/accouttings.modal';
import { AccouttingsService } from '../../core/services/accouttings.service';
import { TransactionsDetailComponent} from '../transactions-detail/transactions-detail.component';
import { BaseComponent } from '../../_base/base-component.component';
import { TransactionsDataSource } from '../transactions.datasource'
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { PaymentCreditCard,PaymentType } from '../../core/models/payment.model';
import * as moment from 'moment';
import { TransactionsSelectedService } from '../transactions.selected.service';

@Component({
  selector: 'tnm-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.scss'],
  animations: fuseAnimations
})
export class TransactionsListComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  displayedColumns = ['dueDate', 'brand','customer', 'licensePlate','status', 'paymentType',  'origin', 'amount']
  //displayedColumns = ['creationDate', 'brand','dueDate', 'paymentType', 'type',  'origin', 'amount', 'deductedAmount', 'status']
  dataSource : TransactionsDataSource
  resultLength: number = 0
  widgets: any;
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  widget5SelectedDay = 'today';
  transactionsList : any;
  transactionsForm: FormGroup
  brandList : any;
  widget5: any;

  balanceAvailable = 0
  balanceSuccess = 0

  start: Date = new Date ("10/07/2017"); 
  end: Date = new Date ("11/25/2017");

  
  colorSeleted : string = 'light-green-bg'
  title : string = ''

  datasets = {
    'today'    : [
        {
            label: 'Depositado',
            data : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            fill : 'start'
        },
        {
            label: 'Capturado',
            data : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            fill : 'start'
        }
    ],
    labels :[]
  }
  colors = [
        {
            borderColor              : 'rgba(95, 168, 241, 0.9)',
            backgroundColor          : 'rgba(95, 168, 241, 0.9)',
            pointBackgroundColor     : 'rgba(95, 168, 241, 0.87)',
            pointHoverBackgroundColor: 'rgba(95, 168, 241, 0.87)',
            pointBorderColor         : '#ffffff',
            pointHoverBorderColor    : '#ffffff'
        },
        {
            borderColor              : 'rgba(231, 150, 35, 0.9)',
            backgroundColor          : 'rgba(231, 150, 35, 0.9)',
            pointBackgroundColor     : 'rgba(231, 150, 35, 0.87)',
            pointHoverBackgroundColor: 'rgba(231, 150, 35, 0.87)',
            pointBorderColor         : '#ffffff',
            pointHoverBorderColor    : '#ffffff'
        }
    ]

options = {
    spanGaps           : false,
    legend             : {
        display: false
    },
    maintainAspectRatio: false,
    tooltips           : {
        position : 'nearest',
        mode     : 'index',
        intersect: false
    },
    layout             : {
        padding: {
            left : 24,
            right: 32
        }
    },
    elements           : {
        point: {
            radius          : 4,
            borderWidth     : 2,
            hoverRadius     : 4,
            hoverBorderWidth: 2
        }
    },
    scales             : {
        xAxes: [
            {
                gridLines: {
                    display: false
                },
                ticks    : {
                    fontColor: 'rgba(0,0,0,0.54)'
                }
            }
        ],
        yAxes: [
            {
                gridLines: {
                    tickMarkLength: 16
                },
                ticks    : {
                    stepSize: 1000
                }
            }
        ]
    },
    plugins            : {
        filler: {
            propagate: false
        }
    }
}

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder, 
    private snackbar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private accouttingsService: AccouttingsService,
    private transactionsSelectedService : TransactionsSelectedService,
    public dialog: MatDialog
   ) { 
    super(injector)

    this.widgets = {
          'widget2'      : {
              'title' : 'Total',
              'data'  : {
                  'label': 'Total',
                  'count': '2.300'  
              }
          }
      }

     this.widget5= {
        chartType: 'line',
        datasets : this.datasets,
        labels   : ['01/08', '02/08', '03/08', '04/08', '05/08', '06/08', '07/08', '08/08', '09/08', '10/08', '11/08', '12/08'],
        colors   : this.colors,
        options  : this.options
     }

      this.registerCustomChartJSPlugin();
   }

   registerCustomChartJSPlugin()
   {
       (<any>window).Chart.plugins.register({
           afterDatasetsDraw: function (chart, easing) {
               // Only activate the plugin if it's made available
               // in the options
               if (
                   !chart.options.plugins.xLabelsOnTop ||
                   (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
               )
               {
                   return;
               }

               // To only draw at the end of animation, check for easing === 1
               const ctx = chart.ctx;

               chart.data.datasets.forEach(function (dataset, i) {
                   const meta = chart.getDatasetMeta(i);
                   if ( !meta.hidden )
                   {
                       meta.data.forEach(function (element, index) {

                           // Draw the text in black, with the specified font
                           ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                           const fontSize = 13;
                           const fontStyle = 'normal';
                           const fontFamily = 'Roboto, Helvetica Neue, Arial';
                           ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                           // Just naively convert to string for now
                           const dataString = dataset.data[index].toString() + 'k';

                           // Make sure alignment settings are correct
                           ctx.textAlign = 'center';
                           ctx.textBaseline = 'middle';
                           const padding = 15;
                           const startY = 24;
                           const position = element.tooltipPosition();
                           ctx.fillText(dataString, position.x, startY);

                           ctx.save();

                           ctx.beginPath();
                           ctx.setLineDash([5, 3]);
                           ctx.moveTo(position.x, startY + padding);
                           ctx.lineTo(position.x, position.y - padding);
                           ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                           ctx.stroke();

                           ctx.restore();
                       });
                   }
               });
           }
       });
   }

   

  ngOnInit() {

    this.transactionsSelectedService.transactionSelected.subscribe(data => {
     
      if(data && data.color){

        this.colorSeleted = data.color;
        this.title = data.title;

      if(data.title == 'Disponível para retirar'){
          console.log('show')
        this.displayedColumns = ['dueDate', 'brand','customer', 'licensePlate','status', 'paymentType',  'origin', 'amount', 'deductedAmount']
      }

      }
      
    })

    let startDate = moment().startOf('month').toDate();
    let endDate   = moment().endOf('month').toDate();

    this.transactionsForm = this.formBuilder.group({
        startDate: this.formBuilder.control(startDate, [Validators.required]),
        endDate: this.formBuilder.control(endDate, [Validators.required])
    })

    this.brandList = this.appContext.brands

    this.dataSource = new TransactionsDataSource(this.accouttingsService, this.utilsService,this.brandList)
    this.dataSource.load('', '', 'creationDate', 'desc', 0, 10)   

    this.filter()
  }

  filter(){

    let startDate = this.transactionsForm.value.startDate;
    let endDate = this.transactionsForm.value.endDate
    
    this.selectInterval(startDate, endDate)
     
   }

 
  transectionsOriginEnumToString(value) : string {
    return AccouttingsOrigin[value]
  }
  
  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    if(this.dataSource && this.dataSource.totalElements$){
      this.dataSource.totalElements$.subscribe( value => {
        this.resultLength = value
    })

    }
    
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadAccouttings())
    )
    .subscribe()
  }

  loadAccouttings(filter? : string) {

    this.dataSource.load(
      filter, 
      '',
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }

  showTransactionDetailDialog(row) {
    this.dialog
      .open(TransactionsDetailComponent, {
        width: '960px',
        data: row
      })
      .afterClosed()
        .subscribe(() => {})
  }

  getStyleHeight(type){
      if(this.resultLength > 5 ){
        if(type == "header"){
            return  "height: 30%"
        }else{
            return  "height: 70%"
        }
      }else{
        if(this.resultLength > 5 ){
            if(type == "header"){
                return  "height: 45%"
            }else{
                return  "height: 55%"
            }
          }
      }
  }

  PaymentCreditCardEnumToString(value) : string {
    return PaymentCreditCard[value]
  }

  PaymentStatusEnum(value) : string {
    return PaymentType[value]
  }

  selectInterval(startOfWeek,endOfWeek){

    let _startOfWeek =  startOfWeek.toISOString().split('T')
    let _endOfWeek =  endOfWeek.toISOString().split('T')
    
    
        
    this.accouttingsService.getFactByInterval(_startOfWeek[0]+'T00:00:00',_endOfWeek[0]+'T00:00:00'
    ).subscribe(report => {
            
            if(report && report.content && report.content.length > 0){

                this.datasets.today[0].data = [];
                this.datasets.today[1].data = [];
                this.datasets.labels = [];
                this.balanceAvailable = 0
                this.balanceSuccess = 0
                
                report.content.forEach(element => {
                   
                    let date = moment(element.date).format("DD/MM")
                    
                        if(this.datasets.labels.indexOf(date) === -1){
                             this.datasets.labels.push(date)
                         }

                        this.datasets.today[0].data.push(element.balanceAvailable.toFixed(2))//Depositado
                        this.datasets.today[1].data.push(element.balanceSuccess.toFixed(2))//Capturado
                        this.balanceSuccess += element.balanceSuccess
                        this.balanceAvailable += element.balanceAvailable

                      
                });

                this.widget5= {
                    chartType: 'line',
                    datasets : this.datasets,
                    labels   :this.datasets.labels,
                    colors   : this.colors,
                    options  : this.options
                 }
            

                
            }
    })
  }

}
