import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";


@Injectable()
export class TransactionsSelectedService {

    private transactionsSource : BehaviorSubject<any> = new BehaviorSubject(null)
    public transactionSelected = this.transactionsSource.asObservable();

    setSelected(data: any) {
      this.transactionsSource.next(data)
    }
    
}