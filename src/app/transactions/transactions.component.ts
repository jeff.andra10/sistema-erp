import { Component, ViewEncapsulation, ViewChild, Injector } from '@angular/core';
import { MatSnackBar, MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material'
import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';
import { Router } from '@angular/router';
import { AccouttingsService } from '../core/services/accouttings.service';
import { TransactionsSelectedService } from './transactions.selected.service';
import { TransactionsDataSource } from './transactions.datasource'
import * as moment from 'moment';
import { BaseComponent } from '../_base/base-component.component';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'

@Component({
    selector: 'tnm-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class TransactionsComponent extends BaseComponent {

    @ViewChild(MatPaginator) paginator: MatPaginator
    @ViewChild(MatSort) sort: MatSort

    widgets: any;
    widget1: any;
    widget9: any;
    widget1SelectedYear = 'year';
    widget5SelectedDay = 'today';
    report: any
    _balanceAvailable = 0
    _balanceSuccess = 0
    brandList: any;
    resultLength: number = 0

    secondColorBrand : string = localStorage.getItem('secondColorBrand');
    primaryColorBrand: string = localStorage.getItem('primaryColorBrand');
    
    displayedColumns = ['dueDate', 'brand','customer', 'licensePlate', 'paymentType', 'origin', 'amount', 'total']
    
    dataSource: TransactionsDataSource

    _datasets = {
        'today': [
            {
                label: 'Saldo',
                data: [100, 200, 150, 180, 0, 100, 180, 300, 500, 0, 100, 50],
                fill: 'start'
            },
            // {
            //     label: 'Capturado',
            //     data : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            //     fill : 'start'
            // }
        ],
        labels: []
    }
    datasets = {
        'week': [
            {
                label: 'Sales',
                data: ['100', '200', '150', '180', '0', '100', '180', '300', '500', '0', '100', '50'],
                fill: 'start'
            }
        ],
        'month': [
            {
                label: 'Sales',
                data: ['100', '200', '150', '180', '0', '100', '180', '300', '500', '0', '100', '50'],
                fill: 'start'
            }
        ],
        'year': [
            {
                label: 'Sales',
                data: ['100', '200', '150', '180', '0', '100', '180', '300', '500', '0', '100', '50'],
                fill: 'start'
            }
        ],
        labels:
        {
            'week': [],
            'month': [],
            'year': []
        }
    }

    colors = [
        {
            borderColor: '#42a5f5',
            backgroundColor: '#42a5f5',
            pointBackgroundColor: '#1e88e5',
            pointHoverBackgroundColor: '#1e88e5',
            pointBorderColor: '#ffffff',
            pointHoverBorderColor: '#ffffff'
        }
    ]

    _colors = [
        {
            borderColor: 'rgba(95, 168, 241, 0.9)',
            backgroundColor: 'rgba(95, 168, 241, 0.9)',
            pointBackgroundColor: 'rgba(95, 168, 241, 0.87)',
            pointHoverBackgroundColor: 'rgba(95, 168, 241, 0.87)',
            pointBorderColor: '#ffffff',
            pointHoverBorderColor: '#ffffff'
        },
        {
            borderColor: 'rgba(231, 150, 35, 0.9)',
            backgroundColor: 'rgba(231, 150, 35, 0.9)',
            pointBackgroundColor: 'rgba(231, 150, 35, 0.87)',
            pointHoverBackgroundColor: 'rgba(231, 150, 35, 0.87)',
            pointBorderColor: '#ffffff',
            pointHoverBorderColor: '#ffffff'
        }
    ]

    _options = {
        spanGaps: false,
        legend: {
            display: false
        },
        maintainAspectRatio: false,
        tooltips: {
            position: 'nearest',
            mode: 'index',
            intersect: false
        },
        layout: {
            padding: {
                left: 24,
                right: 32
            }
        },
        elements: {
            point: {
                radius: 4,
                borderWidth: 2,
                hoverRadius: 4,
                hoverBorderWidth: 2
            }
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        fontColor: 'rgba(0,0,0,0.54)'
                    }
                }
            ],
            yAxes: [
                {
                    gridLines: {
                        tickMarkLength: 16
                    },
                    ticks: {
                        stepSize: 1000
                    }
                }
            ]
        },
        plugins: {
            filler: {
                propagate: false
            }
        }
    }

    options = {
        spanGaps: false,
        legend: {
            display: false
        },
        maintainAspectRatio: false,
        layout: {
            padding: {
                top: 32,
                left: 32,
                right: 32
            }
        },
        elements: {
            point: {
                radius: 4,
                borderWidth: 2,
                hoverRadius: 4,
                hoverBorderWidth: 2
            },
            line: {
                tension: 0
            }
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        display: false,
                        drawBorder: false,
                        tickMarkLength: 18
                    },
                    ticks: {
                        fontColor: '#ffffff'
                    }
                }
            ],
            yAxes: [
                {
                    display: false,
                    ticks: {
                        min: 1.5,
                        max: 5,
                        stepSize: 0.5
                    }
                }
            ]
        },
        plugins: {
            filler: {
                propagate: true
            },
            xLabelsOnTop: {
                active: true
            }
        }
    }

    constructor(
        private injector: Injector,
        private router: Router,
        private fuseConfig: FuseConfigService,
        private accouttingsService: AccouttingsService,
        private transactionsSelectedService: TransactionsSelectedService,
    ) {
        super(injector)
        // this.widget1 = {
        //     chartType: 'line',
        //     datasets : this.datasets,
        //     labels : this.datasets.labels,
        //     colors   : this.colors,
        //     options  : this.options
        // }

        this.widget9 = {
            chartType: 'line',
            datasets: this._datasets,
            labels: ['01/08', '02/08', '03/08', '04/08', '05/08', '06/08', '07/08', '08/08', '09/08', '10/08', '11/08', '12/08'],
            colors: this._colors,
            options: this._options
        }



        this.widgets = {
            'widget1': {
                'title': 'Em aberto',
                'data': {
                    'label':'Visualizar',
                    'count': 500
                }
            },
            'widget2': {
                'title': 'Em atraso',
                'data': {
                    'label':'Visualizar',
                    'count': 300
                }
            },
            'widget3': {
                'title': 'Pagamento efetuado',
                'data': {
                    'label':'Visualizar',
                    'count': 2000
                }
            },
            'widget4': {
                'title': 'Saldo',
                'data': {
                    'label':'Disponível para retirar',
                    'count': 700
                }
            },

            widget5: {
                chartType: 'line',
                datasets: {
                    'yesterday': [
                        {
                            label: 'Visitors',
                            data: [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                            fill: 'start'

                        },
                        {
                            label: 'Page views',
                            data: [2200, 2900, 3900, 2500, 3800, 3200, 2900, 1900, 3000, 3400, 4100, 3800],
                            fill: 'start'
                        }
                    ],
                    'today': [
                        {
                            label: 'Visitors',
                            data: [410, 380, 320, 290, 190, 390, 250, 380, 300, 340, 220, 290],
                            fill: 'start'
                        },
                        {
                            label: 'Page Views',
                            data: [3000, 3400, 4100, 3800, 2200, 3200, 2900, 1900, 2900, 3900, 2500, 3800],
                            fill: 'start'

                        }
                    ]
                },
                labels: ['12am', '2am', '4am', '6am', '8am', '10am', '12pm', '2pm', '4pm', '6pm', '8pm', '10pm'],
                colors: [
                    {
                        borderColor: '#3949ab',
                        backgroundColor: '#3949ab',
                        pointBackgroundColor: '#3949ab',
                        pointHoverBackgroundColor: '#3949ab',
                        pointBorderColor: '#ffffff',
                        pointHoverBorderColor: '#ffffff'
                    },
                    {
                        borderColor: 'rgba(30, 136, 229, 0.87)',
                        backgroundColor: 'rgba(30, 136, 229, 0.87)',
                        pointBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                        pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                        pointBorderColor: '#ffffff',
                        pointHoverBorderColor: '#ffffff'
                    }
                ],
                options: {
                    spanGaps: false,
                    legend: {
                        display: false
                    },
                    maintainAspectRatio: false,
                    tooltips: {
                        position: 'nearest',
                        mode: 'index',
                        intersect: false
                    },
                    layout: {
                        padding: {
                            left: 24,
                            right: 32
                        }
                    },
                    elements: {
                        point: {
                            radius: 4,
                            borderWidth: 2,
                            hoverRadius: 4,
                            hoverBorderWidth: 2
                        }
                    },
                    scales: {
                        xAxes: [
                            {
                                gridLines: {
                                    display: false
                                },
                                ticks: {
                                    fontColor: 'rgba(0,0,0,0.54)'
                                }
                            }
                        ],
                        yAxes: [
                            {
                                gridLines: {
                                    tickMarkLength: 16
                                },
                                ticks: {
                                    stepSize: 1000
                                }
                            }
                        ]
                    },
                    plugins: {
                        filler: {
                            propagate: false
                        }
                    }
                }
            },
            widget7: {
                scheme: {
                    domain: ['#ff9800', '#03a9f4']
                },
                devices: [
                    {
                        name: 'A Receber',
                        value: 0,

                    },
                    {
                        name: 'Confirmado',
                        value: 0,

                    },

                ]
            }
        }

        this.registerCustomChartJSPlugin();
    }

    ngOnInit() {

        this.accouttingsService.getFactByBrand(
        ).subscribe(report => {
            if (report && report.content && report.content.length > 0) {
                this.report = report.content[0]
            }
        })
        localStorage.setItem('hiddenLoading', 'false');
        this.selectInterval('week');

        this.selectIntervalS()

        this.brandList = this.appContext.brands

        this.dataSource = new TransactionsDataSource(this.accouttingsService, this.utilsService, this.brandList)
        this.dataSource.load('', '', 'creationDate', 'desc', 0, 10)

    }

    viewTransactionsList(color, title) {
        let data = {
            color: color,
            title: title
        }
        this.transactionsSelectedService.setSelected(data)
        this.router.navigate(['transactions/list'])
    }

    selectInterval(type) {

        if (type == 'year') {
            this.selectIntervalYear()
            return false
        }

        let startOfWeek = moment().startOf(type).toDate();
        let endOfWeek = moment().endOf(type).toDate();

        let _startOfWeek = startOfWeek.toISOString().split('T')
        let _endOfWeek = endOfWeek.toISOString().split('T')

        this.datasets.week[0].data = [];
        this.datasets.month[0].data = [];

        this.accouttingsService.getFactByInterval(_startOfWeek[0] + 'T00:00:00', _endOfWeek[0] + 'T00:00:00'
        ).subscribe(report => {

            if (report && report.content && report.content.length > 0) {
                this.report = report.content[0]
                this.report['balanceSuccess'] = 0;
                this.report['balanceAvailable'] = 0;
                this.report['balanceCanceled'] = 0;
                report.content.forEach(element => {
                    let date = moment(element.date).format("DD/MM")
                    if (type == 'week') {

                        if (this.datasets.labels.week.indexOf(date) === -1) {
                            this.datasets.labels.week.push(date)
                        }

                        this.datasets.week[0].data.push((element.balanceGrossAmount / 1000).toFixed(2))
                        this.report['balanceSuccess'] += element.balanceSuccess
                        this.report['balanceAvailable'] += element.balanceAvailable
                        this.report['balanceCanceled'] += element.balanceCanceled
                    } else if (type == 'month') {

                        if (this.datasets.labels.month.indexOf(date) === -1) {
                            this.datasets.labels.month.push(date)
                        }

                        this.datasets.month[0].data.push((element.balanceGrossAmount / 1000).toFixed(2))
                        this.report['balanceSuccess'] += element.balanceSuccess
                        this.report['balanceAvailable'] += element.balanceAvailable
                        this.report['balanceCanceled'] += element.balanceCanceled
                    }

                });

                this.widget1 = {
                    chartType: 'line',
                    datasets: this.datasets,
                    labels: this.datasets.labels,
                    colors: this.colors,
                    options: this.options
                }

                this.widget1SelectedYear = type
            }
        })
    }

    selectIntervalYear() {

        let year = new Date()
        this.accouttingsService.getFactByYear(year.getFullYear() + '-01-01T00:00:00', year.getFullYear() + '-12-31T00:00:00'
        ).subscribe(report => {

            if (report && report.content && report.content.length > 0) {
                this.report = report.content[0]
                this.report['balanceSuccess'] = 0;
                this.report['balanceAvailable'] = 0;
                this.report['balanceCanceled'] = 0;

                let _report = report.content.sort(function (a, b) {
                    return a.date.localeCompare(b.date);
                });
                _report.forEach(element => {
                    let date = moment(element.date).format("MMM")


                    if (this.datasets.labels.year.indexOf(date) === -1) {
                        this.datasets.labels.year.push(date)
                    }

                    this.datasets.year[0].data.push((element.balanceGrossAmount / 1000).toFixed(2))
                    this.report['balanceSuccess'] += element.balanceSuccess
                    this.report['balanceAvailable'] += element.balanceAvailable
                    this.report['balanceCanceled'] += element.balanceCanceled


                });

                this.widget1 = {
                    chartType: 'line',
                    datasets: this.datasets,
                    labels: this.datasets.labels,
                    colors: this.colors,
                    options: this.options
                }

                this.widget1SelectedYear = "year"
            }
        })


    }

    registerCustomChartJSPlugin() {
        (<any>window).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing) {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i) {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index) {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (<any>window).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + 'k';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }

    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

        if (this.dataSource && this.dataSource.totalElements$) {
            this.dataSource.totalElements$.subscribe(value => {
                this.resultLength = value
            })

        }

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => this.loadAccouttings())
            )
            .subscribe()
    }

    loadAccouttings(filter?: string) {

        this.dataSource.load(
            filter,
            '',
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize)
    }

    selectIntervalS() {


        this.accouttingsService.getFactByBrand().subscribe(report => {

            if (report && report.content && report.content.length > 0) {

                this._datasets.today[0].data = [];
                //this._datasets.today[1].data = [];
                this._datasets.labels = [];
                this._balanceAvailable = 0
                this._balanceSuccess = 0

                report.content.forEach(element => {

                    let date = moment(element.date).format("DD/MM")

                    if (this._datasets.labels.indexOf(date) === -1) {
                        this._datasets.labels.push(date)
                    }

                    this._datasets.today[0].data.push(element.balanceAvailable.toFixed(2))//Depositado
                    //this._datasets.today[1].data.push(element.balanceSuccess.toFixed(2))//Capturado
                    this._balanceSuccess += element.balanceSuccess
                    this._balanceAvailable += element.balanceAvailable


                });

                // this.widget9= {
                //     chartType: 'line',
                //     datasets : this._datasets,
                //     labels   :this._datasets.labels,
                //     colors   : this._colors,
                //     options  : this._options
                //  }



            }
        })
    }

}