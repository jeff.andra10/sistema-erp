import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";

import { UtilsService } from "../core/utils.service";
import { PaginatedContent } from "../core/models/paginatedContent.model";
import { Accouttings } from "../core/models/accouttings.modal";
import { AccouttingsService } from "../core/services/accouttings.service";
import { Brand } from "../core/models/brand.model";



export class TransactionsDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<Accouttings[]>([]);
    private totalElementsSubject = new BehaviorSubject<number>(0)
    public totalElements$ = this.totalElementsSubject.asObservable()

    constructor(
        private accouttingsService : AccouttingsService, 
        private utilsService : UtilsService,
        private brands: Array<Brand>
    ){}

    load(filter:string,
        status: string,
        sortField:string,
        sortDirection:string,
        pageIndex:number,
        pageSize:number) {

        this.accouttingsService.findAccouttings(
            filter, 
            status,
            sortField,
            sortDirection,
            pageIndex, 
            pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe((result : PaginatedContent<Accouttings>) => {

                if (result.content && this.brands) {
                    result.content.forEach(obj => {
                        obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                    });
                }

                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<Accouttings[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
