import { Component, OnInit, ViewEncapsulation, AfterViewInit, Injector } from '@angular/core';
import { Purchase, PurchaseStatus } from '../../core/models/purchase.model';
import { fuseAnimations } from '@fuse/animations';
import { PurchaseSelectedService } from '../purchase.selected.service';
import { PaymentType, PaymentCreditCard } from '../../core/models/payment.model';
import { PurchasesService } from '../../core/services/purchases.service';
import { BaseComponent } from '../../_base/base-component.component';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { PurchaseConfirmReceivedComponent } from '../purchase-confirm-received/purchase-confirm-received.component';
import { CustomersService } from '../../core/services/customers.service';
import { Address } from '../../core/models/address.model';
import { ProductsService } from 'app/core/services/products.service';
import { ServicesService } from 'app/core/services/services.service';
import { BrandsService } from 'app/core/services/brands.service';
import { PaymentsService } from '../../core/services/payments.service';
import { PlanRecurrence } from '../../core/models/plan.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-purchase-details',
  templateUrl: './purchase-details.component.html',
  styleUrls: ['./purchase-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class PurchaseDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  servicesList: any
  productsList: any
  purchase: Purchase = new Purchase()
  canApprove: boolean = true
  canConfirmReceived: boolean = false
  statusList = [] 
  chargeList = [] 
  address: Address
  statusPayment : string
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  
  constructor(
    private injector: Injector,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private productsService: ProductsService,
    private servicesService: ServicesService,
    private purchaseSelectedService: PurchaseSelectedService,
    private purchaseService: PurchasesService,
    private paymentsService: PaymentsService,
    private customersService: CustomersService,
    private brandsService: BrandsService
  ) { 
    super(injector)
  }

  ngOnInit() {

    this.getPurchase()

    this.statusPayment = 'PENDING'
  }

  ngAfterViewInit() {
    setTimeout(() => {
      
    })
  }

  getBussinessAddress(){
   
    if(this.purchase && this.purchase.brand){
        this.brandsService.getBrandAddress(this.purchase.brand).subscribe(address =>{   
          if(address.content && address.content.length > 0){
              this.address = address.content[0]
          }
      })
    }  
  }

  getPurchase(){

    this.purchaseSelectedService.purchaseSelected.subscribe(purchase => {
      
      if (this.purchase != null) {
        
        this.purchase = purchase
        this.createStatusList()

        if(this.purchase && this.purchase.status){
          this.canApprove = (PurchaseStatus[this.purchase.status] == PurchaseStatus.OPEN)
          this.canConfirmReceived = (PurchaseStatus[this.purchase.status] == PurchaseStatus.SHIPPED)
        }

        this.getBussinessAddress();

        this.paymentsService.getChargesBySaleId(this.purchase['sale'])
                .subscribe(charge =>{
                  if(charge.content.length > 0){
                    this.chargeList = charge.content;
                    this.statusPayment = charge.content[0].status ? charge.content[0].status : 'PENDING'
                  }   
        })

        this.getProducts()
        .subscribe(
          products => {
            this.productsList = products.content
            this.getServices()
              .subscribe(
                services => {
                  this.servicesList = services.content
                  this.purchase['products'] = this.utilsService.getCompleteListByIdsList(this.purchase.relationships['offer'].products, this.productsList)
                  this.purchase['services'] = this.utilsService.getCompleteListByIdsList(this.purchase.relationships['offer'].services, this.servicesList)
                  
                }
              )
          }
        )
      }
    })
  }

  paymentTypeEnumToString(value) : string {
    return PaymentType[value]
  }

  PaymentCreditCardEnumToString(value) : string {
    return PaymentCreditCard[value]
  }

  purchaseStatusEnumToString(value) : string {
    return PurchaseStatus[value]
  }

  purchaseOfferRecurrenceEnumToString(value) : string {
    return PlanRecurrence[value]
  }

  getProducts(): Observable<any> {
    return this.productsService.getProducts()
  }

  getServices(): Observable<any> {
    return this.servicesService.getServices()
  }
  

  downloadBooklet(url){
    window.open(url, "_blank");
  }

  approvePurchase() {

    this.purchaseService.approve(this.purchase.id,this.appContext.session.user.id)
    .subscribe(result => {
      this.snackBar.open('Compra aprovada com sucesso', '', {
        duration: 8000
      })
      this.router.navigate(['/purchases'])
    })
  }

  rejectPurchase() {

    this.purchaseService.reject(this.purchase.id,this.appContext.session.user.id)
    .subscribe(result => {
      this.snackBar.open('Compra rejeitada com sucesso', '', {
        duration: 8000
      })
      this.router.navigate(['/purchases'])
    })
  }

  confirmReceived() {
    
    let dataWrapper = { purchase: this.purchase }

    let amountCount = 0;
    let _height = '500px';

    if(dataWrapper.purchase && dataWrapper.purchase['devices']){

       amountCount = dataWrapper.purchase.devices.length

       if(amountCount == 3 || amountCount == 4){
        _height = '600px';
       }else if(amountCount == 5 || amountCount == 6){
        _height = '700px';
       }else if(amountCount > 5){
        _height = '900px';
       }

       this.dialog
      .open(PurchaseConfirmReceivedComponent, { data: dataWrapper, width: '1000px', height: _height })
      .afterClosed()
      .subscribe(response => {

        if (response && response.data == true) {

          this.canConfirmReceived = false
        }
      })
    }
    
  }

  createStatusList() {

    var statusList = []

    if (this.purchase && this.purchase.lifecycle && this.purchase.lifecycle.createDate) {
      statusList.push({ status: PurchaseStatus.OPEN , date: this.purchase.lifecycle.createDate })
    }

    if (this.purchase && this.purchase.lifecycle && this.purchase.lifecycle.cancelDate) {
      statusList.push({ status: PurchaseStatus.CANCELED , date: this.purchase.lifecycle.cancelDate })
    }

    if (this.purchase && this.purchase.lifecycle && this.purchase.lifecycle.approveDate) {
      statusList.push({ status: PurchaseStatus.APPROVED , date: this.purchase.lifecycle.approveDate })
    }

    if (this.purchase && this.purchase.lifecycle && this.purchase.lifecycle.paidDate) {
      statusList.push({ status: PurchaseStatus.PAID, date: this.purchase.lifecycle.paidDate })
    }
    
    if (this.purchase && this.purchase.lifecycle && this.purchase.lifecycle.rejectDate) {
      statusList.push({ status: PurchaseStatus.REJECTED, date: this.purchase.lifecycle.rejectDate })
    }

    if (this.purchase && this.purchase.lifecycle &&  this.purchase.lifecycle.shippedDate) {
      statusList.push({ status: PurchaseStatus.SHIPPED, date: this.purchase.lifecycle.shippedDate })
    }

    if (this.purchase && this.purchase.lifecycle && this.purchase.lifecycle.inpreparationDate) {
      statusList.push({ status: PurchaseStatus.IN_PREPARATION, date: this.purchase.lifecycle.inpreparationDate })
    }
    
    this.statusList = statusList
  }

  maskPostalCode(number) {
    number+="";
    if(number.length == 8){
        number = number.replace(/\D/g, '');
        number = number.match(/^(\d{5})(\d{3})$/);
        number = (!number) ? null : number[1]+ '-' + number[2];
    }else{
        number = number.replace(/\D/g, '');
        number = number.match(/^(\d{5})(\d{2})$/);
        number = (!number) ? null : number[1]+ '-' + number[2] +'0';
    }
   
    return number;
}   
}
