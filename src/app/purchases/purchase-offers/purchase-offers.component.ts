import { Component } from '@angular/core';
import { Router} from '@angular/router';
import { Offer } from '../../core/models/offer.model';
import { CartService } from '../cart/cart.service';
import { CartItem } from '../../core/models/cart-item.model';
import { OffersService } from '../../core/services/offers.service';
import { AppContext } from '../../core/appcontext';
import { fuseAnimations } from '@fuse/animations';
import { UtilsService } from 'app/core/utils.service';
import { ProductsService } from 'app/core/services/products.service';
import { ServicesService } from 'app/core/services/services.service';
import { Observable } from 'rxjs/Observable';
import { MatSnackBar } from '@angular/material';

@Component({
    selector   : 'tnm-purchase-offers',
    templateUrl: './purchase-offers.component.html',
    styleUrls  : ['./purchase-offers.component.scss'],
    animations   : fuseAnimations
})
export class PurchaseOffersComponent
{

    offersList: Array<Offer> = []
    offersListSimCard: Array<Offer> = []
    productsList: any
    servicesList: any
    commoditiesList: any
    secondColorBrand : string = localStorage.getItem('secondColorBrand');
    primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
    commodities: Array<any> = []
    

    constructor(
        private router: Router, 
        private snackBar : MatSnackBar,
        private cartService: CartService,
        private offersService: OffersService,
        private utilsService: UtilsService,
        private productsService: ProductsService,
        private servicesService: ServicesService,
        private appContext: AppContext
    )
    {

    }

    ngOnInit() {
        this.cartService.cleanCarItem()
        this.getProducts()
        .subscribe(
          products => {
            this.productsList = products.content
            this.getServices()
              .subscribe(
                services => {
                  this.servicesList = services.content 
                }
              )
            this.getOffers('LEASE')  
          }
        )
        this.getCommodities()
    }

    getCommodities(){

        this.commoditiesList = []

        this.productsService.getCommodities()
          .subscribe(commodities => {
            commodities.content.forEach(comoditie => {

                if(comoditie.service){
                   this.commodities.push(comoditie.relationships.service)
                }else{
                   this.commodities.push(comoditie.relationships.product)
                }
                
                if(comoditie.status == "ACTIVE"){
                    let id = comoditie.product ? comoditie.product : comoditie.service
                    this.commoditiesList.push(id)
                }  
            });
                
          })
    
      }

    getProducts(): Observable<any> {
        return this.productsService.getProducts()
    }

    getServices(): Observable<any> {
        return this.servicesService.getServices()
    }
 
    getOffers(agreement) {
        
      
        this.offersService.getOffersByBrand(this.appContext.brandTracknme,agreement)
        .subscribe(offers => {
            if (offers.content != null) {
                
                offers.content.forEach(offer => {

                    if(offer.segment == 'BUSINESS' && (offer.availableForAllBrands || offer.availableForBrands.indexOf(this.appContext.loggedUser.brand) >= 0)){

                        if(offer.products.indexOf(5661458385862656) >= 0){
                            offer.product = this.utilsService.getCompleteListByIdsList(offer.products, this.productsList)[0]
                            this.offersListSimCard.push(offer)
                        } else{
                            offer.product = this.utilsService.getCompleteListByIdsList(offer.products, this.productsList)[0]
                            this.offersList.push(offer)
                        }

                        
                    }
                });
            }
            this.offersListSimCard.sort(function(a, b){
                return a.saleMaxInstallments-b.saleMaxInstallments
            });
            this.offersList.sort(function(a, b){
                return a.saleMaxInstallments-b.saleMaxInstallments
            });

        })
    }

    checkOffers(offer){

        let list = [];
        let status = true;

        if(offer.products){
            offer.products.forEach(id => {
                list.push(id)
            });
        }

        if(offer.services){
            offer.services.forEach(id => {
                list.push(id)
            });
        }

        
        if(list.length > this.commoditiesList.length){
            return false
        }

        list.forEach(id => {
           
            let commodities  = this.utilsService.getCompleteListByIdsList([id], this.commodities)

            if(commodities.length > 0 && !this.commoditiesList.includes(id)){
                status = false;
            }
        });

        return  status;
    }

    onFilterTypePlan(agreement: string) {
        this.offersList = []
        this.offersListSimCard = []
        this.getOffers(agreement)
    }

    buyOffer(offer: Offer) {
      
        if(!this.checkOffers(offer)){

            let products = this.utilsService.getCompleteListByIdsList(offer.products, this.commodities)
            let services = this.utilsService.getCompleteListByIdsList(offer.services, this.commodities)
           
            let itens = '';

            products.forEach(item => {
                itens += " - Produto: "+item['name'] 
            });

            services.forEach(item => {
                itens += " - Serviço: "+item['name'] 
            });

            this.snackBar.open('É necessario a ativação desses itens para comprar essa oferta'+itens,'', {
                duration: 6000
            })

            return false
        }
            
        // add new item
        var cartItem = new CartItem(offer, 20)
        this.cartService.addItem(cartItem)

        // navigate to cart
        this.router.navigate(['purchases/cart'])
    }

    trunck(accession,installments){
        return Math.trunc(accession/installments)
    }

}
