import { Component, OnInit, ViewChild, Injector, AfterViewInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatSnackBar } from '@angular/material';
import { PaymentsComponent } from '../../payments/payments.component';
import { CustomerAddressComponent } from '../../customers/customer-address/customer-address.component';
import { BaseComponent } from '../../_base/base-component.component';
import { PurchasesService } from '../../core/services/purchases.service';
import { Router } from '@angular/router';
import { CartService } from '../cart/cart.service';
import { Product } from '../../core/models/product.model';
import { CartItem } from '../../core/models/cart-item.model';
import { CONSTANTS } from '../../core/common-data-domain';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';
import { Customer } from 'app/core/models/customer.model';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  animations: fuseAnimations
})
export class CheckoutComponent extends BaseComponent implements OnInit, AfterViewInit{

  @ViewChild(CustomerAddressComponent)
  private customerAddressComponent: CustomerAddressComponent

  @ViewChild(PaymentsComponent)
  private paymentsComponent: PaymentsComponent

  cartItens : Array<CartItem>
  totalPrice : number = 0
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  


  constructor(
    private snackBar : MatSnackBar,
    private injector: Injector,
    private router: Router,
    private purchaseService: PurchasesService,
    private cartService: CartService) 
  { 
    super(injector)
  }

  ngOnInit() {

  }

  ngAfterViewInit() {

    let customer = new Customer()
    customer.user = this.appContext.session.user.id;
    
    let business ={
      id : this.appContext.session.user.brand
    }
    this.paymentsComponent.setBusiness(business)
    
    this.cartService.cartItens.subscribe(cartItens =>{
      this.cartItens = cartItens
    })

    if(this.cartItens.length > 0){
      this.paymentsComponent.setPlanInstallment(this.cartItens[0].offer['saleMaxInstallments'])
      this.paymentsComponent.setPaymentsType(this.cartItens[0].offer['availablePaymentMethods'])
    }

    this.cartService.totalPrice.subscribe(totalPrice => {
      if(totalPrice > 0){
        this.totalPrice = totalPrice
      }
       
    })

  }

  onPaymentTypeChanged(type: string) {

    let purchase = {
      brand: this.appContext.session.user.brand,
      broker: this.appContext.session.user.id,
      supplier: CONSTANTS.TRACKNME_SUPPLIER,
      user: this.appContext.session.user.id,
      payment: this.paymentsComponent.getData().id,
      installments:this.paymentsComponent.getInstallments(),
      dueDay: this.paymentsComponent.getDueDay(),
      itens: [],
      offer:this.cartItens[0].offer['id'],
      amount:this.cartItens[0].quantity
    }


    this.purchaseService.createNew(purchase)
    .subscribe(result => {
      
      sessionStorage.removeItem('summaryCheckout')
      sessionStorage.setItem('summaryCheckout', JSON.stringify({'installments':this.paymentsComponent.getInstallments(),'typePayment':this.paymentsComponent.getTypePayment(),'totalPrice':this.totalPrice ,'purchaseId':result.id,'paymentId':this.paymentsComponent.getData(),'dueDay': this.paymentsComponent.getDueDay()}))

      this.router.navigate(['/purchases/summary'])
    })
  }

}
