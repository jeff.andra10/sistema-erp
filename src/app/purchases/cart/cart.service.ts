import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs";
import { Purchase } from "../../core/models/purchase.model";
import { Offer } from "../../core/models/offer.model";
import { Product } from "../../core/models/product.model";
import { CartItem } from "../../core/models/cart-item.model";

@Injectable()
export class CartService {

    private cartItensSubject : BehaviorSubject<Array<CartItem>> = new BehaviorSubject(null)
    public cartItens = this.cartItensSubject.asObservable()

    private totalPriceSubject: BehaviorSubject<number> = new BehaviorSubject(0)
    public totalPrice = this.totalPriceSubject.asObservable()

    private _cartItens : CartItem[] = []

    private _updateTotalPrice() {

      if (!this._cartItens) return 0
  
      var total : number = 0
      this._cartItens.forEach(item=>{
        total += (item.offer.agreement == "LEASE" ? item.offer.leaseAccessionPrice : item.offer.salePrice) * item.quantity
      })
  
      this.totalPriceSubject.next(total)
    }

    addItem(cartItem: CartItem) {
      
      this._cartItens.push(cartItem)
      this.cartItensSubject.next(this._cartItens)
      this._updateTotalPrice()
    }

    cleanCarItem(){
      this._cartItens = []
    }

    removeItem(cartItem: CartItem) {
      
      var index = this._cartItens.indexOf(cartItem);
      if (index > -1) {
        this._cartItens.splice(index, 1)
        this.cartItensSubject.next(this._cartItens)
        this._updateTotalPrice()
      }
    }

    updateItemQuantity(cartItem: CartItem, quantity: number) {

      var index = this._cartItens.indexOf(cartItem);
      
      if (index > -1) {
        this._cartItens[index].quantity = quantity
        this.cartItensSubject.next(this._cartItens)
        this._updateTotalPrice()
      }
    }

    getCartItens() {
      return this._cartItens
    }
}