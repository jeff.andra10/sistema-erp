import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartItem } from '../../core/models/cart-item.model';
import { CartService } from './cart.service';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormControl, FormBuilder , Validators } from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  animations: fuseAnimations
})
export class CartComponent implements OnInit, AfterViewInit {

  cartItens : Array<CartItem> = []
  subtotalPrice: number = 0
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  rateControl : FormControl 
  
  constructor(
    private router: Router,
    private cartService: CartService
  ) {
   }

  ngOnInit() {

    this.cartService.cartItens.subscribe(cartItens =>{
      this.cartItens = cartItens
    })

    this.cartService.totalPrice.subscribe(totalPrice => {
      this.subtotalPrice = totalPrice
    })

    this.rateControl = new FormControl("", [ Validators.min(0)])

  }

  ngAfterViewInit() {
  
  }

  doCheckout() {
    this.router.navigate(['/purchases/checkout'])
  }

  buyMore() {

    this.router.navigate(['/purchases/offers'])
  }

  removeCartItem(item: CartItem) {
    this.cartService.removeItem(item)
  }

  itemQuantityOnChange(item: CartItem, value: string) {

    if (value == "") return;

    this.cartService.updateItemQuantity(item, Number(value))
  }
}
