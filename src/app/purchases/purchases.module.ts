import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../angular-material/material.module";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "@fuse/shared.module";

import { PurchasesComponent } from './purchases.component';
import { PurchaseOffersComponent } from "./purchase-offers/purchase-offers.component";
import { CartComponent } from './cart/cart.component';
import { SummaryComponent } from './summary/summary.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { CustomerAddressComponent } from "../customers/customer-address/customer-address.component";
import { PaymentsComponent } from "../payments/payments.component";
import { CustomersModule } from "../customers/customers.module";
import { PaymentsModule } from "../payments/payments.module";
import { CartService } from "./cart/cart.service";
import { NewCreditCardComponent } from "../payments/new-credit-card/new-credit-card.component";
import { PurchaseDetailsComponent } from './purchase-details/purchase-details.component';
import { PurchaseSelectedService } from "./purchase.selected.service";
import { PurchaseConfirmReceivedComponent } from './purchase-confirm-received/purchase-confirm-received.component';

import { PlanSidenavComponent } from './sidenavs/plan-side/plan-side.component';

const authRouting: ModuleWithProviders = RouterModule.forChild([
    // {path: '', component: PurchasesComponent },
    // {path: 'offers', component: PurchaseOffersComponent },
    // {path: 'cart', component: CartComponent },
    // {path: 'summary', component: SummaryComponent },
    // {path: 'checkout', component: CheckoutComponent },
    // {path: ':id', component: PurchaseDetailsComponent },
    // {path: 'confirm-received/:id', component: PurchaseConfirmReceivedComponent },

    {path: 'purchases', component: PurchasesComponent },
    {path: 'purchases/offers', component: PurchaseOffersComponent },
    {path: 'purchases/cart', component: CartComponent },
    {path: 'purchases/summary', component: SummaryComponent },
    {path: 'purchases/checkout', component: CheckoutComponent },
    {path: 'purchases/:id', component: PurchaseDetailsComponent },
    {path: 'purchases/confirm-received/:id', component: PurchaseConfirmReceivedComponent }
]);

@NgModule({
    declarations:[
        PurchasesComponent,
        PurchaseOffersComponent,
        CartComponent,
        SummaryComponent ,
        CheckoutComponent,
        PurchaseDetailsComponent,
        PurchaseConfirmReceivedComponent,
        PlanSidenavComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        TextMaskModule,
        FlexLayoutModule,
        RouterModule,
        authRouting,
        CustomersModule,
        PaymentsModule
    ],
    exports: [
        
    ],
    entryComponents: [
        CustomerAddressComponent,
        PaymentsComponent,
        NewCreditCardComponent
    ],
    providers: [
        CartService,
        PurchaseSelectedService
    ]
})
export class PurchasesModule {}