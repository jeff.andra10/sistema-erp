import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { MatPaginator, MatSort } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';

import { Offer } from '../core/models/offer.model';
import { PurchasesDataSource } from './purchases.datasource';
import { UtilsService } from '../core/utils.service';
import { PurchaseStatus, Purchase, PurchaseColorStatus, PurchaseColorStatusLight } from '../core/models/purchase.model';
import { Subject } from 'rxjs';
import { PurchasesService } from '../core/services/purchases.service';
import { PurchaseSelectedService } from './purchase.selected.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.scss'],
  animations: fuseAnimations
})
export class PurchasesComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: PurchasesDataSource;
  displayedColumns = ['statusColor','space','id', 'lifecycle.createDate','status', 'accession', 'total'];
  resultLength: number = 0
  purchasesStatusList: any

  searchSubject = new Subject()
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  

  constructor(
    private router: Router,
    private utilsService: UtilsService,
    private purchasesService: PurchasesService,
    private purchaseSelectedService: PurchaseSelectedService
  ) { 
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading','false');
    
    this.loadDomainListData()
    this.dataSource = new PurchasesDataSource(this.purchasesService, this.utilsService)
    this.dataSource.load('', '', 'lifecycle.createDate', 'desc', 0, 10)

    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadPurchases(val)
    })
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)
    
    this.dataSource.totalElements$.subscribe( value => {
      this.resultLength = value
    })

    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadPurchases())
    )
    .subscribe()
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }  

  loadDomainListData() {

    this.purchasesStatusList = this.utilsService.stringEnumToKeyValue(PurchaseStatus)
  }

  loadPurchases(terms: string= '') {

    this.dataSource.load(
      terms, 
      '',
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }

  purchaseStatusEnumToString(value) : string {
    return PurchaseStatus[value]
  }

  viewDetails(purchase: Purchase) {

    this.purchaseSelectedService.setSelected(purchase)
    this.router.navigate(['/purchases/', purchase.id])
  }

  purchaseColorStatusEnumToString(value) : string {
    return PurchaseColorStatus[value]
  }

  purchaseColorStatusLightEnumToString(value) : string {
    return PurchaseColorStatusLight[value]
  }

  
}
