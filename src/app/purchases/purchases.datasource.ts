import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";

import { UtilsService } from "app/core/utils.service";
import { PaginatedContent } from "app/core/models/paginatedContent.model";

import { PurchasesService } from "../core/services/purchases.service";
import { Purchase } from "../core/models/purchase.model";

export class PurchasesDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<Purchase[]>([]);
    private totalElementsSubject = new BehaviorSubject<number>(0)
    public totalElements$ = this.totalElementsSubject.asObservable()

    constructor(
        private purchasesService : PurchasesService, 
        private utilsService : UtilsService) 
    {}

    load(filter:string,
        status: string,
        sortField:string,
        sortDirection:string,
        pageIndex:number,
        pageSize:number) {

        this.purchasesService.findPurchases(
            filter, 
            status,
            sortField,
            sortDirection,
            pageIndex, 
            pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe((result : PaginatedContent<Purchase>) => {

                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<Purchase[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
