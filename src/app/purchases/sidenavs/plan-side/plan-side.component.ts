import { Component, Output, EventEmitter, } from '@angular/core';


@Component({
    selector   : 'plan-sidenav',
    templateUrl: './plan-side.component.html',
    styleUrls  : ['./plan-side.component.scss']
})

export class PlanSidenavComponent{

    @Output() onFilterTypePlan: EventEmitter<any> = new EventEmitter();

    filterBy: string;

    ngOnInit() {
        this.filterBy = 'LEASE' 
    }
    
    changeFilter(filter)
    {
        this.filterBy = filter;
        this.onFilterTypePlan.emit(filter)
    }

}
