import { Component, OnInit, ViewChild, Injector, AfterViewInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatSnackBar, MatDialog } from '@angular/material';
import { PaymentsComponent } from '../../payments/payments.component';
import { CustomerAddressComponent } from '../../customers/customer-address/customer-address.component';
import { BaseComponent } from '../../_base/base-component.component';
import { PurchasesService } from '../../core/services/purchases.service';
import { Router } from '@angular/router';
import { CartService } from '../cart/cart.service';
import { CartItem } from '../../core/models/cart-item.model';
import { CONSTANTS } from '../../core/common-data-domain';
import { CustomersService } from '../../core/services/customers.service';
import { ProductsService } from 'app/core/services/products.service';
import { ServicesService } from 'app/core/services/services.service';
import { PlansService } from 'app/core/services/plans.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';
import { Payment,PaymentType} from '../../core/models/payment.model';
import { PaymentsService } from '../../core/services/payments.service';
import { BrandsService } from '../../core/services/brands.service';
import { SalesFreightComponent } from "../../sales/sales-freight/sales-freight.component";
import { Customer } from '../../core/models/customer.model';


@Component({
  selector: 'summary-purchase',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent extends BaseComponent implements OnInit, AfterViewInit{

    @ViewChild(PaymentsComponent)
    private paymentsComponent: PaymentsComponent
  
    cartItens : Array<CartItem>
    totalPrice : number = 0
    sale : any
    summary : any
    personType : String = "CPF"
    productsList: any
    servicesList: any
    secondColorBrand : string = localStorage.getItem('secondColorBrand');
    primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
    
    
    payment : Payment
    freight : any
  
    constructor(
      private snackBar : MatSnackBar,
      private injector: Injector,
      private router: Router,
      public dialog: MatDialog,
      private purchaseService: PurchasesService,
      private customersService: CustomersService,
      private productsService: ProductsService,
      private servicesService: ServicesService,
      private plansService: PlansService,
      private cartService: CartService,
      private paymentsService: PaymentsService,
      private brandsService: BrandsService
    ) 
    { 
      super(injector)
    }

    ngOnInit() {

        this.summary = {
            customer: {},
            address:{},
            plan:[],
            payment:{}
        }
    
    }

    ngAfterViewInit() {

        let purchase = {
            brand: this.appContext.session.user.brand,
            broker: this.appContext.session.user.id,
            supplier: CONSTANTS.TRACKNME_SUPPLIER,
            user: this.appContext.session.user.id,
            itens:[]
        }

        setTimeout(() => {
        
                this.summary.payment = JSON.parse(sessionStorage.getItem('summaryCheckout'))

                if(this.summary.payment && this.summary.payment.paymentId)
                this.getPayment(this.summary.payment.paymentId.id)

                if(localStorage.getItem('isUserTracknme') && localStorage.getItem('isUserTracknme') == 'true'){

                    let indexBrandUser = this.appContext.brands.map((item) => { return item.id;}).indexOf(this.appContext.loggedUser.brand)
                    let name = this.appContext.brands[indexBrandUser].name
                    
                    let customer ={
                        name : name,
                        email : '-'
                    }
                    
                    this.summary.customer = customer

                }else{

                    this.customersService.getCustomerByUser(this.appContext.session.user.id).subscribe(customer =>{ 
                        if(customer.content && customer.content.length > 0){
                            this.summary.customer = customer.content[0]
                        }
                    })

                }

                this.cartService.cartItens.subscribe(cartItens =>{
                    if(cartItens){
                       
                       cartItens.forEach(item => {

                        let id = item.offer.id
                        this.plansService.getPlansById(id)
                            .subscribe( plan => {
                                
                                let _plan = plan.content[0]

                                this.getProducts()
                                        .subscribe(
                                        products => {
                                            this.productsList = products.content
                                            if(_plan && _plan.products && _plan.products.length > 0){
                                                _plan.products = this.utilsService.getCompleteListByIdsList(_plan.products, this.productsList)
                                            }
                                        }
                                    )

                                this.getServices()
                                        .subscribe(
                                            services => {
                                            this.servicesList = services.content
                                            if(_plan && _plan.services && _plan.services.length > 0){
                                                _plan.services = this.utilsService.getCompleteListByIdsList(_plan.services, this.servicesList)
                                            }
                                        }
                                )

                                if(_plan){
                                    this.summary.plan = [];
                                    this.summary.plan.push(_plan)
                                }

                        })
                       
                           
                       });
                        
                    }
                })

                this.brandsService.getBrandAddress(this.appContext.session.user.brand).subscribe(address =>{ 
                    if(address.content && address.content.length > 0){
                        this.summary.address = address.content[0]
                    }
                })
            
         });
    
        this.cartService.totalPrice.subscribe(totalPrice => {
          this.totalPrice = totalPrice
        })  
    }

    approvePurchase() {

        this.purchaseService.approve(this.summary.payment.purchaseId,this.appContext.session.user.id)
        .subscribe(result => {
          this.snackBar.open('Compra aprovada com sucesso', '', {
            duration: 5000
          })
          // clean card
          this.cartService.cleanCarItem()
          sessionStorage.setItem('summaryCheckout',null)
          this.router.navigate(['/purchases'])
        }, error => {
            this.snackBar.open(error.error ? error.error.message : error.message, '', { duration: 5000 });
       })
    }

    rejectPurchase() {

        this.snackBar.open('Compra salva com sucesso', '', {
            duration: 8000
          })

        this.router.navigate(['/purchases'])
    }

    maskPhoneNumber(number) {
        number+="";
        number = number.replace(/\D/g, '');
        number = number.match(/^(\d{2})(\d{5})(\d{4})$/);
        number = (!number) ? null : '(' + number[1] + ') '+ number[2] + '-' + number[3];

        return number;
    }

    maskRegistrationNumber(number) {
        number+="";
        if(number.length == 11){
            number = number.replace(/\D/g, '');
            number = number.match(/^(\d{3})(\d{3})(\d{3})(\d{2})$/);
            number = (!number) ? null : number[1] + '.' + number[2] + '.' +number[3] + '-' + number[4];
        }else{
            number = number.replace(/\D/g, '');
            number = number.match(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/);
            number = (!number) ? null : number[1] + '.' + number[2] + '.' +number[3] + '/' + number[4]+ '-' + number[5];
        }

        return number;
    }   
    
    maskPostalCode(number) {
        number+="";
        if(number.length == 8){
            number = number.replace(/\D/g, '');
            number = number.match(/^(\d{5})(\d{3})$/);
            number = (!number) ? null : number[1]+ '-' + number[2];
        }else{
            number = number.replace(/\D/g, '');
            number = number.match(/^(\d{5})(\d{2})$/);
            number = (!number) ? null : number[1]+ '-' + number[2] +'0';
        }
       
        return number;
    }   

    getProducts(): any{
        return this.productsService.getProducts()
    }
    
    getServices(): any {
        return this.servicesService.getServices()
    }

    ngOnDestroy()
    {
        sessionStorage.setItem('summaryCheckout',null)
        this.summary = {}
    }

    getPayment(paymentId){
        this.paymentsService.getPaymentById(paymentId)
          .subscribe(payment => {
            this.payment = payment
            });
    }

    paymentTypeEnumToString(value) : string {
        return PaymentType[value]
    }

    calculateFreight(){
        this.dialog
        .open(SalesFreightComponent, {width: '500px', height: '550px'})
        .afterClosed()
        .subscribe(response => {
          if (response && response.freight) {
            this.freight = response.freight
          }
        });
      }
}
