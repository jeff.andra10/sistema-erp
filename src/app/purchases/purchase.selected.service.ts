import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs";
import { Purchase } from "../core/models/purchase.model";

@Injectable()
export class PurchaseSelectedService {

    private purchaseSubject : BehaviorSubject<Purchase> = new BehaviorSubject(null)
    public purchaseSelected = this.purchaseSubject.asObservable();
  
    setSelected(purchase: Purchase) {
      this.purchaseSubject.next(purchase)
    }
}