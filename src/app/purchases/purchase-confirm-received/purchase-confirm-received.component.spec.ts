import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseConfirmReceivedComponent } from './purchase-confirm-received.component';

describe('PurchaseConfirmReceivedComponent', () => {
  let component: PurchaseConfirmReceivedComponent;
  let fixture: ComponentFixture<PurchaseConfirmReceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseConfirmReceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseConfirmReceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
