import { Component, OnInit, Injector, AfterViewInit, ViewEncapsulation, Inject } from '@angular/core';
import { BaseComponent } from '../../_base/base-component.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { PurchaseSelectedService } from '../purchase.selected.service';
import { PurchasesService } from '../../core/services/purchases.service';
import { Purchase, DonePurchase } from '../../core/models/purchase.model';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { DevicesService } from '../../core/services/devices.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'app-purchase-confirm-received',
  templateUrl: './purchase-confirm-received.component.html',
  styleUrls: ['./purchase-confirm-received.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PurchaseConfirmReceivedComponent extends BaseComponent implements OnInit {

  purchase: Purchase
  purchaseItens : Array<PurchaseItemReceived> = []
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(private injector: Injector,
            public dialogRef: MatDialogRef<PurchaseConfirmReceivedComponent>,
            @Inject(MAT_DIALOG_DATA) private data: any,
            private snackbar: MatSnackBar,
            private purchasesService: PurchasesService,
            private devicesService: DevicesService
            ) {
    super(injector)

      this.purchase = data.purchase

   }

  ngOnInit() {
      
    this.loadDevices()
  }

  loadDevices() {
    
    if (this.purchase.devices != null){
      
      this.devicesService.getDevicesByIds(this.purchase.devices)
      .subscribe(result=>{

        if (result.content){
          result.content.forEach(device => {
              this.purchaseItens.push(new PurchaseItemReceived(device.id, device.model, device.imei, false))
          })
        }
      })
    }
  }

  confirmReceived() {

    var doneData = new DonePurchase()
    doneData.loggedUser = this.appContext.session.user.id
    
    doneData.devices = []
    this.purchaseItens.forEach(item=>{
      if (item.received) {
        doneData.devices.push(item.id)
      }
    })

    this.purchasesService.done(this.purchase.id, doneData)
    .subscribe(result => { 

      this.snackbar.open('Confirmação de recebimento concluída com sucesso', '', {
        duration: 8000
      })
      this.dialogRef.close({data : true})
    })
  }

  onItemCheck(item: PurchaseItemReceived, event) {
    
    item.received = event.target.checked
  }
}

export class PurchaseItemReceived {

  id: number
  model: string
  imei: string
  received: boolean

  constructor(id: number, model: string, imei: string, received: boolean) {
    this.id = id
    this.model = model
    this.imei = imei
    this.received = received
  }
}