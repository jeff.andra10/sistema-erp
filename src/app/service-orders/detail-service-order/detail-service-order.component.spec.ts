import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailServiceOrderComponent } from './detail-service-order.component';

describe('NewCustomerComponent', () => {
  let component: DetailServiceOrderComponent;
  let fixture: ComponentFixture<DetailServiceOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailServiceOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailServiceOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
