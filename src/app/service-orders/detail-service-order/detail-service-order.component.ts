import { google } from '@google/maps';
import { Component, OnInit, Injector, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatSnackBar, MatTabGroup } from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import { Observable, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { fuseAnimations } from '@fuse/animations';
import { ServiceOrder, ServiceOrderStatus, ServiceOrderType, Installation, Uninstallation, Maintenance, AssignServiceOrder, RejectServiceOrder, Appointment } from '../../core/models/service-orders.model';
import { StateAddress } from '../../core/common-data-domain';
import { ConfirmDialogComponent } from '../../dialogs/confirm-dialog/confirm-dialog.component'
import { InputDialogComponent } from '../../dialogs/input-dialog/input-dialog.component'
import { SchedulingDialogComponent } from '../../dialogs/scheduling-brand-dialog/scheduling-brand-dialog.component'
import { DocumentsService } from '../../core/services/documents.service';
import { BaseComponent } from '../../_base/base-component.component';
import { Customer } from '../../core/models/customer.model';
import { MaskUtils } from '../../core/mask-utils';
import { CustomersService } from '../../core/services/customers.service';
import { UserService } from '../../core/services/users.service';
import { ServiceOrdersService } from '../../core/services/service-orders.service';
import { PlansService } from '../../core/services/plans.service';
import { ServiceOrdersSelectedService } from '../service-orders.selected.service';
import { Address } from '../../core/models/address.model';
import { Plan } from '../../core/models/plan.model';
import { ServiceOrderCheckList } from '../../core/models/service-orders.model'
import { MaintenanceComponent } from "../service-order-maintenance/service-order-maintence.component";
import { DevicesSearchComponent } from "../devices-search/devices-search.component";
import { Device } from '../../core/models/device.model';
import { DevicesService } from '../../core/services/devices.service';
import { InstallersService } from '../../core/services/installers.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { GoogleMapsService } from '../../core/services/googlemaps.service'
import { CustomerAddressComponent } from '../../customers/customer-address/customer-address.component';
import { BlockEventsComponent } from '../calendar-block-events/block-events.component';
import { CarService } from '../../core/services/car.service'
import { MatTabChangeEvent } from '@angular/material';
import { ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";



declare var google: any


@Component({
  selector: 'tnm-detail-service-order',
  templateUrl: './detail-service-order.component.html',
  styleUrls: ['./detail-service-order.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class DetailServiceOrderComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild("search")
  public searchElementRef: ElementRef;

  @ViewChild(CustomerAddressComponent)
  public customerAddressComponent: CustomerAddressComponent

  @ViewChild(BlockEventsComponent)
  public blockEventsComponent: BlockEventsComponent

  renderOptions = {
    suppressMarkers: true,
  }

  locationsList: Array<any>
  brandList: Array<any>
  generalForm: FormGroup
  deviceForm: FormGroup
  scheduleForm: FormGroup
  reviewForm: FormGroup
  historicForm: FormGroup
  commentsForm: FormGroup
  complementForm: FormGroup
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  serviceOrder: ServiceOrder
  customer: Customer
  address: Address
  plan: Plan
  installerUser: any
  scheduledDate: Date
  serviceOrderId: any
  serviceOrderDocuments?: any
  serviceOrderNewInstalDocuments?: any
  serviceOrderComments?: Array<any> = []
  serviceOrderCheckList?: Array<any> = []
  deviceId: number
  checked: boolean = true
  actionSelected: string = 'DEVICE_EXCHANGE'
  serviceOrderTypeList: any
  stateAddressList: any
  customerPlansList: any
  scheduleTimeList: Array<any>
  scheduledTimeHours = 0
  selectedTime: any
  isEditOS: boolean = false
  isTracknme: boolean = false
  deviceSelected: Device
  travelMode: string = 'DRIVING'
  random: string = ''
  appointment: Appointment
  isEditAddress: boolean = false
  selectedTab: number = 0
  installerList = [];
  installerListId = []
  installerGroup: Map<any, any>;

  groupInstallers = [];

  pendencyType : string
  pendencyDescription : string
  phoneMask = MaskUtils.phoneMask
  mobilePhoneMask = MaskUtils.mobilePhoneMask
  postalCodeMask = MaskUtils.postalCodeMask

  searchField: FormControl
  customersList: Array<any>
  customerName: string = ''
  isDisableCreateOrder: boolean = false
  fileToUpload: File = null

  loadAddresses = []

  userSearchField: FormControl
  usersList: Array<any>
  userName: string = ''
  defaultUrl: string = 'assets/images/ecommerce/product-image-placeholder.png'

  groupSize = 0
  waypoints: any
  schedulingConfirmation : boolean = false

  public searchControl: FormControl;
  zoom: number = 10;

  icon = {
    url: 'assets/images/ico-200.png',
    scaledSize: {
      width: 48,
      height: 48
    }
  }

  //Maps, 
  lat: Number = -23.624676
  lng: Number = -46.702455

  origin = { lat: -27.140850, lng: -48.593339 }
  destination = { lat: -27.122823, lng: -48.608788 }

  origin2 = { lat: -27.121108, lng: -48.622900 }
  destination2 = { lat: -27.135420, lng: -48.617514 }

  markerOptions: any


  public customDateMask = {
    guide: true,
    showMask: true,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
  };

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private devicesService: DevicesService,
    private customersService: CustomersService,
    private usersService: UserService,
    private serviceOrdersService: ServiceOrdersService,
    private documentsService: DocumentsService,
    private plansService: PlansService,
    private serviceOrderSelectedService: ServiceOrdersSelectedService,
    private loadingService: FuseSplashScreenService,
    private googleMapsService: GoogleMapsService,
    private carService: CarService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private installersService: InstallersService
  ) {
    super(injector)
    if(this.appContext.session.user.brand == this.appContext.brandTracknme){
      this.isTracknme = true
    }else{
      this.isTracknme = false
    }
  }

  ngOnInit() {

    this.renderOptions = {
      suppressMarkers: true,
    };

    this.actionSelected = 'DEVICE_EXCHANGE'

    this.deviceSelected = null

   // this.setCurrentPosition()

    this.serviceOrderCheckList = [{ 'part': "Vidro Elétrico", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Trava Elétrica", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Retrovisor Elétrico", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Retrovisor Manual", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Ar-frio / Ar-quente", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Ar-condicionado", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Buzina", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Lanterna", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Farol baixo", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Farol alto", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Faróis auxiliares", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Setas", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Pisca-alerta", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Luz-de-freio", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Bancos", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Funcion. câmbio", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Luz-de-ré", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Luzes de cortesia", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Limpador de para-brisas", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Desembaçador de espelhos", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Relógio", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Painel de instrumentos", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Luzes do painel de instrumentos", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Som", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Alarme original", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Freio-de-mão", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Tampa do porta-luvas", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Acendedor de cigarro", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Antena manual / elétrica", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Volante escamoteável", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Freio", 'beforeInstallation': "", 'afterInstallation': "" },
    { 'part': "Forração teto / quebra-sol", 'beforeInstallation': "", 'afterInstallation': "" }]

    this.searchField = this.formBuilder.control('', [Validators.required])
    this.userSearchField = this.formBuilder.control('', [Validators.required])

    this.serviceOrder = new ServiceOrder()
    this.loadDomainListData()
    this.initForms()

    this.searchField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap(searchTerm =>
        this.customersService.getCustomersBySearchTerms(searchTerm)
          .catch(error => Observable.from([])))
      .subscribe(customers => {
        if (!this.serviceOrder || this.serviceOrder.id == undefined) {
          if (this.customerName != this.generalForm.value.user) {
            this.customersList = customers.content
          }
        }
      })

    this.userSearchField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap(searchTerm =>
        this.usersService.getUsersByProfileAndName("INSTALLER", searchTerm)
          .catch(error => Observable.from([])))
      .subscribe(users => {

        if (this.userName != this.scheduleForm.value.installer) {
          this.usersList = users.content
        }

      })

    this.serviceOrderId = this.activatedRoute.snapshot.params['id']


    this.isDisableCreateOrder = false

    this.serviceOrderSelectedService.serviceOrderSelected.subscribe(serviceOr => {
     
      let date = new Date();
     
      if (serviceOr != null) {
        this.serviceOrder = serviceOr

        if(this.isTracknme){
            this.blockEventsComponent.setServiceOrder(this.serviceOrder);
        }
       
        this.loadServiceOrder()
      }
    })

    this.serviceOrderSelectedService.serviceOrderReloadCalendarSelected.subscribe(date => {

      if(this.isTracknme){

        this.locationsList = []

        localStorage.setItem('hiddenLoading', 'true');
        if (date != null) {
          
          this.getAllInstallers(date)
        } else {
          let date = new Date();

          if (this.serviceOrder && this.serviceOrder.maintenance && this.serviceOrder.maintenance.scheduledDate) {
            date = new Date(this.serviceOrder.maintenance.scheduledDate);
          } else if (this.serviceOrder && this.serviceOrder.uninstallation && this.serviceOrder.uninstallation.scheduledDate) {
            date = new Date(this.serviceOrder.uninstallation.scheduledDate);
          } else if (this.serviceOrder && this.serviceOrder.installation && this.serviceOrder.installation.scheduledDate) {
             date = new Date(this.serviceOrder.installation.scheduledDate);
          }
          
          this.getAllInstallers(date)
        }

      }
      
    })
  }

  startSearchAddress(){

    try {
      this.searchAddress(this.searchElementRef.nativeElement)
    } catch (error) {
      setTimeout(() => {
        this.startSearchAddress()
      }, 1000)
    }
  }

  getCar(id) {

    this.carService.getCar(id)
      .subscribe(car => {
    
        if (car && car.description && !this.deviceForm.value.installationSpot) {
          this.deviceForm.patchValue({
            installationSpot: car.description
          })

        }

      })

  }

  ngAfterViewInit() {
    this.locationsList = []

    if(this.isTracknme){
      //create search FormControl
      this.searchControl = new FormControl();
    }

  }

  public checkGoogleLoad(date) {

    try {
      let anchor = new google.maps.Point(14, 36);
      this.getSchedulesIntasllersByDate(date)
    } catch (error) {

      setTimeout(() => {
        this.checkGoogleLoad(date)
      }, 1000)
    }
  }

  getSchedulesIntasllersByDate(date) {

    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()

    if (day < 10) {
       day = '0' + day
    }

    if (month < 10) {
      month = '0' + month
    }

    let _date = year + '-' + month + '-' + day + 'T' + '00:00:00'

    this.installersService.getSchedules(_date)
      .subscribe(result => {

        const grouped = result.content;
        var countGroupedAppointments = 0;

        if(grouped.length == 0){
          this.generateInstallerList();
        }

        grouped.forEach(group => {

          let index = null
          let color = null
          
              index = this.groupInstallers.map((item) => { return item.id; }).indexOf(group.installer)
                
              if(index > -1){
                color = this.groupInstallers[index].color
              }else{
            
                if(this.appContext.session.user.brand != this.appContext.brandTracknme){
                    let _technician = group.relationships.installer

                    if(_technician && _technician.id){
                      _technician.color = this.generatorColor()
                      this.groupInstallers.push(_technician)
                      color = _technician.color
                    } 
                }
              }

          for (var x = 0; x < group.appointments.length; x++) {

            let os = group.appointments[x].relationships.serviceOrder;
            let address = null;
            let scheduledDate = null;
            let appointmentId = group.appointments[x].id;
          
           
              if (os && os.maintenance) {
                address = os.maintenance.address
                scheduledDate = os.maintenance.scheduledDate;
              } else if (os && os.uninstallation) {
                address = os.uninstallation.address
                scheduledDate = os.uninstallation.scheduledDate;
              } else if (os && os.installation) {
                address = os.installation.address
                scheduledDate = os.installation.scheduledDate;
              }

              if (address && group && group.relationships && group.relationships.installer) {
                this.createPinLocation(address, os.technician, group.relationships.installer.name, scheduledDate, color, os.id, group.appointments.length,appointmentId)
              }
            
          }

          countGroupedAppointments++;

          if (countGroupedAppointments == grouped.length) {
             this.generateInstallerList();
          }
        })

      })

  }

  getSchedulesToday(google, date) {

    this.serviceOrdersService.getSchedulesTodayByBrand(date)
      .subscribe(result => {

        this.locationsList = []

        if (result.content && result.content.length == 0) {
          this.blockEventsComponent.setEmployeeDataSource(this.groupInstallers);
          this.blockEventsComponent.setCurrentView()
          this.loadingService.hide()
          localStorage.setItem('hiddenLoading', 'false');
        }

        const grouped = this.groupBy(result.content, os => os.technician);
        var countGroup = 0;

        grouped.forEach(group => {

          let index = null
          let color = null

            index = this.groupInstallers.map((item) => { return item.id; }).indexOf(group[0].technician)
              
            if(index > -1){
              color = this.groupInstallers[index].color
            }else{

              if(this.appContext.session.user.brand != this.appContext.brandTracknme){

                let _technician = group[0].relationships.technician

               if(_technician && _technician.id){
                  _technician.color = this.generatorColor()
                  this.groupInstallers.push(_technician)
                  color = _technician.color
               }

              }
               
            }
          

          for (var x = 0; x < group.length; x++) {

            let os = group[x];
            let address = null;
            let scheduledDate = null;

            if (color){
              
              if (os && os.maintenance) {
                address = os.maintenance.address
                scheduledDate = os.maintenance.scheduledDate;
              } else if (os && os.uninstallation) {
                address = os.uninstallation.address
                scheduledDate = os.uninstallation.scheduledDate;
              } else if (os && os.installation) {
                address = os.installation.address
                scheduledDate = os.installation.scheduledDate;
              }

              if (address) {
                this.createPinLocation(address, os.technician, os.relationships.technician.name, scheduledDate, color, os.id, group.length,null)
              }
            }
          }

          countGroup++;


          if (countGroup == grouped.size) {
            this.generateInstallerList();
          }

        });
      })
  }

  matTabSelected(event: MatTabChangeEvent) {

    if (event && event.index == 1) {
      this.blockEventsComponent.setCurrentView();
    }

  }

  getAllInstallers(date) {

    this.groupInstallers = []

    this.usersService.getTechnicianByBrand().subscribe(installers => {

      installers.content.forEach(installer => {

        if(this.isTracknme){
          if ( installer.brand === this.appContext.brandTracknme  && installer.status  === 'ACTIVE'){
            installer.color = this.generatorColor();
            this.groupInstallers.push(installer)
          }
        }else{
          if (installer.brand ===  this.serviceOrder.brand  && installer.status  === 'ACTIVE'){
            installer.color = this.generatorColor();
            this.groupInstallers.push(installer)
          }
        }
        
      });

      this.checkGoogleLoad(date);

    })

  }

  groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  }

  generateObjectIcon(color, position) {

    let iconPath,
      anchor;


    if (position) {
      iconPath = 'M14.2021439,0 C6.3574477,0 0,6.21010933 0,13.8692812 C0,27.1116708 14.2021439,44.3816997 14.2021439,44.3816997 C14.2021439,44.3816997 28.4042878,27.1116708 28.4042878,13.8692812 C28.4042878,6.21010933 22.045704,0 14.2021439,0 Z M14.2021439,25.2982747 C7.83173173,25.2982747 2.66928024,20.2559839 2.66928024,14.0357125 C2.66928024,7.81544116 7.83173173,2.77315035 14.2021439,2.77315035 C20.5708475,2.77315035 25.7350076,7.81544116 25.7350076,14.0357125 C25.7350076,20.2559839 20.5708475,25.2982747 14.2021439,25.2982747z',
        anchor = new google.maps.Point(14, 36);

    }

    if (!position) {
      iconPath = 'M14.2021439,0 C6.3574477,0 0,6.21010933 0,13.8692812 C0,27.1116708 14.2021439,44.3816997 14.2021439,44.3816997 C14.2021439,44.3816997 28.4042878,27.1116708 28.4042878,13.8692812 C28.4042878,6.21010933 22.045704,0 14.2021439,0 Z M14.2021439,25.2982747 C7.83173173,25.2982747 2.66928024,20.2559839 2.66928024,14.0357125 C2.66928024,7.81544116 7.83173173,2.77315035 14.2021439,2.77315035 C20.5708475,2.77315035 25.7350076,7.81544116 25.7350076,14.0357125 C25.7350076,20.2559839 20.5708475,25.2982747 14.2021439,25.2982747 Z',//'M37.4,3l-3,3c-2.5-2.1-5.8-3.4-9.3-3.4c-8,0-14.4,6.3-14.4,14.1c0,3.4,0.9,7.1,2.3,10.7l-4.4,4.4l1.8,1.8 L39.2,4.7L37.4,3z M13.4,16.8c0-6.3,5.2-11.4,11.7-11.4c2.8,0,5.3,1,7.3,2.5L16.2,24.2C14.5,22.2,13.4,19.6,13.4,16.8z M39.5,16.7 c0,13.4-14.4,30.9-14.4,30.9s-5.7-6.9-9.9-15.4l5-5c1.5,0.7,3.1,1,4.9,1c6.5,0,11.7-5.1,11.7-11.4c0-1.8-0.4-3.5-1.2-5l2.1-2.1 C38.8,11.8,39.5,14.2,39.5,16.7z',
        anchor = new google.maps.Point(25, 38);

    }

    let objectIcon = {
      path: iconPath,
      fillColor: color,
      fillOpacity: 1,
      scale: 1.0,
      strokeColor: 'white',
      strokeWeight: 0.5,
      anchor: anchor,
      labelOrigin: new google.maps.Point(14, 15),
      rotation: 0
    }


    return objectIcon;

  }

  generatorColor() {

    var hex = '0123456789ABCDEF';
    var color = '#';

    for (var i = 0; i < 6; i++) {
      color += hex[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  googleMapsCheck(address){

    try {
     let anchor = new google.maps.Point(14, 36);
     this.getLocation(address)
   } catch (error) {
     setTimeout(() => {
       this.googleMapsCheck(address)
     }, 1000)
   }

  }


  getLocation(address) {

    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({ 'address': address }, (results, status) => {
      
      this.lat =  results[0].geometry.location.lat();
      this.lng = results[0].geometry.location.lng();
  
      this.zoom = 15;

      if(results[0].geometry.location_type == "APPROXIMATE"){
        this.searchControl.setValue(address);
        this.snackbar.open('Localização Aproximada', '', {
          duration: 8000
      })
      }else{
        this.searchControl.setValue(results[0].formatted_address);
      }

    });

  }

  createPinLocation(address, technician, name, scheduledDate, color, serviceOrderId, lastPosition,appointmentId) {

    let icon = this.generateObjectIcon(color, true);
    
    let origin = address.street + ' ,' + address.number + ' ,' + address.district + ' ,' + address.city + ' - ' + address.state + ' ,' + address.postalCode

    let location = {
      origin: origin,
      technician: technician,
      name: name,
      markerOptions: null,
      drivingOptions: null,
      waypoints: [],
      renderOptions: null,
      scheduledDate: scheduledDate,
      color: color,
      serviceOrderId: serviceOrderId,
      address: address,
      appointmentId: appointmentId
    };

    location.renderOptions = {
      suppressMarkers: true,
      polylineOptions: { strokeColor: location.color }
    }

    location.markerOptions = {
      origin: {
        infoWindow: name + ' - ' + origin,
        icon: icon,
        label: {
          text: '1',
          color: color,
          fontSize: '24px',
          fontWeight: "bold"
        }

      },
      destination: {
        infoWindow: name + ' - ' + origin,
        icon: icon,
        label: {
          text: '' + lastPosition,
          color: color,
          fontSize: '24px',
          fontWeight: "bold"
        }

      },
      waypoints: []
    }

    location.drivingOptions = {
      departureTime: new Date(),
      arrivalTime: new Date(),
      modes: ['BUS'],
    }

    if (this.locationsList.length == 0) {
      this.locationsList.push(location);
    } else {
      let index = this.locationsList.map((location) => { return location.serviceOrderId; }).indexOf(location.serviceOrderId)
      if (index == -1 && technician) {
        this.locationsList.push(location);
      }
    }

  }


  generateInstallerList() {

    this.installerList = []
    this.installerListId = []

    const groupedIntaller = this.groupBy(this.locationsList, location => location.technician);
    this.installerGroup = groupedIntaller;

    this.blockEventsComponent.setCalendarEvents(this.locationsList);

    this.installerGroup.forEach(group => {
      group[0].markerOptions.waypoints = [];
      group[0].waypoints = [];

      var countItem = 0;

      group.sort(function (a, b) {
        return a['scheduledDate'] < b['scheduledDate'] ? -1 : a['scheduledDate'] > b['scheduledDate'] ? 1 : 0;
      });

      let positionIndex = 2;
      group.forEach(item => {
        if ((countItem != 0) && (countItem != group.length - 1)) {

          group[0].markerOptions.waypoints.push({
            infoWindow: `<h4>` + item.name + ' - ' +  item.origin + `<h4>`,
            icon: this.generateObjectIcon(item.color, true),
            label: {
              text: '' + positionIndex,
              color: item.color,
              fontSize: '24px',
              fontWeight: "bold"
            }
          })

          positionIndex++;

          group[0].waypoints.push({
            location: item.origin,
            stopover: false,
          })
        }
        countItem++;
      })

      let item = {
        [group[0].technician]: group
      }
      this.installerList.push(item)
      this.installerListId.push(group[0].technician)
    });

    this.blockEventsComponent.setMapPoints(this.installerListId, this.installerList);
    this.blockEventsComponent.setEmployeeDataSource(this.groupInstallers);
    this.blockEventsComponent.setCurrentView();


    setTimeout(() => {
      this.loadingService.hide()
      localStorage.setItem('hiddenLoading', 'false');
    }, 300)

  }

  initForms() {

    // general form
    this.generalForm = this.formBuilder.group({
      id: this.formBuilder.control({ value: '', disabled: true }),
      status: this.formBuilder.control({ value: '', disabled: true }),
      brand: this.formBuilder.control('', [Validators.required]),
      type: this.formBuilder.control('', [Validators.required]),
      description: this.formBuilder.control('', [Validators.required]),
      user: this.searchField,//this.formBuilder.control({value:'', disabled: false}),
      isInspection: this.formBuilder.control(false),
      plan: this.formBuilder.control('', [Validators.required]),
      planId: this.formBuilder.control(''),
      offer: this.formBuilder.control(''),
      licensePlate: this.formBuilder.control(''),
      deviceId: this.formBuilder.control(''),
      name: this.formBuilder.control(''),
      email: this.formBuilder.control(''),
      mobilePhone: this.formBuilder.control('')
    })

    // comments form
    this.commentsForm = this.formBuilder.group({

    })

    // complement form
    this.complementForm = this.formBuilder.group({
      complement: this.formBuilder.control(''),
      reference: this.formBuilder.control('')
    })

    // schedule form
    this.scheduleForm = this.formBuilder.group({
      installer: this.userSearchField,//this.formBuilder.control({value:'', disabled: true}),
      scheduledDate: this.formBuilder.control({ value: '', disabled: true }, [Validators.required]),
      time: this.formBuilder.control('', [Validators.required]),
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', [])
    })

    // review form
    this.reviewForm = this.formBuilder.group({

    })

    this.deviceForm = this.formBuilder.group({
      device: this.formBuilder.control({ value: '', disabled: true }),
      description: this.formBuilder.control({ value: '', disabled: false }),
      action: this.formBuilder.control({ value: '', disabled: false }),
      installationSpot: this.formBuilder.control({ value: '', disabled: false })
    })

    this.historicForm = this.formBuilder.group({
      openDate: this.formBuilder.control({ value: '', disabled: true }),
      assignDate: this.formBuilder.control({ value: '', disabled: true }),
      assignLoggedUser: this.formBuilder.control({ value: '', disabled: true }),
      inprogressDate: this.formBuilder.control({ value: '', disabled: true }),
      inprogressLoggedUser: this.formBuilder.control({ value: '', disabled: true }),
      doneDate: this.formBuilder.control({ value: '', disabled: true }),
      doneLoggedUser: this.formBuilder.control({ value: '', disabled: true }),
    })
  }

  searchAddress(arg) {
    
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

         
          if (place.address_components && place.address_components.length > 0) {

            let address = null;
            if (place.address_components.length == 6) {
              address = {
                street: place.address_components[0].long_name || null,
                number: '' || null,
                complement: this.complementForm.value.complement,
                district: place.address_components[1].long_name || null,
                city: place.address_components[2].long_name || null,
                state: place.address_components[3].short_name || null,
                postalCode: place.address_components[5].short_name || null,
                reference: this.complementForm.value.reference
              }
            } else {
              address = {
                street: place.address_components[1].long_name || null,
                number: place.address_components[0].long_name || null,
                complement: this.complementForm.value.complement,
                district: place.address_components[2].long_name || null,
                city: place.address_components[3].long_name || null,
                state: place.address_components[4].short_name || null,
                postalCode: place.address_components[6].long_name || null,
                reference: this.complementForm.value.reference
              }

            }


            if (address) {

              this.blockEventsComponent.setEventAddress(address);
              this.loadAddressFields(address)
              this.address = address
            }


          }


          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 18;

        });
      });
    });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 18;
      });
    }
  }


  loadAddressFields(address) {

    this.scheduleForm.patchValue({
      street: address.street,
      number: address.number,
      complement: address.complement,
      district: address.district,
      city: address.city,
      state: address.state,
      postalCode: address.postalCode,
      reference: address.reference
    })

  }

  handleFileInput(files: FileList, type: string, imageType: string) {

    this.fileToUpload = files.item(0);
    let reader = new FileReader();
    reader.readAsDataURL(files.item(0));
    reader.onload = (event) => {
      this.changeImage(type, imageType)
    }
  }

  changeImage(type, imageType) {

    let id = this.deviceId

    if (this.serviceOrder.serviceOrderType == ServiceOrderType.MAINTENANCE) {

      if (imageType == ServiceOrderType.INSTALLATION || imageType == ServiceOrderType.UNINSTALLATION) {
        id = this.serviceOrder.relationships.maintenance.plan.device;
      } else {
        if (this.serviceOrder.relationships.maintenance.plan.device == this.deviceSelected.id) {
          this.dialog
            .open(ConfirmDialogComponent, {
              panelClass: 'event-form-dialog', data: {
                width: '600px',
                height: '300px', title: 'Rastreador', message: 'Selecione o novo rastreador que será instalado.', ok: true
              }
            })
            .afterClosed()
          this.selectedTab = 2;
          return false
        } else {
          id = this.deviceSelected.id
        }
      }
    }

    if (this.fileToUpload) {
      const formData = new FormData();
      formData.append('file', this.fileToUpload);

      this.serviceOrdersService.setPictureDevice(formData, type, id).subscribe(
        picture => {
          this.fileToUpload = null
          if (this.serviceOrder.serviceOrderType == ServiceOrderType.MAINTENANCE && imageType == 'NEW') {
            this[type] = null;
            let id = this.serviceOrder.maintenance['replacement'] ? this.serviceOrder.maintenance['replacement'] : this.deviceId
            this.loadMaintenanceImageDevice(id)
          } else {
            this.loadImageDevice();
          }
        })
    }

  }

  selectCheckList(event, index, type) {
    this.serviceOrderCheckList[index][type] = event.value
  }

  searchDevices() {
    this.dialog
      .open(DevicesSearchComponent, { width: '500px', height: '550px' })
      .afterClosed()
      .subscribe(response => {
        if (response && response.device) {
          this.deviceSelected = response.device
          this.deviceId = response.device.id

          if (this.serviceOrder.serviceOrderType == ServiceOrderType.MAINTENANCE) {
            this.loadMaintenanceImageDevice(this.deviceId)
          } 
          // else {
          //   this.loadImageDevice();
          // }

          this.deviceForm.patchValue({
            device: this.deviceSelected ? this.deviceSelected.imei : ''
          })

        }
      });
  }

  loadDomainListData() {

    this.brandList = this.appContext.brands
    this.serviceOrderTypeList = this.utilsService.stringEnumToKeyValue(ServiceOrderType)
    this.stateAddressList = this.utilsService.stringEnumToKeyValue(StateAddress)


    this.scheduleTimeList = []
    this.scheduleTimeList.push('09:00')
    this.scheduleTimeList.push('10:00')
    this.scheduleTimeList.push('11:00')
    this.scheduleTimeList.push('12:00')
    this.scheduleTimeList.push('13:00')
    this.scheduleTimeList.push('14:00')
    this.scheduleTimeList.push('15:00')
    this.scheduleTimeList.push('16:00')
    this.scheduleTimeList.push('17:00')
    this.scheduleTimeList.push('18:00')
    this.scheduleTimeList.push('19:00')
    this.scheduleTimeList.push('20:00')
    this.scheduleTimeList.push('21:00')
    this.scheduleTimeList.push('22:00')
  }

  searchCustomers() {

    this.customersService.getCustomersBySearchTerms(this.generalForm.value.user)
      .catch(error => Observable.from([]))
      .subscribe(customers => {
        this.customersList = customers.content
      })

  }

  getCustomerByUserId(userId) {

    this.customersService.getCustomerByUser(userId)
      .catch(error => Observable.from([]))
      .subscribe(customer => {
        if (customer.content && customer.content.length > 0) {
          this.customer = customer.content[0]

          this.generalForm.controls.user.disable();
          this.generalForm.controls.name.disable();
          this.generalForm.controls.email.disable();
          this.generalForm.controls.mobilePhone.disable();

          this.generalForm.patchValue({
            user: this.customer.name,
            name: this.customer.name,
            email: this.customer.email,
            mobilePhone: this.customer.mobilePhone
          })

          this.customerName = this.customer.name
        }


      })

  }

  setCustomer(customer) {
    this.customer = customer
    this.customersList = [];
    this.customerName = this.customer.name
    this.generalForm.patchValue({
      user: this.customer.name,
    })

    this.loadCustomerPlans()
  }

  setInstaller(installer) {
    if (installer) {
      this.installerUser = installer
      this.userName = this.installerUser.name
      this.usersList = []
      this.scheduleForm.patchValue({
        installer: this.installerUser.name,
      })
    }
  }

  searchInstallers() {
    this.usersService.getUsersByProfileAndName("INSTALLER", this.scheduleForm.value.installer)
      .catch(error => Observable.from([]))
      .subscribe(users => {
        this.usersList = users.content
      })
  }

  loadPlanById() {

    if (this.plan && this.plan.id) {

      this.plansService.getPlanById(this.plan.id)
        .subscribe(plan => {

          this.plan = plan.content[0]

          if (!this.deviceForm.value.installationSpot) {
            this.getCar(this.plan.vehicle)
          }

          this.generalForm.patchValue({
            plan: this.plan,
            planId: this.plan.id,
            offer: this.plan.relationships ? this.plan.relationships.offer ? this.plan.relationships.offer.name : '-' : '-',
            licensePlate: this.plan.relationships ? this.plan.relationships.vehicle ? this.plan.relationships.vehicle.licensePlate : '-' : '-',
            deviceId: this.plan.relationships ? this.plan.relationships.device ? this.plan.relationships.device.id : '-' : '-',
          })
        })

    }


  }

  loadCustomerPlans() {

    this.customerPlansList = []

    if (!this.customer || !this.customer.user) return

    this.plansService.getPlansByUser(this.customer.user)
      .subscribe(plans => {

        if (this.generalForm.value.type == 'UNINSTALLATION' || this.generalForm.value.type == 'MAINTENANCE') {
          plans.content.forEach(plan => {

            if (plan && this.plan && (plan.id == this.plan.id)) {
              this.plan = plan;

              if (this.plan) {
                this.customerPlansList = [];
                this.customerPlansList.push(this.plan)

                this.generalForm.patchValue({
                  plan: this.plan
                })

              }
            }
            if (plan && plan.relationships && plan.relationships.device && plan.relationships.device.id) {
              this.customerPlansList.push(plan)
            }
          });
        } else {
          this.customerPlansList = plans.content
        }

      })
  }

  getDevice(id) {
    this.devicesService.getDevicesById(id)
      .subscribe(result => {
        if (result) {
          this.deviceSelected = result
        }
      })
  }

  createServiceOrder() {

    if (this.generalForm.invalid) {
      this.validateAllFields(this.generalForm)
      return false
    }

    if (this.customer == null) {
      this.snackbar.open('Informe um Cliente', '', {
        duration: 8000
      })
      return false
    }

    if (this.serviceOrder == null || this.serviceOrder.id == null) {
      this.serviceOrder.status = 'OPEN' //TODO: mudar para function/enum
    }
    this.serviceOrder.brand = this.generalForm.value.brand
    this.serviceOrder.type = this.generalForm.value.type
    this.serviceOrder.description = this.generalForm.value.description

    if (this.generalForm.value.type == 'INSTALLATION') {
      if (this.serviceOrder.installation == null) {
        let installation: Installation = new Installation();
        if (this.generalForm.value.isInspection)
          installation.inspection = true;
        this.serviceOrder.installation = installation
      }
      this.serviceOrder.installation.customer = this.customer.id
      this.serviceOrder.installation.user = this.customer.user
      this.serviceOrder.installation.plan = this.generalForm.value.plan.id
      this.serviceOrder.installation.vehicle = this.generalForm.value.plan ? this.generalForm.value.plan.vehicle : null

      if (this.deviceForm.value.installationSpot) {
        this.serviceOrder.installation.installationSpot = this.deviceForm.value.installationSpot
      }

    } else if (this.generalForm.value.type == 'UNINSTALLATION') {
      if (this.serviceOrder.uninstallation == null) {
        this.serviceOrder.uninstallation = new Uninstallation()
      }

      this.serviceOrder.uninstallation.customer = this.customer.id
      this.serviceOrder.uninstallation.user = this.customer.user
      this.serviceOrder.uninstallation.plan = this.generalForm.value.plan.id
      this.serviceOrder.uninstallation.device = this.generalForm.value.plan.relationships.device.id
      this.serviceOrder.uninstallation.vehicle = this.generalForm.value.plan ? this.generalForm.value.plan.vehicle : null
      this.deviceSelected = this.generalForm.value.plan.relationships.device

      if (this.deviceForm.value.installationSpot) {
        this.serviceOrder.uninstallation.installationSpot = this.deviceForm.value.installationSpot
      }

    } else if (this.generalForm.value.type == 'MAINTENANCE') {

      if (this.serviceOrder.maintenance == null) {
        this.serviceOrder.maintenance = new Maintenance()
      }

      this.serviceOrder.maintenance.customer = this.customer.id
      this.serviceOrder.maintenance.user = this.customer.user
      this.serviceOrder.maintenance.plan = this.generalForm.value.plan.id
      this.serviceOrder.maintenance.device = this.generalForm.value.plan.relationships.device.id
      this.serviceOrder.maintenance.vehicle = this.generalForm.value.plan ? this.generalForm.value.plan.vehicle : null
      this.deviceSelected = this.generalForm.value.plan.relationships.device

      if (this.deviceForm.value.installationSpot) {
        this.serviceOrder.maintenance.installationSpot = this.deviceForm.value.installationSpot
      }

    }

    this.isDisableCreateOrder = true

    this.serviceOrdersService.openServiceOrder(this.serviceOrder)
      .subscribe(serviceOrderCreated => {

        this.isDisableCreateOrder = false

        this.serviceOrder.id = serviceOrderCreated.id
        this.serviceOrder.status = serviceOrderCreated.status
        this.serviceOrder.serviceOrderStatus = <ServiceOrderStatus>ServiceOrderStatus[serviceOrderCreated.status]

        if(this.isTracknme){
          this.getAllInstallers(new Date);
          this.blockEventsComponent.setServiceOrder(this.serviceOrder);
          setTimeout(() => {
            this.startSearchAddress()
          }, 300);
        }
        
        this.snackbar.open('Ordem de Serviço criada com sucesso', '', {
          duration: 8000
        })
      })
  }

  assignServiceOrder() {

    if (this.scheduleForm.invalid) {
      this.validateAllFields(this.scheduleForm)
      return false
    }

    if (this.installerUser == null) {

      this.snackbar.open('Informe um Instalador', '', {
        duration: 8000
      })
      return false
    }

    var currentDate = new Date()

    if (currentDate > this.utcTimeZone(this.scheduledDate)) {

      this.snackbar.open('Informe uma data superior a data atual', '', {
        duration: 8000
      })
      return false
    }

    let momentDate = moment(this.scheduledDate);

    let assignData = new AssignServiceOrder()
    assignData.loggedUser = this.appContext.session.user.id
    assignData.technician = this.installerUser.id
    assignData.scheduledDate = momentDate.toISOString()
    assignData.street = this.scheduleForm.value.street
    assignData.number = this.scheduleForm.value.number
    assignData.complement = this.scheduleForm.value.complement
    assignData.district = this.scheduleForm.value.district
    assignData.city = this.scheduleForm.value.city
    assignData.state = this.scheduleForm.value.state
    assignData.postalCode = this.utilsService.stringToNumber(this.scheduleForm.value.postalCode),
    assignData.reference = this.scheduleForm.value.reference
    assignData.duringBusinessHours = false
    assignData.deslocationTime = '1800'
    assignData.serviceLenght = 'ONE_HOUR'
    assignData.period = "MORNING"
    assignData.beta = false

  
    this.serviceOrdersService.assignServiceOrder(this.serviceOrder.id, assignData)
      .subscribe(result => {

        this.snackbar.open('Ordem de Serviço agendada com sucesso', '', {
          duration: 8000
        })
        this.navigateToServiceOrders()
      },
        response => {
          this.snackbar.open(response.error.message, '', { duration: 8000 })
        })
  }

  scheduledDateChanged(event: MatDatepickerInputEvent<Date>) {

    this.scheduledDate = new Date(event.value)

    if (this.selectedTime) {
      this.scheduledTimeChanged(this.selectedTime)
    }
  }

  scheduledTimeChanged(time: String) {
    this.selectedTime = time
    let hours = Number(time.substring(0, 2))
    this.scheduledTimeHours = hours;
    this.scheduledDate.setUTCHours(hours);
  }

  navigateToServiceOrders() {
    this.router.navigate(['/service-orders'])
  }

  utcTimeZone(time: Date) {

    let date = moment(time).utcOffset(-3);
    date.set({ hour: this.scheduledTimeHours, minute: 0, second: 0, millisecond: 0 })
    let ndate = date.toDate()

    return ndate
  }

  rescheduleServiceOrder(editForm) {

    if (this.scheduleForm.invalid) {
      this.validateAllFields(this.scheduleForm)
      return false
    }

    if (this.installerUser == null) {

      this.snackbar.open('Informe um Instalador', '', {
        duration: 8000
      })
      return false
    }

    var currentDate = new Date()

    if (currentDate > this.utcTimeZone(this.scheduledDate)) {

      this.snackbar.open('Informe uma data superior a data atual', '', {
        duration: 8000
      })
      return false
    }

    let momentDate = moment(this.scheduledDate);

    let assignData = new AssignServiceOrder()
    assignData.loggedUser = this.appContext.session.user.id
    assignData.technician = this.installerUser.id
    assignData.scheduledDate = momentDate.toISOString()
    assignData.street = this.scheduleForm.value.street
    assignData.number = this.scheduleForm.value.number
    assignData.complement = this.scheduleForm.value.complement
    assignData.district = this.scheduleForm.value.district
    assignData.city = this.scheduleForm.value.city
    assignData.state = this.scheduleForm.value.state
    assignData.postalCode = this.utilsService.stringToNumber(this.scheduleForm.value.postalCode),
    assignData.reference = this.scheduleForm.value.reference
    assignData.beta = false

    this.serviceOrdersService.assignServiceOrder(this.serviceOrder.id, assignData)
      .subscribe(result => {

        this.snackbar.open('Ordem de Serviço reagendada com sucesso', '', {
          duration: 8000
        })

      },
        response => {
          this.snackbar.open(response.error.message, '', { duration: 8000 })
        })

  }

  onSearchChange(searchValue : string ) {  
    
    if (this.address && this.address.street) {

      if(this.complementForm.value.reference){
        this.address.reference = this.complementForm.value.reference
      }

      if(this.complementForm.value.complement){
        this.address.complement = this.complementForm.value.complement
      }
      
      this.blockEventsComponent.setEventAddress(this.address);
      this.loadAddressFields(this.address)

    }
  }

  editAddress(status){
    this.isEditAddress = status

    if(!status){
      setTimeout(() => {
        this.customerAddressComponent.loadData(this.address)
      this.customerAddressComponent.getCustomerForm().disable()
      },300);
    }
  }

  updateAddress(){

    this.isEditAddress = false;

    let editForm = this.scheduleForm.value

    let editData = []
    let path = ''
    for (let item in editForm) {

      if(item != "installer" && item != "time"){

      if (this.serviceOrder.type == 'INSTALLATION') {
          path = '/installation/address/'+item
      } else if (this.serviceOrder.type == 'UNINSTALLATION') {
          path = '/uninstallation/address/'+item
      } else if (this.serviceOrder.type == 'MAINTENANCE') {
          path = '/maintenance/address/'+item
      }

      let editItem = { 'op': 'add', 'path':  path, 'value': editForm[item] };
      editData.push(editItem);

      }
      
      
    }


    setTimeout(() => {
      this.customerAddressComponent.loadData(editForm)
     this.customerAddressComponent.getCustomerForm().disable()

     this.complementForm.patchValue({
       reference:  this.scheduleForm.value.reference,
       complement: this.scheduleForm.value.complement
     })

     this.googleMapsCheck(this.scheduleForm.value.street + ' ,' + this.scheduleForm.value.number + ' ,' + this.scheduleForm.value.district + ' ,' + this.scheduleForm.value.city + ' - ' + this.scheduleForm.value.state + ' ,' + this.scheduleForm.value.postalCode)

    },300);

    this.serviceOrdersService.update(this.serviceOrder.id, editData)
      .subscribe(result => {
        this.snackbar.open('Ordem de Serviço atualizado com sucesso', '', {
          duration: 8000
        })

      },error => {
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
     })

  }

  loadServiceOrder() {

    setTimeout(() => {
      this.startSearchAddress()
    }, 300);

    var datePipe = new DatePipe(navigator.language)

    let replacementID = null;
    this.isEditOS = true;

    if (this.serviceOrder.serviceOrderType == ServiceOrderType.INSTALLATION) {
      this.customer = this.serviceOrder.relationships.installation.customer ? this.serviceOrder.relationships.installation.customer : null;
      this.address = this.serviceOrder.installation.address ? this.serviceOrder.installation.address : null;
      this.deviceId = this.serviceOrder.relationships.installation.plan ? this.serviceOrder.relationships.installation.plan.device : null;
      this.plan = this.serviceOrder.relationships.installation.plan ? this.serviceOrder.relationships.installation.plan : null;

      this.schedulingConfirmation = this.serviceOrder.installation.schedulingConfirmation;
      this.pendencyDescription = this.serviceOrder.installation.pendencyDescription;
      this.pendencyType = this.serviceOrder.installation.pendencyType;
      
      this.deviceForm.patchValue({
        installationSpot: this.serviceOrder.installation.installationSpot ? this.serviceOrder.installation.installationSpot : null
      })

      if (!this.customer) {
        this.getCustomerByUserId(this.serviceOrder.installation.user)
      }

      this.loadPlanById()

    } else if (this.serviceOrder.serviceOrderType == ServiceOrderType.UNINSTALLATION) {
      this.customer = this.serviceOrder.relationships.uninstallation.customer ? this.serviceOrder.relationships.uninstallation.customer : null;
      this.address = this.serviceOrder.uninstallation.address ? this.serviceOrder.uninstallation.address : null;
      this.deviceId = this.serviceOrder.relationships.uninstallation.device ? this.serviceOrder.relationships.uninstallation.device.id : null;
      this.plan = this.serviceOrder.relationships.uninstallation.plan ? this.serviceOrder.relationships.uninstallation.plan : null;

      this.schedulingConfirmation = this.serviceOrder.uninstallation.schedulingConfirmation;
      this.pendencyType = this.serviceOrder.uninstallation.pendencyType;
      this.pendencyDescription = this.serviceOrder.uninstallation.pendencyDescription;

      this.deviceForm.patchValue({
        installationSpot: this.serviceOrder.uninstallation.installationSpot ? this.serviceOrder.uninstallation.installationSpot : null
      })

      if (!this.customer) {
        this.getCustomerByUserId(this.serviceOrder.uninstallation.user)
      }

      this.loadCustomerPlans()
    } else if (this.serviceOrder.serviceOrderType == ServiceOrderType.MAINTENANCE) {
      this.customer = this.serviceOrder.relationships.maintenance.customer ? this.serviceOrder.relationships.maintenance.customer : null;
      this.address = this.serviceOrder.maintenance.address ? this.serviceOrder.maintenance.address : null;
      this.deviceId = this.serviceOrder.relationships.maintenance.plan ? this.serviceOrder.relationships.maintenance.plan.device : null;
      this.plan = this.serviceOrder.relationships.maintenance.plan ? this.serviceOrder.relationships.maintenance.plan : null;

      this.schedulingConfirmation = this.serviceOrder.maintenance.schedulingConfirmation;
      this.pendencyType = this.serviceOrder.maintenance.pendencyType;
      this.pendencyDescription = this.serviceOrder.maintenance.pendencyDescription;

      this.deviceForm.patchValue({
        installationSpot: this.serviceOrder.maintenance.installationSpot ? this.serviceOrder.maintenance.installationSpot : null
      })

      if (!this.customer) {
        this.getCustomerByUserId(this.serviceOrder.maintenance.user)
      }

      this.deviceForm.patchValue({
        action: this.serviceOrder.relationships.maintenance.conclusion ? this.serviceOrder.relationships.maintenance.conclusion : ''
      })

      replacementID = this.serviceOrder.maintenance['replacement'] ? this.serviceOrder.maintenance['replacement'] : null

      this.loadCustomerPlans()

    }

    if (replacementID) {
      this.loadMaintenanceImageDevice(replacementID)
      this.getDevice(replacementID)
    } else if (this.deviceId) {
      this.getDevice(this.deviceId)
    }

    this.installerUser = this.serviceOrder.relationships.technician ? this.serviceOrder.relationships.technician : null;

    // load service order
    this.generalForm.patchValue({
      id: this.serviceOrder.id,
      status: this.serviceOrder.serviceOrderStatus,
      brand: this.serviceOrder.brand || null,
      type: this.serviceOrder.type || null,
      isInspection: this.serviceOrder.installation ? this.serviceOrder.installation.inspection : false || false,
      description: this.serviceOrder.description ? this.serviceOrder.description : null || null
    })

    this.historicForm.patchValue({
      openDate: this.serviceOrder.lifecycle.openDate ? datePipe.transform(new Date(this.serviceOrder.lifecycle.openDate), 'dd/MM/yyyy HH:mm:ss') : null,
      assignDate: this.serviceOrder.lifecycle.assignDate ? datePipe.transform(new Date(this.serviceOrder.lifecycle.assignDate), 'dd/MM/yyyy HH:mm:ss') : null,
      assignLoggedUser: this.serviceOrder.lifecycle.assignLoggedUser || null,
      inprogressDate: this.serviceOrder.lifecycle.inprogressDate ? datePipe.transform(new Date(this.serviceOrder.lifecycle.inprogressDate), 'dd/MM/yyyy HH:mm:ss') : null,
      inprogressLoggedUser: this.serviceOrder.lifecycle.inprogressLoggedUser || null,
      doneDate: this.serviceOrder.lifecycle.doneDate ? datePipe.transform(new Date(this.serviceOrder.lifecycle.doneDate), 'dd/MM/yyyy HH:mm:ss') : null,
      doneLoggedUser: this.serviceOrder.lifecycle.doneLoggedUser || null
    })

    if (this.serviceOrder.lifecycle.assignLoggedUser) {
      this.getUserById(this.serviceOrder.lifecycle.assignLoggedUser, 'assignLoggedUser')
    }

    if (this.serviceOrder.lifecycle.inprogressLoggedUser) {
      this.getUserById(this.serviceOrder.lifecycle.inprogressLoggedUser, 'inprogressLoggedUser')
    }

    if (this.serviceOrder.lifecycle.doneLoggedUser) {
      this.getUserById(this.serviceOrder.lifecycle.doneLoggedUser, 'doneLoggedUser')
    }

    this.historicForm.disable()

    this.generalForm.controls.status.disable({ onlySelf: true });
    this.generalForm.controls.brand.disable({ onlySelf: true });
    this.generalForm.controls.type.disable({ onlySelf: true, emitEvent: true });


    if (this.customer) {
      this.generalForm.controls.user.disable();
      this.generalForm.controls.name.disable();
      this.generalForm.controls.email.disable();
      this.generalForm.controls.mobilePhone.disable();

      this.generalForm.patchValue({
        user: this.customer.name,
        name: this.customer.name,
        email: this.customer.email,
        mobilePhone: this.customer.mobilePhone
      })
      this.customerName = this.customer.name
    }

    if (this.serviceOrder && this.serviceOrder.installation && this.serviceOrder.installation.checkList) {
      if (this.serviceOrder.installation.checkList.length > 0) {
        this.serviceOrderCheckList = this.serviceOrder.installation.checkList
      }
    } else if (this.serviceOrder && this.serviceOrder.uninstallation && this.serviceOrder.uninstallation.checkList) {
      if (this.serviceOrder.uninstallation.checkList.length > 0) {
        this.serviceOrderCheckList = this.serviceOrder.uninstallation.checkList
      }
    } else if (this.serviceOrder && this.serviceOrder.maintenance && this.serviceOrder.maintenance.checkList) {
      if (this.serviceOrder.maintenance.checkList.length > 0) {
        this.serviceOrderCheckList = this.serviceOrder.maintenance.checkList
      }
    }

    // plan
    if (this.plan) {
      this.customerPlansList = [];
      this.customerPlansList.push(this.plan)

      if (this.plan && this.plan.vehicle) {
        this.getCar(this.plan.vehicle)
      }


      this.generalForm.patchValue({
        plan: this.plan,
        planId: this.plan.id,
        offer: this.plan.relationships ? this.plan.relationships.offer ? this.plan.relationships.offer.name : '-' : '-',
        licensePlate: this.plan.relationships ? this.plan.relationships.vehicle ? this.plan.relationships.vehicle.licensePlate : '-' : '-',
        deviceId: this.plan.relationships ? this.plan.relationships.device ? this.plan.relationships.device.id : '-' : '-',
      })

      this.generalForm.controls.plan.disable({ onlySelf: true });
      this.generalForm.controls.planId.disable({ onlySelf: true });
      this.generalForm.controls.offer.disable({ onlySelf: true });
      this.generalForm.controls.licensePlate.disable({ onlySelf: true });
      this.generalForm.controls.deviceId.disable({ onlySelf: true });
    }


    if (this.serviceOrder && this.serviceOrder.installation && this.serviceOrder.installation.scheduledDate) {

      this.scheduledDate = new Date(this.serviceOrder.installation.scheduledDate)
      var scheduleTime = this.scheduledDate.getHours().toString() == '9' ? '09:00' : this.scheduledDate.getHours().toString() + ':00'

      this.scheduleForm.patchValue({
        scheduledDate: this.scheduledDate,
        time: scheduleTime
      })
    } else if (this.serviceOrder && this.serviceOrder.maintenance && this.serviceOrder.maintenance.scheduledDate) {

      this.scheduledDate = new Date(this.serviceOrder.maintenance.scheduledDate)
      var scheduleTime = this.scheduledDate.getHours().toString() == '9' ? '09:00' : this.scheduledDate.getHours().toString() + ':00'

      this.scheduleForm.patchValue({
        scheduledDate: this.scheduledDate,
        time: scheduleTime
      })

    } else if (this.serviceOrder && this.serviceOrder.uninstallation && this.serviceOrder.uninstallation.scheduledDate) {

      this.scheduledDate = new Date(this.serviceOrder.uninstallation.scheduledDate)
      var scheduleTime = this.scheduledDate.getHours().toString() == '9' ? '09:00' : this.scheduledDate.getHours().toString() + ':00'

      this.scheduleForm.patchValue({
        scheduledDate: this.scheduledDate,
        time: scheduleTime
      })

    }

    //create search FormControl
    this.searchControl = new FormControl();

    setTimeout(() => {
      this.startSearchAddress()
    }, 300);


    // address
    if (this.address && this.address.street) {

      if(this.address.reference && !this.complementForm.value.reference){
        this.complementForm.patchValue({
          reference: this.address.reference
        })
      }

      if(this.address.complement && !this.complementForm.value.complement){
        this.complementForm.patchValue({
          complement: this.address.complement
        })
      }

      if(this.complementForm.value.reference){
        this.address.reference = this.complementForm.value.reference
      }

      if(this.complementForm.value.complement){
        this.address.complement = this.complementForm.value.complement
      }
      
      this.blockEventsComponent.setEventAddress(this.address);
 
      this.googleMapsCheck(this.address.street + ' ,' + this.address.number + ' ,' + this.address.district + ' ,' + this.address.city + ' - ' + this.address.state + ' ,' + this.address.postalCode)
      this.scheduleForm.patchValue({
        street: this.address.street,
        number: this.address.number,
        complement: this.address.complement,
        district: this.address.district,
        city: this.address.city,
        state: this.address.state,
        postalCode: this.address.postalCode,
        reference: this.address.reference
      })

      setTimeout(() => {
        if (this.customerAddressComponent) {
          this.customerAddressComponent.loadData(this.address)
          this.customerAddressComponent.getCustomerForm().disable()
        }
      }, 300);

    } else {

      if (this.customer && this.customer.user) {
        this.getAddressByUserId(this.customer.user)
      }

    }


    if (!this.assignedSchedule()) { this.scheduleForm.disable() }

    // load installer 
    if (this.installerUser) {
      this.scheduleForm.patchValue({
        installer: this.installerUser.name,
      })
      this.userName = this.installerUser.name
    }

    // load comments
    this.serviceOrdersService.getServiceOrderComments(this.serviceOrder.id)
      .subscribe(result => {
        this.serviceOrderComments = result.content
      })

    // load image documents
    // if (this.deviceId) {
    //   this.loadImageDevice()
    // }
  }

  getUserById(id, param) {

    this.usersService.getUserById(id).subscribe(user => {
      if (user) {
        this.historicForm.patchValue({
          [param]: user.name
        })

      }
    })

  }

  getAddressByUserId(userId) {

    this.customersService.getCustomerAddress(userId)
      .subscribe(addresses => {

        if (addresses.content != null && addresses.content.length > 0) {
          this.address = addresses.content[0]

          if(this.address.reference && !this.complementForm.value.reference){
            this.complementForm.patchValue({
              reference: this.address.reference
            })
          }
    
          if(this.address.complement && !this.complementForm.value.complement){
            this.complementForm.patchValue({
              complement: this.address.complement
            })
          }
    
          if(this.complementForm.value.reference){
            this.address.reference = this.complementForm.value.reference
          }
    
          if(this.complementForm.value.complement){
            this.address.complement = this.complementForm.value.complement
          }

          this.blockEventsComponent.setEventAddress(this.address);
        

          this.scheduleForm.patchValue({
            street: this.address.street,
            number: this.address.number,
            complement: this.address.complement,
            district: this.address.district,
            city: this.address.city,
            state: this.address.state,
            postalCode: this.address.postalCode,
            reference: this.address.reference
          })

          let address = this.address.street + ' ,' + this.address.number + ' ,' + this.address.district + ' ,' + this.address.city + ' - ' + this.address.state + ' ,' + this.address.postalCode;
          this.googleMapsCheck(address);

          setTimeout(() => {
            this.customerAddressComponent.loadData(this.address)
            this.customerAddressComponent.getCustomerForm().disable()
          }, 300);

        }
      })
  }

  loadImageDevice() {
    this.documentsService.getDocumentsDevice(this.deviceId)
      .subscribe(documents => {
        this.random = '?random=' + new Date().getTime()
        this.serviceOrderDocuments = documents
      })
  }

  loadMaintenanceImageDevice(id) {
    this.documentsService.getDocumentsDevice(id)
      .subscribe(documents => {
        this.random = '?random=' + new Date().getTime()
        this.serviceOrderNewInstalDocuments = documents
      })
  }

  openWhatsapp() {
    
    if(this.customer && this.customer.mobilePhone){
      let number =''+this.customer.mobilePhone
      window.open('https://api.whatsapp.com/send?phone=55'+number+'&text=Confirmar o agendamento com o cliente', "_blank");
    }else{
      this.snackbar.open('O cliente não possui número de celular cadastrado', '', {
        duration: 8000
      })
    }
   
  }

  confirmScheduling() {

    this.dialog
      .open(ConfirmDialogComponent, {
        panelClass: 'event-form-dialog', data: {
          width: '600px',
          height: '300px', title: 'Confirmar Agendamento', message: 'Deseja confirmar o agendamento dessa Orderm de Serviço'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm == true) { 

          let path = null;

          if (this.serviceOrder.type == 'INSTALLATION') {
              path = '/installation/schedulingConfirmation'
          } else if (this.serviceOrder.type == 'UNINSTALLATION') {
              path = '/uninstallation/schedulingConfirmation'
          } else if (this.serviceOrder.type == 'MAINTENANCE') {
              path = '/maintenance/schedulingConfirmation'
          }

          let editData = [{ 'op': 'add', 'path': path, 'value': true }]

          this.serviceOrdersService.update(this.serviceOrderId, editData)
          .subscribe(result => {
            this.snackbar.open('Ordem de Serviço atualizado com sucesso', '', {
              duration: 8000
            })

            this.schedulingConfirmation = true

          },error => {
              this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
          })
          
        } else { 

        }
      });
  }

  reportPendency() {

   let pendencyType , pendencyDescription;

    if (this.serviceOrder.type == 'INSTALLATION') {
      pendencyType = this.serviceOrder.installation.pendencyType;
      pendencyDescription = this.serviceOrder.installation.pendencyDescription;
    } else if (this.serviceOrder.type == 'UNINSTALLATION') {
      pendencyType = this.serviceOrder.uninstallation.pendencyType;
      pendencyDescription = this.serviceOrder.uninstallation.pendencyDescription;
    } else if (this.serviceOrder.type == 'MAINTENANCE') {
      pendencyType = this.serviceOrder.maintenance.pendencyType;
      pendencyDescription = this.serviceOrder.maintenance.pendencyDescription;
    }


    this.dialog
      .open(SchedulingDialogComponent, {
        panelClass: 'event-form-dialog', data: {
          width: '600px',
          height: '600px', title: 'Informar Pendência', message: 'Pendência', type: 'pendency', pendencyType:pendencyType, pendencyDescription:pendencyDescription, placeholder: 'Motivo da Pendência', brandList: this.brandList
        }
      })
      .afterClosed()
      .subscribe(response => {

        if (response.data.confirm == true) {
            
            this.pendencyType = response.data.pendencyType;
            this.pendencyDescription = response.data.pendencyDescription;
            this.reportPendencyUpdate(response.data.pendencyType,response.data.pendencyDescription)
        }
      });
  }

  reportPendencyUpdate(pendencyType,pendencyDescription) {

    let pathPendencyType = null;
    let pathPendencyDescription = null;

    if (this.serviceOrder.type == 'INSTALLATION') {
        pathPendencyType = '/installation/pendencyType'
        pathPendencyDescription = '/installation/pendencyDescription'
    } else if (this.serviceOrder.type == 'UNINSTALLATION') {
        pathPendencyType = '/uninstallation/pendencyType'
        pathPendencyDescription = '/uninstallation/pendencyDescription'
    } else if (this.serviceOrder.type == 'MAINTENANCE') {
        pathPendencyType = '/maintenance/pendencyType'
        pathPendencyDescription = '/maintenance/pendencyDescription'
    }

    let editData = []
        editData.push({ 'op': 'add', 'path': pathPendencyType, 'value': pendencyType })
        editData.push({ 'op': 'add', 'path': pathPendencyDescription, 'value': pendencyDescription })

    this.serviceOrdersService.update(this.serviceOrderId, editData)
    .subscribe(result => {
      this.snackbar.open('Ordem de Serviço atualizado com sucesso', '', {
        duration: 8000
      })

      this.schedulingConfirmation = true

    },error => {
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
    })      
        
  }


  changeAddress() {

    this.dialog
      .open(ConfirmDialogComponent, {
        panelClass: 'event-form-dialog', data: {
          width: '600px',
          height: '300px', title: 'Endereço do Agendamento', message: 'Deseja mudar o endereço do agendamento?'
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm == true) { } else { }
      });
  }
  changeBrandScheduling() {

    this.dialog
      .open(SchedulingDialogComponent, {
        panelClass: 'event-form-dialog', data: {
          width: '600px',
          height: '600px', title: 'Transferir Agendamento', message: 'Transferir Agendamento', placeholder: 'Motivo', type: 'scheduling', brandList: this.brandList
        }
      })
      .afterClosed()
      .subscribe(response => {

        if (response.data.confirm == true) {

        }
      });

  }

  addInspection() {

    let message = "Deseja adicionar vistoria a essa Ordem de Serviço?"

    if (this.generalForm.value.isInspection) {
      message = "Deseja remover a vistoria dessa Ordem de Serviço?"
    }

    this.dialog
      .open(ConfirmDialogComponent, {
        panelClass: 'event-form-dialog', data: {
          width: '600px',
          height: '300px', title: 'Vistoria', message: message
        }
      })
      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm == true) {

          if (this.generalForm.value.isInspection) {
            this.generalForm.patchValue({
              isInspection: false
            })
          } else {
            this.generalForm.patchValue({
              isInspection: true
            })
          }

          this.updateServiceOrder()
        }
      });
  }

  cancelServiceOrder() {

    this.dialog
      .open(InputDialogComponent, { data: { title: 'Cancelar OS', message: 'Informe abaixo o motivo do Cancelamento e confirme', placeholder: 'Motivo' } })
      .afterClosed()
      .subscribe(response => {

        if (response.data.confirm == true) {

          let rejectDate = new RejectServiceOrder()
          rejectDate.loggedUser = this.appContext.loggedUser.id
          rejectDate.comment = response.data.inputText

          this.serviceOrdersService.rejectServiceOrder(this.serviceOrder.id, rejectDate)
            .subscribe(response => {
              this.snackbar.open('Ordem de Serviço rejeitada com sucesso', '', { duration: 8000 })
              this.navigateToServiceOrders()
            },
              response => {
                this.snackbar.open(response.error.message, '', { duration: 8000 })
              })
        }
      });
  }

  closeServiceOrder() {

    this.dialog
      .open(ConfirmDialogComponent, {
        panelClass: 'event-form-dialog', data: {
          width: '600px',
          height: '300px', title: 'Encerrar OS', message: 'Deseja encerrar a Ordem de Serviço?'
        }
      })
      .afterClosed()
      .subscribe(response => {

        if (response.data.confirm == true) {
          let body = { loggedUser: this.appContext.loggedUser.id }
          this.serviceOrdersService.closeServiceOrder(this.serviceOrder.id, body)
            .subscribe(response => {
              this.snackbar.open('Ordem de Serviço encerrada com sucesso', '', { duration: 8000 })
              this.navigateToServiceOrders()
            },
              response => {
                this.snackbar.open(response.error.message, '', { duration: 8000 })
              })
        }
      });
  }



  doneServiceOrder() {

    if (!this.validCheckList())
      return false

    if (!this.validDescription())
      return false


    this.dialog
      .open(ConfirmDialogComponent, {
        panelClass: 'event-form-dialog', data: {
          width: '600px',
          height: '300px', title: 'Concluir OS', message: 'Deseja concluir a Ordem de Serviço?'
        }
      })
      .afterClosed()
      .subscribe(response => {

        if (response.data.confirm == true) {
          let body = {
            loggedUser: this.appContext.loggedUser.id,
            device: this.deviceSelected.id,
            checkList: this.serviceOrderCheckList
          }

          if (this.serviceOrder.serviceOrderType == ServiceOrderType.INSTALLATION) {
            //body["description"] = this.deviceForm.value.description;
            body["installationSpot"] = this.deviceForm.value.installationSpot
          } else if (this.serviceOrder.serviceOrderType == ServiceOrderType.MAINTENANCE) {
            body["conclusion"] = this.deviceForm.value.action;
            //body["description"] = this.deviceForm.value.description;
            body["installationSpot"] = this.deviceForm.value.installationSpot
          }

          let inprogressBody = {
            loggedUser: this.appContext.loggedUser.id,
            comment: ""
          }

          this.serviceOrdersService.inprogressServiceOrder(this.serviceOrder.id, inprogressBody)
            .subscribe(response => {

              this.serviceOrdersService.doneServiceOrder(this.serviceOrder.id, body)
                .subscribe(response => {
                  this.snackbar.open('Ordem de Serviço concluída com sucesso', '', { duration: 8000 })
                  this.navigateToServiceOrders()
                },
                  response => {
                    this.snackbar.open(response.error.message, '', { duration: 8000 })
                  })

            })
        }
      });

  }

  validConclusion() {
    let type = true;

    if (this.deviceForm.value.action == '') {
      type = false;
    }

    if (!type) {
      this.dialog
        .open(ConfirmDialogComponent, {
          panelClass: 'event-form-dialog', data: {
            width: '600px',
            height: '300px', title: 'Ação', message: 'Selecione o motivo da manutenção do Rastreador', ok: true
          }
        })
        .afterClosed()
      this.selectedTab = 2;

    }

    return type;
  }

  validDescription() {
    let type = true;

    if (this.serviceOrder.serviceOrderType == ServiceOrderType.INSTALLATION || this.serviceOrder.serviceOrderType == ServiceOrderType.MAINTENANCE) {
      if (this.deviceForm.value.installationSpot == '') {
        type = false;
      }
    }

    if (!type) {
      this.dialog
        .open(ConfirmDialogComponent, {
          panelClass: 'event-form-dialog', data: {
            width: '600px',
            height: '300px', title: 'Descrição', message: 'Insira a descrição do local de instalação do Rastreador', ok: true
          }
        })
        .afterClosed()
      this.selectedTab = 2;

    }

    return type;
  }


  updateServiceOrder() {

    let editForm = this.generalForm.value

    let editData = []
    let path = ''
    for (let item in editForm) {
      path = item

      

      if (this.serviceOrder.type == 'INSTALLATION') {

        if (path == 'installationSpot') {
          path = 'installation/installationSpot'
        }

        if (path == 'isInspection') {
          path = 'installation/inspection'
        }

      } else if (this.serviceOrder.type == 'UNINSTALLATION') {

        if (path == 'installationSpot') {
          path = 'uninstallation/installationSpot'
        }

      } else if (this.serviceOrder.type == 'MAINTENANCE') {

        if (path == 'installationSpot') {
          path = 'maintenance/installationSpot'
        }

      }

      let editItem = { 'op': 'add', 'path': '/' + path, 'value': editForm[item] };
      editData.push(editItem);
    }
    this.serviceOrdersService.update(this.serviceOrder.id, editData)
      .subscribe(product => {
        this.snackbar.open('Ordem de Serviço atualizado com sucesso', '', {
          duration: 8000
        })

      },error => {
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
     })
  }

  validCheckList() {

    let type = true;
    let item = '';

    this.serviceOrderCheckList.forEach(element => {
      if (element.beforeInstallation == "" || element.afterInstallation == "") {
        type = false;
        item = element.part;
      }

    });

    if (!type) {
      this.dialog
        .open(ConfirmDialogComponent, {
          panelClass: 'event-form-dialog', data: {
            width: '600px',
            height: '300px', title: 'CheckList', message: 'Selecione o status do item ' + item, ok: true
          }
        })
        .afterClosed()
      this.selectedTab = 4;

    }

    return type;

  }

  closeServiceOrderMaintenance() {

    this.dialog
      .open(MaintenanceComponent, {
        width: '900px',
      })
      .afterClosed()
      .subscribe(response => {

        if (response.data.confirm == true) {
          let body = { loggedUser: this.appContext.loggedUser.id, device: 324232424256 }
        }
      });
  }

  addComment() {
    this.dialog
      .open(InputDialogComponent, { width: '450px', data: { title: 'Novo Comentário', message: '', placeholder: 'Digite seu comentário' } })
      .afterClosed()
      .subscribe(response => {

        if (response.data.confirm == true) {

          let newDate = new Date()
          let dateTime = moment(newDate).utcOffset("-00:00", true).toISOString()
          let dateTimeArray = dateTime.split('.')

          let comment = {
            loggedUser: this.appContext.loggedUser.id,
            serviceOrder: this.serviceOrder.id,
            comment: response.data.inputText,
            operator: this.appContext.session.user.id,
            dateTime: dateTimeArray[0]
          }

          this.serviceOrdersService.addServiceOrderComment(comment)
            .subscribe(response => {
              this.snackbar.open('Comentário adicionado', '', { duration: 8000 })
              this.serviceOrderComments.unshift(response)
              this.serviceOrdersService.getServiceOrderComments(this.serviceOrder.id)
                .subscribe(result => {
                  this.serviceOrderComments = result.content
                })
            })
        }
      });
  }

  canComment() {
    if (!this.serviceOrder || !this.serviceOrder.id) return false

    return true
  }

  checkListStatusEnumToString(value): string {
    return ServiceOrderCheckList[value]
  }

  hasServiceOrder() {
    return (this.serviceOrder && this.serviceOrder.id)
  }

  assignedSchedule() {

    if (!this.serviceOrder || !this.serviceOrder.id) return false

    if (this.serviceOrder.serviceOrderType == ServiceOrderType.MAINTENANCE) return true

    return this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.ASSIGNED ||
      this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.OPEN ||
      this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.CANCELED ||
      this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.REJECTED
  }

  canReschedule() {
    return this.serviceOrder.serviceOrderStatus != ServiceOrderStatus.OPEN
  }

  canSchedule() {
    if (!this.serviceOrder || !this.serviceOrder.id) return false

    return this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.OPEN
  }

  canViewReview() {
    if (!this.serviceOrder || !this.serviceOrder.id) return false

    return this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.CLOSED ||
      this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.DONE ||
      this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.CANCELED
  }

  canReview() {
    return this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.DONE
  }

  canChangeDevice() {
    return this.serviceOrder.serviceOrderType != ServiceOrderType.UNINSTALLATION
  }

  canDone() {
    return this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.ASSIGNED ||
      this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.IN_PROGRESS
  }

  canChangeCheckList() {
    return this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.CLOSED
  }

  canCanceled() {
    if (this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.CLOSED || this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.CANCELED || this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.REJECTED) {
      return false
    } else {
      return true
    }
  }
}

