import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import { ServiceOrderType, ServiceOrderStatus } from "../core/models/service-orders.model";
import { ServiceOrdersService } from "../core/services/service-orders.service";
import { UtilsService } from "../core/utils.service";
import { Brand } from "../core/models/brand.model";
import { DevicesService } from '../core/services/devices.service';


export class ServiceOrdersDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);
    private totalElementsSubject = new BehaviorSubject<number>(-1)
    public totalElements$ = this.totalElementsSubject.asObservable()


    constructor(
        private serviceOrdersService : ServiceOrdersService,
        private utilsService : UtilsService,
        private brands: Array<Brand>,
        private devicesService: DevicesService) {
    }

    load(   brand: number,
            filter:string,
            serviceOrderType:string,
            serviceOrderStatus:string,
            technician: number,
            sortField: string,
            sortDirection:string,
            pageIndex:number,
            pageSize:number) {


            if(filter && filter.length > 0 ){

             let resultInstallation = null
             let resultUninstallation = null
             let resultMaintenance = null
             let finalResul = null

             if(serviceOrderType.length == 0 || serviceOrderType == 'INSTALLATION'){

              this.searchByType(brand,filter,serviceOrderType,serviceOrderStatus,technician,sortField,sortDirection,pageIndex,pageSize,
                    'INSTALLATION','installation').subscribe(
                        result => {
                            
                            if(!resultInstallation && !resultUninstallation && !resultUninstallation && !finalResul){
                                finalResul = result
                                resultInstallation = result;
                            }else{
                                resultInstallation = result;
                                finalResul['content'] = finalResul['content'].concat(result['content']);
                                finalResul['numberOfElements'] = parseInt(finalResul['numberOfElements']) + parseInt(result['numberOfElements'])
                                finalResul['totalElements']= parseInt(finalResul['totalElements']) + parseInt(result['totalElements'])
                            }

                            if(resultInstallation && resultUninstallation && resultUninstallation || serviceOrderType == 'INSTALLATION'){

                                if (finalResul.content) {
                                    finalResul.content.forEach(obj => {
                                        obj.serviceOrderStatus = ServiceOrderStatus[obj.status]
                                        obj.serviceOrderType = ServiceOrderType[obj.type]
                                        obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                                    });
                                }
        
                                this.totalElementsSubject.next(finalResul.totalElements)
                                this.dataSubject.next(finalResul.content)
                            
                            }
                           
                        }
                      )
                }

                if(serviceOrderType.length == 0 || serviceOrderType == 'UNINSTALLATION'){

                this.searchByType(brand,filter,serviceOrderType,serviceOrderStatus,technician,sortField,sortDirection,pageIndex,pageSize,
                'UNINSTALLATION','uninstallation').subscribe(
                    result => {

                        if(!resultInstallation && !resultUninstallation && !resultUninstallation && !finalResul){
                            finalResul = result
                            resultUninstallation = result
                        }else{
                            resultUninstallation = result
                            finalResul['content'] = finalResul['content'].concat(result['content']);
                            finalResul['numberOfElements'] = parseInt(finalResul['numberOfElements']) + parseInt(result['numberOfElements'])
                            finalResul['totalElements']= parseInt(finalResul['totalElements']) + parseInt(result['totalElements'])
                        }

                        if(resultInstallation && resultUninstallation && resultUninstallation || serviceOrderType == 'UNINSTALLATION'){

                            if (finalResul.content) {
                                finalResul.content.forEach(obj => {
                                    obj.serviceOrderStatus = ServiceOrderStatus[obj.status]
                                    obj.serviceOrderType = ServiceOrderType[obj.type]
                                    obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                                });
                            }
    
                            this.totalElementsSubject.next(finalResul.totalElements)
                            this.dataSubject.next(finalResul.content)
                        
                        }
                      
                    }
                    )
                }

                if(serviceOrderType.length == 0 || serviceOrderType == 'MAINTENANCE'){
                this.searchByType(brand,filter,serviceOrderType,serviceOrderStatus,technician,sortField,sortDirection,pageIndex,pageSize,
                    'MAINTENANCE','maintenance').subscribe(
                        result => {
                            
                            if(!resultInstallation && !resultUninstallation && !resultUninstallation && !finalResul){
                                finalResul = result
                                resultMaintenance = result
                            }else{
                                resultMaintenance = result
                                finalResul['content'] = finalResul['content'].concat(result['content']);
                                finalResul['numberOfElements'] = parseInt(finalResul['numberOfElements']) + parseInt(result['numberOfElements'])
                                finalResul['totalElements']= parseInt(finalResul['totalElements']) + parseInt(result['totalElements'])
                            }
                           
        
                            if(resultInstallation && resultUninstallation && resultUninstallation || serviceOrderType == 'MAINTENANCE'){

                                if (finalResul.content) {
                                    finalResul.content.forEach(obj => {
                                        obj.serviceOrderStatus = ServiceOrderStatus[obj.status]
                                        obj.serviceOrderType = ServiceOrderType[obj.type]
                                        obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                                    });
                                }
        
                                this.totalElementsSubject.next(finalResul.totalElements)
                                this.dataSubject.next(finalResul.content)
                            
                            }
                        }
                        )

                }

            }else{

                this.serviceOrdersService.findServiceOrders(
                    brand,
                    filter, 
                    serviceOrderType, 
                    serviceOrderStatus, 
                    technician,
                    sortField,
                    sortDirection,
                    pageIndex, 
                    pageSize).pipe(
                        catchError(() => of([]))
                    )
                    .subscribe(result => {

                        if (result.content) {
                            result.content.forEach(obj => {
                                obj.serviceOrderStatus = ServiceOrderStatus[obj.status]
                                obj.serviceOrderType = ServiceOrderType[obj.type]
                                obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                            });
                        }

                        this.totalElementsSubject.next(result.totalElements)
                        this.dataSubject.next(result.content)

                    })
            }
    }

    searchByType(brand: number,
        filter:string,
        serviceOrderType:string,
        serviceOrderStatus:string,
        technician: number,
        sortField: string,
        sortDirection:string,
        pageIndex:number,
        pageSize:number,
        type:string,
        fieldType:string): Observable<any> {


      return this.serviceOrdersService.findServiceOrdersByType(
            brand,
            filter, 
            serviceOrderType, 
            serviceOrderStatus, 
            technician,
            sortField,
            sortDirection,
            pageIndex, 
            pageSize,
            type,
            fieldType)

    }

    extend(obj, src) {
        for (var key in src) {
            if (src.hasOwnProperty(key)) obj[key] = src[key];
        }
        return obj;
    }
    

    loadDevices(devices:Array<number>,
        sortField: string,
        sortDirection:string,
        pageIndex:number,
        pageSize:number,
        ) {
            this.devicesService.findDevicesByIds(devices,
                sortField,
                sortDirection,
                pageIndex, 
                pageSize).pipe(
                    catchError(() => of([]))
                ).subscribe(result=>{

            this.totalElementsSubject.next(result.totalElements)
            this.dataSubject.next(result.content)
        }) 
    }

    setDeviceList(devices:Array<any>){
        this.totalElementsSubject.next(devices.length)
        this.dataSubject.next(devices)
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
