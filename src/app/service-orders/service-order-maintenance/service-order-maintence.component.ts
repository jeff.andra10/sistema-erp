import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { Observable } from 'rxjs/Observable';
import { MatTableDataSource, MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DevicesService } from '../../core/services/devices.service';
import { ServiceOrder } from '../../core/models/service-orders.model'

@Component({
  selector: 'tnm-maintenance-type',
  templateUrl: './service-order-maintenance.component.html',
  styleUrls: ['../service-orders.component.scss'],
})

export class MaintenanceComponent implements OnInit {

  deviceForm: FormGroup
  searchField: FormControl
  devicesList: Array<any>
  selected : string =  'DEVICE_EXCHANGE'

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<MaintenanceComponent>,
    private devicesService: DevicesService,
  ) { }

  ngOnInit() {
    this.initForms()
  }

  initForms() {

    this.devicesList =[{'type':'Troca de Dispositivo','value':'DEVICE_EXCHANGE'},
    {'type':'Troca de local do Dispositivo','value':'DEVICE_LOCAL_CHANGE'},
    {'type':'Cancelamento de Dispositivo','value':'DEVICE_CANCELLATION'}]
    
  }

  searchDevices(){
    
  }

  onYesClick() {
    this.dialogRef.close({data : {confirm: true}})
  }

  onNoClick() {
    this.dialogRef.close({data: {confirm: false}})
  }

  setDevice(device) {
    this.dialogRef.close({device: device})
  }


  
}