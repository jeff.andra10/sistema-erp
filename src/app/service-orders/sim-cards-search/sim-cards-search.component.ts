import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormControl, FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { Observable } from 'rxjs/Observable';
import {  MatDialogRef} from '@angular/material';
import { SimCardsService } from '../../core/services/sim-cards.service';

@Component({
  selector: 'tnm-sim-cards-search',
  templateUrl: './sim-cards-search.component.html',
})

export class SimCardsSearchComponent implements OnInit {

  simCardForm: FormGroup
  searchField: FormControl
  simCardList: Array<any>

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<SimCardsSearchComponent>,
    private simCardsService: SimCardsService,
  ) { }

  ngOnInit() {
    
    this.initForms()
  }

  initForms() {
    this.searchField = this.formBuilder.control('')

    this.simCardForm = this.formBuilder.group({
      searchField: this.searchField
    });

    this.searchField.valueChanges
    .debounceTime(2000)
    .distinctUntilChanged()
    .switchMap(searchTerm =>

      this.simCardsService.getSimCardsBySearchTerms(searchTerm)
      .catch(error => Observable.from([])))
      .subscribe(simCards => {  
        this.simCardList = simCards.content
      })
  }

  setSimCard(simCard) {
    this.dialogRef.close({simCard: simCard})
  }

  searchSimCards(){
    this.searchField.valueChanges
    .debounceTime(0)
    .distinctUntilChanged()
    .switchMap(searchTerm =>

      this.simCardsService.getSimCardsBySearchTerms(searchTerm)
      .catch(error => Observable.from([])))
      .subscribe(simCards => {  
        this.simCardList = simCards.content
      })
  }
  
}