import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs";
import { ServiceOrder } from "../core/models/service-orders.model";

@Injectable()
export class ServiceOrdersSelectedService {

    private serviceOrderSource : BehaviorSubject<ServiceOrder> = new BehaviorSubject(null)
    public serviceOrderSelected = this.serviceOrderSource.asObservable();

    private serviceOrderReloadCalendar : BehaviorSubject<ServiceOrder> = new BehaviorSubject(null)
    public serviceOrderReloadCalendarSelected = this.serviceOrderReloadCalendar.asObservable();
  
    setSelected(sale: ServiceOrder) {
      this.serviceOrderSource.next(sale)
    }

    reloadCalendar(date: any) {
      this.serviceOrderReloadCalendar.next(date)
    }
}