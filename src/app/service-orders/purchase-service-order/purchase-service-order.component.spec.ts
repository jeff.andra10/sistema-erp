import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseServiceOrderComponent } from './purchase-service-order.component';

describe('PurchaseServiceOrderComponent', () => {
  let component: PurchaseServiceOrderComponent;
  let fixture: ComponentFixture<PurchaseServiceOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseServiceOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseServiceOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
