import { Component, OnInit, AfterViewInit, Injector, ViewChild  } from '@angular/core';
import { MatTableDataSource, MatSnackBar, MatDialog, MatPaginator, MatSort} from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import { Observable, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { IntervalObservable } from "rxjs/observable/IntervalObservable";
import { merge } from "rxjs/observable/merge";
import { BaseComponent } from '../../_base/base-component.component';
import { ServiceOrder, DonePurchaseServiceOrder, ServiceOrderStatus } from '../../core/models/service-orders.model';
import { ServiceOrdersSelectedService } from '../service-orders.selected.service';
import { Device } from '../../core/models/device.model';
import { MaskUtils } from '../../core/mask-utils';
import { DevicesService } from '../../core/services/devices.service';
import { ServiceOrdersService } from '../../core/services/service-orders.service';
import { UtilsService } from '../../core/utils.service';
import { DeviceStatus} from '../../core/models/device.model';
import { ProductsService } from 'app/core/services/products.service';
import { OffersService } from '../../core/services/offers.service'
import { DevicesSearchComponent } from "../devices-search/devices-search.component";
import { SimCardsSearchComponent } from "../sim-cards-search/sim-cards-search.component";
import { tap } from 'rxjs/operators'
import { ServiceOrdersDataSource } from '../service-orders.datasource';
import { ConfirmDialogComponent } from 'app/dialogs/confirm-dialog/confirm-dialog.component';
import { DevicesModel} from '../../core/models/device.model';


@Component({
  selector: 'app-purchase-service-order',
  templateUrl: './purchase-service-order.component.html',
  styleUrls: ['./purchase-service-order.component.scss'],
  animations: fuseAnimations
})
export class PurchaseServiceOrderComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  serviceOrder : ServiceOrder = new ServiceOrder()
  productDescription: string = ""
  productQuantity: number = 0
  deviceList : Array<Device> = []
  deviceForm : FormGroup
  generalForm: FormGroup
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  deviceSelected  : any
  simCardSelected : any
  deviceModelList : any
  phoneMask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  simCardMask = MaskUtils.simCardMask

  displayedColumns: string[]
  //dataSource: any
  contentLength: number = 0
  pageSize: number = 10
  //pageEvent: PageEvent
  productsList: any
  dataSource: ServiceOrdersDataSource
  brandsList: Array<any>
  isDone : boolean = true
  localDevices : any
  skipTests : boolean = false
  
  constructor(
    private injector: Injector,
    private snackbar: MatSnackBar,
    private router: Router,
    private utils: UtilsService,
    private formBuilder: FormBuilder,
    private serviceOrderSelectedService: ServiceOrdersSelectedService,
    private devicesService: DevicesService,
    public  dialog: MatDialog,
    private serviceOrdersService: ServiceOrdersService,
    private productsService: ProductsService,
    private offersService: OffersService
  ) { 
    super(injector)
  }

  ngOnInit() {

    this.displayedColumns = ['model', 'operator', 'imei', 'simCard', 'number', 'status','reload', 'delete']
    this.brandsList = this.appContext.brands
    this.initForms()

    this.loadDomainListData()
   
  this.getProducts().subscribe(
      products => {
          this.productsList = products.content

          this.serviceOrderSelectedService.serviceOrderSelected.subscribe(serviceOrder => {

            
            if (serviceOrder != null) {
              this.serviceOrder = serviceOrder
             
              this.isDone = this.serviceOrder.status == 'DONE' ? true : false

              this.productQuantity =   this.serviceOrder.relationships.purchase.purchase ?  this.serviceOrder.relationships.purchase.purchase.amount : ''
              let offerId = this.serviceOrder.relationships.purchase.purchase ? this.serviceOrder.relationships.purchase.purchase.offer : null
              
              if(offerId){
                this.offersService.getOffersById(offerId).subscribe(offer=>{
                  
                  let product = this.utilsService.getCompleteListByIdsList(offer.products, this.productsList)
                  
                  if(product && product.length > 0){
                    this.productDescription = product[0]['name']
                  }
                    
                })
              }

              if (this.serviceOrder && this.serviceOrder.purchase && this.serviceOrder.purchase.devices && this.serviceOrder.purchase.devices.length > 0) {
                  this.loadDevices()
              }

              let localDevices = localStorage.getItem('devices_'+this.serviceOrder.id);
              
              if(localDevices ){
                  this.deviceList = JSON.parse(localDevices);
                  this.contentLength = this.deviceList.length
                  this.dataSource.setDeviceList(this.deviceList);
              }
            }
      })
      }
  )
 
}

loadDomainListData() {
  this.deviceModelList = this.utils.stringEnumToKeyValue(DevicesModel)
}

  ngAfterViewInit() {


    setTimeout(() => {

      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

      this.dataSource.totalElements$.subscribe( value => {
         this.contentLength = value
       })
  
      merge(this.sort.sortChange, this.paginator.page)
      .pipe(
          tap(() => this.loadDevices())
      )
      .subscribe()
  
      },200)
  }

  loadDevices(){

    this.dataSource.loadDevices(
      this.serviceOrder.purchase.devices,
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }
  getProducts(): any{
    return this.productsService.getProducts()
  }

  initForms() {

    this.dataSource = new ServiceOrdersDataSource(this.serviceOrdersService, this.utilsService, this.brandsList, this.devicesService)
    //this.dataSource.loadDevices([], 'lifecycle.testedDate', 'desc', 0, 10)

    this.deviceForm = this.formBuilder.group({
      model: this.formBuilder.control('', [Validators.required]),
      operator: this.formBuilder.control('', [Validators.required]),
      imei: this.formBuilder.control('', [Validators.required]),
      simCard: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required])
    })

    this.generalForm = this.formBuilder.group({
      device: this.formBuilder.control({value:'', disabled: true}),
      simCard: this.formBuilder.control({value:'', disabled: true})
    })

  }

  startInterval() {

    IntervalObservable.create(5000)
    .takeWhile(() => { 
      // tries when not all devices are online
      return !this.checkAllDevicesIsOnLine(false)
    }) 
    .subscribe(() => {

      this.deviceList.forEach(device=>{
      
        if (device.status != "TESTED") {
          
          this.devicesService.getOne(device.id)
          .subscribe(deviceResult=>{
            
          })
        }
      })
    });
  }

  skipTest(event){
    console.log(event.checked)
    this.skipTests = event.checked
  }

  addDevice() {

    if (this.deviceForm.invalid) {
      return false;
    }

    if(this.checkDevice(this.utilsService.stringOnlyDigits(this.deviceForm.value.simCard))){
      this.snackbar.open('Esse rastreador já esta cadastrado', '', { duration: 5000 });
      return false;
    }

    var device = new Device()
    device.model = this.deviceForm.value.model
    device.operator = this.deviceForm.value.operator
    device.imei = this.utilsService.stringToNumber(this.deviceForm.value.imei)
    device.simCard = this.utilsService.stringOnlyDigits(this.deviceForm.value.simCard)
    device.number = this.utilsService.stringToNumber(this.deviceForm.value.number)
    device.brand = this.serviceOrder.brand
    device.status = 'INACTIVE'


    this.devicesService.createDevice(device)
    .subscribe(deviceCreated => {
      device.id = deviceCreated.id
      this.deviceList.push(device)
      this.contentLength = this.deviceList.length
      this.dataSource.setDeviceList(this.deviceList)
      this.clearDeviceForm()
      localStorage.setItem('devices_'+this.serviceOrder.id,JSON.stringify(this.deviceList));
    }, error => {
      console.log('error:',error)
      this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um ao tentar cadastrar o rastreador. tente novamente.', '', { duration: 7000 });
    })
  }

  canDeactivate() : Observable<boolean> | Boolean {

    const subject = new Subject<boolean>();
    
    if (this.deviceList && this.deviceList.length > 0  && !this.isDone) {

      this.dialog.open(ConfirmDialogComponent, 
        { 
          panelClass: 'event-form-dialog',
          data: { 
            width: '600px',
            height: '300px',
            title: 'Confirmação', 
            message: 'Deseja salvar os rastreadores cadastrados ?' 
          }
        })
        .afterClosed()
        .subscribe(response=>{
          subject.next(true);
          subject.complete();
          if(!response.data.confirm){
            localStorage.removeItem('devices_'+this.serviceOrder.id);
          }
        });

      return subject.asObservable();
    }

    return true
  }

  private checkDevice(simCard: string) {
    let device = this.deviceList.filter(device => device['simCard'] ? device['simCard'].indexOf(simCard) === 0 : '');
    return (device && device.length > 0) ? true : false
  }

  clearDeviceForm() {

    this.deviceForm.patchValue({
      imei: null,
      simCard: null,
      number: null
    })
  }

  navigateToServiceOrders() {
    this.router.navigate(['/service-orders'])
  }

  deviceActivityToString(device: Device) : string {
    return DeviceStatus[device.status]
  }

  doneServiceOrder() {

    if (!this.checkDevicesQuantityIsEqualsPurshasedQuantity()) {
      return false
    }

    if (!this.skipTests && !this.checkAllDevicesIsOnLine()) {
      return false
    }

    let doneData = new DonePurchaseServiceOrder()
    doneData.loggedUser = this.appContext.session.user.id
    doneData.skipTests = this.skipTests
    
    doneData.devices = []
    this.deviceList.forEach(device=>{
      doneData.devices.push(device.id)
    })

    this.serviceOrdersService.donePurchaseServiceOrder(this.serviceOrder.id, doneData)
    .subscribe(serviceOrderCreated => { 

      localStorage.removeItem('devices_'+this.serviceOrder.id);
      this.isDone = true;

      this.snackbar.open('Ordem de Serviço concluída com sucesso', '', {
        duration: 8000
      })
      this.navigateToServiceOrders()
    })
  }

  checkDevicesQuantityIsEqualsPurshasedQuantity() : boolean {

    if (this.deviceList.length < this.productQuantity) {
      this.snackbar.open('A quantidade de rastreadores informada é menor que quantidade solicitada na Compra.', '', {
        duration: 8000
      })
      return false
    }

    return true
  }

  checkAllDevicesIsOnLine(showMessage: boolean = true) : boolean {

    var isOnline = true

    this.deviceList.forEach(device=>{
      
      if (device.status != "TESTED") {
        isOnline = false
      }
    })

    if (!isOnline && showMessage) {
      this.snackbar.open('Para concluir a OS todos os rastreadores precisam estar Online.', '', {
        duration: 8000
      })
    }

    return isOnline
  }

  deleteDevice(device: Device) {
    
    let index = this.deviceList.indexOf(device)
    if (index > -1) {

      this.devicesService.delete(device.id)
      .subscribe(result=> {
        this.deviceList.splice(index, 1)

        this.contentLength = this.deviceList.length
        this.dataSource.setDeviceList(this.deviceList)
        localStorage.setItem('devices_'+this.serviceOrder.id,JSON.stringify(this.deviceList));
      })
    }
  }

  updateStatusDevice(device: Device) {
    
    this.devicesService.testDevices(device.id)
    .subscribe(dev=> {
      device.status = dev.status
    })
  }

  onInputKeydown(control, event) {
    if (control == 'number') {
      this.addDevice()
    }
  }

  maskPhoneNumber(number) {
    
      number+="";
      number = number.replace(/\D/g, '');
      number = number.match(/^(\d{2})(\d{5})(\d{4})$/);
      number = (!number) ? null : '(' + number[1] + ') '+ number[2] + '-' + number[3];
    
    return number;
  }

  canDone() : boolean {
    return this.serviceOrder.serviceOrderStatus == ServiceOrderStatus.OPEN
  }

  searchDevices() {
    this.dialog
    .open(DevicesSearchComponent, {width: '500px', height: '550px'})
    .afterClosed()
    .subscribe(response => {
      if (response && response.device ) {
        this.deviceSelected = response.device
        
        this.generalForm.patchValue ({
          device: this.deviceSelected ? this.deviceSelected.imei : '',
          simCard: this.simCardSelected ? this.simCardSelected.iccid : ''
        })

        this.loadDevice()
      }
    });
  }
    

  searchSimCards(){
    this.dialog
    .open(SimCardsSearchComponent, {width: '500px', height: '550px'})
    .afterClosed()
    .subscribe(response => {
      if (response && response.simCard ) {
        this.simCardSelected = response.simCard
        
        this.generalForm.patchValue ({
          device: this.deviceSelected ? this.deviceSelected.imei : '',
          simCard: this.simCardSelected ? this.simCardSelected.iccid : ''
        })
        this.loadDevice()
      }
    });
  }

  loadDevice(){

    let _simCard = this.deviceSelected ? this.deviceSelected.simCard  ? this.deviceSelected.simCard : null : null
    let _number = this.deviceSelected ? this.deviceSelected.number  ? this.deviceSelected.number : null : null
    let _operator = this.deviceSelected ? this.deviceSelected.operator  ? this.deviceSelected.operator : null : null
    
    this.deviceSelected
    this.deviceForm = this.formBuilder.group({
      model: this.formBuilder.control(this.deviceSelected ? this.deviceSelected.model : '', [Validators.required]),
      operator: this.formBuilder.control(_operator ? _operator : this.simCardSelected ? this.simCardSelected.operator  ? this.simCardSelected.operator : '' : '', [Validators.required]),
      imei: this.formBuilder.control(this.deviceSelected  ? this.deviceSelected.imei : '', [Validators.required]),
      simCard: this.formBuilder.control(_simCard ? _simCard : this.simCardSelected ? this.simCardSelected.iccid : '', [Validators.required]),
      number: this.formBuilder.control(_number ? _number : this.simCardSelected ? this.simCardSelected.msisdn : '', [Validators.required])
    })
  }
}
