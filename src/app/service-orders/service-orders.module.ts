import { NgModule, ModuleWithProviders } from "@angular/core";
import { DetailServiceOrderComponent } from "./detail-service-order/detail-service-order.component"
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component'
import { CalendarInstallerDialogComponent } from '../dialogs/calendar-installer-dialog/calendar-installer-dialog.component'
import { DialogsModule } from '../dialogs/dialogs.module'
import { InputDialogComponent } from '../dialogs/input-dialog/input-dialog.component'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../angular-material/material.module";
import { FuseSharedModule } from "@fuse/shared.module";
import { RouterModule } from "@angular/router";
import { ServiceOrdersComponent } from "./service-orders.component";
import { ServiceOrdersSelectedService } from "./service-orders.selected.service";
import { PurchaseServiceOrderComponent } from './purchase-service-order/purchase-service-order.component';
import { DevicesSearchComponent } from "./devices-search/devices-search.component";
import { SimCardsSearchComponent } from "./sim-cards-search/sim-cards-search.component";
import { MaintenanceComponent } from "./service-order-maintenance/service-order-maintence.component";
import { CustomerPersonalComponent } from "../customers/customer-personal/customer-personal.component";
import { CustomerDocumentsComponent } from "../customers/customer-documents/customer-documents.component";
import { CustomerAddressComponent } from "../customers/customer-address/customer-address.component";
import { CustomersModule } from "../customers/customers.module";
import { CanDeactivateGuard } from "../core/can-deactivate-guard.service";
import { AgmCoreModule,GoogleMapsAPIWrapper } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CalendarBlockEventsModule } from './calendar-block-events/block-events.module';
import { BlockEventsComponent } from './calendar-block-events/block-events.component';


const authRouting: ModuleWithProviders = RouterModule.forChild([
    {path: '', component: ServiceOrdersComponent },
    {path: 'new', component: DetailServiceOrderComponent },
    {path: ':id', component: DetailServiceOrderComponent },
    {path: 'purchase-order/:id', component: PurchaseServiceOrderComponent, canDeactivate: [CanDeactivateGuard] }

]);

@NgModule({
    declarations:[
        ServiceOrdersComponent,
        DetailServiceOrderComponent,
        PurchaseServiceOrderComponent,
        DevicesSearchComponent,
        SimCardsSearchComponent,
        MaintenanceComponent,
        
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        TextMaskModule,
        FlexLayoutModule,
        DialogsModule,
        authRouting,
        CustomersModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBKaSWoT0uDq74-a2j3n1EnBp1wmDtw3yM',
            libraries: ["places"]
        }),
        FuseWidgetModule,
        //NgxChartsModule,
        AgmDirectionModule,
        CalendarBlockEventsModule
    ],
    exports: [
        DetailServiceOrderComponent,
        DevicesSearchComponent,
        SimCardsSearchComponent,
        MaintenanceComponent
    ],
    entryComponents: [
        DetailServiceOrderComponent,
        DevicesSearchComponent,
        SimCardsSearchComponent,
        MaintenanceComponent,
        ConfirmDialogComponent,
        InputDialogComponent,
        CustomerPersonalComponent,
        CustomerDocumentsComponent,
        CustomerAddressComponent,
        CalendarInstallerDialogComponent,
        BlockEventsComponent
    ],
    providers: [
        ServiceOrdersSelectedService,
        GoogleMapsAPIWrapper
    ]
})
export class ServiceOrdersModule {}