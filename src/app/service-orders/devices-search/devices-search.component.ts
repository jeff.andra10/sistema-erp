import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { Observable } from 'rxjs/Observable';
import { MatTableDataSource, MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DevicesService } from '../../core/services/devices.service';

@Component({
  selector: 'tnm-devices-search',
  templateUrl: './devices-search.component.html',
})

export class DevicesSearchComponent implements OnInit {

  deviceForm: FormGroup
  searchField: FormControl
  devicesList: Array<any>

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<DevicesSearchComponent>,
    private devicesService: DevicesService,
  ) { }

  ngOnInit() {
    
    this.initForms()
  }

  initForms() {
    this.searchField = this.formBuilder.control('')

    this.deviceForm = this.formBuilder.group({
      searchField: this.searchField
    });

    this.searchField.valueChanges
    .debounceTime(2000)
    .distinctUntilChanged()
    .switchMap(searchTerm =>

      this.devicesService.getDevicesBySearchTerms(searchTerm)
      .catch(error => Observable.from([])))
      .subscribe(devices => {
        this.devicesList = devices.content
      })
  }

  searchDevices(){
    this.searchField.valueChanges
    .debounceTime(0)
    .distinctUntilChanged()
    .switchMap(searchTerm =>

      this.devicesService.getDevicesBySearchTerms(searchTerm)
      .catch(error => Observable.from([])))
      .subscribe(devices => {
        this.devicesList = devices.content
      })
  }

  setDevice(device) {
    this.dialogRef.close({device: device})
  }


  
}