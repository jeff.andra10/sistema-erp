import { Component, OnInit, Injector } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { tap } from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';
import { MatPaginator, MatSort, MatSelect } from '@angular/material';
import { merge } from "rxjs/observable/merge";
import { DatePipe } from '@angular/common'
import { BaseComponent } from '../_base/base-component.component';
import { ServiceOrdersDataSource } from './service-orders.datasource';
import { ServiceOrdersService } from '../core/services/service-orders.service';
import { ServiceOrderType, ServiceOrderStatus, ServiceOrder, ServiceOrderColorStatus, ServiceOrderColorStatusLight } from '../core/models/service-orders.model';
import { fuseAnimations } from '@fuse/animations';
import { ServiceOrdersSelectedService } from './service-orders.selected.service';
import { Router } from '@angular/router';
import { DevicesService } from '../core/services/devices.service';
import { UserService } from 'app/core/services/users.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { User } from "../core/models/user.model"


@Component({
  selector: 'tnm-service-orders',
  templateUrl: './service-orders.component.html',
  styleUrls: ['./service-orders.component.scss'],
  animations: fuseAnimations

})
export class ServiceOrdersComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  resultLength: number = 0
  brandsSelect: number = 0

  displayedColumns = ['statusColor', 'space', 'brand', 'status','licensePlate', 'customerName', 'lifecycle.openDate', 'zipCode', 'scheduling', 'type', 'priority', 'pendency']
  dataSource: ServiceOrdersDataSource
  searchForm: FormGroup
  searchControl: FormControl
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');
  dangerColor: string =  'mat-red-500-bg'
  serviceOrderStatusList: Array<any>
  serviceOrderTypeList: Array<any>
  technicianList: Array<any>
  serviceOrders: Array<any>
  brandsList: Array<any>
  panelOpenState: boolean = false;
  panelOpenStateToday: boolean = false;
  panelOpenStateAll: boolean = false;
  panelOpenStateScheduling: boolean = false;

  widgets: any = {};
  widget5: any = {};

  icon = {
    url: 'assets/images/avatars/alice.jpg',
    scaledSize: {
      width: 20,
      height: 20
    }
  }

  user: User


  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private router: Router,
    private serviceOrdersService: ServiceOrdersService,
    private serviceOrderSelectedService: ServiceOrdersSelectedService,
    private devicesService: DevicesService,
    private usersService: UserService,
    private loadingService: FuseSplashScreenService
  ) {
    super(injector)
    this.serviceOrderSelectedService.setSelected(null)

    this.widget5 = {
      currentRange: 'TW',
      xAxis: true,
      yAxis: true,
      gradient: false,
      legend: false,
      showXAxisLabel: false,
      xAxisLabel: 'Days',
      showYAxisLabel: false,
      yAxisLabel: 'Isues',
      scheme: {
        domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
      },
      onSelect: (ev) => {
        console.log(ev);
      }
    };

    this.widgets = {
      'widget1': {
        'title': 'Agendamentos',
        'data': {
          'count': 0,
          'extra': {
            'label': "Agendamentos do Dia",
            'count': 0,
          }
        }
      },
      'widget2': {
        'title': 'OS de Instalação',
        'data': {
          'count': 0,
          'extra': {
            'label': "OS Criada Hoje",
            'count': 0,
          }
        }
      },
      'widget3': {
        'title': 'OS de Manutenção',
        'data': {
          'count': 0,
          'extra': {
            'label': "OS Criada Hoje",
            'count': 0,
          }
        }
      },
      'widget4': {
        'title': 'OS de Desistalação',
        'data': {
          'count': 0,
          'extra': {
            'label': "OS Criada Hoje",
            'count': 0,
          }
        }
      },
      'widget5': {
        'title': 'Agendamentos do Ano',
        'ranges': {
          'TW': 'Hoje',
          'LW': 'Semana',
          'LM': 'Mês',
          'LY': 'Ano'
        },
        'mainChart': {
          'TW': [
            {
              'name': 'Janeiro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 40,
                },
                {
                  'name': 'Instalações',
                  'value': 20,
                },
                {
                  'name': 'Manuteção',
                  'value': 15,
                },
                {
                  'name': 'Desistalação',
                  'value': 5,
                }
              ]
            },
            {
              'name': 'Fevereiro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Março',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Abril',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Maio',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Junho',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Julho',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Agosto',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Setembro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Outubro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Novembro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Dezembro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            }

          ],
          'LW': [
            {
              'name': 'Janeiro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 10,
                },
                {
                  'name': 'Instalações',
                  'value': 5,
                },
                {
                  'name': 'Manuteção',
                  'value': 15,
                },
                {
                  'name': 'Desistalação',
                  'value': 15,
                }
              ]
            },
            {
              'name': 'Fevereiro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Março',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Abril',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Maio',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Junho',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Julho',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Agosto',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Setembro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Outubro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Novembro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            },
            {
              'name': 'Dezembro',
              'series': [
                {
                  'name': 'Agendamentos',
                  'value': 0,
                },
                {
                  'name': 'Instalações',
                  'value': 0,
                },
                {
                  'name': 'Manuteção',
                  'value': 0,
                },
                {
                  'name': 'Desistalação',
                  'value': 0,
                }
              ]
            }

          ]
        }
      },
      'widget6': {
        'title': 'OS de Compra',
        'data': {
          'count': 0,
          'extra': {
            'label': "OS Criada Hoje",
            'count': 0,
          }
        }
      }
    }
  }

  ngOnInit() {

    localStorage.setItem('hiddenLoading', 'true');
    this.loadingService.show()

    this.brandsList = this.appContext.brands
    this.initForms()
    this.loadDomainListData()


  }


  ngAfterViewInit() {

    this.sort.sortChange.subscribe( ( ) => this.paginator.pageIndex = 0)

    this.dataSource.totalElements$.subscribe(value => {

      if(value > -1){
        this.resultLength = value;
        this.loadingService.hide()
      }
      
    })

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(

       // tap(() => this.user && this.user.id ? this.loadServiceOrdersByUserId(this.user.id) : this.loadServiceOrders())
        tap(() => this.loadServiceOrders())
      )
      .subscribe()

  }


  initForms() {
   
    this.dataSource = new ServiceOrdersDataSource(this.serviceOrdersService, this.utilsService, this.brandsList, this.devicesService)
    //this.dataSource.load(this.brandsSelect, '', '', '', null, 'lifecycle.openDate', 'desc', 0, 10)

      this.searchControl = this.formBuilder.control('')

      this.searchForm = this.formBuilder.group({
        searchControl: this.searchControl,
        type: this.formBuilder.control(''),
        status: this.formBuilder.control(''),
        technician: this.formBuilder.control(''),
        brand:this.formBuilder.control(''),
        viewType: this.formBuilder.control('TABLE')
      });
  
     
    if(localStorage.getItem('filterServiceOders')){

      let filters =  JSON.parse(localStorage.getItem('filterServiceOders'));

      if(filters.searchControl && filters.searchControl.length > 0){
        this.searchControl = this.formBuilder.control(filters.searchControl)
      }else{

        if(filters.brand){
          this.brandsSelect = filters.brand
        }
       
        this.dataSource.load(this.brandsSelect, '', filters.type ? filters.type : "", filters.status ? filters.status : "", filters.technician ? filters.technician : null , 'lifecycle.openDate', 'desc', 0, 10)
      }
     
      this.searchForm = this.formBuilder.group({
        searchControl: this.searchControl,
        type: this.formBuilder.control(filters.type),
        status: this.formBuilder.control(filters.status),
        technician: this.formBuilder.control(filters.technician),
        brand: this.formBuilder.control(filters.brand),
        viewType: this.formBuilder.control('TABLE')
      });

      if( this.searchForm.value.searchControl && this.searchForm.value.searchControl.length > 0){
  
        setTimeout(() => {

            this.paginator.pageIndex = 0;

            if (this.onlyDigits(this.searchForm.value.searchControl)) {
            
              this.loadServiceOrders()
            } else {
              this.loadServiceOrders()
              // this.usersService.getUsersByName(this.searchForm.value.searchControl).subscribe(users => {
              //   if (users.content && users.content.length > 0) {
                
              //     this.user = users.content[0]
              //     this.loadServiceOrdersByUserId(users.content[0].id)
              //   }
              // })
            }
        },300)
      }

    }else{
      this.dataSource.load(this.brandsSelect, '', '', '', null, 'lifecycle.openDate', 'desc', 0, 10)
    }

    localStorage.removeItem('filterServiceOders');
  

    this.searchControl.valueChanges
      .debounceTime(1000)
      .distinctUntilChanged()
      .subscribe(value => {
        this.paginator.pageIndex = 0;

        if (this.onlyDigits(value)) {
         
          this.loadServiceOrders()
        } else {
          // this.usersService.getUsersByName(value).subscribe(users => {
          //   if (users.content && users.content.length > 0) {
             
          //     this.user = users.content[0]
          //    this.loadServiceOrdersByUserId(users.content[0].id)
          //   }
          // })
          this.loadServiceOrders()
        }
      })
  }

  onlyDigits(s) {
    for (let i = s.length - 1; i >= 0; i--) {
      const d = s.charCodeAt(i);
      if (d < 48 || d > 57) return false
    }
    return true
  }

  loadDomainListData() {

    this.serviceOrderTypeList = this.utilsService.stringEnumToKeyValue(ServiceOrderType)
    this.serviceOrderStatusList = this.utilsService.stringEnumToKeyValue(ServiceOrderStatus)
    this.getTechnicianList()
  }

  loadServiceOrders() {
    this.loadingService.show()

    // let direction = 'desc'

    // if(this.searchForm.value.status == "OPEN"){
    //   direction = 'asc'
    // }

    this.dataSource.load(
      this.brandsSelect,
      this.searchControl.value,
      this.searchForm.value.type,
      this.searchForm.value.status != "0" ? this.searchForm.value.status : '',
      this.searchForm.value.technician != "0" ? this.searchForm.value.technician : null,
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize)
  }


  checkPendencyType(element){

    if(element.installation && element.installation.pendencyType  && element.installation.pendencyType ==  'NOT_ENOUGH_DATA'){
      return true;
    }else if(element.uninstallation && element.uninstallation.pendencyType  && element.uninstallation.pendencyType ==  'NOT_ENOUGH_DATA'){
      return true;
    }else if(element.maintenance && element.maintenance.pendencyType  && element.maintenance.pendencyType  ==  'NOT_ENOUGH_DATA'){
      return true;
    }
    
    return false;

  }
 

  loadServiceOrdersByUserId(userId) {

    this.loadingService.show()

    let direction = 'desc'

    if(this.searchForm.value.status == "OPEN"){
      direction = 'asc'
    }

    this.dataSource.load(
      this.brandsSelect,
      userId,
      this.searchForm.value.type,
      this.searchForm.value.status != "0" ? this.searchForm.value.status : '',
      this.searchForm.value.technician != "0" ? this.searchForm.value.technician : null,
      this.sort.active,
      direction,
      this.paginator.pageIndex,
      this.paginator.pageSize)
  }

  checkPlus48Hours(_date2,status){

      if(status != "OPEN"){
        return false
      }

      var date1 = new Date(_date2);
      var date2 = new Date();

      var one_day=1000*60*60*24;
    
      var date1_ms = date1.getTime();
      var date2_ms = date2.getTime();
    
      var difference_ms = date2_ms - date1_ms;
  
      return Math.round(difference_ms/one_day) > 48 ? true : false 
  }

  getTechnicianList() {

    this.technicianList = []
    this.usersService.getTechnician("INSTALLER")
      .catch(error => Observable.from([]))
      .subscribe(technician => {
        this.technicianList.push({ 'name': 'Todos', 'id': 0 })

        technician.content.forEach(technician => {
          technician.name = technician.name.toUpperCase()
          this.technicianList.push(technician)
        });

        this.technicianList.sort(function(a, b){
          return a['name'] < b['name'] ? -1 : a['name'] > b['name'] ? 1 : 0;
        });

      })

  }

  onBrandsSelectionChange(event: any) {
    this.brandsSelect = event.value
    this.loadServiceOrders()
  }

  onListValueChanged() {
    this.loadServiceOrders()
  }

  editServiceOrder(serviceOrder: ServiceOrder) {
   
    let filters = {
      searchControl:this.searchForm.value.searchControl,
      type:this.searchForm.value.type,
      status:this.searchForm.value.status,
      technician:this.searchForm.value.technician,
      brand:this.searchForm.value.brand
    }

    localStorage.setItem('filterServiceOders', JSON.stringify(filters));

    this.serviceOrderSelectedService.setSelected(serviceOrder)
    let url = this.editURLByServiceOrderType(serviceOrder)
    this.router.navigate([url, serviceOrder.id])
  }

  editURLByServiceOrderType(serviceOrder: ServiceOrder): string {

    if (ServiceOrderType[serviceOrder.type] == ServiceOrderType.PURCHASE) {
      return '/service-orders/purchase-order/'
    } else {
      return '/service-orders/'
    }
  }

  checkLicensePlate(serviceOrder: ServiceOrder): string{

    let type = ServiceOrderType[serviceOrder.type]

    if (type == ServiceOrderType.INSTALLATION) {
      return serviceOrder.relationships.installation.vehicle ? serviceOrder.relationships.installation.vehicle.licensePlate.toUpperCase() : '-'
    }
    else if (type == ServiceOrderType.UNINSTALLATION) {
      return serviceOrder.relationships.uninstallation.vehicle ? serviceOrder.relationships.uninstallation.vehicle.licensePlate.toUpperCase() : '-'
    }
    else if (type == ServiceOrderType.MAINTENANCE) {
      return serviceOrder.relationships.maintenance.vehicle ? serviceOrder.relationships.maintenance.vehicle.licensePlate.toUpperCase() : '-'
    }

    return '-'
  }

  customerNameByServiceOrderType(serviceOrder: ServiceOrder): string {

    let type = ServiceOrderType[serviceOrder.type]

    if (type == ServiceOrderType.PURCHASE) {
      return serviceOrder.relationships.purchase.business ? serviceOrder.relationships.purchase.business.name : ''
    }
    else if (type == ServiceOrderType.INSTALLATION) {
      return serviceOrder.relationships.installation.customer ? serviceOrder.relationships.installation.customer.name : (serviceOrder.relationships.installation && serviceOrder.relationships.installation.user) ? serviceOrder.relationships.installation.user.name : ''
    }
    else if (type == ServiceOrderType.UNINSTALLATION) {
      return serviceOrder.relationships.uninstallation.customer ? serviceOrder.relationships.uninstallation.customer.name : (serviceOrder.relationships.uninstallation && serviceOrder.relationships.uninstallation.user) ? serviceOrder.relationships.uninstallation.user.name : ''
    }
    else if (type == ServiceOrderType.MAINTENANCE) {
      return serviceOrder.relationships.maintenance.customer ? serviceOrder.relationships.maintenance.customer.name : (serviceOrder.relationships.maintenance && serviceOrder.relationships.maintenance.user) ? serviceOrder.relationships.maintenance.user.name : ''
    }

    return ''
  }
  maskPostalCode(number) {
    number += "";
    if (number.length == 8) {
      number = number.replace(/\D/g, '');
      number = number.match(/^(\d{5})(\d{3})$/);
      number = (!number) ? null : number[1] + '-' + number[2];
    } else {
      number = number.replace(/\D/g, '');
      number = number.match(/^(\d{5})(\d{2})$/);
      number = (!number) ? null : number[1] + '-' + number[2] + '0';
    }

    return number;
  }

  serviceOrderColorEnumToString(value): string {
    return ServiceOrderColorStatus[value]
  }

  serviceOrderColorLightEnumToString(value): string {
    return ServiceOrderColorStatusLight[value]
  }

}