import { Component, ViewEncapsulation, Inject, ViewChild } from '@angular/core';
import { OnInit, Injector, AfterViewInit } from '@angular/core';

import { extend } from '@syncfusion/ej2-base';
import {
    EventSettingsModel, View, GroupModel, TimelineViewsService, TimelineMonthService, DayService,
    ResizeService, DragAndDropService, ResourceDetails, ScheduleComponent
} from '@syncfusion/ej2-angular-schedule';

//import {EventSettingsModel, View, GroupModel,ResourceDetails, ScheduleComponent} from '@syncfusion/ej2-angular-schedule';
//, TimelineViewsService, TimelineMonthService, DayService,  ResizeService, DragAndDropService,

import { BaseComponent } from '../../_base/base-component.component';
import { ServiceOrdersService } from '../../core/services/service-orders.service';
import * as moment from 'moment';
import { CalendarInstallerDialogComponent } from '../../dialogs/calendar-installer-dialog/calendar-installer-dialog.component';
import { MatDialog , MatDialogRef} from '@angular/material';
import {  AssignServiceOrder } from '../../core/models/service-orders.model';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { ServiceOrdersSelectedService } from '../../service-orders/service-orders.selected.service';
import {  MatSnackBar } from '@angular/material';
import { DOCUMENT } from '@angular/platform-browser';
import { InstallersService } from '../../core/services/installers.service';
import { ConfirmDialogComponent } from '../../dialogs/confirm-dialog/confirm-dialog.component'

@Component({
    selector: 'calendar-control-content',
    templateUrl: 'block-events.component.html',
    styleUrls: ['block-events.component.css'],
    encapsulation: ViewEncapsulation.None,

})
export class BlockEventsComponent extends BaseComponent implements OnInit, AfterViewInit{
    @ViewChild('scheduleObj')
    public scheduleObj: ScheduleComponent;
    
    avatar : string = "assets/images/avatars/profile.jpg"
    data: Object[] = <Object[]>extend([], [], null, true);
    selectedDate: Date = new Date();
    currentView: View = 'Day';
    employeeDataSource: Object[]
    address: any
    points: any
    installers: any
    serviceOrder: any
    eventSelected : any = null

    deleteAppointment : boolean = false

    isEvent: boolean = false
    
    group: GroupModel = { enableCompactView: false, resources: ['Employee'] };
    allowMultiple: Boolean = false;
    
    eventSettings: EventSettingsModel;
   
    constructor(
        @Inject(DOCUMENT) 
        private document: any,
        private injector: Injector,
        private serviceOrdersService: ServiceOrdersService,
        private loadingService: FuseSplashScreenService,
        private serviceOrderSelectedService: ServiceOrdersSelectedService,
        private snackbar: MatSnackBar,
        public dialog: MatDialog,
        private installersService: InstallersService
    )
    { super(injector)}

    ngOnInit() {
        localStorage.setItem('hiddenLoading','true');
    }

    ngAfterViewInit() {

    }
    
    getEmployeeName(value: ResourceDetails): string {
        return (value as ResourceDetails).resourceData[(value as ResourceDetails).resource.textField] as string;
    }
    getEmployeeDesignation(value: ResourceDetails): string {
        let resourceName: string =
            (value as ResourceDetails).resourceData[(value as ResourceDetails).resource.textField] as string;
        return (value as ResourceDetails).resourceData.Designation as string;
    }
    getEmployeeImageName(value: ResourceDetails): string {
        return this.getEmployeeName(value).toLowerCase();
    }

    generatorColor(){

        var hex = '0123456789ABCDEF';
        var color = '#';
        
        for (var i = 0; i < 6; i++ ) {
            color += hex[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    public setEmployeeDataSource(grouped){

        this.employeeDataSource = [];

        grouped.forEach(group => {
        
            let name = group.name.split(' ')
            let fullName = ''

            if(name.length > 1){
                fullName = name[0]+' '+name[1]
            }else{
                fullName = group.name
            }
            
            this.employeeDataSource.push({ Text: fullName.toUpperCase(), Id: group.id, GroupId: 1, Color: group.color, Designation: 'Instalador' , Name:group.name.toUpperCase()})
            
        });

        setTimeout(() => {
            this.currentView = 'Day';
        }, 0)

    }

    public setCurrentView(){
       
        this.currentView = 'Day';
       setTimeout(() => {
            this.currentView = 'TimelineDay';
       },0)
    }

    onChange(args: any): void{
       //console.log('change date:',args)
    }

    onActionBegin(args: any): void{
       //console.log('onActionBegin:',args)
    }

    onActionComplete(args: any): void{
       //console.log('onActionComplete:',args)
    }

    onActionFailure(args: any): void{
        //console.log('onActionFailure:',args)
    }

    onDrag(args: any): void{
    //console.log('onDrag:',args)
    }

    onDragStart(args: any): void{
        //console.log('onDragStart:',args)
    }

    onDragStop(args: any): void{
        //console.log('onDragStop:',args)
        this.assignServiceOrder(args.data)
    }

    
    onBeforeAppointmentCreate(args: any){
       // console.log('onBeforeAppointmentCreate:',args)
    }

    onCellClick(event: any){
        this.eventSelected = event;
    }
   

    onEventRendered(args) { 

        if (args.data.Id == this.serviceOrder.id) { 
            args.element.style.border = '2px solid black';
        } 
    } 

    onPopupOpen(args: any): void{
        
        if (args.type === 'QuickInfo') {
           
            
            if( this.eventSelected && this.serviceOrder && this.serviceOrder.status != 'OPEN'){
                args.cancel = true;
                this.eventSelected = null;
            }

        }

        if (args.type === 'Editor') {

            console.log('args:',args);

            this.dialog.open(CalendarInstallerDialogComponent, {
                panelClass: 'event-form-dialog', data: {
                    width: '900px',
                    height: '1200px', 
                    title: 'Transferir Agendamento', 
                    message: 'Transferir Agendamento', 
                    placeholder: 'Motivo', 
                    event:event, 
                    address: args.data.address, 
                    points:this.points, 
                    installers: this.employeeDataSource, 
                    installer:args.data.EmployeeId,
                    serviceOrder: this.serviceOrder,
                    date:args.data.StartTime,
                    Id:args.data.Id,
                    selectedTime:args.data.StartTime,
                    appointmentId:args.data.appointmentId,
                    hour:null,
                    period:null,
                    edit:true
                }
            })
            .afterClosed()
            .subscribe(response => {
            if (response && response.data && response.data.confirm == true) {
                
            }
            });
           args.cancel = true;
        }

        if(args.type === 'DeleteAlert'){
            args.cancel = true;


            this.dialog
            .open(ConfirmDialogComponent, {
              panelClass: 'event-form-dialog', data: {
                width: '600px',
                height: '300px', title: 'Remover Agendamento', message: 'Deseja remover esse agendamento dessa Orderm de Serviço'
              }
            })
            .afterClosed()
            .subscribe(response => {
              if (response && response.data && response.data.confirm == true) { 
                this.installersService.deleteAppointment(args.data.event.appointmentId)
                .subscribe(result => {

                    this.snackbar.open('Ordem de Serviço removida com sucesso', '', {
                        duration: 8000
                    })
                    
                   this.serviceOrderSelectedService.reloadCalendar(args.data.event.StartTime)
                   this.deleteAppointment = true;
                })
                
              } else { 
                
                  
              }
            });
        }
        
    }


    clickCalendarEvent(event){

        if(event.toElement && (event.toElement.className == "e-btn-icon e-icon-prev e-icons" || event.toElement.className == "e-btn-icon e-icon-next e-icons")){
           
            var elementDate = document.getElementsByClassName('e-tbar-btn-text');
            var elementDateDivs = Array.prototype.filter.call(elementDate, function(element) {
                return element.nodeName === 'DIV';
            });

            let  textDate = elementDateDivs[0].textContent;
            
            this.loadingService.show()
            this.serviceOrderSelectedService.reloadCalendar(new Date(textDate))

        }

        if(event.toElement && event.toElement.className == "e-day e-ripple"){
           this.loadingService.show()
           this.serviceOrderSelectedService.reloadCalendar(new Date(event.toElement.title))
        }

        var elements = document.getElementsByClassName('e-quick-popup-wrapper');
        var elementDivs = Array.prototype.filter.call(elements, function(element) {
            return element.nodeName === 'DIV';
        });
      
        let  textContent = elementDivs[0].textContent;

        let time = null;
        let name = null;
        let installer = null;
        let date = null;
        let period = null;
        let hour = null;


        if(( this.eventSelected && textContent.length > 0) || this.deleteAppointment ){

            
            this.eventSelected = null;

            let arrayText = textContent.split(')');

            let arrayDate = arrayText[0].split('(')
        
            let arrayTime = arrayDate[1].split('-')
        
            let arrayHour = arrayTime[0].split(' ')
            
            period = arrayHour[1].replace(' ','')
           
            if(period.indexOf('PM') > -1){
                time = arrayDate[0]+' '+this.convertHoursTime(arrayHour[0])
                hour = this.convertHoursTime(arrayHour[0])
            }else{
                time = arrayDate[0]+' '+arrayHour[0]
                hour = arrayHour[0]
            }

            let _date =  moment(time).toISOString().split('T')

            date = _date[0]+'T'+hour+':00.000Z'
 
            name = arrayText[1].replace('More DetailsSave','')

            let index =this.employeeDataSource.map((item) => { return item['Name'];}).indexOf(name)
           
        
            if(index > -1){
                installer = this.employeeDataSource[index]
            }else{
                index =this.employeeDataSource.map((item) => { return item['Text'];}).indexOf(name)
                
                if(index > -1){
                    installer = this.employeeDataSource[index]
                }
            }

            elementDivs[0].innerHTML = '';

            console.log('O.S:',this.serviceOrder);
           
            if((this.serviceOrder && this.serviceOrder.status == 'OPEN') || this.deleteAppointment){

                this.deleteAppointment = false;
                
                this.dialog.open(CalendarInstallerDialogComponent, {
                    panelClass: 'event-form-dialog', data: {
                        width: '900px',
                        height: '1200px', 
                        title: 'Transferir Agendamento', 
                        message: 'Transferir Agendamento', 
                        placeholder: 'Motivo', 
                        event:event, 
                        address: this.address, 
                        points:this.points, 
                        installers: this.employeeDataSource, 
                        installer:installer,
                        serviceOrder: this.serviceOrder,
                        date:date,
                        selectedTime:moment(time).toDate(),
                        hour:hour,
                        period:period,
                        edit:false
                    }
                })
                .afterClosed()
                .subscribe(response => {
                if (response && response.data && response.data.confirm == true) {
                    
                }
                });

            }

        }
  

    }

    convertHoursTime(time){
        let arrayTime = time.split(':');
        if(arrayTime[0] == 12){
            return (arrayTime[0])  +':00'
        }
        return (parseInt(arrayTime[0]) + 12) +':00'
    }

    utcTimeZone(time: Date, hour: number) {

        let date = moment(time).utcOffset(-3);
        date.set({ hour: hour, minute: 0, second: 0, millisecond: 0 })
        let ndate = date.toDate()
    
        return ndate
    }

    public setMapPoints(installers,points){
        this.points = points
        this.installers = installers
    }

    public setServiceOrder(serviceOrder){
       this.serviceOrder = serviceOrder

       let date = new Date();

       if (this.serviceOrder && this.serviceOrder.maintenance && this.serviceOrder.maintenance.scheduledDate) {
        date = new Date(this.serviceOrder.maintenance.scheduledDate);
      } else if (this.serviceOrder && this.serviceOrder.uninstallation && this.serviceOrder.uninstallation.scheduledDate) {
        date = new Date(this.serviceOrder.uninstallation.scheduledDate);
      } else if (this.serviceOrder && this.serviceOrder.installation && this.serviceOrder.installation.scheduledDate) {
         date = new Date(this.serviceOrder.installation.scheduledDate);
      }
     
       this.selectedDate = date;
    }

    public setCalendarEvents(events){

        let eventsData = [];

        for(var i =0; i< events.length; i++){

            let startTime = new Date(events[i].scheduledDate);
            let endTime = moment(startTime).add(1, 'hours').toDate(); 

            let event =     {
                Id: events[i].serviceOrderId,
                Subject: events[i].origin,
                StartTime: startTime ,
                EndTime: endTime,
                IsAllDay: false,
                IsBlock: false,
                address:events[i].address,
                EmployeeId: events[i].technician,
                appointmentId:events[i].appointmentId
            }

            eventsData.push(event)

        }

       let data: Object[] = <Object[]>extend([], eventsData, null, true);
        this.eventSettings = {
            dataSource: data
        };

        this.currentView = 'TimelineDay';
    }

    public setEventAddress(address){
        this.address = address
    }

    assignServiceOrder(data) {

    
        let appointmentId = data.appointmentId;

        var currentDate = new Date()
        
        if (currentDate > data.StartTime) {

            this.serviceOrderSelectedService.reloadCalendar(data.StartTime)
            
            this.snackbar.open('Informe uma data superior a data atual', '', {
                duration: 8000
            })

            return false
        }


       let hour = data.StartTime.getHours()
       let day =  data.StartTime.getDate()
       let month = data.StartTime.getMonth() + 1
       let year =  data.StartTime.getFullYear()

       if(month <10){
          month = '0'+month
       }

       let date = year+'-'+month+'-'+day+'T'+hour+':00:00'

        let assignData = new AssignServiceOrder()
            assignData.loggedUser = this.appContext.session.user.id
            assignData.technician = data.EmployeeId
            assignData.scheduledDate = date
            assignData.street = data.address.street
            assignData.number =data.address.number
            assignData.complement = data.address.complement
            assignData.district = data.address.district
            assignData.city = data.address.city
            assignData.state = data.address.state
            assignData.postalCode = this.utilsService.stringToNumber(data.address.postalCode),
            assignData.reference = data.address.reference
            assignData.duringBusinessHours = false
            assignData.deslocationTime = (30 * 60).toString()
            assignData.serviceLenght = 'ONE_HOUR'
            assignData.period =  hour > 12 ? "AFTERNOON" : "MORNING"
            assignData.beta = true



        this.dialog.open(ConfirmDialogComponent, {
          panelClass: 'event-form-dialog', data: {
            width: '600px',
            height: '300px', title: 'Confirmar Reagendamento', message: 'Deseja confirmar o reagendamento dessa Orderm de Serviço'
          }
        })
        .afterClosed()
        .subscribe(response => {
          if (response && response.data && response.data.confirm == true) { 

            this.loadingService.show()

            this.installersService.deleteAppointment(appointmentId)
            .subscribe(result => {

                    this.serviceOrdersService.assignServiceOrder(data.Id, assignData)
                    .subscribe(result => {
            
                    this.snackbar.open('Ordem de Serviço reagendada com sucesso', '', {
                        duration: 8000
                    })
                    
                    this.serviceOrderSelectedService.reloadCalendar(data.StartTime)
                    
                    },
                    response => { 
                        this.serviceOrderSelectedService.reloadCalendar(data.StartTime)
                        this.snackbar.open(response.error.message, '', { duration: 8000 })
                    })

            })
          } else { 
            this.loadingService.show()
            this.serviceOrderSelectedService.reloadCalendar(data.StartTime)
              
          }
        });
        
      }
}


