import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : '',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            { id: 'dashboard', title: 'Dashboard', type: 'item', icon: 'dashboard', url: '/dashboard' },
            { id: 'devices', title: 'Rastreadores', type: 'item', icon: 'mobile_friendly', url: 'devices'},
            { id: 'customers', title: 'Clientes', type: 'item', icon: 'group', url: 'customers' },
            { id: 'vehicles', title: 'Veículos', type: 'item', icon: 'directions_car', url: 'vehicles' },
            { id: 'contracts', title: 'Contratos', type: 'item', icon: 'description', url: 'contracts' }, 
            { id: 'transactions', title: 'Financeiro', type: 'item', icon: 'bar_chart', url: 'transactions' },   
            { id: 'sale', title: 'Vendas', type: 'item', icon: 'local_offer',url: '/sales'},
            { id: 'serviceOrders', title: 'Ordens de Serviço - Beta', type: 'item', icon: 'assignment', url: 'service-orders'},
            {   
                id: 'insurance',
                title: 'Seguros',
                type: 'collapsable', 
                icon: 'receipt',
                children : [
                    { id: 'quotes', title: 'Cotações', type: 'item', icon: 'receipt',url: 'insurance/quotes'},
                    { id: 'proposal', title: 'Propostas', type: 'item', icon: 'receipt',url: 'insurance/proposal'},
                    { id: 'policies', title: 'Apólices', type: 'item', icon: 'description',url: 'insurance/policies'}
                ]
            },
            {
                id       : 'admin',
                title    : 'Administração',
                type     : 'collapsable',
                icon     : 'settings',
                children : [
                    { id: 'offers', title: 'Ofertas', type: 'item', icon: 'library_books', url: '/admin/offers'},
                    { id: 'products', title: 'Produtos', type: 'item', icon: 'apps', url: '/admin/products' },
                    { id: 'services', title: 'Serviços', type: 'item', icon: 'headset_mic', url: '/admin/services' },  
                    { id: 'simcards', title: 'Sim Cards', type: 'item', icon: 'sim_card', url: '/admin/simcards' },     
                    { id: 'brands', title: 'Marcas', type: 'item', icon: 'bookmarks', url: '/admin/brands' },
                    // {   
                    //     id: 'permissions',
                    //     title: 'Permissões',
                    //     type: 'collapsable', 
                    //     icon: 'security',
                    //     children : [
                    //         { id: 'permissions_users', title: 'Perfis de Acesso', type: 'item', icon: 'security',url: '/admin/permissions/group'},
                    //         { id: 'access_profiles', title: 'Permissões por Marcas', type: 'item', icon: 'group',url: '/admin/permissions/brands'}
                    //     ]
                    // },
                    { id: 'suppliers', title: 'Fornecedores', type: 'item', icon: 'store', url: '/admin/suppliers'},
                    {   
                        id: 'users',
                        title: 'Usuários',
                        type: 'collapsable', 
                        icon: 'supervised_user_circle',
                        children : [
                            { id: 'administrators', title: 'Administradores', type: 'item', icon: 'assignment_ind',url: 'users/adminstrators'},
                            { id: 'operators', title: 'Operadores', type: 'item', icon: 'assignment_ind',url: 'users/operators'},
                            { id: 'installers', title: 'Instaladores', type: 'item', icon: 'assignment_ind',url: 'users/installers'},
                            { id: 'brokers', title: 'Corretores', type: 'item', icon: 'assignment_ind',url: 'users/brokers'}
                        ]
                    }
                ]
            },
            
        ]
    }
];


export const navigationAssociation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : '',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            { id: 'dashboard', title: 'Dashboard', type: 'item', icon: 'dashboard', url: '/dashboard' },
            { id: 'devices', title: 'Rastreadores', type: 'item', icon: 'mobile_friendly', url: '/devices'},
            { id: 'customers', title: 'Clientes', type: 'item', icon: 'group', url: 'customers' },
            { id: 'vehicles', title: 'Veículos', type: 'item', icon: 'directions_car', url: 'vehicles' },
            { id: 'contracts', title: 'Contratos', type: 'item', icon: 'description', url: 'contracts' },    
            { id: 'sale', title: 'Vendas', type: 'item', icon: 'local_offer',url: '/sales'},
            { id: 'purchases', title: 'Compras', type: 'item', icon: 'shopping_cart',url: '/purchases'},
            { id: 'serviceOrders', title: 'Ordens de Serviço', type: 'item', icon: 'assignment', url: 'service-orders'},
            {
                id       : 'admin',
                title    : 'Administração',
                type     : 'collapsable',
                icon     : 'settings',
                children : [
                    { id: 'offers', title: 'Ofertas', type: 'item', icon: 'library_books', url: '/admin/offers'},
                    { id: 'products', title: 'Produtos', type: 'collapsable', icon: 'apps', children : [
                        { id: 'productsCreate', title: 'Meus Produtos', type: 'item', icon: 'apps', url: '/admin/products' },
                        { id: 'activationProducts', title: 'Ativação Produtos', type: 'item', icon: 'apps', url: '/admin/activation/product' }
                     ] },
                     { id: 'services', title: 'Serviços', type: 'collapsable', icon: 'headset_mic', children : [
                        { id: 'servicesCreate', title: 'Meus Serviços', type: 'item', icon: 'headset_mic', url: '/admin/services' }, 
                        { id: 'activationServices', title: 'Ativação Serviços', type: 'item', icon: 'headset_mic', url: '/admin/activation/service' }
                     ] },
                      
                    { id: 'brands', title: 'Marcas', type: 'item', icon: 'bookmarks', url: '/admin/brands' },
                    {   
                        id: 'users',
                        title: 'Usuários',
                        type: 'collapsable', 
                        icon: 'supervised_user_circle',
                        children : [
                            { id: 'administrators', title: 'Administradores', type: 'item', icon: 'assignment_ind',url: 'users/adminstrators'},
                            { id: 'operators', title: 'Operadores', type: 'item', icon: 'assignment_ind',url: 'users/operators'},
                            { id: 'installers', title: 'Instaladores', type: 'item', icon: 'assignment_ind',url: 'users/installers'},
                            { id: 'brokers', title: 'Corretores', type: 'item', icon: 'assignment_ind',url: 'users/brokers'}
                        ]
                    }
                   
                ]
            }
        ]
    }
];

export const navigationBroker: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : '',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            {   
                id: 'insurance',
                title: 'Seguros',
                type: 'collapsable', 
                icon: 'receipt',
                children : [
                    { id: 'quotes', title: 'Cotações', type: 'item', icon: 'receipt',url: 'insurance/quotes'},
                    { id: 'proposal', title: 'Propostas', type: 'item', icon: 'receipt',url: 'insurance/proposal'},
                    { id: 'policies', title: 'Apólices', type: 'item', icon: 'description',url: 'insurance/policies'}
                ]
            }
        ]
    }
];

export const navigationOperator: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : '',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            { id: 'customers', title: 'Clientes', type: 'item', icon: 'group', url: 'customers' },
            { id: 'sale', title: 'Vendas', type: 'item', icon: 'local_offer',url: '/sales'},
            { id: 'serviceOrders', title: 'Ordens de Serviço', type: 'item', icon: 'assignment', url: 'service-orders'}
        ]
    }
];
