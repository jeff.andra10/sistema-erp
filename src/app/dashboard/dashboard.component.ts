import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { BaseComponent } from '../_base/base-component.component';
import { BrandsService } from '../core/services/brands.service';
import { SuppliersService } from '../core/services/suppliers.service';
import { Brand } from '../core/models/brand.model';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { Subscription } from 'rxjs/Subscription';
import { fuseAnimations } from '@fuse/animations';
import { DevicesService } from '../core/services/devices.service'
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FuseConfig } from '@fuse/types';
import { FuseConfigService } from '@fuse/services/config.service';
import { PalletColors } from '../../app/core/models/pallet.model';
import { SessionModel } from '../../app/core/models/session.model';
import { DocumentsService } from '../../app/core/services/documents.service';
import { MatDialog } from '@angular/material';
import { FirstAccessComponent } from '../auth/login/first-access/first-access.component';
import { PlanColorStatus } from 'app/core/models/plan.model';


@Component({
    selector: 'tnm-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class DashboardComponent extends BaseComponent implements OnInit, AfterViewInit {
    allBrands: Array<object> = []
    appContextOnInitWatcher: Subscription;
    isLoadBrand: boolean = false
    primaryColor: string;
    urlLogo: String = "assets/images/logos/fuse.svg"
    session: SessionModel
    aliasArray = []

    report: any;
    widget5: any = {};
    widget11: any = {};
    widget12: any = {};
    widget1: any = {};
    widgets: any = {};

    constructor(protected injector: Injector,
        private brandsService: BrandsService,
        private suppliersService: SuppliersService,
        private loadingService: FuseSplashScreenService,
        private devicesService: DevicesService,
        private titleService: Title,
        private router: Router,
        private dialog: MatDialog,
        private fuseConfig: FuseConfigService,
        private documentsService: DocumentsService
    ) {
        super(injector)

        /**
         * Widget 5
         */
        this.widget5 = {
            currentRange: 'TW',
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: 'Days',
            showYAxisLabel: false,
            yAxisLabel: 'Isues',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            onSelect: (ev) => {
                console.log(ev);
            }
        };

        this.widget11 = {
            currentRange: 'TW',
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: 'Days',
            showYAxisLabel: false,
            yAxisLabel: 'Isues',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            onSelect: (ev) => {
                console.log(ev);
            }
        };

        this.widget12 = {
            currentRange: 'TW',
            xAxis: true,
            yAxis: true,
            gradient: false,
            legend: false,
            showXAxisLabel: false,
            xAxisLabel: 'Days',
            showYAxisLabel: false,
            yAxisLabel: 'Isues',
            scheme: {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            onSelect: (ev) => {
                console.log(ev);
            }
        };

        this.widgets = {
            'widget1': {
                'title': 'Total',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Adicionados Hoje',
                        'count': 0,
                    }
                }
            },
            'widget2': {
                'title': 'Estoque',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Cadastrados Hoje',
                        'count': 0,
                    }
                }
            },
            'widget3': {
                'title': 'Ativos',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Ativados Hoje',
                        'count': 0,
                    }
                }
            },
            'widget4': {
                'title': 'Cancelados',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Cancelados hoje',
                        'count': 0,
                    }
                }
            },
            'widget5': {
                'title': 'Rastreadores sem comunicação',
                'ranges': {
                    'TW': 'Rastreadores'
                },
                'mainChart': {
                    'TW': [
                        {
                            'name': '1 Hora',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        },
                        {
                            'name': '6 Horas',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        },
                        {
                            'name': '24 Horas',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        },
                        {
                            'name': '48 Horas',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        },
                        {
                            'name': '72 Horas',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        },
                        {
                            'name': '72 Horas +',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        }

                    ]
                }
            },
            'widget6': {
                'title': 'Recebidos',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Recebidos hoje',
                        'count': 0,
                    }
                }
            },
            'widget7': {
                'title': 'Retornado',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Retornados hoje',
                        'count': 0,
                    }
                }
            }
            ,
            'widget8': {
                'title': 'Testados',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Testados hoje',
                        'count': 0,
                    }
                }
            }
            ,
            'widget9': {
                'title': 'Manutenção',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Foi para manutenção hoje',
                        'count': 0,
                    }
                }
            },
            'widget10': {
                'title': 'Desinstalados',
                'data': {
                    'count': 0,
                    'extra': {
                        'label': 'Desinstalados hoje',
                        'count': 0,
                    }
                }
            },
            'widget11': {
                'title': 'Incidentes',
                'ranges': {
                    'TW': 'Incidentes'
                },
                'mainChart': {
                    'TW': [
                        {
                            'name': 'Total',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        },
                        {
                            'name': 'Abertos',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        },
                        {
                            'name': 'Resolvidos',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        },
                        {
                            'name': 'Cancelados',
                            'series': [
                                {
                                    'name': '',
                                    'value': 0,
                                }
                            ]
                        }
                    ]
                }
            },
            'widget12': {
                'title': 'Rastreadores',
                'ranges': {
                    'TW': 'Rastreadores'
                },
                'mainChart': {
                    'TW': []
                }
            },
        }
    }

    ngOnInit() {

        localStorage.setItem('hiddenLoading', 'true');
        this.loadingService.show()

        var session = this.appContext.session;

        if (!session || session == undefined) {

            let href = this.router['location']['_platformStrategy']['_platformLocation']['_doc']['baseURI'];

            let hrefArray = href.split("//");

            this.aliasArray = hrefArray[1].split(".");

            let jsonObj: any = JSON.parse(sessionStorage.getItem('loggedUser'));

            this.session = jsonObj;

            this.appContext.initContext(this.session)

            if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'OPERATOR' || this.appContext.loggedUser.profile == 'BROKER' || this.appContext.loggedUser.profile == 'ADMINISTRATOR') {

                this.getBrands()

                if (this.appContext && this.appContext.isLoggedUserTracknme()) {
                    localStorage.setItem('isUserTracknme', 'true');
                } else {
                    localStorage.setItem('isUserTracknme', 'false');
                }

                if (this.session.user.terms) {

                    if (this.aliasArray[0] == 'localhost:4200/' || this.aliasArray[0] == 'dev') {
                        this.loadConfig(true)
                    } else {
                        this.setColorBrand()
                    }

                } else {

                    if (this.aliasArray[0] == 'localhost:4200/' || this.aliasArray[0] == 'dev') {
                        this.loadConfig(false)
                    }

                    this.firstAccessDialog()
                }
            }

        } 

        this.getSuppliers()
        this.getBrands()
        this.getReportDevices()
        

    }

    ngAfterViewInit() {

    }

    //TODO: mover para classe de inicializacao do app
    getSuppliers() {

        if (sessionStorage.getItem('suppliers') != null) {
            return
        }

        this.suppliersService.getSuppliers()
            .subscribe(suppliers => sessionStorage.setItem('suppliers', JSON.stringify(suppliers.content)))
    }

    //TODO: mover para classe de inicializacao do app
    getBrands() {

        if (this.appContext.brands != null && this.appContext.brands.length > 0) {
            return
        }

        this.isLoadBrand = true;

        this.brandsService.getBrandsByTree('', this.appContext.loggedUser.brand)
            .subscribe(brands => {
                if (brands.content && brands.content.length > 0) {
                    this.makeBrandList(brands.content)
                    this.appContext.brands = <[Brand]>this.allBrands
                    this.isLoadBrand = false;
                }
            })
    }

    getReportDevices() {
        this.devicesService.getReportDevicesByBrand(this.appContext.loggedUser.brand).subscribe(report => {
            if (report && report.content) {

                this.report = report.content[0]

                //Total
                this.widgets.widget1.data.count = this.report ? this.report.total : 0;
                this.widgets.widget1.data.extra.count = this.report ? this.report.inactiveTodayTotal : 0;
                //Estoque
                this.widgets.widget2.data.count = this.report ? this.report.inactiveTotal : 0;
                this.widgets.widget2.data.extra.count = this.report ? this.report.inactiveTodayTotal : 0;
                //Ativos
                this.widgets.widget3.data.count = this.report ? this.report.activeTotal : 0;
                this.widgets.widget3.data.extra.count = this.report ? this.report.activeTodayTotal : 0;
                //Cancelados
                this.widgets.widget4.data.count = this.report ? this.report.canceledTotal : 0;
                this.widgets.widget4.data.extra.count = this.report ? this.report.canceledTodayTotal : 0;
                //Recebidos
                this.widgets.widget6.data.count = this.report ? this.report.receivedTotal : 0;
                this.widgets.widget6.data.extra.count = this.report ? this.report.receivedTodayTotal : 0;
                //Retornado
                this.widgets.widget7.data.count = this.report ? this.report.returnedTotal : 0;
                this.widgets.widget7.data.extra.count = this.report ? this.report.returnedTodayTotal : 0;
                //Testados
                this.widgets.widget8.data.count = this.report ? this.report.testedTotal : 0;
                this.widgets.widget8.data.extra.count = this.report ? this.report.testedTodayTotal : 0;
                //Manutenção
                this.widgets.widget9.data.count = this.report ? this.report.underMaintenanceTotal : 0;
                this.widgets.widget9.data.extra.count = this.report ? this.report.underMaintenanceTodayTotal : 0;
                //Desistalados
                this.widgets.widget10.data.count = this.report ? this.report.uninstalledTotal : 0;
                this.widgets.widget10.data.extra.count = this.report ? this.report.uninstalledTodayTotal : 0;

                //1 Hora
                this.widgets.widget5.mainChart.TW[0].series[0].value = this.report ? this.report.noCommunication1hTotal : 0;

                //6 Horas
                this.widgets.widget5.mainChart.TW[1].series[0].value = this.report ? this.report.noCommunication6hTotal : 0;

                //24 Horas
                this.widgets.widget5.mainChart.TW[2].series[0].value = this.report ? this.report.noCommunication24hTotal : 0;

                //48 Horas
                this.widgets.widget5.mainChart.TW[3].series[0].value = this.report ? this.report.noCommunication48hTotal : 0;

                //72 Horas
                this.widgets.widget5.mainChart.TW[4].series[0].value = this.report ? this.report.noCommunication72hTotal : 0;

                //72 Horas +
                this.widgets.widget5.mainChart.TW[5].series[0].value = this.report ? this.report.noCommunication72hMoreTotal : 0;

                //Incidentes Total
                this.widgets.widget11.mainChart.TW[0].series[0].value = this.report ? this.report.incidentTotal : 0;

                //Incidentes Abertos
                this.widgets.widget11.mainChart.TW[1].series[0].value = this.report ? this.report.incidentOpenTotal : 0;

                //Incidentes Resolvidos
                this.widgets.widget11.mainChart.TW[2].series[0].value = this.report ? this.report.incidentResolvedTotal : 0;

                //Incidentes Cancelados
                this.widgets.widget11.mainChart.TW[3].series[0].value = this.report ? this.report.incidentCancelledTotal : 0;



                this.loadData();



            }
        })
    }

    //TODO: mover para classe de inicializacao do app
    private makeBrandList(brandList) {

        let filteredBrands: Array<object>

        brandList.forEach(brand => {
            this.allBrands.push(brand);
            if (brand.brandChildren) {
                this.makeBrandList(brand.brandChildren);
            }
        });
        this.utilsService.arrayRemoveDuplicates(this.allBrands, 'id')
    }

    loadData() {

        //Total
        this.widgets.widget1.data.count = this.report ? this.report.total : 0;
        this.widgets.widget1.data.extra.count = this.report ? this.report.inactiveTodayTotal : 0;
        //Estoque
        this.widgets.widget2.data.count = this.report ? this.report.inactiveTotal : 0;
        this.widgets.widget2.data.extra.count = this.report ? this.report.inactiveTodayTotal : 0;
        //Ativos
        this.widgets.widget3.data.count = this.report ? this.report.activeTotal : 0;
        this.widgets.widget3.data.extra.count = this.report ? this.report.activeTodayTotal : 0;
        //Cancelados
        this.widgets.widget4.data.count = this.report ? this.report.canceledTotal : 0;
        this.widgets.widget4.data.extra.count = this.report ? this.report.canceledTodayTotal : 0;
        //Recebidos
        this.widgets.widget6.data.count = this.report ? this.report.receivedTotal : 0;
        this.widgets.widget6.data.extra.count = this.report ? this.report.receivedTodayTotal : 0;
        //Retornado
        this.widgets.widget7.data.count = this.report ? this.report.returnedTotal : 0;
        this.widgets.widget7.data.extra.count = this.report ? this.report.returnedTodayTotal : 0;
        //Testados
        this.widgets.widget8.data.count = this.report ? this.report.testedTotal : 0;
        this.widgets.widget8.data.extra.count = this.report ? this.report.testedTodayTotal : 0;
        //Manutenção
        this.widgets.widget9.data.count = this.report ? this.report.underMaintenanceTotal : 0;
        this.widgets.widget9.data.extra.count = this.report ? this.report.underMaintenanceTodayTotal : 0;
        //Desistalados
        this.widgets.widget10.data.count = this.report ? this.report.uninstalledTotal : 0;
        this.widgets.widget10.data.extra.count = this.report ? this.report.uninstalledTodayTotal : 0;

        if (this.report && this.report.totalModel) {

            let totalModel = this.utilsService.stringEnumToKeyValue(this.report.totalModel);

            totalModel.sort(function (a, b) {
                return a['value'] > b['value'] ? -1 : a['value'] < b['value'] ? 1 : 0;
            });

            totalModel.forEach(device => {

                if (device.value > 0) {

                    let item = {
                        'name': device.key,
                        'series': [
                            {
                                'name': 'Total',
                                'value': device.value,
                            }
                        ]
                    }

                    this.widgets.widget12.mainChart.TW.push(item)
                }

            });

        }


        //1 Hora
        this.widgets.widget5.mainChart.TW[0].series[0].value = this.report ? this.report.noCommunication1hTotal : 0;

        //6 Horas
        this.widgets.widget5.mainChart.TW[1].series[0].value = this.report ? this.report.noCommunication6hTotal : 0;

        //24 Horas
        this.widgets.widget5.mainChart.TW[2].series[0].value = this.report ? this.report.noCommunication24hTotal : 0;

        //48 Horas
        this.widgets.widget5.mainChart.TW[3].series[0].value = this.report ? this.report.noCommunication48hTotal : 0;

        //72 Horas
        this.widgets.widget5.mainChart.TW[4].series[0].value = this.report ? this.report.noCommunication72hTotal : 0;

        //72 Horas +
        this.widgets.widget5.mainChart.TW[5].series[0].value = this.report ? this.report.noCommunication72hMoreTotal : 0;

        //Incidentes Total
        this.widgets.widget11.mainChart.TW[0].series[0].value = this.report ? this.report.incidentTotal : 0;

        //Incidentes Abertos
        this.widgets.widget11.mainChart.TW[1].series[0].value = this.report ? this.report.incidentOpenTotal : 0;

        //Incidentes Resolvidos
        this.widgets.widget11.mainChart.TW[2].series[0].value = this.report ? this.report.incidentResolvedTotal : 0;

        //Incidentes Cancelados
        this.widgets.widget11.mainChart.TW[3].series[0].value = this.report ? this.report.incidentCancelledTotal : 0;

        this.loadingService.hide()
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions

    }

    loadConfig(status: boolean) {

        this.loadingService.show()

        this.brandsService.getBrands().subscribe(brands => {

            let indexBrandUser = 0;

            if (this.appContext && this.appContext.brandTracknme && this.appContext.loggedUser && this.appContext.loggedUser.brand && brands.content.length > 0) {
                indexBrandUser = brands.content.map((item) => { return item.id; }).indexOf(this.appContext.loggedUser.brand)
                this.setTitle(brands.content[indexBrandUser].name + ' - ERP')
            } else {
                indexBrandUser = brands.content.map((item) => { return item.id; }).indexOf(6137206846521344)
            }

            let brandPallet = brands.content[indexBrandUser].palette ? brands.content[indexBrandUser].palette : "blue-grey"
            let logoBrand = brands.content[indexBrandUser]['logo']

            if (logoBrand && logoBrand != undefined) {
                localStorage.setItem('logoBrand', logoBrand);
            } else {
                localStorage.setItem('logoBrand', "assets/images/logos/logo-tracknme.svg");
            }

            this.urlLogo = localStorage.getItem('logoBrand');

            this.primaryColor = PalletColors[brandPallet] + '-A700-bg'
            let secondColorBrand = PalletColors[brandPallet] + '-A400-bg'

            localStorage.setItem('primaryColorBrand', this.primaryColor);
            localStorage.setItem('secondColorBrand', secondColorBrand);

            if (status) {
                this.setColorBrand()
            } else {

                if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'OPERATOR') {
                    this.router.navigate(['sales'])
                } else if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'BROKER') {
                    this.router.navigate(['insurance/quotes'])
                } else {
                    location.reload();
                    this.router.navigate(['dashboard'])
                }

                this.loadingService.hide()

            }

        })

    }

    setColorBrand() {

        this.loadingService.show()

        let fuse: FuseConfig = {
            layout: {
                style: 'vertical-layout-1',
                width: 'fullwidth',
                navbar: {
                    hidden: false,
                    position: 'left',
                    folded: false,
                    background: this.primaryColor
                },
                toolbar: {
                    hidden: false,
                    position: 'below-static',
                    background: this.primaryColor
                },
                footer: {
                    hidden: false,
                    position: 'below-static',
                    background: this.primaryColor
                }
            },
            customScrollbars: true
        };

        this.fuseConfig.setConfig(fuse)
        this.fuseConfig.setDefault(fuse)

        if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'OPERATOR') {
            this.router.navigate(['sales'])
        } else if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'BROKER') {
            this.router.navigate(['insurance/quotes'])
        } else {
            location.reload();
            this.router.navigate(['dashboard'])
        }

        this.loadingService.hide()

    }

    public setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }

    firstAccessDialog() {
        this.loadingService.hide()
        this.documentsService.getDocumentsTerms().subscribe(terms => {
            this.dialog
                .open(FirstAccessComponent, {
                    width: '800px',
                    data: {
                        title: 'Termos do Primeiro Acesso',
                        term: terms.term,
                        firstAccess: true
                    }
                })
        });
    }


}


