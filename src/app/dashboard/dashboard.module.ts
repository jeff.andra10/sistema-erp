import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CdkTableModule } from '@angular/cdk/table';
import { FuseSharedModule } from '@fuse/shared.module';
import { DashboardComponent } from './dashboard.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatButtonModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatMenuModule, MatSelectModule, MatTableModule, MatTabsModule } from '@angular/material';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { DashboardRoutingModule } from './dashboard-routing.module'

const authRouting: ModuleWithProviders = RouterModule.forChild([
   // { path : '', component: DashboardComponent }
  ]);

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports     : [
        CdkTableModule,
        authRouting,
        FuseSharedModule,
        NgxChartsModule,
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatMenuModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        FuseWidgetModule,
        DashboardRoutingModule
    ],
    exports     : [
        DashboardComponent
    ]
})
export class DashboardModule{}
