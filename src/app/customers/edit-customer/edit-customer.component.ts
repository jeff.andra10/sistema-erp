import { Component, OnInit, Inject, Input} from '@angular/core'; 
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder, AbstractControl } from '@angular/forms';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import 'rxjs/Rx';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { CustomerPersonalComponent } from '../customer-personal/customer-personal.component'
import { CustomerDocumentsComponent } from '../customer-documents/customer-documents.component';
import { Customer } from '../../core/models/customer.model';
import { Address } from '../../core/models/address.model';
import { Contact, ContactType } from '../../core/models/contact.model';
import { MaskUtils } from '../../core/mask-utils';
import { CustomersService } from '../../core/services/customers.service';
import { UtilsService } from '../../core/utils.service';
import { PaymentsService } from '../../core/services/payments.service';
import { PlansService } from '../../core/services/plans.service';
import { NewCreditCardComponent } from "../../payments/new-credit-card/new-credit-card.component";
import {animate, state, style, transition, trigger} from '@angular/animations';
import { EditCustomerDetailRowDirective } from './edit-customer-detail-row.directive';
import { StateAddress } from '../../core/common-data-domain';
import { Payment,PaymentType,ChargeType,PaymentCreditCard} from '../../core/models/payment.model';
import { PlanStatus,PlanRecurrence} from '../../core/models/plan.model';
import { Vehicle } from '../../core/models/vehicle.model';
import { UserService } from '../../core/services/users.service';
import { VehiclesService } from '../../core/services/vehicles.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";

@Component({
  selector: 'tnm-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['../customers.component.scss' ],
  animations: [
    trigger('detailExpand', [
      state('void', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('*', style({height: '*', visibility: 'visible'})),
      transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})
export class EditCustomerComponent implements OnInit, AfterViewInit {
  
  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  @ViewChild(CustomerDocumentsComponent)
  private customerDocumentsComponent: CustomerDocumentsComponent

  @ViewChild(NewCreditCardComponent)
  private newCreditCardComponentt: NewCreditCardComponent

  contactTypeList: Array<any>

  addressForm: FormGroup
  contactsForm: FormGroup
  paymentForm: FormGroup
  vehicleForm: FormGroup

  customerId: number 
  customer: Customer
  customerAddress : Address
  user: any

  statusSelected: String = 'ACTIVE'

  currentContact: Contact = new Contact()
  showContactForm: boolean = false
  contactFormIsEditing: boolean = false
  contacts: Array<Contact> = []
  contactDataSource: MatTableDataSource<Contact>
  contactDisplayedColumns: Array<string>
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  currentPayment: any
  showPaymentForm: boolean = false
  paymentFormIsEditing: boolean = false
  payments: Array<any> = []
  paymentDataSource: MatTableDataSource<any>
  paymentDisplayedColumns: Array<string>  
  paymentTypeList: any
  stateAddressList: any

  postalCodeMask  =  MaskUtils.postalCodeMask
  typeMask =  MaskUtils.mobilePhoneMask

  typeMobile: boolean = true

  logValue :  string = ''
  
  loginForm: FormGroup

  plans: Array<any> = []
  plansDataSource: MatTableDataSource<PlansService>
  plansDisplayedColumns: Array<string>
  
  planCharges: Array<any> = []
  planChargesDataSource: MatTableDataSource<PlansService>
  planChargesDisplayedColumns: Array<string>

  planChargeLogs: Array<any> = []
  planChargeLogsDataSource: MatTableDataSource<PlansService>
  planChargeLogsDisplayedColumns: Array<string>

  vehicles: Array<any> = []
  vehicleDisplayedColumns: Array<string> 
  vehicleDataSource: MatTableDataSource<Vehicle>

  isExpansionDetailRow = (index, row) => row.hasOwnProperty('detailRow');
  isExpansionLogRow = (index, row) => row.hasOwnProperty('logRow');

  @Input() singleChildRowDetail: boolean;

  private openedRow: EditCustomerDetailRowDirective
  onToggleChange(customerDetailRow: EditCustomerDetailRowDirective) : void {
    
    if(!this.openedRow || this.openedRow == undefined){
      this.loadPlanCharge(customerDetailRow)
    }else{
      this.openedRow = customerDetailRow.expended ? customerDetailRow : undefined;
    }
  }

  private openedLogRow:EditCustomerDetailRowDirective
  
  onToggleLogChange(customerLogRow: EditCustomerDetailRowDirective) : void {

    if(!this.openedLogRow || this.openedLogRow == undefined){
      this.loadChargeLog(customerLogRow)
    }else{
      this.openedLogRow = customerLogRow.expended ? customerLogRow : undefined;
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private customersService: CustomersService,
    private utils: UtilsService,
    private vehicleService: VehiclesService,
    private paymentsService: PaymentsService,
    private plansService: PlansService,
    private loadingService: FuseSplashScreenService
  ) { }

  selectPlan(plan) {
    this.plansService.setPlan(plan)
    this.router.navigate([`/plans-cancel/${plan.id}`])
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading','true');
    

    this.customerId = this.activatedRoute.snapshot.params['id']

    this.initForms()
    this.loadDomainListData()
    this.loadCustomerData()   
  }

  ngAfterViewInit() {

    this.customerPersonalComponent.personType
    .subscribe(type => {
      this.customerDocumentsComponent.setPersonType(type)
    })

    this.customerPersonalComponent.loadStatusCustomer(true)
    
  }

  initForms() {

    this.loginForm = this.formBuilder.group({
      login:this.formBuilder.control('',[Validators.required]),
      password:this.formBuilder.control('',[Validators.required, Validators.minLength(8)]),
      confirmPassword:this.formBuilder.control('',[Validators.required, checkConfirmPassword])
  })
    
    this.addressForm = this.formBuilder.group({
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', [])
    })

    this.contactsForm = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required]),
      type: this.formBuilder.control('', [Validators.required]),
      mobileNumber: this.formBuilder.control(''),
      email: this.formBuilder.control('')
    })

    this.paymentForm = this.formBuilder.group({
      paymentType: [ PaymentType.BOOKLET, Validators.required],
      creditCardBrand: ['', Validators.required],
      creditCardHolderName: ['', Validators.required],
      creditCardNumber: ['', Validators.required],  
      creditCardExpMonth: ['', Validators.required],
      creditCardExpYear: ['', Validators.required],
      creditCardSecurityCode: ['', Validators.required] 
    });  

    this.vehicleForm = this.formBuilder.group({
      licensePlate: this.formBuilder.control({value : '', disabled: true}),
      type: this.formBuilder.control({value : '', disabled: true}),
      chassi: this.formBuilder.control({value : '', disabled: true}),
      manufacture: this.formBuilder.control({value : '', disabled: true}),
      model: this.formBuilder.control({value : '', disabled: true}),
      color: this.formBuilder.control({value : '', disabled: true}),
      registrationYear: this.formBuilder.control({value : '', disabled: true}),
      modelYear: this.formBuilder.control({value : '', disabled: true}),
    })
    
    this.contactDisplayedColumns = ['type', 'name', 'value', 'edit', 'delete'];
    this.contactDataSource = new MatTableDataSource<Contact>(this.contacts);

    this.paymentDisplayedColumns = ['type', 'creditCardBrand', 'creditCardNumber'];
    this.paymentDataSource = new MatTableDataSource<any>(this.payments);

    this.plansDisplayedColumns = ['offerName', 'licensePlate', 'recurrence', 'status'];
    this.plansDataSource = new MatTableDataSource<any>(this.plans);

    this.planChargesDisplayedColumns = [ 'paymentType', 'creditCardBrand', 'creditCardNumber', 'scheduledDateTime', 'status','amount','type','tries', 'chevron-down'];
    
    this.vehicleDisplayedColumns = ['manufacture', 'model', 'registrationYear', 'modelYear' ,'color' ,'licensePlate', 'chassi']

    this.planChargeLogsDisplayedColumns  = ['date', 'status']

    this.contactsForm.valueChanges.subscribe((newForm) => { 
      this.typeMobile = (newForm.type == 'MOBILE')

      if(newForm.type == 'MOBILE'){
        this.contactsForm.controls.mobileNumber.setValidators([Validators.required]);
        this.contactsForm.controls.email.setValidators(null);
      }else{
        this.contactsForm.controls.mobileNumber.setValidators(null);
        this.contactsForm.controls.email.setValidators([Validators.required]);
      } 

    });

  }

  loadVehicleData(userID) {

    this.vehicleService.getVehiclesByUser(userID)
    .subscribe(vehicles => {
      this.vehicles = vehicles.content
      if(vehicles.content.length == 1){

        let vehicle = vehicles.content[0];

        this.vehicleForm.patchValue ({
          licensePlate: vehicle.licensePlate,
          type: vehicle.type,
          chassi: vehicle.chassi,
          manufacture: vehicle.manufacture,
          model: vehicle.model,
          color: vehicle.color,
          registrationYear: vehicle.registrationYear,
          modelYear: vehicle.modelYear
        })
      } else if(vehicles.content.length > 1){
        this.vehicleDataSource = new MatTableDataSource<Vehicle>(this.vehicles);
      }

    })
  }

  loadCustomerData() {

    this.customersService.getOne(this.customerId)
    .subscribe(customer => {

      this.customer = customer

      this.loadVehicleData(this.customer.user)

      this.customerPersonalComponent.loadCustomerData(this.customer)
      this.customerDocumentsComponent.loadCustomerData(this.customer)

      this.userService.getOne(customer.user)
        .subscribe(user => {
          
          this.user = user
          this.loginForm.patchValue({
            'login':user['login']
          })

          this.statusSelected = this.user.status;
          console.log('this.statusSelected:',this.statusSelected)

          this.loadingService.hide()

        }, error => {
          console.log(error)
          this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
          this.loadingService.hide()
        })

      this.customersService.getCustomerAddress(this.customer.user)
      .subscribe(addresses => {

        if (addresses.content != null && addresses.content.length > 0) {
          
          this.customerAddress = addresses.content[0]
          
          this.addressForm.setValue ({
            street: this.customerAddress.street || null,
            number: this.customerAddress.number || null,
            complement: this.customerAddress.complement || null,
            district: this.customerAddress.district || null,
            city: this.customerAddress.city || null,
            state: this.customerAddress.state || null,
            postalCode: this.customerAddress.postalCode || null,
            reference: this.customerAddress.reference || null
          })
        }
      })

      this.customersService.getCustomerPlans(this.customer.user)
      .subscribe(plans => {
        if (plans.content != null) {
          this.plans = []
          this.plans = plans.content
          this.plansDataSource = new MatTableDataSource<PlansService>(this.plans)
        }
        this.loadPayments()
      })

      this.customersService.getCustomerContacts(this.customer.user)
      .subscribe(contacts => {
        if (contacts.content != null) {
          this.contacts = contacts.content
          this.contactDataSource = new MatTableDataSource<Contact>(this.contacts)
        }
      })
      
      
    })
  } 

  loadDomainListData() {
    this.contactTypeList = this.utils.stringEnumToKeyValue(ContactType)
    this.paymentTypeList = this.utils.stringEnumToKeyValue(PaymentType)
    this.stateAddressList = this.utils.stringEnumToKeyValue(StateAddress)
  }

  loadPayments(){
    this.paymentsService.getPaymentsByUser(this.customer.user)
      .subscribe(payments => {
        if (payments.content != null) {

          this.payments = payments.content 

          if (this.payments != null) {
            this.payments.forEach(obj => {
              obj.paymentType = this.utils.getItemByColumnNameAndValue("key", obj.type, this.paymentTypeList)
            });
          }
          
          this.paymentDataSource = new MatTableDataSource<any>(this.payments);
        }
        this.loadingService.hide()
      })      
  }

  loadPlanCharge(customerDetailRow: EditCustomerDetailRowDirective){
  
    this.customersService.getCustomerPlanCharges(customerDetailRow['row'].id)
      .subscribe(charges => {
        if (charges.content != null) {
          this.planCharges = []
          let _charges =  charges.content
          
          _charges.forEach(charge => {
            charge.payment = customerDetailRow['row'].relationships.payment
            this.planCharges.push(charge)
          });
          
          this.planChargesDataSource = new MatTableDataSource<PlansService>(this.planCharges)
          if (this.singleChildRowDetail && this.openedRow && this.openedRow.expended) {
            this.openedRow.toggle();    
          }
          this.openedRow = customerDetailRow.expended ? customerDetailRow : undefined;
        }
      })
  }

  loadChargeLog(customerLogRow: EditCustomerDetailRowDirective){
  
    this.customersService.getCustomerChargeLogs(customerLogRow['row'].id)
      .subscribe(log => {
        if (log.content != null) {
           this.planChargeLogs = log.content;
           this.planChargeLogsDataSource = new MatTableDataSource<PlansService>(this.planChargeLogs)
          if (this.singleChildRowDetail && this.openedRow && this.openedRow.expended) {
            this.openedLogRow.toggle();    
          }
          this.openedLogRow = customerLogRow.expended ? customerLogRow : undefined;
        }
      })
  }

  isCustomerInvalid()  {
    return this.customerPersonalComponent.isInvalid() || this.customerDocumentsComponent.isInvalid()
  }

  confirmContactForm(editForm) {

      this.currentContact.name = editForm.name
      this.currentContact.type = editForm.type
      this.currentContact.user = this.customer.user

      if(editForm.type == 'MOBILE'){
        this.currentContact.value =  this.utils.stringOnlyDigits(editForm.mobileNumber)
      }else{
        this.currentContact.value =  editForm.email
      } 

      if (this.contactFormIsEditing == false) {
        
          this.customersService.createCustomerContact(this.currentContact)
          .subscribe(result => {
            this.currentContact.id = result.id
            this.contacts.push(this.currentContact)
            this.contactDataSource = new MatTableDataSource<Contact>(this.contacts)
          })
      }  else {

        let editData = []
        for(let item in editForm){
          let editItem = {'op': 'add', 'path': '/'+item, 'value': editForm[item]};
          editData.push(editItem);
        }

        this.customersService.updateCustomerContact(this.currentContact.id, editData).subscribe(result => {})
      }

      this.contactFormIsEditing = false
      this.showContactForm = false
      this.clearContactForm()
  }
  clearContactForm() {

    this.contactsForm.patchValue({
      name:  null,
      type:  null,
      email: null,
      mobileNumber:  null
    })
  }
  addNewContact() {
    
    this.currentContact = new Contact()
    this.showContactForm = true
  }
  cancelContactForm() {
    this.showContactForm = false
  }
  editContact(contact) {

    this.contactsForm.setValue ({
      name: contact.name || null,
      type: contact.type || null,
      email: contact.value || null,
      mobileNumber: contact.value || null
    })

    this.currentContact = contact
    this.showContactForm = true
    this.contactFormIsEditing = true
  }
  deleteContact(contact) {
    let idx = this.contacts.indexOf(contact)
    if (idx != -1) {

      this.customersService.deleteCustomerContact(contact.id)
      .subscribe(result => {
        this.contacts.splice(idx, 1)
        this.contactDataSource = new MatTableDataSource<Contact>(this.contacts)
      })
    }
  }

  showContract(){
    this.loadingService.show()
    localStorage.setItem('contractCustomerId',this.customer.id.toString());
  }

  updateCustomer() {

    let updatePassword = false;

    if(this.loginForm.value.password && this.loginForm.value.password.length > 0){
      
      if(this.loginForm.value.password.length < 8){
        this.snackbar.open('A senha deve ter no mínimo 8 caracteres.', '', { duration: 6000 });
        updatePassword = false;
        return;
      }

      if(this.loginForm.value.confirmPassword && this.loginForm.value.confirmPassword.length > 0){

        updatePassword = true;
        if(this.loginForm.value.confirmPassword != this.loginForm.value.password){
          this.snackbar.open('As senhas devem ser iguais.', '', { duration: 6000 });
          updatePassword = false;
          return;
        }
      }else{
        updatePassword = false;
        this.snackbar.open('A confimação de senha deve ser  preenchida.', '', { duration: 6000 });
        return;
      }

    }

    this.loadingService.show()

    var customerData = []

    let documentsData = this.customerDocumentsComponent.getCustomerData()

    for(let item in documentsData){
      if(item == 'nationalIdentity'){
        documentsData[item]= this.utils.stringOnlyDigits(documentsData[item])
      }else if(item == 'driverLicense'){
        documentsData[item]= this.utils.stringOnlyDigits(documentsData[item])
      }else if(item == 'registrationNumber'){
        documentsData[item]= this.utils.stringOnlyDigits(documentsData[item])
      }
    }

    for(let item in documentsData){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': item == 'nationalIdentityExpeditionDate' ? this.convertDate(documentsData[item]) : documentsData[item]};
      customerData.push(editItem);
    }

    let personalData = this.customerPersonalComponent.getCustomerData()

    for(let item in personalData){
      if(item == 'mobilePhone'){
        personalData[item]= this.utils.stringToNumber(personalData[item])
      }else if(item == 'residencialPhone'){
        personalData[item]= this.utils.stringToNumber(personalData[item])
      }else if(item == 'commercialPhone'){
        personalData[item]= this.utils.stringToNumber(personalData[item])
      }
    }

    for(let item in personalData){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': item == 'birthDate' ? this.convertDate(personalData[item]) : personalData[item]};
      customerData.push(editItem);
    }

    let addressData = []
    for(let item in this.addressForm.value){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': this.addressForm.value[item]};
      addressData.push(editItem);
    }

    var updatesCall = [this.customersService.update(this.customer.id, customerData).map((res:Response) => res)]
    if (this.customerAddress) {
      updatesCall.push(this.customersService.updateCustomerAddress(this.customerAddress.id, addressData).map((res:Response) => res))
    }else{
      var address = this.addressForm.value;
      address.user = this.customer.user;
      
      if(address.city && address.address){
        updatesCall.push(this.customersService.createCustomerAddress(address).map((res:Response) => res))
      }
      
    }

    Observable.forkJoin(updatesCall).subscribe(result => {

      if(this.user.login != this.loginForm.value.login){
        this.changeLoginUser(this.user.id)
      }
       
      if(updatePassword){
          this.changePassword(this.user.id)
      }

      this.snackbar.open('Cliente atualizado com sucesso', '', {
        duration: 8000
      })

      this.loadingService.hide()
    },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000 })
        this.loadingService.hide()
    })
  }

  updateUserStatus(){

    this.loadingService.show()

    let userData = []
    let editItem = {'op': 'add', 'path': '/status', 'value': this.statusSelected};
        userData.push(editItem);

    this.userService.update(this.user.id, userData)
    .subscribe(device => {
      this.snackbar.open('Cliente atualizado com sucesso', '', {
        duration: 8000
      })

      this.loadingService.hide()

    }, error => {
      console.log(error)
      this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
      this.loadingService.hide()
    })

  }

  cancel() {
    this.router.navigate(['/customers'])
  }

  // confirmPaymentForm(editForm, invalid) {
  //   if (editForm.type == PaymentType.CREDIT_CARD && invalid) return

  //   this.currentPayment.type = editForm.paymentType
  //   this.currentPayment.creditCardBrand = editForm.creditCardBrand
  //   this.currentPayment.creditCardHolderName = editForm.creditCardHolderName
  //   this.currentPayment.creditCardNumber = editForm.creditCardNumber
  //   this.currentPayment.creditCardExpMonth = editForm.creditCardExpMonth
  //   this.currentPayment.creditCardExpYear = editForm.creditCardExpYear
  //   this.currentPayment.creditCardSecurityCode = editForm.creditCardSecurityCode
  //   this.currentPayment.user = this.customer.user

  //   if (this.paymentFormIsEditing == false) {

  //     this.paymentsService.create(this.currentPayment)
  //     .subscribe(result => {

  //       this.currentPayment.id = result.id
  //       this.payments.push(this.currentPayment)
  //       this.paymentDataSource = new MatTableDataSource<any>(this.payments);
  //     })
      
  //   } else {

  //     let editData = []
  //     for(let item in editForm){
  //       let editItem = {'op': 'add', 'path': '/'+item, 'value': editForm[item]};
  //       editData.push(editItem);
  //     }
  //     console.log(editData)
  //     //this.customersService.updateCustomerPayment(this.currentPayment.id, editData).subscribe(result => {})
  //   }

  //   this.paymentFormIsEditing = false
  //   this.showPaymentForm = false
  // }
  addNewPayment(type) {
  
    let id = this.customer.user;
   
    this.dialog
    .open(NewCreditCardComponent, { width:'1100px',height:'550px', data: { user :  id, type: type} })
    .afterClosed()
    .subscribe(response => {
      this.loadPayments()
    })
  }

  cancelPaymentForm() {
    this.showPaymentForm = false
  }
  editPayment(payment) {

    let id = this.customer.user;
    
    this.dialog
    .open(NewCreditCardComponent, {  width:'1100px',height:'550px', data: { user :  id} })
    .afterClosed()
    .subscribe(response => {
      this.loadPayments()
    })

    this.newCreditCardComponentt.loadPaymentData(payment);
    
    this.currentPayment = payment
    // this.showPaymentForm = true
    // this.paymentFormIsEditing = true
  }
  deletePayment(payment) {
    let idx = this.payments.indexOf(payment)
    if (idx != -1) {

      this.paymentsService.delete(payment.id)
      .subscribe(result => {
        this.payments.splice(idx, 1)
        this.paymentDataSource = new MatTableDataSource<any>(this.payments)
      })
    }
  }

  convertDate(date){
    if(date){
      let  _date = date.split('/');
          _date =_date[1] +'-'+_date[0]+'-'+_date[2]
      return new Date(_date)
    }
  }

  paymentTypeEnumToString(value) : string {
    return PaymentType[value]
  }

  planStatusEnumToString(value) : string {
    return PlanStatus[value]
  }

  PlanRecurrenceEnumToString(value) : string {
    return PlanRecurrence[value]
  }

  ChargeTypeEnumToString(value) : string {
    return ChargeType[value]
  }

  PaymentCreditCardEnumToString(value) : string {
    return PaymentCreditCard[value]
  }

  changeLoginUser(userId){

    let editData = []
        
    let editItem = {'op': 'add', 'path': '/login', 'value': this.loginForm.value.login};
        editData.push(editItem);
        
    this.userService.update(userId,editData)
    .subscribe(response => {
      
      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        this.loadingService.hide()
      })
  
  }

  changePassword(userId){
  
    let body = {
        "token": '',
        "user":userId,
        "password": this.loginForm.value.password,
        "confirmPassword": this.loginForm.value.confirmPassword,
      }

     this.userService.changePassword(body)
     .subscribe(result => {
       
     }, error => {
      console.log(error)
      this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
      this.loadingService.hide()
    })
 }
}

function checkConfirmPassword(control: AbstractControl)
{

    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('confirmPassword');


    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}