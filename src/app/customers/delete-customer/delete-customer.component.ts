import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Customer } from '../../core/models/customer.model';
import { CustomersService } from '../../core/services/customers.service';

@Component({
  selector: 'tnm-delete-customer',
  templateUrl: './delete-customer.component.html'
})
export class DeleteCustomerComponent implements OnInit {

  customer: Customer

  constructor(
    private dialogRef: MatDialogRef<DeleteCustomerComponent>,
    private customerService: CustomersService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackbar: MatSnackBar

  ) { }

  ngOnInit() {
    this.customer = this.data
  }

  deleteCustomer(){
    this.customerService.delete(this.customer.id)
      .subscribe(suppliers => {
        this.dialogRef.close()
        this.snackbar.open('Cliente excluído com sucesso', '', {
          duration: 8000
        })
      })
  }
}
