import {
  Component,
  OnInit,
  Injector,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatSort, MatPaginator } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';

import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { BaseComponent } from '../_base/base-component.component';
import { CustomersService } from '../core/services/customers.service';
import { UserService } from '../core/services/users.service';

import { CustomersDataSource } from './customers.datasource';

@Component({
  selector: 'tnm-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
  animations: fuseAnimations
})
export class CustomersComponent extends BaseComponent
  implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  brandList: Array<any>;
  displayedColumns = ['name', 'registrationNumber', 'email', 'brand'];
  dataSource: CustomersDataSource;

  resultLength = 0;

  searchSubject = new Subject();
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');

  constructor(
    private injector: Injector,
    public dialog: MatDialog,
    private customersService: CustomersService,
    private loadingService: FuseSplashScreenService,
    private router: Router
  ) {
    super(injector);
  }

  searchOnChange(event: { currentTarget: { value: {} } }) {
    this.searchSubject.next(event.currentTarget.value);
  }

  selectCustomer(row: { id: any }) {
    this.loadingService.show();
    this.router.navigate(['/customers/', row.id]);
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading', 'false');
    this.searchSubject.debounceTime(500).subscribe((val: string) => {
      this.loadCustomers(val);
    });

    this.brandList = this.appContext.brands;

    this.dataSource = new CustomersDataSource(
      this.customersService,
      this.utilsService,
      this.brandList
    );
    this.dataSource.load('', '', 'name', 'asc', 0, 10);
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    if (this.dataSource && this.dataSource.totalElements$) {
      this.dataSource.totalElements$.subscribe(value => {
        this.resultLength = value;
      });
    }

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(tap(() => this.loadCustomers()))
      .subscribe();
  }

  loadCustomers(filter?: string) {
    this.dataSource.load(
      filter,
      '',
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
  }
}
