import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { Customer } from '../core/models/customer.model';

@Injectable()
export class CustomerSelectedService {
  private customerSource: BehaviorSubject<Customer> = new BehaviorSubject(null);
  public customerSelected = this.customerSource.asObservable();

  setSelected(value: Customer) {
    this.customerSource.next(value);
  }
}
