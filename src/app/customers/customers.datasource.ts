import { CollectionViewer, DataSource } from '@angular/cdk/collections';

import { catchError, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { UtilsService } from 'app/core/utils.service';
import { PaginatedContent } from 'app/core/models/paginatedContent.model';

import { Brand } from '../core/models/brand.model';
import { Customer } from '../core/models/customer.model';
import { CustomersService } from '../core/services/customers.service';

export class CustomersDataSource implements DataSource<any> {
  private dataSubject = new BehaviorSubject<Customer[]>([]);
  private totalElementsSubject = new BehaviorSubject<number>(0);
  public totalElements$ = this.totalElementsSubject.asObservable();

  constructor(
    private customersService: CustomersService,
    private utilsService: UtilsService,
    private brands: Array<Brand>
  ) {}

  load(
    filter: string,
    status: string,
    sortField: string,
    sortDirection: string,
    pageIndex: number,
    pageSize: number
  ) {
    this.customersService
      .findCustomers(
        filter,
        status,
        sortField,
        sortDirection,
        pageIndex,
        pageSize
      )
      .pipe(catchError(() => of([])))
      .subscribe((result: PaginatedContent<Customer>) => {
        if (result.content && this.brands) {
          result.content.forEach(obj => {
            obj.brandObj = this.utilsService.getItemById(
              obj.brand,
              this.brands
            );
          });
        }

        this.totalElementsSubject.next(result.totalElements);
        this.dataSubject.next(result.content);
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<Customer[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.dataSubject.complete();
    this.totalElementsSubject.complete();
  }
}
