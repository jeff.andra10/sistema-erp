import { Component, OnInit } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

import { CustomerPersonalComponent } from '../customer-personal/customer-personal.component'
import { CustomerDocumentsComponent } from '../customer-documents/customer-documents.component';
import { CustomerAddressComponent } from '../customer-address/customer-address.component';
import { Customer } from '../../core/models/customer.model';
import { Address } from '../../core/models/address.model';
import { CustomersService } from '../../core/services/customers.service';
import { UtilsService } from '../../core/utils.service';

@Component({
  selector: 'tnm-new-customer',
  templateUrl: './new-customer.component.html',
  styleUrls: [ './new-customer.component.scss' ]
})
export class NewCustomerComponent implements OnInit, AfterViewInit {
  
  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  @ViewChild(CustomerDocumentsComponent)
  private customerDocumentsComponent: CustomerDocumentsComponent

  @ViewChild(CustomerAddressComponent)
  private customerAddressComponent: CustomerAddressComponent

  customer: Customer
  customerAddress: Address
  
  constructor(
    private customersService: CustomersService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private router: Router,
    private utils: UtilsService
  ) { }

  ngOnInit() {
   
  }

  ngAfterViewInit() {

    this.customerPersonalComponent.personType
    .subscribe(type => {
      this.customerDocumentsComponent.setPersonType(type)
    })

    this.customerPersonalComponent.loadStatusCustomer(false)
  }

  cancel() {
    this.router.navigate(['/customers'])
  }

  isCustomerInvalid()  {

    return this.customerPersonalComponent.isInvalid() 
            || this.customerDocumentsComponent.isInvalid()
            || this.customerAddressComponent.isInvalid()
  }

  createCustomer() {

    if (this.isCustomerInvalid()) {
      this.snackbar.open('Preencha todas as informações', '', { duration: 8000 })
      return false
    }

    this.customer = new Customer()
    
    let personalData = this.customerPersonalComponent.getCustomerData()
    this.customer.brand = personalData.brand 
    this.customer.personType = personalData.personType
    this.customer.name = personalData.name
    this.customer.mobilePhone = this.utils.stringToNumber(personalData.mobilePhone)
    this.customer.residencialPhone = this.utils.stringToNumber(personalData.residencialPhone)
    this.customer.commercialPhone = this.utils.stringToNumber(personalData.commercialPhone)
    this.customer.email = personalData.email

    if(personalData.personType == 'PERSON'){
      this.customer.birthDate = this.convertDate(personalData.birthDate)
      this.customer.gender = personalData.gender
      this.customer.occupation = personalData.occupation
      this.customer.maritialStatus = personalData.maritialStatus
    }else{
      this.customer.procurator = personalData.procurator
    }

    let documentsData = this.customerDocumentsComponent.getCustomerData()
    this.customer.registrationNumber = this.utils.numberOnlyDigits(documentsData.registrationNumber)
   
    if(personalData.personType == 'PERSON'){
      this.customer.nationalIdentity = this.utils.stringOnlyDigits(documentsData.nationalIdentity)
      this.customer.nationalIdentityExpeditionDate =  this.convertDate(documentsData.nationalIdentityExpeditionDate)
      this.customer.nationalIdentityExpeditionAgency = documentsData.nationalIdentityExpeditionAgency
      this.customer.driverLicense = this.utils.stringOnlyDigits(documentsData.driverLicense)
      this.customer.driverLicenseCategory = documentsData.driverLicenseCategory
    }else{
      this.customer.stateRegistration = documentsData.stateRegistration
    }

    let addressData = this.customerAddressComponent.getData()
    this.customerAddress = new Address()
    this.customerAddress.street = addressData.street
    this.customerAddress.number = addressData.number
    this.customerAddress.complement = addressData.complement
    this.customerAddress.district = addressData.district
    this.customerAddress.city = addressData.city
    this.customerAddress.state = addressData.state
    this.customerAddress.postalCode = this.utils.stringToNumber(addressData.postalCode)
    this.customerAddress.reference = addressData.reference

    let wrapper = {
      brand: this.customer.brand,
      customer : this.customer,
      address: this.customerAddress
    }

    this.customersService.registerCustomer(wrapper)
    .subscribe(response => {
      this.snackbar.open('Cliente criado com sucesso', '', { duration: 8000 })
      this.router.navigate(['/customers'])
      },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000 })
    })
  }

  convertDate(date){
    if(date){
      let  _date = date.split('/');
          _date =_date[1] +'-'+_date[0]+'-'+_date[2]
      return new Date(_date)
    }
      
  }

}
