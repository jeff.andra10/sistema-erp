import { NgModule, ModuleWithProviders } from "@angular/core";
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../angular-material/material.module";
import { FuseSharedModule } from "@fuse/shared.module";
import { RouterModule } from "@angular/router";

import { NewCustomerComponent } from "./new-customer/new-customer.component";
import { DeleteCustomerComponent } from "./delete-customer/delete-customer.component";
import { EditCustomerComponent } from "./edit-customer/edit-customer.component";
import { CustomerPersonalComponent } from "./customer-personal/customer-personal.component";
import { CustomerDocumentsComponent } from "./customer-documents/customer-documents.component";
import { CustomerAddressComponent } from "./customer-address/customer-address.component";
import { CustomersComponent } from "./customers.component";
import { EditCustomerDetailRowDirective } from './edit-customer/edit-customer-detail-row.directive';
import { CustomerSelectedService } from './customer.selected.service';

const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'customers', component: CustomersComponent },
    { path: 'customers/new', component: NewCustomerComponent },
    { path: 'customers/:id', component: EditCustomerComponent }
  ]);

@NgModule({
    declarations:[
        CustomersComponent,
        NewCustomerComponent,
        EditCustomerComponent,
        DeleteCustomerComponent,
        CustomerPersonalComponent,
        CustomerDocumentsComponent,
        CustomerAddressComponent,
        EditCustomerDetailRowDirective
       
    ],
    imports: [
        TextMaskModule,
        MaterialModule,
        ReactiveFormsModule,
        CommonModule,
        FuseSharedModule,
        RouterModule,
        authRouting
    ],
    exports: [
        NewCustomerComponent,
        EditCustomerComponent,
        DeleteCustomerComponent,
        CustomerPersonalComponent,
        CustomerDocumentsComponent,
        CustomerAddressComponent

    ],
    entryComponents: [
        NewCustomerComponent,
        EditCustomerComponent,
        DeleteCustomerComponent,
        CustomerPersonalComponent,
        CustomerDocumentsComponent,
        CustomerAddressComponent
    ],
    providers: [
        CustomerSelectedService
    ]
})
export class CustomersModule {}