import { Component, OnInit, Inject} from '@angular/core';
import { Validators, FormGroup, AbstractControl, FormBuilder } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { PersonType, Customer, DriverLicenseCategory } from '../../core/models/customer.model';
import { MaskUtils } from '../../core/mask-utils';
import { UtilsService } from '../../core/utils.service';
import { StateAddress } from '../../core/common-data-domain';

@Component({
  selector: 'tnm-customer-documents',
  templateUrl: './customer-documents.component.html'
})

export class CustomerDocumentsComponent implements OnInit {

  generalForm: FormGroup
  driverLicenseCategoryList: any
  expeditionDate : Date
  personType : PersonType = PersonType.PERSON
  PersonType : typeof PersonType = PersonType
  isPerson : Boolean = false
  stateAddressList: any
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask
  rgMask = MaskUtils.rgMask
  cnhMask = MaskUtils.cnhMask
  dateMask = MaskUtils.dateMask
  isRequired : Boolean = false

  constructor(
    private formBuilder: FormBuilder,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.isPerson = true
    this.initForms()
    this.loadDomainListData()
  }

  public loadCustomerData(customer: Customer) {

    this.generalForm.setValue ({
      registrationNumber: customer.registrationNumber || null,
      nationalIdentity: customer.nationalIdentity || null,
      nationalIdentityExpeditionDate: this.convertLoadDate(customer.nationalIdentityExpeditionDate) || null,
      nationalIdentityExpeditionAgency: customer.nationalIdentityExpeditionAgency || null,
      stateRegistration: customer.stateRegistration || null,
      driverLicense: customer.driverLicense || null,
      driverLicenseCategory: customer.driverLicenseCategory || null
    })
  } 

  public isInvalid() {
    return this.generalForm.invalid
  }

  public setRequired() {
    this.isRequired = true

    this.generalForm = this.formBuilder.group({
      registrationNumber: this.formBuilder.control('', [Validators.required]),
      nationalIdentity: this.formBuilder.control(null, [Validators.required]),
      nationalIdentityExpeditionDate: this.formBuilder.control(null, [Validators.required]),
      nationalIdentityExpeditionAgency: this.formBuilder.control(null, [Validators.required]),
      stateRegistration: this.formBuilder.control(null),
      driverLicense: this.formBuilder.control(null, [Validators.required]),
      driverLicenseCategory: this.formBuilder.control(null, [Validators.required])
    })
  }

  public setRequiredCompany() {
    this.isRequired = true

    this.generalForm = this.formBuilder.group({
      registrationNumber: this.formBuilder.control('', [Validators.required]),
      nationalIdentity: this.formBuilder.control(null),
      nationalIdentityExpeditionDate: this.formBuilder.control(null),
      nationalIdentityExpeditionAgency: this.formBuilder.control(null),
      stateRegistration: this.formBuilder.control(null),
      driverLicense: this.formBuilder.control(null),
      driverLicenseCategory: this.formBuilder.control(null)
    })
  }
  
  public getCustomerData() : any {
    return this.generalForm.value
  }

  public getCustomerForm(): FormGroup{
    return this.generalForm
  }

  public setPersonType(type : PersonType) {
    this.isPerson = (this.PersonType.PERSON == type);
      
    this.personType = type

    this.generalForm.valueChanges.subscribe((newForm) => {
      
      if(this.isPerson){
        this.generalForm.controls.registrationNumber.setValidators([Validators.required,validCPF])
      }else{
        this.generalForm.controls.registrationNumber.setValidators([Validators.required,validCNPJ])
        this.generalForm.controls.stateRegistration.setValidators(null);
        this.generalForm.controls.driverLicense.setValidators(null);
        this.generalForm.controls.driverLicenseCategory.setValidators(null);
      } 
    });
  }
  

  private initForms() {
   
    this.generalForm = this.formBuilder.group({
      registrationNumber: this.formBuilder.control('', [Validators.required]),
      nationalIdentity: this.formBuilder.control(null),
      nationalIdentityExpeditionDate: this.formBuilder.control(null),
      nationalIdentityExpeditionAgency: this.formBuilder.control(null),
      stateRegistration: this.formBuilder.control(null),
      driverLicense: this.formBuilder.control(null),
      driverLicenseCategory: this.formBuilder.control(null)
    })

    this.generalForm.valueChanges.subscribe((newForm) => {
      
      if(this.isPerson){
        this.generalForm.controls.registrationNumber.setValidators([Validators.required,validCPF])
       
        //this.generalForm.controls.stateRegistration.setValidators(null);
        //this.generalForm.controls.driverLicense.setValidators([Validators.required]);
        //this.generalForm.controls.driverLicenseCategory.setValidators([Validators.required]);
        //this.generalForm.controls.nationalIdentityExpeditionDate.setValidators([Validators.required]);
        //this.generalForm.controls.nationalIdentityExpeditionAgency.setValidators([Validators.required]);
        //this.generalForm.controls.nationalIdentity.setValidators([Validators.required]);
        
      }else{
       
        this.generalForm.controls.registrationNumber.setValidators([Validators.required,validCNPJ])
        this.generalForm.controls.stateRegistration.setValidators(null);
        this.generalForm.controls.driverLicense.setValidators(null);
        this.generalForm.controls.driverLicenseCategory.setValidators(null);
        //this.generalForm.controls.nationalIdentityExpeditionDate.setValidators(null);
        //this.generalForm.controls.nationalIdentityExpeditionAgency.setValidators(null);
        //this.generalForm.controls.nationalIdentity.setValidators(null);
      } 
    });
  }

  private loadDomainListData() {
    this.driverLicenseCategoryList = this.utils.stringEnumToKeyValue(DriverLicenseCategory)
    this.stateAddressList = this.utils.stringEnumToKeyValue(StateAddress)
  }

  expeditionDateChanged(event: MatDatepickerInputEvent<Date>) {
    this.expeditionDate = new Date(event.value)
  }  

  convertLoadDate(date){
    if(date){
      let _date = new Date(date)
      let day = _date.getDate()
      let month = _date.getMonth()+1
      return (day < 10 ? '0'+day : day) +'/'+ ( month < 10 ? '0'+month : month) +'/' + _date.getFullYear()
    }
  }
}

function validCPF(control: AbstractControl) {

  if ( !control.parent || !control )
    {
        return;
    }

  const cpfCnpj = control.parent.get('registrationNumber');

  let c = cpfCnpj.value.replace(/\D+/g, '')

  if (!/[0-9]{11}/.test(c)){
      return {
        invalidCPF: true
    };
  } 

  var r;
  var s = 0;   
  
  for (var i=1; i<=9; i++)
    s = s + parseInt(c[i-1]) * (11 - i); 

  r = (s * 10) % 11;

  if ((r == 10) || (r == 11)) 
    r = 0;

  if (r != parseInt(c[9])){
    return {
      invalidCPF: true
    };
  }


  s = 0;

  for (i = 1; i <= 10; i++)
    s = s + parseInt(c[i-1]) * (12 - i);

  r = (s * 10) % 11;

  if ((r == 10) || (r == 11)) 
    r = 0;

  if (r != parseInt(c[10])){
    return {
      invalidCPF: true
    };
  }
  
  return
}

function validCNPJ(control: AbstractControl) {

if ( !control.parent || !control )
  {
      return;
  }

const registrationNumber = control.parent.get('registrationNumber');

let cnpj = registrationNumber.value.replace(/\D+/g, '')

var b = [6,5,4,3,2,9,8,7,6,5,4,3,2];

  if((cnpj = cnpj.replace(/[^\d]/g,"")).length != 14){
    return {
        invalidCNPJ: true
    }
  }
    

  if(/0{14}/.test(cnpj)){
    return {
        invalidCNPJ: true
    }
  }
  

  for (var i = 0, n = 0; i < 12; n += cnpj[i] * b[++i]);
  if(cnpj[12] != (((n %= 11) < 2) ? 0 : 11 - n)){
    return {
        invalidCNPJ: true
    }
  }

  for (var i = 0, n = 0; i <= 12; n += cnpj[i] * b[i++]);
  if(cnpj[13] != (((n %= 11) < 2) ? 0 : 11 - n))
  {
    return {
        invalidCNPJ: true
    }
  }
       
return 

}
