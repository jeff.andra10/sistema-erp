import { Component, OnInit, Inject} from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MaskUtils } from '../../core/mask-utils';
import { UtilsService } from '../../core/utils.service';
import { Address } from '../../core/models/address.model';
import { StateAddress } from '../../core/common-data-domain';

@Component({
  selector: 'tnm-customer-address',
  templateUrl: './customer-address.component.html'
})

export class CustomerAddressComponent implements OnInit {

  addressForm: FormGroup
  stateAddressList: any
  postalCodeMask =  MaskUtils.postalCodeMask
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  
  constructor(
    private formBuilder: FormBuilder,
    private utilsService: UtilsService
  ) { }

  ngOnInit() {

    this.initForms()
    this.loadDomainListData()
  }

  public loadData(address: Address) {

    this.addressForm.setValue({
      //name: address.name || null,
      street: address.street || null,
      number: address.number || null,
      complement: address.complement || null,
      district: address.district || null,
      city: address.city || null,
      state: address.state || null,
      postalCode: address.postalCode || null,
      reference: address.reference || null
    })
  } 

  public isInvalid() {
    return this.addressForm.invalid
  }

  public getData() : any {
    return this.addressForm.value
  }

  public getCustomerForm(): FormGroup{
    return this.addressForm
  }

  private initForms() {

    this.addressForm = this.formBuilder.group({
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', [])
    })
  }

  private loadDomainListData() {
    this.stateAddressList = this.utilsService.stringEnumToKeyValue(StateAddress)
  }
  

}
