import { Component, OnInit, Inject, Injector, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder, AbstractControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { BehaviorSubject } from 'rxjs';
import { BaseComponent } from '../../_base/base-component.component';
import { Customer, PersonType, MaritalStatus, PersonGender } from '../../core/models/customer.model';
import { MaskUtils } from '../../core/mask-utils';
import { UtilsService } from '../../core/utils.service';
import { CustomersService } from '../../core/services/customers.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';


@Component({
  selector: 'tnm-customer-personal',
  templateUrl: './customer-personal.component.html'
})

export class CustomerPersonalComponent extends BaseComponent implements OnInit {

  generalForm: FormGroup

  customer: Customer
  brandList: Array<any>
  maritialStatusList: Array<any>
  personGenderList: Array<any>
  isPerson: Boolean = true
  birthDate: Date
  isEdition: Boolean = false
  customerName: String = ''
  isSale: Boolean = false
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');
  phoneMask = MaskUtils.phoneMask
  mobilePhoneMask = MaskUtils.mobilePhoneMask
  dateMask = MaskUtils.dateMask

  isHideField: boolean = false

  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask

  searchField: FormControl
  customersList: Array<any>
  isRequired: Boolean = false
  isCreateUserAdmin: Boolean = false

  personTypeSubject: BehaviorSubject<PersonType> = new BehaviorSubject(PersonType.PERSON)
  personType = this.personTypeSubject.asObservable();

  cutomerSubject: BehaviorSubject<Customer> = new BehaviorSubject(Customer['id'])
  customerSelected = this.cutomerSubject.asObservable();

  searchSubject = new Subject()

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private utils: UtilsService,
    private router: Router,
    private customersService: CustomersService
  ) {
    super(injector)
  }

  ngOnInit() {

    if (this.router.url == '/new/contract' || this.router.url == '/sales/new/contract' || this.router.url == '/new/sale'|| this.router.url == '/sales/new/sale' || this.router.url == '/insurance/quotes/new' || this.router.url.indexOf('/devices/details/') > -1 ) {
      this.isSale = true
    } else {
      this.isSale = false
    }

    this.searchField = this.formBuilder.control('', [Validators.required])

    this.initForms()
    this.loadDomainListData()

    if (this.isSale) {
      this.searchSubject
        .debounceTime(1000)
        .subscribe((val: string) => {
          this.loadCustomers(val)
        })
    }

  }

  searchOnChange(event) {
    this.searchSubject.next(event.currentTarget.value);
  }

  public loadCustomerData(customer: Customer) {

    this.brandList = [];
    this.brandList = this.appContext.brands

    this.generalForm.setValue({
      brand: customer.brand || null,
      personType: customer.personType || null,
      name: customer.name || null,
      birthDate: this.convertLoadDate(customer.birthDate) || null,
      gender: customer.gender || null,
      mobilePhone: customer.mobilePhone || null,
      residencialPhone: customer.residencialPhone || null,
      commercialPhone: customer.commercialPhone || null,
      occupation: customer.occupation || null,
      maritialStatus: customer.maritialStatus || null,
      email: customer.email || null,
      procurator: customer.procurator || null,
      registrationNumber: customer['registrationNumber'] || null,
      password:null,
      confirmPassword:null
    })

    this.generalForm.controls.brand.disable({ onlySelf: true });
  }

  openWhatsapp() {
    window.open('https://api.whatsapp.com/send?phone=55' + this.generalForm.value.mobilePhone + '&text=sua%20mensagem#', "_blank");
  }

  loadStatusCustomer(status) {
    setTimeout(() => {
      this.isEdition = status;
    });

  }

  public setRequired() {
    this.isRequired = true

    this.generalForm = this.formBuilder.group({
      personType: this.formBuilder.control('PERSON', [Validators.required]),
      name: this.searchField,//this.formBuilder.control('', [Validators.required]),
      birthDate: this.formBuilder.control(null),
      gender: this.formBuilder.control('NONE'),
      occupation: this.formBuilder.control(null),
      maritialStatus: this.formBuilder.control(null),
      mobilePhone: this.formBuilder.control('', [Validators.required]),
      residencialPhone: this.formBuilder.control(null, [Validators.required]),
      commercialPhone: this.formBuilder.control(null),
      email: this.formBuilder.control('', [Validators.required]),
      brand: this.formBuilder.control(this.appContext.session.user.brand, [Validators.required]),
      procurator: this.formBuilder.control(null),
      registrationNumber: this.formBuilder.control(null),
      password:this.formBuilder.control(null),
      confirmPassword:this.formBuilder.control(null)
    })

  }

  public setRequiredCompany() {
    this.isRequired = false

    this.generalForm = this.formBuilder.group({
      personType: this.formBuilder.control('PERSON', [Validators.required]),
      name: this.searchField,//this.formBuilder.control('', [Validators.required]),
      birthDate: this.formBuilder.control(null),
      gender: this.formBuilder.control('NONE'),
      occupation: this.formBuilder.control(null),
      maritialStatus: this.formBuilder.control(null),
      mobilePhone: this.formBuilder.control('', [Validators.required]),
      residencialPhone: this.formBuilder.control(null, [Validators.required]),
      commercialPhone: this.formBuilder.control(null),
      email: this.formBuilder.control('', [Validators.required]),
      brand: this.formBuilder.control(this.appContext.session.user.brand),
      procurator: this.formBuilder.control(null),
      registrationNumber: this.formBuilder.control(null),
      password:this.formBuilder.control(null),
      confirmPassword:this.formBuilder.control(null)
    })

  }

  changeType() {
    setTimeout(() => {
      if (this.isHideField && this.generalForm.value.personType == 'PERSON') {
        this.generalForm.controls.registrationNumber.setValidators([Validators.required, validCPF])
      } else if (this.isHideField) {
        this.generalForm.controls.registrationNumber.setValidators([Validators.required, validCNPJ])
      }
    }, 100)
  }

  public setHideFields() {
    this.isHideField = true
    setTimeout(() => {
      this.generalForm.controls.registrationNumber.setValidators([Validators.required, validCPF])
    }, 100)
  }

  public setCreatUserAdmin() {
    this.isCreateUserAdmin = true
    
    setTimeout(() => {
      this.generalForm.controls.password.setValidators([Validators.required, Validators.minLength(8)])
      this.generalForm.controls.confirmPassword.setValidators([Validators.required, checkConfirmPassword])
      this.searchSubject
        .debounceTime(1000)
        .subscribe((val: string) => {
          this.loadCustomers(val)
      })
    }, 100)
  }

  public getCustomerData(): any {
    return this.generalForm.value
  }

  public getCustomerForm(): FormGroup {
    return this.generalForm
  }

  public isInvalid() {
    return this.generalForm ? this.generalForm.invalid : false
  }

  public setReadOnly() {
    this.generalForm.disable()
  }

  searchCustomer() {

    this.customersService.getCustomersBySearchTerms(this.searchField.value)
      .catch(error => Observable.from([]))
      .subscribe(customers => {
        this.customersList = customers.content
      })

  }

  public setCustomer(customer) {
   
    this.customersList = [];
    this.loadCustomerData(customer)
    this.cutomerSubject.next(customer)
  }

  public initForms() {

    this.generalForm = this.formBuilder.group({
      personType: this.formBuilder.control('PERSON', [Validators.required]),
      name: this.searchField,//this.formBuilder.control('', [Validators.required]),
      birthDate: this.formBuilder.control(null),
      gender: this.formBuilder.control('NONE'),
      occupation: this.formBuilder.control(null),
      maritialStatus: this.formBuilder.control(null),
      mobilePhone: this.formBuilder.control('', [Validators.required]),
      residencialPhone: this.formBuilder.control(null),
      commercialPhone: this.formBuilder.control(null),
      email: this.formBuilder.control('', [Validators.required]),
      brand: this.formBuilder.control(this.appContext.session.user.brand, [Validators.required]),
      procurator: this.formBuilder.control(null),
      registrationNumber: this.formBuilder.control(null),
      password:this.formBuilder.control(null),
      confirmPassword:this.formBuilder.control(null)
    })

    this.generalForm.valueChanges.subscribe((newForm) => {
      this.isPerson = (newForm.personType == 'PERSON');

      if (this.isPerson) {
        this.personTypeSubject.next(PersonType.PERSON)
        this.generalForm.controls.procurator.setValidators(null);
        this.generalForm.controls.maritialStatus.setValidators(null);
        this.generalForm.controls.birthDate.setValidators(null);
        this.generalForm.controls.gender.setValidators(null);
        this.generalForm.controls.occupation.setValidators(null);
      } else {
        this.personTypeSubject.next(PersonType.COMPANY)
        this.generalForm.controls.procurator.setValidators([Validators.required]);
        this.generalForm.controls.maritialStatus.setValidators(null);
        this.generalForm.controls.birthDate.setValidators(null);
        this.generalForm.controls.gender.setValidators(null);
        this.generalForm.controls.occupation.setValidators(null);
      }
    });
  }

  private loadDomainListData() {

    let index = this.appContext.brands.map((brand) => { return brand.id; }).indexOf(this.appContext.loggedUser.brand)
    this.brandList = [];
    if (index > -1) {
      this.brandList.push(this.appContext.brands[index])
    }

    this.maritialStatusList = this.utils.stringEnumToKeyValue(MaritalStatus)
    this.personGenderList = this.utils.stringEnumToKeyValue(PersonGender)
  }

  birthDateChanged(event: MatDatepickerInputEvent<Date>) {
    this.birthDate = new Date(event.value)
  }

  convertLoadDate(date) {
    let _date = new Date(date)
    let day = _date.getDate()
    let month = _date.getMonth() + 1
    return (day < 10 ? '0' + day : day) + '/' + (month < 10 ? '0' + month : month) + '/' + _date.getFullYear()
  }

  loadCustomers(filter?: string) {
    
    if (this.validateEmail(filter)) {

      let _filter =  filter.replace(/\s/g, '');
      
      this.customersService.getCustomersByEmail(
        filter).subscribe(result => {
         
          if (result.content && result.content.length == 1) {
            this.setCustomer(result.content[0])
          }else if(result.content && result.content.length > 1){
            result.content.forEach(customer => {
              if(customer.email == _filter){
                this.setCustomer(customer)
              }
            });
          }
        })

    }

  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}

function validCPF(control: AbstractControl) {

  if (!control.parent || !control) {
    return;
  }

  const cpfCnpj = control.parent.get('registrationNumber');

  if (!cpfCnpj || !cpfCnpj.value) {
    return
  }

  let c = cpfCnpj.value.replace(/\D+/g, '')

  if (!/[0-9]{11}/.test(c)) {
    return {
      invalidCPF: true
    };
  }

  var r;
  var s = 0;

  for (var i = 1; i <= 9; i++)
    s = s + parseInt(c[i - 1]) * (11 - i);

  r = (s * 10) % 11;

  if ((r == 10) || (r == 11))
    r = 0;

  if (r != parseInt(c[9])) {
    return {
      invalidCPF: true
    };
  }


  s = 0;

  for (i = 1; i <= 10; i++)
    s = s + parseInt(c[i - 1]) * (12 - i);

  r = (s * 10) % 11;

  if ((r == 10) || (r == 11))
    r = 0;

  if (r != parseInt(c[10])) {
    return {
      invalidCPF: true
    };
  }

  return
}

function validCNPJ(control: AbstractControl) {

  if (!control.parent || !control) {
    return;
  }

  const registrationNumber = control.parent.get('registrationNumber');

  if (!registrationNumber || !registrationNumber.value) {
    return
  }

  let cnpj = registrationNumber.value.replace(/\D+/g, '')

  var b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];

  if ((cnpj = cnpj.replace(/[^\d]/g, "")).length != 14) {
    return {
      invalidCNPJ: true
    }
  }


  if (/0{14}/.test(cnpj)) {
    return {
      invalidCNPJ: true
    }
  }


  for (var i = 0, n = 0; i < 12; n += cnpj[i] * b[++i]);
  if (cnpj[12] != (((n %= 11) < 2) ? 0 : 11 - n)) {
    return {
      invalidCNPJ: true
    }
  }

  for (var i = 0, n = 0; i <= 12; n += cnpj[i] * b[i++]);
  if (cnpj[13] != (((n %= 11) < 2) ? 0 : 11 - n)) {
    return {
      invalidCNPJ: true
    }
  }

  return

}

function checkConfirmPassword(control: AbstractControl)
{

    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('confirmPassword');


    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}