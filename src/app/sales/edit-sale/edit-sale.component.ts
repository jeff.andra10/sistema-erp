import { Component, OnInit, Injector, ViewEncapsulation } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder, Form } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatStepper } from '@angular/material';
import { PlansService } from '../../core/services/plans.service';
import { Observable } from 'rxjs/Observable';
import { Vehicle } from '../../core/models/vehicle.model';
import { BaseComponent } from '../../_base/base-component.component';
import { Sale, SalesStatus } from '../../core/models/sale.model';
import { SalesService } from '../../core/services/sales.service';
import { SalesSelectedService } from '../sales.selected.service';
import { PaymentType, Payment,PaymentCreditCard,ChargeType } from '../../core/models/payment.model';
import { fuseAnimations } from '@fuse/animations';
import { PaymentsService } from '../../core/services/payments.service';
import { CustomersService } from '../../core/services/customers.service';
import { Address } from '../../core/models/address.model';
import { ProductsService } from 'app/core/services/products.service';
import { ServicesService } from 'app/core/services/services.service';
import { BrandsService } from '../../core/services/brands.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";


@Component({
  selector: 'tnm-edit-sale',
  templateUrl: './edit-sale.component.html',
  styleUrls: ['./edit-sale.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations  
})
export class EditSaleComponent extends BaseComponent implements OnInit, AfterViewInit {

  sale: Sale = new Sale()
  payment? : Payment
  vehicle: Vehicle
  canApprove: boolean = true
  canCancel: boolean = true
  isList: boolean = false
  isSale: boolean = false
  statusList = [] 
  address: Address
  statusPayment : string
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  chargeList = []
  servicesList: any
  productsList: any
  
  installmentsPayment : number = 1 
  
  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private router: Router,
    private productsService: ProductsService,
    private servicesService: ServicesService,
    private salesService: SalesService,
    private salesSelectedService: SalesSelectedService,
    private paymentsService: PaymentsService,
    private customersService: CustomersService,
    private plansService: PlansService,
    private brandsService: BrandsService,
    private loadingService: FuseSplashScreenService
  ) {
    super(injector)
   }

  ngOnInit() {

    localStorage.setItem('hiddenLoading','true');
    
    this.statusPayment = 'PENDING'

    this.getSales()

   }

  

   getSales(){
    this.salesSelectedService.salesSelected.subscribe(sale => {
      
      if (sale != null) {
        
        this.sale = sale
        this.isSale = true
 
        this.createStatusList()
        this.canApprove = (SalesStatus[this.sale.status] == SalesStatus.OPEN)
        this.canCancel = (SalesStatus[this.sale.status] == SalesStatus.PAID ? false : true)

        this.installmentsPayment = sale['installments']
        this.payment = sale.relationships['payment']

        this.paymentsService.getChargesBySaleId(this.sale.id)
                .subscribe(charge =>{
                  if(charge.content.length > 0){
                    this.chargeList = charge.content;
                    this.statusPayment = charge.content[0].status ? charge.content[0].status : 'PENDING'
                    this.loadingService.hide()
                  }else{
                    this.loadingService.hide()
                  }   
                
        })

        this.getProducts()
        .subscribe(
          products => {
            this.productsList = products.content
            this.getServices()
              .subscribe(
                services => {
          
                    this.servicesList = services.content
                    if(this.sale.relationships['offer'] && this.sale.relationships['offer'].products){
                      this.sale['products'] = this.utilsService.getCompleteListByIdsList(this.sale.relationships['offer'].products, this.productsList)
                    }
                    if(this.sale.relationships['offer'] && this.sale.relationships['offer'].services){
                      this.sale['services'] = this.utilsService.getCompleteListByIdsList(this.sale.relationships['offer'].services, this.servicesList)
                    }
                    
                }               
              )
          }
        )

      this.plansService.getPlanById(this.sale.plan)
        .subscribe(plan => {
          console.log('Plan:',plan)
          this.vehicle = plan.content[0].relationships.vehicle
        })

      }
    })
   }

  ngAfterViewInit() {

    if(this.sale && this.sale['segment'] == 'CUSTOMERS'){
        setTimeout(() => {
          this.customersService.getCustomerAddress(this.appContext.session.user.id).subscribe(address =>{   
            if(address.content && address.content.length > 0){
                this.address = address.content[0]
            }
          })
        })
   }else{

      setTimeout(() => {
        this.brandsService.getBrandAddress(this.sale['business']).subscribe(address =>{ 
          if(address.content && address.content.length > 0){
            this.address = address.content[0]
          }
        })
      })

   }

  }

  goBack(){
    this.router.navigate(['/sales'])
  }

  salesStatusEnumToString(value) : string {
    return SalesStatus[value]
  }

  paymentTypeEnumToString(value) : string {
    return PaymentType[value]
  }

   phone (tel) {
    if (!tel) { return ''; }

    var value = tel.toString().trim().replace(/^\+/, '');

    if (value.match(/[^0-9]/)) {
        return tel;
    }

    var city, number;

    switch (value.length) {
        case 10: // +1PPP####### -> C (PPP) ###-####
           
            city = value.slice(0, 2);
            number = value.slice(2);
            break;

        case 11: // +CPPP####### -> CCC (PP) ###-####
           
            city = value.slice(0, 2);
            number = value.slice(2);
            break;

        case 12: // +CCCPP####### -> CCC (PP) ###-####
           
            city = value.slice(3, 5);
            number = value.slice(5);
            break;

        default:
            return tel;
    }


    number = number.slice(0, 5) + '-' + number.slice(5);

    return (" (" + city + ") " + number).trim();
};

  approveSales() {
    
    if(this.sale && this.sale['segment'] == 'CUSTOMERS'){
        this.salesService.approveToCustomer(this.sale.id, this.appContext.session.user.id)
          .subscribe (result=>{

            this.snackbar.open('Venda aprovada com sucesso', '', {
              duration: 8000
            })
            this.goBack()
        })
    }else{
        this.salesService.approve(this.sale.id, this.appContext.session.user.id)
          .subscribe (result=>{

            this.snackbar.open('Venda aprovada com sucesso', '', {
              duration: 8000
            })
            this.goBack()
        })
    }
    
  }

  cancelSales(){
    this.salesService.cancel(this.sale.id, this.appContext.session.user.id, this.sale['segment'])
    .subscribe (result=>{

      this.snackbar.open('Venda cancelada com sucesso', '', {
        duration: 8000
      })
      this.goBack()
    })
  }

  rejectSales() {

     if(this.sale && this.sale['segment'] == 'CUSTOMERS'){
      this.salesService.rejectToCustomer(this.sale.id, this.appContext.session.user.id)
      .subscribe (result=>{

        this.snackbar.open('Venda rejeitada com sucesso', '', {
          duration: 8000
        })
        this.goBack()
      })
  }else{
    this.salesService.reject(this.sale.id, this.appContext.session.user.id)
    .subscribe (result=>{

      this.snackbar.open('Venda rejeitada com sucesso', '', {
        duration: 8000
      })
      this.goBack()
    })
  }
}

getProducts(): Observable<any> {
  return this.productsService.getProducts()
}

getServices(): Observable<any> {
  return this.servicesService.getServices()
}

downloadBooklet(url){
  window.open(url, "_blank");
} 

createStatusList() {

    var statusList = []

    if (this.sale.lifecycle.createDate) {
      statusList.push({ status: SalesStatus.OPEN , date: this.sale.lifecycle.createDate })
    }

    if (this.sale.lifecycle.approveDate) {
      statusList.push({ status: SalesStatus.APPROVED , date: this.sale.lifecycle.approveDate })
    }
    
    if (this.sale.lifecycle.rejectDate) {
      statusList.push({ status: SalesStatus.REJECTED, date: this.sale.lifecycle.rejectDate })
    }

    if(statusList.length > 0){
      this.isList = true;
    }else{
      this.isList = false;
    }
    
    this.statusList = statusList
  }

  PaymentCreditCardEnumToString(value) : string {
    return PaymentCreditCard[value]
  }

  maskPostalCode(number) {
    number+="";
    if(number.length == 8){
        number = number.replace(/\D/g, '');
        number = number.match(/^(\d{5})(\d{3})$/);
        number = (!number) ? null : number[1]+ '-' + number[2];
    }else{
        number = number.replace(/\D/g, '');
        number = number.match(/^(\d{5})(\d{2})$/);
        number = (!number) ? null : number[1]+ '-' + number[2] +'0';
    }
   
    return number;
  }   


  
}
