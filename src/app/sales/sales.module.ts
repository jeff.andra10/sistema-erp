import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../angular-material/material.module";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "@fuse/shared.module";


import { SalesComponent } from "./sales.component";
import { NewSaleComponent } from "./new-sale/new-sale.component";
//import { DialogsModule } from './../dialogs/dialogs.module'
//import { InputDialogComponent } from './../dialogs/input-dialog/input-dialog.component'
//import { ConfirmDialogComponent } from './../dialogs/confirm-dialog/confirm-dialog.component'
import { CustomerPersonalComponent } from "../customers/customer-personal/customer-personal.component";
import { CustomerDocumentsComponent } from "../customers/customer-documents/customer-documents.component";
import { VehicleDetailsComponent } from "../vehicles/vehicle-details/vehicle-details.component";
import { CustomersModule } from "../customers/customers.module";
import { VehiclesModule } from "../vehicles/vehicles.module";
import { ContractsModule } from '../contracts/contracts.module';
import { CustomerAddressComponent } from "../customers/customer-address/customer-address.component";
import { PaymentsModule } from "../payments/payments.module";
import { PaymentsComponent } from "../payments/payments.component";
import { EditSaleComponent } from "./edit-sale/edit-sale.component";
import { SalesSelectedService } from "./sales.selected.service";
import { SalesFreightComponent } from "./sales-freight/sales-freight.component";


const authRouting: ModuleWithProviders = RouterModule.forChild([
    {path: '', component: SalesComponent },
    {path: 'new/sale', component: NewSaleComponent },
    {path: 'new/contract', component: NewSaleComponent },
    {path: ':id', component: EditSaleComponent }

]);

@NgModule({
    declarations:[
        SalesComponent,
        NewSaleComponent,
        EditSaleComponent,
        SalesFreightComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        TextMaskModule,
        FlexLayoutModule,
        //DialogsModule,
        RouterModule,
        CustomersModule,
        VehiclesModule,
        ContractsModule,
        PaymentsModule,
        authRouting
    ],
    exports: [
        SalesComponent,
        SalesFreightComponent
    ],
    entryComponents: [
        SalesComponent,
        //ConfirmDialogComponent,
        //InputDialogComponent,
        CustomerDocumentsComponent,
        CustomerPersonalComponent,
        VehicleDetailsComponent,
        CustomerAddressComponent,
        PaymentsComponent,
        EditSaleComponent,
        SalesFreightComponent
    ],
    providers: [
        SalesSelectedService
    ]
})
export class SalesModule {}