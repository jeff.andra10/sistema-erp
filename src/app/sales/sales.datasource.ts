import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";

import { SalesService } from "app/core/services/sales.service";
import { UtilsService } from "app/core/utils.service";
import { PaginatedContent } from "app/core/models/paginatedContent.model";
import { Sale } from "app/core/models/sale.model";
import { Brand } from "../core/models/brand.model";
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";

export class SalesDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<Sale[]>([]);
    private totalElementsSubject = new BehaviorSubject<number>(0)
    public totalElements$ = this.totalElementsSubject.asObservable()
    

    constructor(
        private salesService : SalesService, 
        private utilsService : UtilsService,
        private brands: Array<Brand> ,
        private loadingService: FuseSplashScreenService)
    {}

    load(filter:string,
        status: string,
        sortField:string,
        sortDirection:string,
        pageIndex:number,
        pageSize:number) {

        // if(!this.loadingService.isLoading){
        //     this.loadingService.show()
        // }
        
        this.salesService.findSales(
            filter, 
            status,
            sortField,
            sortDirection,
            pageIndex, 
            pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe((result : PaginatedContent<Sale>) => {

                if (result.content && this.brands) {
                    result.content.forEach(obj => {
                        obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                    });
                }

                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)

               // this.loadingService.hide()
           
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<Sale[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
