import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { Observable } from 'rxjs/Observable';
import { MatTableDataSource, MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SalesService } from '../../core/services/sales.service';

@Component({
  selector: 'tnm-sales-freight-search',
  templateUrl: './sales-freight.component.html',
})

export class SalesFreightComponent implements OnInit {

  salesFreightForm: FormGroup
  searchField: FormControl
  freightList: Array<any>

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<SalesFreightComponent>,
    private salesService: SalesService
  ) { }

  ngOnInit() {
    
    this.initForms()
  }

  initForms() {
    this.searchField = this.formBuilder.control('')

    this.salesFreightForm = this.formBuilder.group({
      searchField: this.searchField
    });

    this.searchField.valueChanges
    .debounceTime(2000)
    .distinctUntilChanged()
    .switchMap(searchTerm =>

      this.salesService.calculateFreight(searchTerm)
      .catch(error => Observable.from([])))
      .subscribe(freights => {
        this.freightList = freights
      })
  }

  searchFreight(){
    this.searchField.valueChanges
    .debounceTime(0)
    .distinctUntilChanged()
    .switchMap(searchTerm =>

      this.salesService.calculateFreight(searchTerm)
      .catch(error => Observable.from([])))
      .subscribe(freights => {
        this.freightList = freights
      })
  }
  setSalesFreight(freight) {
    this.dialogRef.close({freight: freight})
  }
  
}