import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

import { Sale } from "../core/models/sale.model";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class SalesSelectedService {

    private saleSource : BehaviorSubject<Sale> = new BehaviorSubject(null)
    public salesSelected = this.saleSource.asObservable();
  
    setSaleSelected(sale: Sale) {
      this.saleSource.next(sale)
    }
}