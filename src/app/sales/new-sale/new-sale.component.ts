import { Component, OnInit, Injector } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder, Form} from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatStepper, MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { CustomerPersonalComponent } from '../../customers/customer-personal/customer-personal.component';
import { CustomerDocumentsComponent } from '../../customers/customer-documents/customer-documents.component';
import { VehicleDetailsComponent } from '../../vehicles/vehicle-details/vehicle-details.component';
import { ProductsService } from 'app/core/services/products.service';
import { ServicesService } from 'app/core/services/services.service';
import { PlansService } from 'app/core/services/plans.service';
import { SalesService } from 'app/core/services/sales.service';
import { PaymentsComponent } from '../../payments/payments.component';
import { Address } from 'app/core/models/address.model';
import { Offer } from 'app/core/models/offer.model';
import { Brand } from 'app/core/models/brand.model';
import { Customer } from 'app/core/models/customer.model';
import { CustomerAddressComponent } from '../../customers/customer-address/customer-address.component';
import { BaseComponent } from '../../_base/base-component.component';
import { OffersService } from '../../core/services/offers.service';
import { Payment,PaymentType} from '../../core/models/payment.model';
import { CustomersService } from '../../core/services/customers.service';
import { PaymentsService } from '../../core/services/payments.service';
import { SalesFreightComponent } from "../sales-freight/sales-freight.component";
import { AssignServiceOrder } from '../../core/models/service-orders.model';
import { StateAddress } from '../../core/common-data-domain';
import { MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MaskUtils } from '../../core/mask-utils';
import { ServiceOrdersService } from '../../core/services/service-orders.service';
import { CONSTANTS } from '../../core/common-data-domain';
import { UserService } from 'app/core/services/users.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { UtilsService } from '../../core/utils.service';
import * as moment from 'moment';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'tnm-new-sale',
  templateUrl: './new-sale.component.html',
  styleUrls: ['./new-sale.component.scss']
})
export class NewSaleComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(CustomerPersonalComponent)
  public customerPersonalComponent: CustomerPersonalComponent;

  @ViewChild(CustomerDocumentsComponent)
  public customerDocumentsComponent: CustomerDocumentsComponent

  @ViewChild(CustomerAddressComponent)
  public customerAddressComponent: CustomerAddressComponent

  @ViewChild(VehicleDetailsComponent)
  public vehicleDetailsComponent: VehicleDetailsComponent

  @ViewChild(PaymentsComponent)
  private paymentsComponent: PaymentsComponent

  @ViewChild('stepper') 
  private stepper: MatStepper;

  servicesList: any
  productsList: any
  offersList: Array<Offer>
  selectedPlan: Offer
  brandList: Array<Brand>
  paymentSelected : Payment
  customerSelected : Customer
  customerAddress : Address
  generalForm: FormGroup
  freePlan : boolean = false  
  installmentSelected: 1
  dueDaySelected : 1
  summary : any
  sale : any
  personType : String = "CPF"
  payment : Payment
  freight : any
  offers : any;
  buttonSale : boolean = false 
  isContract : boolean = false 
  scheduleForm: FormGroup
  stateAddressList: any
  scheduleTimeList: Array<any>
  installerUser: any
  scheduledDate: Date
  postalCodeMask =  MaskUtils.postalCodeMask
  offersAgreementFree = 0
  assign : any
  searchField: FormControl
  usersList: Array<any>
  userName : string = ''
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  typePayment : string = ''
  scheduledTimeHours = 0 

  vehicleComplete : boolean = false
  offersComplete : boolean = false
  summaryComplete : boolean = false
  paymentComplete : boolean = false
  isScheduling: boolean = false
  selectedTime: any

  public customDateMask = {
    guide: true,
    showMask : true,
    mask: [/\d/, /\d/, '/', /\d/, /\d/, '/',/\d/, /\d/,/\d/, /\d/]
  };

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    public  dialog: MatDialog,
    private router: Router,
    private productsService: ProductsService,
    private servicesService: ServicesService,
    private plansService: PlansService,
    private salesService: SalesService,
    private customersService: CustomersService,
    private offersService: OffersService,
    private paymentsService: PaymentsService,
    private serviceOrdersService: ServiceOrdersService,
    private usersService: UserService,
    private loadingService: FuseSplashScreenService,
    private utils: UtilsService
  ) {
    super(injector)
    
   }

   initForms() {
    this.generalForm = this.formBuilder.group({
      user: this.formBuilder.control({value:'', disabled: true})
    })

    // schedule form
    this.scheduleForm = this.formBuilder.group({ 
      installer: this.searchField,
      scheduledDate: this.formBuilder.control({value:'', disabled: true}, [Validators.required]),
      time: this.formBuilder.control('', [Validators.required]),
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', []),
      check: this.formBuilder.control('', [])
    })

  }


  ngOnInit() {

    localStorage.setItem('hiddenLoading','true');

    this.searchField = this.formBuilder.control('', [Validators.required])

    this.searchField.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap(searchTerm =>
        this.usersService.getUsersByProfileAndName("INSTALLER", searchTerm)
        .catch(error => Observable.from([])))
        .subscribe(users => {
          if(this.userName != this.scheduleForm.value.installer){
             this.usersList = users.content
          }
      })

    if(this.router.url == '/new/contract' || this.router.url == '/sales/new/contract'){
      this.isContract = true
      this.paymentComplete = true
      this.loadScheduleTime()
    }else{
      this.isContract = false
    }

    this.brandList = this.appContext.brands
    this.buttonSale  = false
    
    this.getProducts()
    .subscribe(
      products => {
        this.productsList = products.content
        
        this.getServices()
          .subscribe(
            services => {
              this.servicesList = services.content
              
              this.getPlans()

            }
          )
      }
    )

    this.initForms()

    

  }

  ngAfterViewInit() {

    this.customerPersonalComponent.personType
    .subscribe(type => {
      this.customerDocumentsComponent.setPersonType(type)
    })

    this.customerPersonalComponent.customerSelected
    .subscribe(customer => {
      if(customer){

          this.customerSelected = customer
          this.customerDocumentsComponent.loadCustomerData(this.customerSelected)
          if(!this.isContract){
             this.paymentsComponent.setCustomer(this.customerSelected)
          }

          this.generalForm.patchValue ({
            user: this.customerSelected.name
          })

          this.getPlans()

          this.customersService.getCustomerAddress(this.customerSelected.user)
          .subscribe(addresses => {
              if (addresses.content != null && addresses.content.length > 0) {
                this.customerAddress = addresses.content[0]
                this.customerAddressComponent.loadData(this.customerAddress)
                this.loadAddress(this.customerAddress)
              }
          })
      }
    })

    let customer = new Customer()
        customer.user = null

    if(!this.isContract){
      this.paymentsComponent.setCustomer(customer)
    }

  }

  getPlans() {

    let params = { limit: 200, page: 0}

    let maxProductLength = 0;
    let maxServiceLength = 0;

    let allOffersList = [];

    this.offersService.getAvailableForAllBrands(params)
      .subscribe(allOffersList => {

        

      allOffersList = allOffersList.content;
      this.offersService.getOffers(params).subscribe(offersList => {

       
        let offers = [];

        offersList.content.forEach(offer => {
          allOffersList.push(offer)
        })

      
        allOffersList.forEach(offer => {

          if(maxProductLength < offer.products.length){
            maxProductLength = offer.products.length
          }
          if(maxProductLength < offer.services.length){
            maxServiceLength = offer.services.length
          }
          
          if(this.isContract){
            if(offer.segment == 'CUSTOMERS' && offer.agreement == 'FREE'){
              offers.push(offer)
              this.freePlan = true;
            }
          }else{
            if(offer.segment == 'CUSTOMERS' && offer.agreement != 'FREE'){
              offers.push(offer)

            }
          }
        });

        
        offers.forEach(offer => {
          offer.brand =  this.utilsService.getItemById(offer.brand, this.brandList)
          offer.products = this.utilsService.getCompleteListByIdsList(offer.products, this.productsList)
          offer.services = this.utilsService.getCompleteListByIdsList(offer.services, this.servicesList)
        });

        offers.forEach(offer => {
          if(maxProductLength > offer.products.length){
            for(var i=0; i< (maxProductLength-offer.products.length)+1; i++){offer.products.push({})}
          }
          if(maxServiceLength > offer.services.length){
            for(var i=0; i< (maxServiceLength-offer.services.length)+1; i++){offer.services.push({})}
          }
        });

        if(this.isContract){
          if(offers.length == 1){
              this.selectedPlan = offers[0]
          }
        }

        this.offersList = offers
      })
    })
  }

  onCheck(event) {
    
    this.isScheduling = event.checked
    if(this.isScheduling){
      this.scheduleForm.controls.installer.disable({onlySelf: true});
      this.scheduleForm.controls.scheduledDate.disable({onlySelf: true});
      this.scheduleForm.controls.time.disable({onlySelf: true});
      this.scheduleForm.controls.street.disable({onlySelf: true});
      this.scheduleForm.controls.number.disable({onlySelf: true});
      this.scheduleForm.controls.complement.disable({onlySelf: true});
      this.scheduleForm.controls.district.disable({onlySelf: true});
      this.scheduleForm.controls.city.disable({onlySelf: true});
      this.scheduleForm.controls.state.disable({onlySelf: true});
      this.scheduleForm.controls.postalCode.disable({onlySelf: true});
      this.scheduleForm.controls.reference.disable({onlySelf: true});
    }else{
      this.scheduleForm.controls.installer.enable({onlySelf: true});
      this.scheduleForm.controls.scheduledDate.enable({onlySelf: true});
      this.scheduleForm.controls.time.enable({onlySelf: true});
      this.scheduleForm.controls.street.enable({onlySelf: true});
      this.scheduleForm.controls.number.enable({onlySelf: true});
      this.scheduleForm.controls.complement.enable({onlySelf: true});
      this.scheduleForm.controls.district.enable({onlySelf: true});
      this.scheduleForm.controls.city.enable({onlySelf: true});
      this.scheduleForm.controls.state.enable({onlySelf: true});
      this.scheduleForm.controls.postalCode.enable({onlySelf: true});
      this.scheduleForm.controls.reference.enable({onlySelf: true});

    }
  }

  loadAddress(address: Address) {

    this.scheduleForm.setValue({ 
      installer: null,
      scheduledDate: null,
      time: null,
      street: address.street || null,
      number: address.number || null,
      complement: address.complement || null,
      district: address.district || null,
      city: address.city || null,
      state: address.state || null,
      postalCode: address.postalCode || null,
      reference: address.reference || null,
      check:false
    })
  } 

  getProducts(): Observable<any> {
    return this.productsService.getProducts()
  }

  getServices(): Observable<any> {
    return this.servicesService.getServices()
  }

  stepperGoBack(stepper: MatStepper){
    stepper.previous();
  }

  stepCustomerComplete(){
    this.createCustomer()
  }

  stepperGoForward(stepper: MatStepper){

    this.vehicleComplete = this.vehicleDetailsComponent.isInvalid()    ? false : true;
   
    if(this.isContract  && stepper.selectedIndex  ==  (this.offersList && this.offersList.length > 1 ? 2 : 1)){
      let addressData = this.customerAddressComponent.getData()
      this.loadAddress(addressData)
    }

    //stepper.next();

    setTimeout(() => {
      stepper.next();
    },0);
    
  }

  goBack(){
    this.router.navigate(['/sales'])
  }

  setPlan(stepper: MatStepper,plan) {

    this.offersComplete = true;
    this.paymentComplete = true;

    if(this.isContract){
      let addressData = this.customerAddressComponent.getData()
      this.freePlan = true;
      this.loadAddress(addressData)
    }

    this.selectedPlan = plan
    
    if(!this.isContract){
      this.paymentsComponent.setPlanInstallment(plan.saleMaxInstallments)
      this.paymentsComponent.setPaymentsType(plan['availablePaymentMethods'])
    }

    setTimeout(() => {
      stepper.next();
    },0);
    
  }

  trunck(salePrice,saleMaxInstallments){
    return Math.trunc(salePrice/saleMaxInstallments)
  }

  createSales(stepper: MatStepper) {

    if (!this.validateSales(this.stepper)) {
      return false
    }

    this.generateSale()

    this.summaryComplete = true

    setTimeout(() => {
      stepper.next();
    },0);
  }

  generateContract(stepper: MatStepper){

    if (!this.validateSales(this.stepper)) {
      return false
    }

    this.summaryComplete = true

    if ((!this.isScheduling && !this.installerUser)|| (!this.isScheduling && !this.installerUser.id)) {
      this.snackbar.open('Selecione um Instalador.', '', { duration: 8000 })
      return false
    }

    if (!this.isScheduling && !this.scheduledDate) {
      this.snackbar.open('Selecione uma Data.', '', { duration: 8000 })
      return false
    }

    var currentDate = new Date()

  
    if (!this.isScheduling && (currentDate > this.summaryUtcTimeZome(this.scheduledDate))) {

      this.snackbar.open('Informe uma data superior a data atual', '', {
        duration: 5000
      })
      return false
    }

    
    let momentDate = moment(this.scheduledDate);
   

    let assignData = new AssignServiceOrder ()
    if(!this.isScheduling){
      assignData.loggedUser = this.appContext.session.user.id
      assignData.technician = this.installerUser.id
      assignData.scheduledDate = momentDate.toISOString()
      assignData.street = this.scheduleForm.value.street
      assignData.number = this.scheduleForm.value.number
      assignData.complement = this.scheduleForm.value.complement
      assignData.district = this.scheduleForm.value.district
      assignData.city = this.scheduleForm.value.city
      assignData.state = this.scheduleForm.value.state
      assignData.postalCode = this.utilsService.stringToNumber(this.scheduleForm.value.postalCode),
      assignData.reference = this.scheduleForm.value.reference      
    }
    
    this.assign = assignData

    let customer = new Customer()
    
    let personalData = this.customerPersonalComponent.getCustomerData()
    customer.brand = personalData.brand
    customer.personType = personalData.personType
    customer.name = personalData.name
    customer.mobilePhone = this.utilsService.stringToNumber(personalData.mobilePhone)
    customer.residencialPhone = this.utilsService.stringToNumber(personalData.residencialPhone)
    customer.commercialPhone = this.utilsService.stringToNumber(personalData.commercialPhone)
    customer.email = personalData.email

    if(personalData.personType == 'PERSON'){
      this.personType = "CPF"
      customer.birthDate = this.convertDate(personalData.birthDate)
      customer.gender = personalData.gender
      customer.occupation = personalData.occupation
      customer.maritialStatus = personalData.maritialStatus
    }else{
      this.personType = "CNPJ"
      customer.procurator = personalData.procurator
    }

    let documentsData = this.customerDocumentsComponent.getCustomerData()
    customer.registrationNumber = this.utilsService.stringToNumber(documentsData.registrationNumber)
    
    if(personalData.personType == 'PERSON'){
      customer.nationalIdentity = this.utilsService.stringOnlyDigits(documentsData.nationalIdentity)
      customer.nationalIdentityExpeditionDate = this.convertDate(documentsData.nationalIdentityExpeditionDate)
      customer.nationalIdentityExpeditionAgency = documentsData.nationalIdentityExpeditionAgency
      customer.driverLicense = this.utilsService.stringOnlyDigits(documentsData.driverLicense)
      customer.driverLicenseCategory = documentsData.driverLicenseCategory
    }else{
      customer.stateRegistration = documentsData.stateRegistration
    }

    let addressData = this.customerAddressComponent.getData()
    let customerAddress = new Address()
    customerAddress.street = addressData.street
    customerAddress.number = addressData.number
    customerAddress.complement = addressData.complement
    customerAddress.district = addressData.district
    customerAddress.city = addressData.city
    customerAddress.state = addressData.state
    customerAddress.postalCode = this.utilsService.stringToNumber(addressData.postalCode)
    customerAddress.reference = addressData.reference

    let vehicle = this.vehicleDetailsComponent.getData()

    console.log('vehicle:',vehicle)

    this.summary = {
      customer : customer,
      vehicle: vehicle,
      offer: this.selectedPlan,
      address : customerAddress,
      postalCode: addressData.postalCode,
      assignData: assignData
    }

    setTimeout(() => {
      stepper.next();
    },0);

  
    this.offers = {
      loggedUser:this.appContext.session.user.id,
      brand: this.appContext.session.user.brand,
      broker: this.appContext.session.user.id,
      customer : this.customerSelected ? {'id':this.customerSelected.id} : customer,
      address: this.customerAddress ? {'id':this.customerAddress.id} : customerAddress,
      payment: { type: "NONE" },
      vehicle: vehicle,
      offer: this.selectedPlan.id,
      status: "APPROVED"
    }

  }

  summaryUtcTimeZome(time:Date){
    let date = moment(time).utcOffset(-3);
    date.set({hour:this.scheduledTimeHours,minute:0,second:0,millisecond:0})
    let ndate = date.toDate()
    return  ndate
  }

  createContract(){

    this.loadingService.show()

    this.generateSale()
  
    this.salesService.createNew(this.offers)
    .subscribe(result => {
           this.sale = result

           if(this.sale && !this.isScheduling){
            this.serviceOrdersService.assignServiceOrder(this.sale.serviceOrder, this.assign)
            .subscribe(result => { 

                this.snackbar.open('Contrato criado com sucesso', '', {
                  duration: 5000
                })
                this.router.navigate(['/sales'])
              })
              
          }else{
            this.snackbar.open('Contrato criado com sucesso', '', {
              duration: 5000
            })
            this.router.navigate(['/sales'])
          }
           
    }
    , error => {
      console.log(error)
      this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
      this.buttonSale = false
    })


  }

  generateSale(){

    let customer = new Customer()
    
    let personalData = this.customerPersonalComponent.getCustomerData()
    customer.brand = personalData.brand
    customer.personType = personalData.personType
    customer.name = personalData.name
    customer.mobilePhone = this.utilsService.stringToNumber(personalData.mobilePhone)
    customer.residencialPhone = this.utilsService.stringToNumber(personalData.residencialPhone)
    customer.commercialPhone = this.utilsService.stringToNumber(personalData.commercialPhone)
    customer.email = personalData.email

    if(personalData.personType == 'PERSON'){
      this.personType = "CPF"
      customer.birthDate = this.convertDate(personalData.birthDate)
      customer.gender = personalData.gender
      customer.occupation = personalData.occupation
      customer.maritialStatus = personalData.maritialStatus
    }else{
      this.personType = "CNPJ"
      customer.procurator = personalData.procurator
    }

    let documentsData = this.customerDocumentsComponent.getCustomerData()
    customer.registrationNumber = this.utilsService.stringToNumber(documentsData.registrationNumber)
    
    if(personalData.personType == 'PERSON'){
      customer.nationalIdentity = this.utilsService.stringOnlyDigits(documentsData.nationalIdentity)
      customer.nationalIdentityExpeditionDate = this.convertDate(documentsData.nationalIdentityExpeditionDate)
      customer.nationalIdentityExpeditionAgency = documentsData.nationalIdentityExpeditionAgency
      customer.driverLicense = this.utilsService.stringOnlyDigits(documentsData.driverLicense)
      customer.driverLicenseCategory = documentsData.driverLicenseCategory
    }else{  
      customer.stateRegistration = this.utils.numberOnlyDigits(documentsData.stateRegistration)
    }

    let addressData = this.customerAddressComponent.getData()
    let customerAddress = new Address()
    customerAddress.street = addressData.street
    customerAddress.number = addressData.number
    customerAddress.complement = addressData.complement
    customerAddress.district = addressData.district
    customerAddress.city = addressData.city
    customerAddress.state = addressData.state
    customerAddress.postalCode = this.utilsService.stringToNumber(addressData.postalCode)
    customerAddress.reference = addressData.reference

    let vehicle = this.vehicleDetailsComponent.getData()

    console.log('this vehicle:',vehicle)

    if(this.customerSelected && this.customerSelected.id != CONSTANTS.TRACKNME_CONSTANT_ID){
        this.updateCustomer()
    }

    if(this.paymentSelected && this.paymentSelected.id && this.paymentSelected.id != CONSTANTS.TRACKNME_CONSTANT_ID){
      this.getPayment(this.paymentSelected.id)
    }else{
      this.payment = this.paymentSelected
    }

    this.summary = {
      customer : customer,
      vehicle: vehicle,
      offer: this.selectedPlan,
      installments: this.installmentSelected,
      dueDay: this.dueDaySelected,
      payment:  PaymentType[this.typePayment],
      address : customerAddress,
      postalCode: addressData.postalCode
    }
 
    if(this.paymentSelected && (this.paymentSelected.id == CONSTANTS.TRACKNME_CONSTANT_ID)){
      delete this.paymentSelected['id']
    }

    this.offers = {
      loggedUser:this.appContext.session.user.id,
      brand: this.appContext.session.user.brand,
      broker: this.appContext.session.user.id,
      customer : this.customerSelected ? {'id':this.customerSelected.id} : customer,
      address: this.customerAddress ? {'id':this.customerAddress.id} : customerAddress,
      vehicle: vehicle,
      payment: this.paymentSelected ? this.paymentSelected : { type: "BOOKLET" },
      installments: this.installmentSelected,
      dueDay: this.dueDaySelected,
      offer: this.selectedPlan.id ,
      status: "APPROVED"
    }

  }

  calculateFreight(){

    this.dialog
    .open(SalesFreightComponent, {width: '500px', height: '550px'})
    .afterClosed()
    .subscribe(response => {
      if (response && response.freight) {
        this.freight = response.freight
      }
    });

  }

  saveSale(){
    
    this.generateSale()

    this.buttonSale = true

    this.offers.status = "OPEN";

    this.salesService.createNew(this.offers)
    .subscribe(result => {

           this.buttonSale = false

           this.sale = result
           this.snackbar.open('Venda salva com sucesso', '', {
              duration: 5000
           })
          this.router.navigate(['/sales'])
    }, error => {
         this.buttonSale = false
         this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
    })
  }

  approveSales() {

    this.loadingService.show()
    
    this.generateSale()

    this.buttonSale = true

    this.salesService.createNew(this.offers)
    .subscribe(result => {

           this.sale = result
           this.snackbar.open('Venda aprovada com sucesso', '', {
               duration: 8000
            })
            this.router.navigate(['/sales'])   
    }
    , error => {
      console.log(error)
      this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
      this.buttonSale = false
    })
  }

  convertDate(date){
    if(date){
      let  _date = date.split('/');
           _date =_date[1] +'-'+_date[0]+'-'+_date[2]
      return new Date(_date)
    }
    
  }

  rejectSales() {

      if(this.sale && this.sale['segment'] == 'CUSTOMERS'){
        this.salesService.rejectToCustomer(this.sale.id, this.appContext.session.user.id)
        .subscribe (result=>{

          this.snackbar.open('Venda rejeitada com sucesso', '', {
            duration: 5000
          })
          this.router.navigate(['/sales'])
        })
    }else{
      this.salesService.reject(this.sale.id, this.appContext.session.user.id)
      .subscribe (result=>{

        this.snackbar.open('Venda rejeitada com sucesso', '', {
          duration: 5000
        })
        this.router.navigate(['/sales'])
      })
    }
  }

  onPaymentTypeChanged(type: string) {
    this.paymentSelected = this.paymentsComponent.getData()
    this.installmentSelected = this.paymentsComponent.getInstallments()
    this.dueDaySelected  = this.paymentsComponent.getDueDay()
    this.typePayment = this.paymentsComponent.getTypePayment()
    
    this.createSales(this.stepper)
  }

  mathRound(totalPrice,saleMaxInstallments){
    Math.round(totalPrice/saleMaxInstallments)
  }

  updateCustomer() {

    var customerData = []

    let documentsData = this.customerDocumentsComponent.getCustomerData() 

    if(documentsData.commercialPhone)
      this.utils.numberOnlyDigits(documentsData.stateRegistration)

    for(let item in documentsData){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': item == 'nationalIdentityExpeditionDate' ? this.convertDate(documentsData[item]) : documentsData[item]};
      
      if(documentsData[item] && documentsData[item] != ""){
        customerData.push(editItem);
      }
    }

    let personalData = this.customerPersonalComponent.getCustomerData()
    if(personalData.mobilePhone)
       personalData.mobilePhone = personalData.mobilePhone.replace(/\D+/g, '')
    if(personalData.residencialPhone)
       personalData.residencialPhone = personalData.residencialPhone.replace(/\D+/g, '')
    if(personalData.commercialPhone)
       personalData.commercialPhone = personalData.commercialPhone.replace(/\D+/g, '')
      
    for(let item in personalData){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': item == 'birthDate' ? this.convertDate(personalData[item]) : personalData[item]};
      if(personalData[item] && personalData[item] != ""){
        customerData.push(editItem);
      }
      
    }

    let addressData = []
    let personalAddressData = this.customerAddressComponent.getData()
    for(let item in personalAddressData){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': personalAddressData[item]};
      if(personalAddressData[item] && personalAddressData[item] != ""){
        addressData.push(editItem);
      }
    }

    var updatesCall = [this.customersService.update(this.customerSelected.id, customerData).map((res:Response) => res)]
    
    if (this.customerAddress) {
      updatesCall.push(this.customersService.updateCustomerAddress(this.customerAddress.id, addressData).map((res:Response) => res))
    }

    Observable.forkJoin(updatesCall).subscribe()

  }

  createCustomer() {

    let customer = new Customer()
    
    let personalData = this.customerPersonalComponent.getCustomerData()

    customer.brand = personalData.brand
    customer.personType = personalData.personType
    customer.name = personalData.name
    customer.mobilePhone = this.utils.stringToNumber(personalData.mobilePhone)
    customer.residencialPhone = this.utils.stringToNumber(personalData.residencialPhone)
    customer.commercialPhone = this.utils.stringToNumber(personalData.commercialPhone)
    customer.email = personalData.email

    if(personalData.personType == 'PERSON'){
      customer.birthDate = this.convertDate(personalData.birthDate)
      customer.gender = personalData.gender
      customer.occupation = personalData.occupation
      customer.maritialStatus = personalData.maritialStatus
    }else{
      customer.procurator = personalData.procurator
    }

    let documentsData = this.customerDocumentsComponent.getCustomerData()
    customer.registrationNumber = this.utils.numberOnlyDigits(documentsData.registrationNumber)
   
    if(personalData.personType == 'PERSON'){
      customer.nationalIdentity = this.utils.stringOnlyDigits(documentsData.nationalIdentity)
      customer.nationalIdentityExpeditionDate =  this.convertDate(documentsData.nationalIdentityExpeditionDate)
      customer.nationalIdentityExpeditionAgency = documentsData.nationalIdentityExpeditionAgency
      customer.driverLicense = this.utils.stringOnlyDigits(documentsData.driverLicense)
      customer.driverLicenseCategory = documentsData.driverLicenseCategory
    }else{
      customer.stateRegistration = this.utils.numberOnlyDigits(documentsData.stateRegistration)
    }

    let addressData = this.customerAddressComponent.getData()
    let customerAddress = new Address()
      customerAddress.street = addressData.street
      customerAddress.number = addressData.number
      customerAddress.complement = addressData.complement
      customerAddress.district = addressData.district
      customerAddress.city = addressData.city
      customerAddress.state = addressData.state
      customerAddress.postalCode = this.utils.stringToNumber(addressData.postalCode)
      customerAddress.reference = addressData.reference

    let wrapper = {
      brand: customer.brand,
      customer : customer,
      address: customerAddress
    }
    
    if(!this.customerSelected || this.customerSelected.id == undefined){

      this.customersService.registerCustomer(wrapper)
        .subscribe(response => {

            this.customerSelected = response.customer
            this.customerAddress  = response.address

            this.getPlans()

            this.customerDocumentsComponent.loadCustomerData(this.customerSelected)
            if(!this.isContract){
              this.paymentsComponent.setCustomer(this.customerSelected)
            }

          this.snackbar.open('Cliente criado com sucesso', '', { duration: 8000 })
          }, error => {
            console.log(error)
            this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
          }
     )
    }
  }


  validateSales(stepper: MatStepper) : boolean {

    if (this.customerPersonalComponent.isInvalid()) {
      stepper.selectedIndex = 0
      this.snackbar.open('Informe os dados do Cliente.', '', { duration: 8000 })
      return false
    }

    if (this.customerDocumentsComponent.isInvalid()) {
      stepper.selectedIndex = 0
      this.snackbar.open('Informe os documentos do Cliente.', '', { duration: 8000})
      return false
    }

    if (this.customerAddressComponent.isInvalid()) {
      stepper.selectedIndex = 0
      this.snackbar.open('Informe o endereço do Cliente.', '', { duration: 8000})
      return false
    }    

    if (this.vehicleDetailsComponent.isInvalid()) {
      stepper.selectedIndex = 1
      this.snackbar.open('Informe os dados do Veículo.', '', { duration: 8000 })
      return false
    }

    if (this.selectedPlan == null) {
      stepper.selectedIndex = 2
      this.snackbar.open('Informe o Plano.', '', { duration: 8000 })
      return false
    }

    if (!this.freePlan) {
      if (!this.paymentsComponent.isInvalid()) {
        stepper.selectedIndex = 3
        this.snackbar.open('Informe a forma de pagamento.', '', { duration: 8000 })
        return false
      }
    }

    return true
  }


  maskPhoneNumber(number) {
    number+="";
    number = number.replace(/\D/g, '');
    number = number.match(/^(\d{2})(\d{5})(\d{4})$/);
    number = (!number) ? null : '(' + number[1] + ') '+ number[2] + '-' + number[3];

    return number;
  }

  maskRegistrationNumber(number) {
    number+="";
    if(number.length == 11){
        number = number.replace(/\D/g, '');
        number = number.match(/^(\d{3})(\d{3})(\d{3})(\d{2})$/);
        number = (!number) ? null : number[1] + '.' + number[2] + '.' +number[3] + '-' + number[4];
    }else{
        number = number.replace(/\D/g, '');
        number = number.match(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/);
        number = (!number) ? null : number[1] + '.' + number[2] + '.' +number[3] + '/' + number[4]+ '-' + number[5];
    }

    return number;
  }   
  
  getPayment(paymentId){
    this.paymentsService.getPaymentById(paymentId)
      .subscribe(payment => {
        this.payment = payment
        });
  }

  paymentTypeEnumToString(value) : string {
    return PaymentType[value]
  }

  setInstaller(installer) {
    if (installer) {
        this.installerUser = installer
        this.userName = this.installerUser.name
        this.usersList = []
        this.scheduleForm.patchValue ({
          installer: this.installerUser.name,
        })
    }
  }

  searchInstallers() {
    this.usersService.getUsersByProfileAndName("INSTALLER", this.scheduleForm.value.installer)
        .catch(error => Observable.from([]))
        .subscribe(users => {
          this.usersList = users.content
      })
  }

  scheduledDateChanged(event: MatDatepickerInputEvent<Date>) {
    this.scheduledDate = new Date(event.value)
    if(this.selectedTime){
      this.scheduledTimeChanged(this.selectedTime)
    }
  }

  scheduledTimeChanged(time : String) {
    this.selectedTime = time
    this.scheduledTimeHours = Number(time.substring(0, 2))
    this.scheduledDate.setUTCHours((this.scheduledTimeHours),0,0);
  }

  loadScheduleTime() {

    this.stateAddressList = this.utilsService.stringEnumToKeyValue(StateAddress)

    this.scheduleTimeList = []
    this.scheduleTimeList.push('09:00') 
    this.scheduleTimeList.push('10:00') 
    this.scheduleTimeList.push('11:00') 
    this.scheduleTimeList.push('12:00')
    this.scheduleTimeList.push('13:00')
    this.scheduleTimeList.push('14:00')
    this.scheduleTimeList.push('15:00') 
    this.scheduleTimeList.push('16:00') 
    this.scheduleTimeList.push('17:00') 
    this.scheduleTimeList.push('18:00')
    this.scheduleTimeList.push('19:00') 
    this.scheduleTimeList.push('20:00') 
    this.scheduleTimeList.push('21:00')
    this.scheduleTimeList.push('22:00')
  }
}



