import { Component, OnInit, AfterViewInit, ViewChild, Injector } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatPaginator, MatSort } from '@angular/material';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Router } from '@angular/router';

import { SalesDataSource } from './sales.datasource';
import { SalesService } from 'app/core/services/sales.service';
import { UtilsService } from 'app/core/utils.service';
import { SalesSelectedService } from './sales.selected.service';
import { Sale, SalesStatus, SaleColorStatus, SaleColorStatusLight } from 'app/core/models/sale.model';
import { fuseAnimations } from '@fuse/animations';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';
import { AppContext } from "app/core/appcontext";
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";


@Component({
  selector: 'tnm-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss'],
  animations: fuseAnimations
})
export class SalesComponent  implements OnInit, AfterViewInit{

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  displayedColumns = ['statusColor','space','id','licensePlate', 'brand', 'customerName','status', 'lifecycle.createDate', 'totalPrice']
  dataSource : SalesDataSource
  searchForm: FormGroup
  searchControl: FormControl  
  resultLength: number = 0
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  salesStatusList: any
  brandList: any
  
  

  widget5 : any = {
    'title'     : 'Github Issues',
    'ranges'    : {
      'TW': 'This Week',
      'LW': 'Last Week',
      '2W': '2 Weeks Ago'
  }
}
    

  constructor(
    private formBuilder: FormBuilder,
    private salesService: SalesService,
    private utilsService: UtilsService,
    private appContext: AppContext,
    private salesSelectedService: SalesSelectedService,
    private router: Router,
    private loadingService: FuseSplashScreenService
  ) { 
  
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading','false');
    this.initForms()
    this.loadDomainListData()
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    this.dataSource.totalElements$.subscribe( value => {
      this.resultLength = value
    })

    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadSales())
    )
    .subscribe()
  }

  initForms() {

    this.brandList = this.appContext.brands

    this.dataSource = new SalesDataSource(this.salesService, this.utilsService, this.brandList,this.loadingService)
    this.dataSource.load('', '', 'lifecycle.createDate', 'desc', 0, 10)

    this.searchControl = this.formBuilder.control('')

    this.searchForm = this.formBuilder.group({
      searchControl: this.searchControl,
      type: this.formBuilder.control(''),
      status: this.formBuilder.control('')
    });

    this.searchControl.valueChanges
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe( value => {
        this.paginator.pageIndex = 0;
        this.loadSales()
    })
  }

  loadDomainListData() {
    this.salesStatusList = this.utilsService.stringEnumToKeyValue(SalesStatus)
  }
  

  loadSales() {

    this.dataSource.load(
      this.searchControl.value, 
      this.searchForm.value.status,
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }

  onListValueChanged() {
    this.loadSales()
  }

  salesStatusEnumToString(value) : string {
    return SalesStatus[value]
  }

  viewDetails(sale: Sale) {
    this.loadingService.show()
    this.salesSelectedService.setSaleSelected(sale)
    this.router.navigate(['/sales/', sale.id])
  }

  saleColorStatusEnumToString(value) : string {
    return SaleColorStatus[value]
  }

  salesColorStatusLightEnumToString(value) : string {
    return SaleColorStatusLight[value]
  }

}



