import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import { PlansService } from "../core/services/plans.service";
import { UtilsService } from "app/core/utils.service";
import { Brand } from "../core/models/brand.model";

export class ContractsDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private totalElementsSubject = new BehaviorSubject<number>(0)

    public totalElements$ = this.totalElementsSubject.asObservable()


    constructor(
        private plansService: PlansService,
        private brands: Array<Brand>,
        private utilsService : UtilsService
    ) {
    }

    loadPlans(filter:string,
                status:string,
                statusPayment:string,
                brand: number,
                sortField: string,
                sortDirection:string,
                pageIndex:number,
                pageSize:number) {

                if(filter == undefined){
                    filter = ''
                }

                if(brand == undefined){
                    brand = 0
                }

                if(status == undefined){
                    status = 'ALL'
                }

                if(statusPayment == undefined){
                    statusPayment = 'ALL'
                }

                
               // sortDirection = 'asc'
                


        this.plansService.findPlans(filter,status,statusPayment,brand, sortField,sortDirection,
            pageIndex, pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe(plans => {

                if (plans.content && this.brands) {
                    plans.content.forEach(obj => {
                        obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                    });
                }
                
                this.totalElementsSubject.next(plans.totalElements)
                this.dataSubject.next(plans.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}