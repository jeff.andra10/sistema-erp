import { NgModule, ModuleWithProviders } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { TextMaskModule } from 'angular2-text-mask';
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../angular-material/material.module";
import { FuseSharedModule } from "@fuse/shared.module";

import { ContractsComponent } from "./contracts.component";
import { ContractDetailsComponent } from "./contract-details/contract-details.component";

import { ContractCancelComponent} from "./contract-cancel/contract-cancel.component";

import { RouterModule } from "@angular/router";
import { PaymentsModule } from "../payments/payments.module";
import { PaymentsComponent } from "../payments/payments.component";

const authRouting: ModuleWithProviders = RouterModule.forChild([
    // { path: '', component: ContractsComponent },
    // { path: 'details/:id', component: ContractDetailsComponent },
    // { path: 'contracts-cancel/new/:id', component: ContractCancelComponent }

    { path: 'contracts', component: ContractsComponent },
    { path: 'contracts/details/:id', component: ContractDetailsComponent },
    { path: 'contracts/contracts-cancel/new/:id', component: ContractCancelComponent }
]);

@NgModule({
    declarations:[
       ContractsComponent,
       ContractDetailsComponent,
       ContractCancelComponent
    ],
    imports: [
        FlexLayoutModule,
        TextMaskModule,
        CommonModule,
        ReactiveFormsModule,
        MaterialModule,
        FuseSharedModule,
        authRouting,
        PaymentsModule
    ],
    exports: [
        ContractDetailsComponent,
        ContractCancelComponent
    ],
    entryComponents: [
        ContractDetailsComponent,
        PaymentsComponent
    ]
})
export class ContractsModule {}