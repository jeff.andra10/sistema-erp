import { Component, OnInit, ViewChild, Injector} from '@angular/core';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { tap } from 'rxjs/operators'
import { MatPaginator, MatSort } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { PlansService } from "../core/services/plans.service";
import { Subject } from 'rxjs';
import { ContractsDataSource } from './contracts.datasource';
import { merge } from "rxjs/observable/merge";
import { PlanStatus, PaymentStatus, PlanColorStatus, PlanColorStatusLight} from '../core/models/plan.model';
import { UtilsService } from '../core/utils.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { BaseComponent } from '../_base/base-component.component';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { Router } from '@angular/router';


@Component({
  selector: 'tnm-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss'],
  animations: fuseAnimations
})
export class ContractsComponent extends BaseComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  displayedColumns = ['statusColor','space','userName', 'brand','status', 'licensePlate', 'offerName', 'debt']
  dataSource: ContractsDataSource
  
  brandList: Array<any>
  resultLength: number = 0
  searchSubject = new Subject()
  planStatusList : any
  paymentStatusList : any
  searchForm: FormGroup
  searchControl: FormControl  
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private plansService: PlansService,
    private utils: UtilsService,
    private loadingService: FuseSplashScreenService,
    private router: Router
  ) { 
    super(injector)
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading','false');
    this.initForms()
    this.loadDomainListData()
    localStorage.removeItem('contractCustomerId');
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    this.dataSource.totalElements$.subscribe( value => {
      this.resultLength = value
    })

    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadPlans())
    )
    .subscribe()
    
  }

  initForms() {
 
    this.brandList = this.appContext.brands

    this.dataSource = new ContractsDataSource(this.plansService,this.brandList,this.utilsService)
    this.dataSource.loadPlans('','ALL','ALL',0,'debt','desc', 0, 10)

    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadPlans(val,'ALL','ALL',0)
    })

    this.searchForm = this.formBuilder.group({
      searchControl: this.searchControl,
      brand: this.formBuilder.control(''),
      type: this.formBuilder.control(''),
      status: this.formBuilder.control(''),
      statusPayment: this.formBuilder.control('')
    });
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }  

  loadPlans(terms? : string,status? : string,statusPayment? : string, brand?: number) {
    this.dataSource.loadPlans(
      terms, 
      status,
      statusPayment,
      brand,
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }

  loadDomainListData() {
    this.planStatusList = this.utils.stringEnumToKeyValue(PlanStatus)
    this.paymentStatusList = this.utils.stringEnumToKeyValue(PaymentStatus)
  }

  planStatusEnumToString(value) : string {
    return PlanStatus[value]
  }

  onListValueChanged() {

    let status = this.searchForm.value.status ? this.searchForm.value.status : 'ALL'
    let statusPayment = this.searchForm.value.statusPayment ? this.searchForm.value.statusPayment : 'ALL'
    let brand = this.searchForm.value.brand ? this.searchForm.value.brand : 0
    this.loadPlans('',status,statusPayment,brand)
  }

  selectContract(row){
    this.loadingService.show()
    this.router.navigate(['/contracts/details/', row.id])
  }

  planColorStatusEnumToString(value) : string {
    return PlanColorStatus[value]
  }

  planColorStatusLightEnumToString(value) : string {
    return PlanColorStatusLight[value]
  }

}