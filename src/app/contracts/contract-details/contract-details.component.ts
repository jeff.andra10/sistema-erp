import { Component, OnInit} from '@angular/core'; 
import { AfterViewInit, ViewChild  } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import {  MatSnackBar } from '@angular/material';
import { MatDialog} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
//import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Customer } from '../../core/models/customer.model';
import { DatePipe } from '@angular/common';
import { MaskUtils } from '../../core/mask-utils';
import { CustomersService } from '../../core/services/customers.service';
import { UtilsService } from '../../core/utils.service';
import { Plan } from '../../core/models/plan.model';
import { DeviceStatus, Device} from '../../core/models/device.model';
import { PaymentsService } from '../../core/services/payments.service';
import { PlansService } from '../../core/services/plans.service';
import { AppContext } from '../../core/appcontext';
import { VehiclesService } from '../../core/services/vehicles.service';
import { PlanStatus} from '../../core/models/plan.model';
import { PaymentType,PaymentCreditCard,ChargeType,Payment} from '../../core/models/payment.model';
import { fuseAnimations } from '@fuse/animations';
import { Vehicle } from '../../core/models/vehicle.model';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { PaymentsComponent } from '../../payments/payments.component';





@Component({
  selector: 'tnm-contract-details',
  templateUrl: './contract-details.component.html',
  styleUrls: ['./contract-details.component.scss' ],
  animations: fuseAnimations
})

export class ContractDetailsComponent implements OnInit, AfterViewInit {

  @ViewChild(PaymentsComponent)
  private paymentsComponent: PaymentsComponent
  
  planId: number 
  customer: Customer
  generalForm: FormGroup
  vehicleForm: FormGroup
  deviceForm: FormGroup
  planForm: FormGroup
  brandList: Array<object>
  planName : String
  status : Boolean = true
  plan : Plan
  statusPlan : String = ''

  phoneMask = MaskUtils.phoneMask
  mobilePhoneMask = MaskUtils.mobilePhoneMask
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');

  planChargesDataSource: any
  planChargeLogsDataSource: any
   

  constructor(
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    public  dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customersService: CustomersService,
    private plansService: PlansService,
    private appContext: AppContext,
    private vehicleService: VehiclesService,
    private plansCancelService: PlansService,
    private loadingService: FuseSplashScreenService,
    private paymentsService: PaymentsService
  ) { }

  
  ngOnInit() {

    localStorage.setItem('hiddenLoading','false');

    this.status = true
    this.planId = this.activatedRoute.snapshot.params['id']

    this.initForms()
    this.getPlanData()

    

    setTimeout(() => {
      this.brandList = this.appContext.brands
    })
   
  }

  getPlanData(){

    this.plansService.getPlanById(this.planId)
      .subscribe(plan => {
        
        this.plan = plan.content[0]
        this.loadPlanCharge(this.planId)
        this.loadPlanData(plan.content[0])
        //this.getCustomerData(plan.content[0].relationships.customer.id)
        
        this.loadCustomerData(plan.content[0].relationships.customer)
        this.loadVehicleData(plan.content[0].relationships.vehicle)
        if(plan.content[0].relationships && plan.content[0].relationships.device){
          this.loadDevice(plan.content[0].relationships.device)
        }

        this.loadingService.hide()
        
      })
  }

  chargesRetry(id,index){
    this.paymentsService.getChargesRetryById(id)
    .subscribe(charge => {
      this.planChargesDataSource[index] = charge
    })
  }

  sendBooklet(id,index,charge){

    let bookletExpiryDate = new Date(charge.bookletExpiryDate)

    var currentDate = new Date()

   
    if((charge.status == 'FAILED') || (charge.status == 'FAIL')){

      this.paymentsService.getChargesSendBookletById(id)
      .subscribe(charge => {
        this.planChargesDataSource[index] = charge
        this.downloadBooklet(charge.bookletUrl)
      })

    }else if(charge.bookletUrl){

      if (currentDate > bookletExpiryDate) {
        
        this.paymentsService.getChargesSendBookletById(id)
        .subscribe(charge => {
          this.planChargesDataSource[index] = charge
          this.downloadBooklet(charge.bookletUrl)
        })

      }else{
        this.downloadBooklet(charge.bookletUrl)
      }
      
    }
    
  }

  initForms() {
    
    this.generalForm = this.formBuilder.group({
      brand: this.formBuilder.control('', [Validators.required]),
      name: this.formBuilder.control('', [Validators.required]),
      mobilePhone: this.formBuilder.control('', []),
      residencialPhone: this.formBuilder.control('', [Validators.required]),
      commercialPhone: this.formBuilder.control('', [Validators.required]),
      email: this.formBuilder.control('', [Validators.required])
    })

    this.planForm = this.formBuilder.group({
      brand: this.formBuilder.control('', [Validators.required]),
      id: this.formBuilder.control('', [Validators.required]),
      name: this.formBuilder.control('', [Validators.required]),
      planStatus: this.formBuilder.control('', []),
      paymentStatus: this.formBuilder.control('', [Validators.required]),
      installDate: this.formBuilder.control({value : '', disabled: true}),
      installUser: this.formBuilder.control({value : '', disabled: true}),
      cancelDate: this.formBuilder.control({value : '', disabled: true}),
      cancelUser: this.formBuilder.control({value : '', disabled: true}),
      reactivateDate: this.formBuilder.control({value : '', disabled: true}),
      reactivateUser: this.formBuilder.control({value : '', disabled: true}),   
    })

    this.vehicleForm = this.formBuilder.group({
      licensePlate: this.formBuilder.control({value : '', disabled: true}),
      type: this.formBuilder.control({value : '', disabled: true}),
      chassi: this.formBuilder.control({value : '', disabled: true}),
      manufacturer: this.formBuilder.control({value : '', disabled: true}),
      model: this.formBuilder.control({value : '', disabled: true}),
      color: this.formBuilder.control({value : '', disabled: true}),
      manufacturerYear: this.formBuilder.control({value : '', disabled: true}),
      modelYear: this.formBuilder.control({value : '', disabled: true}),
    })

    this.deviceForm = this.formBuilder.group({
      id: this.formBuilder.control({value : '', disabled: true}),
      operator: this.formBuilder.control({value : '', disabled: true}),
      number: this.formBuilder.control({value : '', disabled: true}),
      identifier: this.formBuilder.control({value : '', disabled: true}),
      model: this.formBuilder.control({value : '', disabled: true}),
      imei: this.formBuilder.control({value : '', disabled: true}),
      simCard: this.formBuilder.control({value : '', disabled: true}),
      status: this.formBuilder.control({value : '', disabled: true}),
    })
  }

  getCustomerData(id) {

    this.customersService.getOne(id)
    .subscribe(customer => {
      this.customer = customer
      this.loadCustomerData(customer)
    })

  }

  loadVehicleData(vehicle: Vehicle) {

    if(vehicle && vehicle.licensePlate){

      this.vehicleForm.patchValue ({
        licensePlate: vehicle.licensePlate,
        type: vehicle.type,
        chassi: vehicle.chassi,
        manufacturer: vehicle.manufacture,
        model: vehicle.model,
        color: vehicle.color,
        manufacturerYear: vehicle.registrationYear,
        modelYear: vehicle.modelYear
      })

    }

    
  }

  routerGoTo(){
   
    if(localStorage.getItem('contractCustomerId')){
      let id = localStorage.getItem('contractCustomerId')
          localStorage.removeItem('contractCustomerId');
          this.router.navigate([`/customers/${id}`])
    }else{
      this.router.navigate([`/contracts`])
    }
   
  }

  loadCustomerData(customer: Customer) {
   
    if(customer){
      this.generalForm.setValue ({
        brand: customer.brand || null,
        name: customer.name || null,
        mobilePhone: customer.mobilePhone || null,
        residencialPhone: customer.residencialPhone || null,
        commercialPhone: customer.commercialPhone || null,
        email: customer.email || null
      })
  
      this.generalForm.disable()
    }
    
  } 

  loadDevice(device: Device){

    this.deviceForm.setValue ({
      id: device.id || null,
      operator: device.operator || null,
      number: device.number || null,
      identifier: device.identifier || null,
      model: device.model || null,
      imei: device.imei || null,
      simCard: device.simCard || null,
      status: this.deviceActivityToString(device.status) || null
    })
  }

  loadPlanCharge(id){

    this.planChargesDataSource = []
    this.customersService.getCustomerPlanCharges(id)
      .subscribe(charges => {
        if (charges.content != null) {
          let _charges =  charges.content
          
          _charges.forEach(charge => {
            charge.payment = this.plan['relationships'].payment
            this.planChargesDataSource.push(charge)
          });
         
          //this.planChargesDataSource.sort((d1, d2) => new Date(d1['scheduledDateTime']).getTime() - new Date(d2['scheduledDateTime']).getTime());
          
  
          if(this.planChargesDataSource.length > 0)
             this.loadChargeLog(this.planChargesDataSource[0])

        
             setTimeout(() => {
               if(this.paymentsComponent){
                this.paymentsComponent.hideFields()
                this.paymentsComponent.setCustomer(this.plan.relationships.customer)
               }
               
            })
             
        }
      })
  }

  deviceActivityToString(status) : string {

    if(!status){
      return 'Inativo'
    }
    
    if(status == 'RECEIVED'){
      return 'Disponivel'
    }

    return DeviceStatus[status]
  }

  cancelPlan() {
    this.plansService.setPlan(this.plan)
    this.router.navigate([`/contracts/contracts-cancel/new/${this.plan.id}`])
  }
  
  loadPlanData(plan) {
    var datePipe = new DatePipe(navigator.language)
    //this.status = plan.status
    
    this.planForm.setValue ({
      brand: plan.brand || null,
      id: plan.id || null,
      name: plan.relationships.offer ? plan.relationships.offer.name : null,
      planStatus: this.planStatusEnumToString(plan.status)|| null,
      paymentStatus: plan.agreement == "FREE" ? 'Contrato sem Pagamento' : (plan.debt == 'DUE' ? 'Em Dia' : plan.debt == 'OVERDUE' ? 'Em Atraso' : '-')|| null,//plan.debt == 'DUE' ? 'Em Dia' : 'Em Atraso' || null,
      cancelDate: plan.lifecycle.cancelDate ? datePipe.transform(new Date(plan.lifecycle.cancelDate), 'dd/MM/yyyy HH:mm:ss') : null,
      cancelUser: plan.relationships.lifecycle.cancelLoggedUser ? plan.relationships.lifecycle.cancelLoggedUser.login : null,
      installDate: plan.lifecycle.installDate ? datePipe.transform(new Date(plan.lifecycle.installDate), 'dd/MM/yyyy HH:mm:ss') : null,
      installUser: plan.relationships.lifecycle.installLoggedUser ? plan.relationships.lifecycle.installLoggedUser.login : null,
      reactivateDate:plan.lifecycle.reactivateDate ? datePipe.transform(new Date(plan.lifecycle.reactivateDate), 'dd/MM/yyyy HH:mm:ss') : null,
      reactivateUser: plan.relationships.lifecycle.reactivateLoggedUser ? plan.relationships.lifecycle.reactivateLoggedUser.login : null,
    })

    if(plan.status == 'CANCELED'){
      this.statusPlan = 'lightcoral'
      this.status = true
    }else{
      this.status = false
    }
    

    this.planForm.disable()
  } 

  ngAfterViewInit() {

  }

  planStatusEnumToString(value) : string {
    return PlanStatus[value]
  }

  PaymentTypeEnumToString(value) : string {
    return PaymentType[value]
  }

  PaymentCreditCardEnumToString(value) : string {
    return PaymentCreditCard[value]
  }

  ChargeTypeCardEnumToString(value) : string {
    return ChargeType[value]
  }

  downloadBooklet(url){
    window.open(url, "_blank");
  }

  loadChargeLog(plan){

    this.planName = plan.type
    this.planChargeLogsDataSource = []
    this.customersService.getCustomerChargeLogs(plan.id)
      .subscribe(log => {
        if (log.content != null) {
          this.planChargeLogsDataSource = log.content
        }
      })
  }

  cancel() {
    this.router.navigate(['/contracts'])
  }

  reactivatePlan() {
    this.plansCancelService.reactivatePlan(this.plan.id, { loggedUser : this.appContext.session.user.id })
    .subscribe(
      result => {
        this.snackbar.open('Plano reativado com sucesso', '', { duration: 5000})
        this.router.navigate(['/contracts'])
      },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000})
      })
  }

  onPaymentTypeChanged(type: string) {
    let paymentSelected = this.paymentsComponent.getData()
    
    let body ={
        "loggedUser": this.appContext.session.user.id,
        "payment": paymentSelected
      }

    this.plansService.paymentChangePlan(this.plan.id, body)
    .subscribe(
      result => {
        this.snackbar.open('Forma de pagamento atualizada com sucesso', '', { duration: 5000})
        this.getPlanData();
      },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000})
      })
  }

}
