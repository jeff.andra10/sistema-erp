import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { BrandsService } from "../../../core/services/brands.service";

import { FuseConfig } from '@fuse/types';
import * as _ from 'lodash';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { MatDialog} from '@angular/material';
import { navigation, navigationAssociation, navigationBroker, navigationOperator} from 'app/navigation/navigation';
import { AppContext } from '../../../core/appcontext';
import { PalletColors } from '../../../core/models/pallet.model';
import { FirstAccessComponent } from '../../../auth/login/first-access/first-access.component';
import { DocumentsService } from '../../../core/services/documents.service';

declare var require: any;


@Component({
    selector   : 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls  : ['./toolbar.component.scss']
})

export class ToolbarComponent implements OnInit, OnDestroy
{

    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    languages: any;
    navigation: any;
    selectedLanguage: any;
    showLoadingBar: boolean;
    userStatusOptions: any[];
    noNav: boolean;
    userName: string
    brandForm: FormGroup
    brandList: Array<any>
    primaryColor : string;
    isTracknme : boolean = false;
    version : any

    // Private
    private _unsubscribeAll: Subject<any>;
   

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {Router} _router
     * @param {TranslateService} _translateService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _router: Router,
        private brandsService: BrandsService,
        private _translateService: TranslateService,
        private appContext: AppContext,
        private formBuilder: FormBuilder,
        private documentsService: DocumentsService,
        private dialog: MatDialog,
    )
    {
        // Set the defaults
        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon' : 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon' : 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon' : 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                id   : 'en',
                title: 'English',
                flag : 'us'
            },
            {
                id   : 'tr',
                title: 'Turkish',
                flag : 'tr'
            }
        ];

        if(this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'BROKER'){
            this.navigation = navigationBroker;
        }else if(this.appContext && this.appContext.loggedUser &&  this.appContext.loggedUser.profile == 'OPERATOR'){
            this.navigation = navigationOperator;
        }else if(this.appContext && this.appContext.isLoggedUserTracknme()){
            this.navigation = navigation;
       }else{
            this.navigation = navigationAssociation;
        }

        // Set the private defaults
        this._unsubscribeAll = new Subject();

        if (appContext.isLoggedIn()) {
            this.userName = appContext.session.user.name
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {

        let appVersion = require('../../../../../version.json');

        if(appVersion.version.indexOf('development') != -1){
            this.version = appVersion.version.replace('-development','')
        }else if(appVersion.version.indexOf('master') != -1){
            this.version = appVersion.version.replace('-master','')
        }

       this.loadBrands()

        if(localStorage.getItem('isUserTracknme') && localStorage.getItem('isUserTracknme') === 'true' &&  this.appContext.loggedUser.profile != 'BROKER'){
            this.isTracknme = true
        }else{
            this.isTracknme = false
        }

        let brand = null

        if(this.appContext && this.appContext.loggedUser){
            brand = this.appContext.loggedUser.brand
        }
           
        this.brandForm = this.formBuilder.group({
            brand: this.formBuilder.control(brand) 
        });

        // Subscribe to the router events to show/hide the loading bar
        this._router.events
            .pipe(
                filter((event) => event instanceof NavigationStart),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe((event) => {
                this.showLoadingBar = true;
            });

        this._router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd)
            )
            .subscribe((event) => {
                this.showLoadingBar = false;
            });

        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((settings) => {
                this.horizontalNavbar = settings.layout.navbar.position === 'top';
                this.rightNavbar = settings.layout.navbar.position === 'right';
                this.hiddenNavbar = settings.layout.navbar.hidden === true;
            });

        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, {'id': this._translateService.currentLang});   
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onListValueChanged(event){

            let indexBrandUser = this.brandList.map((item) => { return item.id;}).indexOf(event.value)

            let brandPallet = this.brandList[indexBrandUser].palette ? this.brandList[indexBrandUser].palette : "blue-grey"
            let logoBrand   = this.brandList[indexBrandUser] && this.brandList[indexBrandUser]['logo'] ? this.brandList[indexBrandUser]['logo'] : "assets/images/logos/logo-tracknme.svg"
           
            if(logoBrand && logoBrand != undefined){
                localStorage.setItem('logoBrand',logoBrand );
                
                let session =  this.appContext.session;
               
                session.user.brand = event.value;

                setTimeout(() => {
                    this.appContext.initContext(session) 
                });
                
                
            }else{
                localStorage.setItem('logoBrand',"assets/images/logos/logo-tracknme.svg");
            }

            this.primaryColor = PalletColors[brandPallet]+'-A700-bg'
            let secondColorBrand = PalletColors[brandPallet]+'-A400-bg'

            localStorage.setItem('primaryColorBrand',this.primaryColor);
            localStorage.setItem('secondColorBrand',secondColorBrand);

            this.setColorBrand();
            location.reload();
            this._router.navigate(['/dashboard'])
            
    }

    setColorBrand(){

        let fuse: FuseConfig = {
            layout          : {
                style         : 'vertical-layout-1',
                width         : 'fullwidth',
                navbar        : {
                    hidden    : false,
                    position  : 'left',
                    folded    : false,
                    background: this.primaryColor
                },
                toolbar       : {
                    hidden    : false,
                    position  : 'below-static',
                    background: this.primaryColor
                },
                footer        : {
                    hidden    : false,
                    position  : 'below-static',
                    background: this.primaryColor
                }
            },
            customScrollbars: true
        };

        this._fuseConfigService.setConfig(fuse)
        this._fuseConfigService.setDefault(fuse)
    }

    loadBrands(){
       
        let session =  this.appContext.session;
        
        if(session){
           
            this.brandsService.getBrands().subscribe(brands => {
                if(brands && brands.content && brands.content.length > 0){
                    this.brandList = this.orderByName(brands.content)
                }
            })
        }
        
    }

    orderByName(list){
        if(list && list.length > 0){
            return list.sort(function(a,b) {
                return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
            });
        }else{
            return list
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void
    {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    /**
     * Search
     *
     * @param value
     */
    search(value): void
    {
        // Do your search here...
        console.log(value);
    }

    /**
     * Set the language
     *
     * @param langId
     */
    setLanguage(langId): void
    {
        // Set the selected language for toolbar
        this.selectedLanguage = _.find(this.languages, {'id': langId});

        // Use the selected language for translations
        this._translateService.use(langId);
    }

    logout() {
       
        localStorage.setItem('isUserTracknme','false');
        this.appContext.destroyContext()
        this._router.navigate(['/login'])
        let config = {
            layout: {
                navbar : {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer : {
                    hidden: true
                }
            }
        };

        this.appContext.destroyContext()
        this._fuseConfigService.setConfig(config)
    }

    termsUsoDialog() {
        this.documentsService.getDocumentsTerms().subscribe(terms => {
            this.dialog
              .open(FirstAccessComponent, {
                width: '800px',
                data:{
                    title:'Termos de Uso',
                    term :terms.term,
                    firstAccess: false
                }

            })
        });
    }

    privacyUsoDialog() {
        this.documentsService.getDocumentsPrivacy().subscribe(terms => {
            this.dialog
              .open(FirstAccessComponent, {
                width: '800px',
                data:{
                    title:'Política de Privacidade',
                    term :terms.term,
                    firstAccess: false
                }

            })
        });
    }
}
