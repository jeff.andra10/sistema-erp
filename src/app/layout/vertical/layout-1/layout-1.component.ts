import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import {
  navigation,
  navigationAssociation,
  navigationBroker,
  navigationOperator
} from 'app/navigation/navigation';

import { AppContext } from '../../../core/appcontext';

@Component({
  selector: "vertical-layout-1",
  templateUrl: './layout-1.component.html',
  styleUrls: ['./layout-1.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VerticalLayout1Component implements OnInit, OnDestroy {
  fuseConfig: any;
  navigation: any;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private appContext: AppContext,
    private loadingService: FuseSplashScreenService
  ) {
    // Set the defaults

    if (
      this.appContext &&
      this.appContext.loggedUser &&
      this.appContext.loggedUser.profile === 'BROKER'
    ) {
      this.navigation = navigationBroker;
    } else if (
      this.appContext &&
      this.appContext.loggedUser &&
      this.appContext.loggedUser.profile === 'OPERATOR'
    ) {
      this.navigation = navigationOperator;
    } else if (this.appContext && this.appContext.isLoggedUserTracknme()) {
      this.navigation = navigation;
    } else {
      this.navigation = navigationAssociation;
    }

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
      console.log('chegou no vertical');
      
    // Subscribe to config changes
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(config => {
        this.fuseConfig = config;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  onClickNavBar(event: any) {
    localStorage.removeItem('filterServiceOders');
  }
}
