import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { navigation, navigationAssociation, navigationBroker, navigationOperator  } from 'app/navigation/navigation';
import { AppContext } from '../../../core/appcontext';

@Component({
    selector     : 'vertical-layout-3',
    templateUrl  : './layout-3.component.html',
    styleUrls    : ['./layout-3.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VerticalLayout3Component implements OnInit, OnDestroy
{
    fuseConfig: any;
    navigation: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private appContext: AppContext
    )
    {

        // Set the defaults
        if(this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'BROKER'){
            this.navigation = navigationBroker;
        }else if(this.appContext && this.appContext.loggedUser &&  this.appContext.loggedUser.profile == 'OPERATOR'){
            this.navigation = navigationOperator;
        }else if(this.appContext && this.appContext.isLoggedUserTracknme()){
            this.navigation = navigation;
        }else{
            this.navigation = navigationAssociation;
        }
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
