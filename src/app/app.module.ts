import { HttpClientModule } from '@angular/common/http';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatIconModule } from '@angular/material';
import { BrowserModule, Title } from '@angular/platform-browser';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { NgModule, LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseModule } from '@fuse/fuse.module';
import { FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { TranslateModule } from '@ngx-translate/core';

import 'hammerjs';

import { LayoutModule } from 'app/layout/layout.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AdminModule } from './admin/admin.module';
import { AuthModule } from './auth/auth.module';
import { ContractsModule } from './contracts/contracts.module';
import { CoreModule } from './core/core.module';
import { CustomersModule } from './customers/customers.module';
import { DialogsModule } from './dialogs/dialogs.module';
import { fuseConfig } from './fuse-config/index';
import { FuseMainModule } from './main/main.module';
import { PurchasesModule } from './purchases/purchases.module';
import { UsersModule } from './users/users.module';
import { VehiclesModule } from './vehicles/vehicles.module';

registerLocaleData(localePt, 'pt');

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot(),

    // Fuse Main and Shared modules
    FuseModule.forRoot(fuseConfig),
    FuseSharedModule,
    FuseMainModule,
    MatMomentDateModule,
    FuseThemeOptionsModule,
    FuseSidebarModule,
    AppRoutingModule,
    DialogsModule,

    // Material
    MatButtonModule,
    MatIconModule,

    CoreModule.forRoot(),
    AuthModule,
    LayoutModule,

    //App modules

    AdminModule,
    CustomersModule,
    UsersModule,
    VehiclesModule,
    ContractsModule,
    PurchasesModule
  ],
  bootstrap: [AppComponent],
  providers: [{ provide: LOCALE_ID, useValue: 'pt' }, Title],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
