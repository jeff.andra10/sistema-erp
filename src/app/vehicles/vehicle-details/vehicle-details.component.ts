import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormGroup,
  FormControl,
  FormBuilder,
  AbstractControl
} from '@angular/forms';

import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { MaskUtils } from 'app/core/mask-utils';
import { Vehicle } from 'app/core/models/vehicle.model';

import { VehiclesService } from '../../core/services/vehicles.service';

@Component({
  selector: 'tnm-vehicle-details',
  templateUrl: './vehicle-details.component.html'
})
export class VehicleDetailsComponent implements OnInit {
  vehicleForm: FormGroup;
  licensePlateMask = MaskUtils.licensePlateMask;
  chassiMask = MaskUtils.chassiMask;
  yearMask = MaskUtils.yearMask;
  manufactureList: Array<object>;
  modelList: Array<object>;
  yearsList: Array<object>;
  registrationYearList: any;
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');
  manufacture: any;
  model: any;
  vehicle: any;
  isHideField = false;
  isVehicleNotFound = false;
  colorVehicleList = [
    'Amarelo',
    'Azul',
    'Branco',
    'Chumbo',
    'Cinza',
    'Creme',
    'Dourado',
    'Laranja',
    'Marrom',
    'Branco',
    'Prata',
    'Preto',
    'Roxo',
    'Verde',
    'Vermelho'
  ];

  typeVehicleList = [
    {
      name: 'Carros e utilitários',
      id: 'CAR'
    },
    {
      name: 'Caminhões e Micro-ônibus',
      id: 'TRUCK'
    },
    {
      name: 'Motos',
      id: 'MOTORCYCLE'
    },
    {
      name: 'Carreta',
      id: 'TRAILER'
    },
    {
      name: 'Barco/Lancha',
      id: 'BOAT'
    }
  ];

  modelControl = new FormControl();
  filteredOptions: Observable<string[]>;
  optionsModel = [];

  constructor(
    private formBuilder: FormBuilder,
    private vehicleService: VehiclesService,
    private loadingService: FuseSplashScreenService
  ) {}

  ngOnInit() {
    this.initForms();

    this.vehicle = {
      licensePlate: null,
      type: null,
      chassi: null,
      manufacture: null,
      model: null,
      color: null,
      registrationYear: null,
      modelYear: null
    };

    this.filteredOptions = this.modelControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  addVehicleNotFound() {
    this.isVehicleNotFound = true;
  }

  private initForms() {
    this.vehicleForm = this.formBuilder.group({
      licensePlate: this.formBuilder.control('', [
        Validators.required,
        validLicensePlate
      ]),
      type: this.formBuilder.control('', [Validators.required]),
      chassi: this.formBuilder.control('', [Validators.required, validChassi]),
      manufacture: this.formBuilder.control('', [Validators.required]),
      model: this.formBuilder.control('', [Validators.required]),
      color: this.formBuilder.control('', [Validators.required]),
      registrationYear: this.formBuilder.control('', [Validators.required]),
      modelYear: this.formBuilder.control('', [Validators.required])
    });
  }

  public loadData(vehicle: Vehicle) {
    this.changeManufacture(vehicle.type, vehicle);

    this.vehicleForm.setValue({
      licensePlate: vehicle.licensePlate || null,
      type: vehicle.type || null,
      chassi: vehicle.chassi || null,
      manufacture: vehicle.manufacture || null,
      model: vehicle.model || null,
      color: vehicle.color || null,
      registrationYear: vehicle.registrationYear || null,
      modelYear: vehicle.modelYear || null
    });

    setTimeout(() => {
      this.loadingService.hide();
    }, 5000);
  }

  public setHideFields() {
    this.isHideField = true;
  }

  changeManufacture(type: string, vehicle: Vehicle) {
    if (type === 'TRAILER' || type === 'BOAT' || this.isVehicleNotFound) {
      return false;
    }

    this.manufactureList = [{ name: 'Carregando, aguarde...', id: '0000' }];

    this.vehicleForm.patchValue({
      manufacture: '0000',
      model: '',
      registrationYear: '',
      modelYear: ''
    });

    this.modelList = [];
    this.yearsList = [];
    this.registrationYearList = [];
    this.vehicleService.getManufacturers(type).subscribe(
      manufacture => {
        this.manufactureList = [];
        this.manufactureList = manufacture.content;
        this.manufactureList.sort(function(a, b) {
          return a['name'] < b['name'] ? -1 : a['name'] > b['name'] ? 1 : 0;
        });

        if (vehicle) {
          manufacture.content.forEach(
            // tslint:disable-next-line: no-shadowed-variable
            (manufacture: {
              [x: string]: {
                toLowerCase: () => { indexOf: (arg0: string) => number };
              };
              id: any;
            }) => {
              if (
                manufacture['name'] === vehicle.manufacture ||
                manufacture['name']
                  .toLowerCase()
                  .indexOf(vehicle.manufacture.toLowerCase()) > -1
              ) {
                this.vehicleForm.patchValue({
                  manufacture: manufacture.id || null
                });

                this.changeModel(manufacture.id, vehicle);
              } else {
                this.loadingService.hide();
              }
            }
          );
        } else {
          this.loadingService.hide();
        }
      },
      error => {
        this.loadingService.hide();
      }
    );
  }

  changeModel(manufacture: any, vehicle: Vehicle) {
    if (!vehicle) {
      this.vehicleForm.patchValue({
        model: 'Carregando, aguarde...',
        registrationYear: '',
        modelYear: ''
      });
    }

    this.optionsModel = [];
    this.manufacture = manufacture;
    this.modelList = [];
    this.yearsList = [];
    this.registrationYearList = [];
    this.vehicleService.getModels(manufacture).subscribe(
      models => {
        this.modelList = models.content;
        this.modelList.sort(function(a, b) {
          return a['name'] < b['name'] ? -1 : a['name'] > b['name'] ? 1 : 0;
        });
        models.content.forEach(
          (model: {
            [x: string]: {
              toLowerCase: () => { indexOf: (arg0: any) => number };
            };
            name: any;
            id: any;
          }) => {
            this.optionsModel.push({ name: model.name, id: model.id });
            if (vehicle) {
              if (
                model['name'] === vehicle.model ||
                model['name']
                  .toLowerCase()
                  .indexOf(vehicle.model.toLowerCase()) > -1
              ) {
                this.vehicleForm.patchValue({
                  model: model.name || null
                });

                this.changeYears(model.name, vehicle);
              } else {
                this.loadingService.hide();
              }
            }
          }
        );

        if (!vehicle) {
          this.vehicleForm.patchValue({
            model: '',
            registrationYear: '',
            modelYear: ''
          });
        }
      },
      error => {
        this.loadingService.hide();
      }
    );
  }

  changeYears(model: string, vehicle: { modelYear: any }) {
    this.yearsList = [{ name: 'Carregando, aguarde...', idModeloAno: '0000' }];

    if (!vehicle) {
      this.vehicleForm.patchValue({
        model: model,
        registrationYear: '',
        modelYear: '0000'
      });
    }

    const selected = this._filter(model);
    if (selected && selected.length > 0) {
      this.model = selected[0]['id'];

      this.registrationYearList = [];
      this.vehicleService.getYears(this.manufacture, this.model).subscribe(
        years => {
          this.yearsList = [];

          years.content.forEach((year: object) => {
            if (year['name'] !== '32000 Gasolina') {
              this.yearsList.push(year);
            }

            if (vehicle) {
              if (year['name'].indexOf(vehicle.modelYear) >= 0) {
                this.vehicleForm.patchValue({
                  modelYear: year || null
                });

                this.changeVehicle(year, this.vehicle);
              } else {
                this.loadingService.hide();
              }
            }
          });

          this.yearsList.sort(function(a, b) {
            return a['idt'] - b['idt'];
          });

          if (!vehicle) {
            this.vehicleForm.patchValue({
              modelYear: ''
            });
          }
        },
        error => {
          this.loadingService.hide();
        }
      );
    }
  }

  changeVehicle(year: any, vehicle: { registrationYear: any }) {
    this.registrationYearList = [
      { name: 'Carregando, aguarde...', idModeloAno: '0000' }
    ];

    this.vehicleForm.patchValue({
      registrationYear: '0000'
    });

    this.vehicleService
      .getVehicle(this.manufacture, this.model, year)
      .subscribe(
        vehicles => {
          vehicles.content.forEach(
            (_vehicle: {
              idModeloAno: any;
              modelo: any;
              marca: any;
              ano: string;
            }) => {
              if (_vehicle.idModeloAno === year) {
                this.vehicle.model = _vehicle.modelo;
                this.vehicle.manufacture = _vehicle.marca;
                this.vehicle.modelYear = _vehicle.ano;
                this.registrationYearList = [];
                this.registrationYearList.push(_vehicle.ano);
                this.registrationYearList.push(
                  (parseInt(_vehicle.ano) - 1).toString()
                );
                this.registrationYearList.sort(function(a: number, b: number) {
                  return a - b;
                });

                if (!vehicle) {
                  this.vehicleForm.patchValue({
                    registrationYear: ''
                  });
                } else {
                  this.vehicleForm.patchValue({
                    registrationYear: vehicle.registrationYear
                  });

                  this.loadingService.hide();
                }
              }
            }
          );
        },
        error => {
          this.loadingService.hide();
        }
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.optionsModel.filter(option =>
      option['name']
        ? option['name'].toLowerCase().indexOf(filterValue) === 0
        : ''
    );
  }

  public getData(): any {
    this.vehicle.licensePlate = this.vehicleForm.value.licensePlate;
    this.vehicle.type = this.vehicleForm.value.type;
    this.vehicle.chassi = this.vehicleForm.value.chassi;
    this.vehicle.color = this.vehicleForm.value.color;
    this.vehicle.registrationYear = this.vehicleForm.value.registrationYear;

    if (
      this.vehicle.type === 'TRAILER' ||
      this.vehicle.type === 'BOAT' ||
      this.isVehicleNotFound
    ) {
      this.vehicle.model = this.vehicleForm.value.model;
      this.vehicle.modelYear = this.vehicleForm.value.modelYear;
      this.vehicle.manufacture = this.vehicleForm.value.manufacture;
    }
    return this.vehicle;
  }

  public getDataForm() {
    return this.vehicleForm;
  }

  public isInvalid() {
    return this.vehicleForm.invalid;
  }
}

function validChassi(control: AbstractControl) {
  if (!control.parent || !control) {
    return;
  }

  const chassi = control.parent.get('chassi');
  const chassiNumber = chassi.value.replace(/[^a-z0-9]/gi, '');

  if (chassiNumber.length < 17) {
    return {
      validNumberChassi: true
    };
  }

  return;
}

function validLicensePlate(control: AbstractControl) {
  if (!control.parent || !control) {
    return;
  }

  const licensePlate = control.parent.get('licensePlate');
  const licensePlateNumber = licensePlate.value.replace(/[^a-z0-9]/gi, '');

  if (licensePlateNumber.length < 7) {
    return {
      validNumberLicensePlate: true
    };
  }

  return;
}
