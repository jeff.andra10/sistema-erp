import { CollectionViewer, DataSource } from '@angular/cdk/collections';

import { catchError, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { VehiclesService } from '../core/services/vehicles.service';

export class VehiclesDataSource implements DataSource<any> {
  private dataSubject = new BehaviorSubject<any[]>([]);

  private totalElementsSubject = new BehaviorSubject<number>(0);

  public totalElements$ = this.totalElementsSubject.asObservable();

  constructor(private vehiclesService: VehiclesService) {}

  load(
    filter: string,
    brandId = 0,
    sortField: string,
    sortDirection: string,
    pageIndex: number,
    pageSize: number
  ) {
    this.vehiclesService
      .findVehicles(
        filter,
        brandId,
        sortField,
        sortDirection,
        pageIndex,
        pageSize
      )
      .pipe(catchError(() => of([])))
      .subscribe(result => {
        this.totalElementsSubject.next(result.totalElements);
        this.dataSubject.next(result.content);
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.dataSubject.complete();
    this.totalElementsSubject.complete();
  }
}
