import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  Injector
} from '@angular/core';
import { MatDialog, MatSort, MatPaginator, MatSelect } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';

import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';

import { Brand } from 'app/core/models/brand.model';

import { BaseComponent } from '../_base/base-component.component';
import { VehiclesService } from '../core/services/vehicles.service';

import { VehiclesDataSource } from './vehicles.datasource';

import { EditVehicleComponent } from './edit-vehicle/edit-vehicle.component';

@Component({
  selector: 'tnm-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss'],
  animations: fuseAnimations
})
export class VehiclesComponent extends BaseComponent
  implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSelect) brandsSelect: MatSelect;

  displayedColumns = ['licensePlate', 'manufacture', 'model', 'edit'];
  dataSource: VehiclesDataSource;
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');
  resultLength = 0;
  searchSubject = new Subject();
  brandsList: Array<Brand>;

  constructor(
    private vehiclesService: VehiclesService,
    private dialog: MatDialog,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading', 'false');
    this.searchSubject.debounceTime(500).subscribe((val: string) => {
      this.loadVehicles(val);
    });

    this.dataSource = new VehiclesDataSource(this.vehiclesService);
    this.dataSource.load('', 0, 'licensePlate', 'asc', 0, 10);

    this.brandsList = this.utilsService.sortByField(
      this.appContext.brands,
      'name'
    );
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    if (this.dataSource && this.dataSource.totalElements$) {
      this.dataSource.totalElements$.subscribe(value => {
        this.resultLength = value;
      });
    }

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(tap(() => this.loadVehicles()))
      .subscribe();
  }

  searchOnChange(event) {
    this.searchSubject.next(event.currentTarget.value);
  }

  loadVehicles(filter?: string) {
    this.dataSource.load(
      filter,
      this.brandsSelect.value,
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
  }

  editVehicleDialog(row) {
    this.dialog
      .open(EditVehicleComponent, {
        data: row,
        width: '700px'
      })
      .afterClosed()
      .subscribe(() => this.loadVehicles());
  }

  onListValueChanged() {
    this.loadVehicles();
  }
}
