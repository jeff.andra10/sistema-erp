import { Component, OnInit, Inject, Injector } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatSnackBar
} from '@angular/material';
import { Router } from '@angular/router';
import {
  Validators,
  FormGroup,
  FormControl,
  FormBuilder
} from '@angular/forms';

import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { BaseComponent } from '../../_base/base-component.component';
import { MaskUtils } from '../../core/mask-utils';
import { DeviceStatus, Device } from '../../core/models/device.model';
import { DevicesService } from '../../core/services/devices.service';
import { ManufacturesService } from '../../core/services/manufactures.service';
import { VehiclesService } from '../../core/services/vehicles.service';

@Component({
  selector: 'tnm-edit-vehicle',
  templateUrl: './edit-vehicle.component.html',
  styleUrls: ['./edit-vehicle.component.scss']
})
export class EditVehicleComponent extends BaseComponent implements OnInit {
  vehicleForm: FormGroup;
  deviceForm: FormGroup;
  brandList: Array<object>;
  type: FormControl;
  manufacturer: FormControl;
  currentId: number;
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  manufacturesList: Array<object>;
  modelsList: Array<object>;
  yearsList: Array<object>;
  registrationYearList: any;
  manufacture: any;
  model: any;
  vehicle: any;
  device: Device;

  //TODO - Put on shared file
  yearMask = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
  licensePlateMask = MaskUtils.licensePlateMask;

  typeVehicleList = [
    {
      name: 'Carros e utilitários',
      id: 'CAR'
    },
    {
      name: 'Caminhões e Micro-ônibus',
      id: 'TRUCK'
    },
    {
      name: 'Motos',
      id: 'MOTORCYCLE'
    },
    {
      name: 'Carreta',
      id: 'TRAILER'
    },
    {
      name: 'Barco/Lancha',
      id: 'BOAT'
    }
  ];

  constructor(
    private injector: Injector,
    private router: Router,
    private dialogRef: MatDialogRef<EditVehicleComponent>,
    private devicesService: DevicesService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private vehiclesService: VehiclesService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private manufacturesService: ManufacturesService,
    private loadingService: FuseSplashScreenService
  ) {
    super(injector);
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading', 'true');

    this.loadingService.show();

    this.createForm(this.data);

    this.vehicle = {
      licensePlate: null,
      type: null,
      chassi: null,
      manufacture: null,
      model: null,
      color: null,
      registrationYear: null,
      modelYear: null
    };

    this.deviceForm = this.formBuilder.group({
      id: this.formBuilder.control({ value: '', disabled: true }),
      operator: this.formBuilder.control({ value: '', disabled: true }),
      number: this.formBuilder.control({ value: '', disabled: true }),
      identifier: this.formBuilder.control({ value: '', disabled: true }),
      model: this.formBuilder.control({ value: '', disabled: true }),
      imei: this.formBuilder.control({ value: '', disabled: true }),
      simCard: this.formBuilder.control({ value: '', disabled: true }),
      status: this.formBuilder.control({ value: '', disabled: true })
    });
  }

  getDevice(id) {
    this.devicesService.getDevicesByCarId(id).subscribe(result => {
      if (result && result.content) {
        this.device = result.content[0];
        this.loadDevice(this.device);
      }
      this.loadingService.hide();
    });
  }

  createForm(data) {
    this.currentId = data.id;
    this.changeManufacture(data.type, data);

    setTimeout(() => {
      this.brandList = this.appContext.brands;
    });

    this.registrationYearList = [];
    this.registrationYearList.push(data.modelYear);
    this.registrationYearList.push((parseInt(data.modelYear) - 1).toString());

    this.registrationYearList.sort(function(a, b) {
      return a - b;
    });

    this.vehicleForm = this.formBuilder.group({
      manufacture: this.formBuilder.control(data.manufacture, [
        Validators.required
      ]),
      model: this.formBuilder.control(data.model, [Validators.required]),
      registrationYear: this.formBuilder.control(data.registrationYear, [
        Validators.required
      ]),
      modelYear: this.formBuilder.control(data.modelYear, [
        Validators.required
      ]),
      licensePlate: this.formBuilder.control(data.licensePlate, [
        Validators.required
      ]),
      chassi: this.formBuilder.control(data.chassi, [Validators.required]),
      color: this.formBuilder.control(data.color, [Validators.required]),
      brand: this.formBuilder.control(data.brand, [Validators.required]),
      type: this.formBuilder.control(data.type, [Validators.required])
    });

    this.getDevice(data.id);
  }

  loadDevice(device: Device) {
    if (device && device.imei) {
      this.deviceForm.setValue({
        id: device.id || null,
        operator: device.operator || null,
        number: device.number || null,
        identifier: device.identifier || null,
        model: device.model || null,
        imei: device.imei || null,
        simCard: device.simCard || null,
        status: this.deviceActivityToString(device.status) || null
      });
    }
  }

  deviceActivityToString(status): string {
    if (!status) {
      return 'Inativo';
    }

    if (status == 'RECEIVED') {
      return 'Disponivel';
    }

    return DeviceStatus[status];
  }

  updateVehicle(editForm) {
    let editData = [];

    this.vehicle.licensePlate = this.vehicleForm.value.licensePlate;
    this.vehicle.type = this.vehicleForm.value.type;
    this.vehicle.chassi = this.vehicleForm.value.chassi;
    this.vehicle.color = this.vehicleForm.value.color;
    this.vehicle.registrationYear = this.vehicleForm.value.registrationYear;

    if (this.vehicle.type == 'TRAILER' || this.vehicle.type == 'BOAT') {
      this.vehicle.model = this.vehicleForm.value.model;
      this.vehicle.modelYear = this.vehicleForm.value.modelYear;
      this.vehicle.manufacture = this.vehicleForm.value.manufacture;
    }

    for (let item in this.vehicle) {
      let editItem = { op: 'add', path: '/' + item, value: this.vehicle[item] };
      editData.push(editItem);
    }

    this.vehiclesService
      .update(this.currentId, editData)
      .subscribe(supplier => {
        this.snackbar.open('Veículo atualizado com sucesso', '', {
          duration: 8000
        });

        this.dialogRef.close();
      });
  }

  changeManufacture(type, data) {
    if (type == 'TRAILER' || type == 'BOAT') {
      return false;
    }

    this.vehiclesService.getManufacturers(type).subscribe(manufactures => {
      this.manufacturesList = manufactures.content;
      this.manufacturesList.sort(function(a, b) {
        return a['name'] < b['name'] ? -1 : a['name'] > b['name'] ? 1 : 0;
      });
      if (data) {
        manufactures.content.forEach(manufacture => {
          if (manufacture['name'] == data.manufacture) {
            this.changeModel(manufacture.id, data);
          }
        });
      }
    });
  }

  changeModel(manufacture, data) {
    this.manufacture = manufacture;
    this.modelsList = [];
    this.vehiclesService.getModels(manufacture).subscribe(models => {
      this.modelsList = models.content;
      this.modelsList.sort(function(a, b) {
        return a['name'] < b['name'] ? -1 : a['name'] > b['name'] ? 1 : 0;
      });
      if (data) {
        models.content.forEach(model => {
          if (model['name'] == data.model) {
            this.changeYears(model.id, data);
          }
        });
      }
    });
  }

  changeYears(model, data) {
    this.yearsList = [];
    this.model = model;
    this.vehiclesService.getYears(this.manufacture, model).subscribe(years => {
      years.content.forEach(model => {
        if (model['name'] != '32000 Gasolina') {
          this.yearsList.push(model);
        }
      });
      //this.yearsList = years.content

      this.yearsList.sort(function(a, b) {
        return a['idt'] - b['idt'];
      });

      if (data) {
        years.content.forEach(year => {
          if (year['name'].indexOf(data.modelYear) >= 0) {
            this.vehicleForm = this.formBuilder.group({
              type: this.formBuilder.control(data.type, [Validators.required]),
              modelYear: this.formBuilder.control(year.idModeloAno, [
                Validators.required
              ]),
              manufacture: this.formBuilder.control(this.manufacture, [
                Validators.required
              ]),
              model: this.formBuilder.control(this.model, [
                Validators.required
              ]),
              registrationYear: this.formBuilder.control(
                data.registrationYear,
                [Validators.required]
              ),
              licensePlate: this.formBuilder.control(data.licensePlate, [
                Validators.required
              ]),
              chassi: this.formBuilder.control(data.chassi, [
                Validators.required
              ]),
              color: this.formBuilder.control(data.color, [
                Validators.required
              ]),
              brand: this.formBuilder.control(data.brand, [Validators.required])
            });
            this.changeVehicle(year.idModeloAno);
          }
        });
      }
    });
  }

  changeVehicle(year) {
    this.vehiclesService
      .getVehicle(this.manufacture, this.model, year)
      .subscribe(vehicles => {
        vehicles.content.forEach(vehicle => {
          if (vehicle.idModeloAno == year) {
            this.vehicle.model = vehicle.modelo;
            this.vehicle.manufacture = vehicle.marca;
            this.vehicle.modelYear = vehicle.ano;
          }
        });
      });
  }
}
