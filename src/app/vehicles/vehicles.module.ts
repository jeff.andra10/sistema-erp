import { NgModule, ModuleWithProviders } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { TextMaskModule } from 'angular2-text-mask';

import { MaterialModule } from '../angular-material/material.module';

import { VehiclesComponent } from './vehicles.component';

import { EditVehicleComponent } from './edit-vehicle/edit-vehicle.component';
import { VehicleDetailsComponent } from './vehicle-details/vehicle-details.component';

const authRouting: ModuleWithProviders = RouterModule.forChild([
  { path: 'vehicles', component: VehiclesComponent },
  { path: 'vehicles/:id', component: EditVehicleComponent }
]);

@NgModule({
  declarations: [
    VehiclesComponent,
    EditVehicleComponent,
    VehicleDetailsComponent
  ],
  imports: [
    FlexLayoutModule,
    TextMaskModule,
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    FuseSharedModule,
    authRouting
  ],
  exports: [EditVehicleComponent, VehicleDetailsComponent],
  entryComponents: [EditVehicleComponent]
})
export class VehiclesModule {}
