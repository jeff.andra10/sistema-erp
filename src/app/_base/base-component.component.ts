import { Component, Injector } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { AppContext } from '../core/appcontext';
import { UtilsService } from '../core/utils.service';

export abstract class BaseComponent {

  protected utilsService: UtilsService;
  protected appContext: AppContext

  constructor(injector: Injector) {

    this.utilsService = injector.get(UtilsService)
    this.appContext = injector.get(AppContext)
  }


  validateAllFields(formGroup: FormGroup) {       
    Object.keys(formGroup.controls).forEach(field => {  
        const control = formGroup.get(field);            
        if (control instanceof FormControl) {             
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        
            this.validateAllFields(control);  
        }
    });
  }

  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
}
