import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'devices',
    loadChildren: './devices/devices.module#DevicesModule'
  },
  {
    path: 'transactions',
    loadChildren: './transactions/transactions.module#TransactionsModule'
  },
  {
    path: 'sales',
    loadChildren: './sales/sales.module#SalesModule'
  },
  {
    path: 'service-orders',
    loadChildren: './service-orders/service-orders.module#ServiceOrdersModule'
  },
  {
    path: 'insurance',
    loadChildren: './insurance/insurance.module#InsuranceModule'
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
