import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { MatPaginator, MatSort } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';

import { BaseComponent } from 'app/_base/base-component.component';
import { PoliciesDataSource } from './policies.datasource';
import { PoliciesService } from 'app/core/services/policies.service';
import { PoliciesSelectedService } from './policies.selected.service';
import { Policy } from 'app/core/models/policy.model';
import { Quote } from '../../core/models/quote.model';
import { QuotesService } from 'app/core/services/quotes.service';
import { BrokersService } from '../../core/services/brokers.service';
import { QuotesSelectedService } from '../quotes/quotes.selected.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";

@Component({
  selector: 'app-policies',
  templateUrl: './policies.component.html',
  styleUrls: ['./policies.component.scss'],
  animations: fuseAnimations
})
export class PoliciesComponent  extends BaseComponent implements OnInit {

 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  displayedColumns = ['id', 'vehicle.model', 'cpfCnpj',  'createdDate', 'status'];
  dataSource: PoliciesDataSource
  resultLength: number = 0
  searchSubject = new Subject()
  brandList: Array<any>
  brokerList: Array<any>
  isBroker : boolean = false
  brandSelected  : number = 0
  brokerSelected : number = 0
  statusSelected : string = 'POLICY'

  constructor(private injector: Injector,
              private router: Router,
              private quotesService: QuotesService,
              private policiesService: PoliciesService,
              private brokersService: BrokersService,
              private loadingService: FuseSplashScreenService,
              private policySelected: PoliciesSelectedService,
              private quoteSelected: QuotesSelectedService) { 
    super(injector)
  }

  ngOnInit() {

    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.load(val,this.statusSelected, 0, this.brokerSelected);
    });

    this.dataSource = new PoliciesDataSource(this.quotesService);
    

    this.loadDomainListData()

    if(this.appContext.loggedUser.profile ==  "BROKER"){
      this.isBroker = true
      this.getBrokerByUser()
    }else{
      this.dataSource.load('', 0,this.statusSelected,this.brokerSelected,'lifecycle.lastUpdate', 'desc', 0, 10);
    }
  }


  load(filter? :string, status?: string, brand?: number, broker?: number) {

    this.dataSource.load(
      filter, 
      brand,
      status,
      broker,
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  loadDomainListData() {

    this.brandList = this.appContext.brands
   
    this.brokersService.getBrokers().subscribe(brokers => {
      
      if (brokers && brokers.content) {
        this.brokerList = brokers.content
        this.brokerList.push({'name':'Todos','id':0})
        this.brokerList.sort(function(a, b){
          return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });
      }
    })
  }

  statusOnSelected(){
    this.load('',this.statusSelected, this.brandSelected, this.brokerSelected)
  }

  brandOnSelected(event){
    this.brandSelected =event.value;
    this.load('',this.statusSelected, this.brandSelected, this.brokerSelected)
  }

  brokerOnSelected(event){
    this.brokerSelected =event.value;
    this.load('',this.statusSelected, this.brandSelected, this.brokerSelected)
  }

  quoteStatusAsString(value):string {
    switch (value) {
      case 'QUOTE':
        return 'Cotação'
      case 'QUOTATION':
        return 'Cotação'
      case 'PROPOSAL':
        return 'Proposta'
      case 'PROPOSAL_SENT':
        return 'Proposta Enviada'
      case 'PROPOSAL_ACCEPTED':
        return 'Proposta Efetivada'
      case 'POLICY':
        return 'Apólice'
      case 'CANCELLED':
        return 'Cancelado.'
      case 'REJECTED':
        return 'Rejeitado pela seguradora.'
      default:
        return ''
    }
  }

  edit(policy: Quote) {
    
    localStorage.setItem('hiddenLoading','true');
    this.loadingService.show()
    
    this.quoteSelected.setSelected(policy)
    let url = 'insurance/policies/'
    this.router.navigate([url, policy.id])

  }

  getBrokerByUser(){
    this.brokersService.getBrokerByUser().subscribe(brokers => {
      if (brokers && brokers.content) {
        this.brokerSelected = brokers.content[0].id
        this.dataSource.load('', 0,this.statusSelected,this.brokerSelected,'lifecycle.lastUpdate', 'desc', 0, 10);
      }
    })
  }
}
