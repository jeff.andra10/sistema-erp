import { Component, OnInit, ViewChild, Injector, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators , FormControl} from '@angular/forms';
import { MatSnackBar, MatStepper, MatDialog, MatSlideToggleChange } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { merge, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BaseComponent } from 'app/_base/base-component.component';
import { MaskUtils } from 'app/core/mask-utils';
import { VehiclesService } from 'app/core/services/vehicles.service';
import { Vehicle } from 'app/core/models/vehicle.model';
import { CoveragesService } from 'app/core/services/coverages.service';
import { CustomerPersonalComponent } from 'app/customers/customer-personal/customer-personal.component';
import { CustomerDocumentsComponent } from 'app/customers/customer-documents/customer-documents.component';
import { CustomerAddressComponent } from 'app/customers/customer-address/customer-address.component';
import { QuotePaymentComponent } from '../../quotes/quote-payment/quote-payment.component';
import { Quote, FuelType } from 'app/core/models/quote.model';
import { QuotesSelectedService } from '../../quotes/quotes.selected.service';
import { QuotesService } from 'app/core/services/quotes.service';
import { UsebensService } from 'app/core/services/usebens.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { Payment, PaymentType, CreditCardBrand,PaymentCreditCard,ChargeType } from '../../../core/models/payment.model';
import { UtilsService } from '../../../core/utils.service';
import { SalesService } from 'app/core/services/sales.service';
import { Customer, PersonType, MaritalStatus, PersonGender, MaritalStatusUsebens } from '../../../core/models/customer.model';
import { QuotePrintComponent } from '../../quotes/quote-print/quote-print.component';
import { CustomersService } from '../../../core/services/customers.service';
import { HttpClient } from "@angular/common/http";
import { ConfirmDialogComponent } from 'app/dialogs/confirm-dialog/confirm-dialog.component';
import { PlansService } from '../../../core/services/plans.service';
import { Plan } from '../../../core/models/plan.model';
import { PlanStatus} from '../../../core/models/plan.model';


@Component({
  selector: 'app-policy-details',
  templateUrl: './policy-details.component.html',
  //styleUrls: ['../quote.component.scss'],
  animations: fuseAnimations
})
export class PolicyDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatStepper) stepper: MatStepper

  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  @ViewChild(CustomerDocumentsComponent)
  private customerDocumentsComponent: CustomerDocumentsComponent

  @ViewChild(CustomerAddressComponent)
  private customerAddressComponent: CustomerAddressComponent

  @ViewChild(QuotePaymentComponent)
  private quotePaymentComponent: QuotePaymentComponent  

  vehicleForm: FormGroup
  vehicleDetailsForm: FormGroup
  coveragesForm: FormGroup
  dataForm: FormGroup
  secureForm: FormGroup
  conductorForm: FormGroup
  quote : Quote
  isLoading: boolean = false
  creditCard : Payment
  manufactureList : Array<any>
  modelList : Array<any>
  yearsList : Array<any>
  registrationYearList: any
  plan : Plan
  
  isSamePerson : boolean = true
  manufacture : any
  model: any
  vehicle : Vehicle
  vehiclePrice: string 
  selectedVehicle : any
  occupation = "1"
  licensePlateMask = MaskUtils.licensePlateMask
  chassiMask = MaskUtils.chassiMask
  yearMask = MaskUtils.yearMask
  postalCodeMask =  MaskUtils.postalCodeMask
  cpfMask =  MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask
  dateMask = MaskUtils.dateMask
  renavamMask = MaskUtils.renavamMask
  quoteSelected : any
  isTuned : Boolean = false
  coveragesList : Array<any> = []
  coveragesSelectedList: Array<any> = []
  maritialStatusList: Array<any> = [{ 'text': 'Solteiro', 'code': '1'},{ 'text': 'Casado', 'code': '2'},{ 'text': 'Separado', 'code': '6'},{ 'text': 'Divorciado', 'code': '4'},{ 'text': 'Outro', 'code': '5'}]
  personGenderList: Array<any> = []
  
  quoteIsChanged = true
  perfilStepCompleted: Boolean = false
  conductorStepCompleted: Boolean = false
  secureStepCompleted: Boolean = false
  calcStepCompleted: Boolean = false
  proposalStepCompleted: Boolean = false
  paymentStepCompleted: Boolean = false
  personTypeList = [{'code':'1','name': "Pessoa Física"},{'code':'2','name': "Pessoa Jurídica"}]

  modelControl = new FormControl();
  occupationControl = new FormControl();

  filteredOptions: Observable<string[]>;
  occupationsOptions: Observable<string[]>;

  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');

  optionsModel = [];

  planChargesDataSource: any
  planChargeLogsDataSource: any
 

  constructor(private injector: Injector, 
              private formBuilder: FormBuilder,
              private vehicleService: VehiclesService,
              private coveragesService: CoveragesService, 
              private loadingService: FuseSplashScreenService,
              private dialog: MatDialog, 
              private quoteSelectedService: QuotesSelectedService,
              private snackbar: MatSnackBar,
              private customersService: CustomersService,
              private plansService: PlansService,
              private salesService: SalesService,
              private usebensService: UsebensService,
              private quoteService: QuotesService,
              private router: Router,
              private http: HttpClient,
              private utils: UtilsService) 
  { 
    super(injector)

    this.quote = new Quote()
    this.quote.coverages = []
    this.vehicle = new Vehicle()

    this.loadDomainData()
    this.initForms()
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading','true');
  }

  ngAfterViewInit() {

    this.quoteSelectedService.quoteSelected.subscribe(quoteSelected => {

      if (quoteSelected != null ) {
        
        if(quoteSelected.sale){
          this.getPlanData(quoteSelected.sale)
        }

        this.quote = quoteSelected
        this.quoteSelected = quoteSelected
        this.loadingService.hide()
        if (this.quote.coverages == null) {
          this.quote.coverages = []
        }
        this.loadData()
      }else{
        this.loadingService.hide()
      }
    })

    this.usebensService.getCoverages()
      .subscribe(coverages => {
        if (coverages && coverages.content) {
          this.coveragesList = coverages.content

          this.loadCoverages()
        }
      })
  }

  private initForms() {

    this.secureForm= this.formBuilder.group({
      renovationType: this.formBuilder.control('', [Validators.required]),
      vehicleLastYearThefts: this.formBuilder.control('', [Validators.required]),
      vehicleIncidents: this.formBuilder.control('', [Validators.required]),
      currentPolicyBonusId: this.formBuilder.control('', [Validators.required]),
      currentPolicyBonusIdClass: this.formBuilder.control('', [Validators.required]),
      insurerPolicyId: this.formBuilder.control('', [Validators.required]),
      currentPolicyEndValidity: this.formBuilder.control('', [Validators.required]),
      currentPolicyId: this.formBuilder.control('', [Validators.required]),
    })

    this.dataForm = this.formBuilder.group({
      occupation: this.formBuilder.control('', [Validators.required]),
      motoristPersonType:this.formBuilder.control('', [Validators.required]),
      vehicleMonthlyUsage:this.formBuilder.control('', [Validators.required]),
      vehicleOwner:this.formBuilder.control('', [Validators.required]),
      garageResidential:this.formBuilder.control("", [Validators.required]),
      garageWork:this.formBuilder.control("", [Validators.required]),
      garageStudy:this.formBuilder.control("", [Validators.required]),
      isVehicleUsedUnder25YearsOld:this.formBuilder.control('', [Validators.required]),
      relationshipWithVehicleOwner: this.formBuilder.control("", [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required]),
      personType: this.formBuilder.control('', [Validators.required]),
      registrationNumber: this.formBuilder.control('', [Validators.required]),
      birthDate: this.formBuilder.control('', [Validators.required]),
      maritialStatus: this.formBuilder.control('', [Validators.required]),
      gender: this.formBuilder.control('', [Validators.required]),
      boughtDate: this.formBuilder.control('', [Validators.required]),
      crvSigned: this.formBuilder.control('', [Validators.required]),
      detran: this.formBuilder.control('', [Validators.required]),
      kindShipWithThirdParty: this.formBuilder.control('', [Validators.required])
    })

    this.vehicleForm = this.formBuilder.group({
      type: this.formBuilder.control('CAR', [Validators.required]),
      manufacture: this.formBuilder.control('', [Validators.required]),
      model: this.formBuilder.control('', [Validators.required]),
      registrationYear: this.formBuilder.control('', [Validators.required]),
      modelYear: this.formBuilder.control('', [Validators.required]),
      vehicleUse: this.formBuilder.control('', [Validators.required])
    })

    this.coveragesForm = this.formBuilder.group({
      rouboFurtoCoverage: this.formBuilder.control(true),
      assistAutoCoverage: this.formBuilder.control('', []),
      assistAutoResidenciaCoverage: this.formBuilder.control('', []),
      morteCoverage: this.formBuilder.control('', []),
      morteCoverageLMI: this.formBuilder.control('', []),
      invalidezCoverage: this.formBuilder.control('', []),
      invalidezCoverageLMI: this.formBuilder.control('', []),
    })

    this.vehicleDetailsForm = this.formBuilder.group({
      licensePlate: this.formBuilder.control('', [Validators.required]),
      chassi: this.formBuilder.control('', [Validators.required]),
      renavam: this.formBuilder.control('', [Validators.required]),
      vehiclePcd: this.formBuilder.control('', [Validators.required]),
      vehicleUseType: this.formBuilder.control('', [Validators.required]),
      gnv : this.formBuilder.control('', [Validators.required]),
      vehicleModificationType: this.formBuilder.control('', [Validators.required]),
      armoredVehicle : this.formBuilder.control('', [Validators.required]),
      color : this.formBuilder.control('', [Validators.required]),
      mileage : this.formBuilder.control('', [Validators.required]),
      isVehicleFinanced : this.formBuilder.control('', [Validators.required]),
      brandNew: this.formBuilder.control('', [Validators.required]),
      politicallyExposedPerson :  this.formBuilder.control('', [Validators.required]),
      relationshipWithPoliticallyExposedPerson : this.formBuilder.control('', [Validators.required])
    })

    this.conductorForm = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required]),
      thirdPartyNationalIdentity: this.formBuilder.control('', [Validators.required]),
      birthdate: this.formBuilder.control('', [Validators.required]),
      maritalStatus: this.formBuilder.control('', [Validators.required]),
      gender: this.formBuilder.control('', [Validators.required]),
      kindshipWithVehicleOwner: this.formBuilder.control('', [Validators.required]),//; //ID_PESSOA_PARENTESCO_PROPRIETARIO
    })

    // listen form changes
    merge(this.vehicleForm.valueChanges, this.coveragesForm.valueChanges)
    .pipe(
        tap(() => {

        })
    ).subscribe()
  }

  private loadDomainData() {
  
  }

  loadData() {

    this.isLoading = true

    this.vehicleDetailsForm.patchValue({
      licensePlate: this.quote.vehicle.licensePlate || null,
      chassi:  this.quote.vehicle.chassi || null,
      renavam:  this.quote.vehicle.renavam || null,
      vehiclePcd:  this.quote.vehicle.pcd || null,
      vehicleUseType:  this.quote.vehicle.useType || null,
      gnv :  this.quote.vehicle.gnv ,
      vehicleModificationType:  this.quote.vehicle.modificationType || null,
      armoredVehicle : this.quote.vehicle.armoredVehicle,
      color :  this.quote.vehicle.color || null,
      mileage :  this.quote.vehicle.mileage || null,
      isVehicleFinanced :  this.quote.vehicle.isVehicleFinanced,
      brandNew:  this.quote.vehicle.brandNew,
      politicallyExposedPerson :  this.quote.customer.politicallyExposedPerson|| null,
      relationshipWithPoliticallyExposedPerson : this.quote.customer.politicallyExposedPerson|| null
    })
    
    this.dataForm.patchValue({
      occupation:this.quote.customer.ocupation|| null,
      motoristPersonType: this.quote.driver.personType || null,
      vehicleMonthlyUsage: this.quote.vehicle.monthlyUsage || null,
      vehicleOwner: this.quote.customer.vehicleOwner,
      garageResidential: this.quote.vehicle.garageResidential || null,
      garageWork: this.quote.vehicle.garageWork || null,
      garageStudy: this.quote.vehicle.garageStudy || null,
      isVehicleUsedUnder25YearsOld: this.quote.vehicle.isVehicleUsedUnder25YearsOld, 
      relationshipWithVehicleOwner: this.quote.customer.relationshipWithVehicleOwner || null,
      postalCode: this.quote.customer.addressPostalCode || null,
      personType:this.quote.customer.personType || null,
      registrationNumber: this.quote.customer.registrationNumber || null,
      birthDate: this.quote.customer.birthdate ? this.convertLoadDate(this.quote.customer.birthdate) : null,
      maritialStatus: this.quote.customer.maritalStatus || null,
      gender: this.quote.customer.gender == '1' ?  'MALE' : 'FEMALE' || null,
      boughtDate: this.quote.vehicle.boughtDate ? this.convertLoadDate(this.quote.vehicle.boughtDate) : null,
      crvSigned:this.quote.vehicle.crvSigned || null,
      detran: this.quote.vehicle.detran || null,
      kindShipWithThirdParty: this.quote.customer.kindShipWithThirdParty || null
    })

    this.secureForm.patchValue({
      renovationType: this.quote.renovationType || null,
      vehicleLastYearThefts: this.quote.vehicle.vehicleLastYearThefts|| null,
      vehicleIncidents: this.quote.vehicle.vehicleIncidents || null,
      currentPolicyBonusId: this.quote.currentPolicyBonusId || null,
      currentPolicyBonusIdClass: this.quote.currentPolicyBonusId || null,
      insurerPolicyId: this.quote.insurerPolicyId || null,
      currentPolicyEndValidity: this.quote.currentPolicyEndValidity ? this.convertLoadDate(this.quote.currentPolicyEndValidity) : null,
      currentPolicyId:  this.quote.currentPolicyId || null,
    })

    this.conductorForm.patchValue({
      name: this.quote.driver.name || null,
      nationalIdentity:  this.quote.driver.nationalIdentity || null,
      birthdate: this.quote.driver.birthdate || null,
      maritalStatus:  this.quote.driver.maritalStatus || null,
      gender:  this.quote.driver.gender == '1' ?  'MALE' : 'FEMALE' || null,
      kindshipWithVehicleOwner:  this.quote.customer.kindshipWithVehicleOwner || null,//; //ID_PESSOA_PARENTESCO_PROPRIETARIO
    })

    let ocupation = this.quote.customer.ocupation
            
    let customer = new Customer()
        customer.registrationNumber =  this.quote.customer.registrationNumber || null;
        customer.personType       = this.quote.customer.personType == '1' ? 'PERSON' : 'COMPANY';
        customer.birthDate =  this.quote.customer.birthDate || null;
        customer.gender = this.quote.customer.gender == '1' ?  'MALE' : 'FEMALE'|| null;
        customer.maritialStatus = this.loadMaritalStatusUsebens(this.quote.customer.maritalStatus) || null;
        customer.brand =  this.quote.brand || null;
        customer.name =  this.quote.customer.name || null;
        customer.email =  this.quote.customer.email || null;
        customer.mobilePhone =  this.quote.customer.cellphone || null;
        customer.residencialPhone =  this.quote.customer.residencialPhone || null;

        if(ocupation && ocupation.length > 0){
            customer.occupation = ocupation[0]['nmProfissao'] 
        }
        

    this.creditCard = this.quote.payment

    this.quote.totalPrice     = this.quote.premiumTotalAmount

    this.loadCoverages()
  
  }

  getPlanData(id){

    this.salesService.getSaleById(id)
      .subscribe(sales => {
        this.plansService.getPlanById(sales.plan)
        .subscribe(plan => {
          this.plan = plan.content[0]
          this.loadPlanCharge(sales.plan)
        })
      })
  }

  loadPlanCharge(id){

    this.planChargesDataSource = []
    this.customersService.getCustomerPlanCharges(id)
      .subscribe(charges => {
        if (charges.content != null) {
          let _charges =  charges.content
          
          _charges.forEach(charge => {
            charge.payment = this.plan['relationships'].payment
            this.planChargesDataSource.push(charge)
          });

          this.planChargesDataSource.sort((d1, d2) => new Date(d1['scheduledDateTime']).getTime() - new Date(d2['scheduledDateTime']).getTime());
  
          if(this.planChargesDataSource.length > 0)
             this.loadChargeLog(this.planChargesDataSource[0])
        }
      })
  }

  loadChargeLog(plan){

    this.planChargeLogsDataSource = []
    this.customersService.getCustomerChargeLogs(plan.id)
      .subscribe(log => {
        if (log.content != null) {
          this.planChargeLogsDataSource = log.content
        }
      })
  }

  planStatusEnumToString(value) : string {
    return PlanStatus[value]
  }

  PaymentTypeEnumToString(value) : string {
    return PaymentType[value]
  }

  PaymentCreditCardEnumToString(value) : string {
    return PaymentCreditCard[value]
  }

  ChargeTypeCardEnumToString(value) : string {
    return ChargeType[value]
  }

  downloadBooklet(url){
    window.open(url, "_blank");
  }

  convertLoadDate(date){
    if(date){
      let _date = new Date(date)
      let day = _date.getDate()
      let month = _date.getMonth()+1
      return (day < 10 ? '0'+day : day) +'/'+ ( month < 10 ? '0'+month : month) +'/' + _date.getFullYear()
    }
  }

  sendToApprove(){

    this.loadingService.show()
    this.quoteService.approvePropose(this.quoteSelected.id)
        .subscribe(result => {
          this.router.navigate(['/insurance/policy/'])
          this.loadingService.hide()
        }, error => {
             this.loadingService.hide()
             console.log(error)
             this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
    })
  }

  sendProposal(){

    this.loadingService.show()
    this.quoteService.createPolicy(this.quoteSelected.id,this.quoteSelected)
        .subscribe(result => {
          this.router.navigate(['/insurance/policy/'])
          this.loadingService.hide()
        }, error => {
             this.loadingService.hide()
             console.log(error)
             this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
    })
  }

  cancelProposal() {

    this.dialog.open(ConfirmDialogComponent, 
      { 
        panelClass: 'event-form-dialog',
        data: { 
          width: '600px',
          height: '300px',
          title: 'Confirmação', 
          message: 'Deseja cancelar essa proposta?' 
        }
      })

      .afterClosed()
      .subscribe(response=>{
        if(response.data.confirm){
          this.loadingService.show()
          this.quoteService.cancelPropose(this.quote.id)
            .subscribe(quote => {
              this.router.navigate(['/insurance/policy/'])
              this.loadingService.hide()
              this.snackbar.open('Proposta cancelada com sucesso', '', {
                duration: 5000
              })
            })
        }
       
      });
    
  }

  loadCoverages(){

    this.coveragesSelectedList = [];

    this.coveragesList.forEach(element => {
      let idx = this.quote.coverages.findIndex(c => c.id === element.idProdutoCobertura)
      
      if (idx != -1) {
        this.coveragesSelectedList.push(element)
      }
    });
  }

  printPolicy(){

    this.loadingService.show()
    this.getPolicy().subscribe(result => {

      var file = new Blob([result], {type: 'application/pdf'});
      var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
      this.loadingService.hide()
      
     }, error => {
       this.loadingService.hide()
       this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
     });
  
  }

  getPolicy() : Observable<any>{
      return this.usebensService.printPolicy(this.quoteSelected.insurerEndorsementId)
  }

  contractInsurance() {

    this.calcStepCompleted = true;
    setTimeout(() => {           
      this.stepper.next();
     }, 1);
  }

  proposalComplete() {
    
  }
  
  paymentTypeEnumToString(value) : string {
    return PaymentType[value]
  }

  quoteStatusAsString(value):string {
    switch (value) {
      case 'PROPOSAL':
        return 'Proposta'
      case 'PROPOSAL_APPROVED':
        return 'Proposta em Aprovada'
      case 'PROPOSAL_UNDER_ANALYSIS':
        return 'Proposta em Analise'
      case 'POLICY':
        return 'Apólice'
      case 'CANCELLED':
        return 'Cancelado.'
      case 'REJECTED':
        return 'Rejeitado pela seguradora.'
      default:
        return ''

    }
  }

  quoteStatus(value):string {
    switch (value) {

      case 'PROPOSAL':
        return 'Aguardando aprovação do cliente'
      case 'PROPOSAL_UNDER_ANALYSIS':
        return 'Aguardando análise da seguradora'
      case 'PROPOSAL_APPROVED':
        return 'Aprovada pelo cliente. Aguardando instalação do rastreador'
      case 'POLICY':
        return 'Apólice'
      case 'CANCELLED':
        return 'Cancelado.'
      case 'REJECTED':
        return 'Rejeitado pela seguradora.'
      default:
        return ''
    }
  }


  loadMaritalStatusUsebens(value):string {

    switch (value) {
      case '1':
        return 'SINGLE'
      case '2':
        return 'MARRIED'
      case '4':
        return 'DIVORCED'
      case '5':
        return 'WIDOWED'
      case '6':
        return 'SEPARATED'
      default:
        return ''
    }

  }


  quotePrintDialog() {
    
    this.dialog
      .open(QuotePrintComponent, {
        width: '900px',
        height:'900px',
        data: this.quote
      })
      .afterClosed()
        .subscribe()
  }  

}

