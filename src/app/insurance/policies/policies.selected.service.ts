import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Policy } from "app/core/models/policy.model";

@Injectable()
export class PoliciesSelectedService {

    private quoteSource : BehaviorSubject<Policy> = new BehaviorSubject(null)
    public quoteSelected = this.quoteSource.asObservable();
  
    setSelected(value: Policy) {
      this.quoteSource.next(value)
    }
}