import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { MatPaginator, MatSort, MatSelect } from '@angular/material';
import { FormControl } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { BaseComponent } from 'app/_base/base-component.component';
import { QuotesService } from 'app/core/services/quotes.service';
import { QuotesDataSource } from '../quotes/quotes.datasource';
import { Quote,QuotaStatus } from '../../core/models/quote.model';
import { QuotesSelectedService } from '../quotes/quotes.selected.service';
import { Router } from '@angular/router';
import { BrokersService } from '../../core/services/brokers.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";

@Component({
  selector: 'proposal-component',
  templateUrl: './proposal.component.html',
  styleUrls: ['./proposal.component.scss'],
  animations: fuseAnimations
})
export class ProposalComponent extends BaseComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  displayedColumns = ['id','vehicle.model', 'cpfCnpj', 'createdDate', 'status'];
  dataSource: QuotesDataSource
  resultLength: number = 0
  statusList: any
  searchSubject = new Subject()
  brandList: Array<any>
  brokerList: Array<any>
  statusSelected : string = '(PROPOSAL,PROPOSAL_APPROVED,PROPOSAL_UNDER_ANALYSIS)'
  brandSelected  : number = 0
  brokerSelected : number = 0
  isBroker : boolean = false
  
  constructor(private injector: Injector,
              private router: Router,
              private quotesService: QuotesService,
              private loadingService: FuseSplashScreenService,
              private brokersService: BrokersService,
              private quoteSelected: QuotesSelectedService) { 
    super(injector)
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  statusOnSelected(){
    this.load('',this.statusSelected, this.brandSelected, this.brokerSelected)
  }

  brandOnSelected(event){
    this.brandSelected =event.value;
    this.load('',this.statusSelected, this.brandSelected, this.brokerSelected)
  }

  brokerOnSelected(event){
    this.brokerSelected =event.value;
    this.load('',this.statusSelected, this.brandSelected, this.brokerSelected)
  }

  ngOnInit() {

    localStorage.setItem('hiddenLoading','false');

    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.load(val,this.statusSelected,0,this.brokerSelected);
    });

    this.loadDomainListData()

    this.brandList = this.appContext.brands

    this.dataSource = new QuotesDataSource(this.quotesService);
    

    this.quoteSelected.setSelected(null)

    if(this.appContext.loggedUser.profile ==  "BROKER"){
      this.isBroker = true
      this.getBrokerByUser()
    }else{
      this.dataSource.load('', 0,this.statusSelected,this.brokerSelected,'lifecycle.lastUpdate', 'desc', 0, 10);
    }

  }

  load(filter? :string, status?: string, brand?: number, broker?: number) {

    this.dataSource.load(
      filter, 
      brand,
      status,
      broker,
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }

  getBrokerByUser(){
    this.brokersService.getBrokerByUser().subscribe(brokers => {
      if (brokers && brokers.content) {
        this.brokerSelected = brokers.content[0].id
        this.dataSource.load('', 0,this.statusSelected,this.brokerSelected,'lifecycle.lastUpdate', 'desc', 0, 10);
      }
    })
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    if(this.dataSource && this.dataSource.totalElements$){
      this.dataSource.totalElements$.subscribe( value => {
        this.resultLength = value
      })
    }
    
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.load('',this.statusSelected,0,this.brokerSelected))
    )
    .subscribe()

  }


  onListValueChanged() {
    this.load()
  }
  
  edit(quote: Quote) {

    localStorage.setItem('hiddenLoading','true');
    this.loadingService.show()
    this.quoteSelected.setSelected(quote)

    let url = 'insurance/proposal/'
    this.router.navigate([url, quote.id])
  }

  quoteStatusAsString(value):string {
    switch (value) {
    
      case 'PROPOSAL':
        return 'Aguardando aprovação do cliente.'
      case 'PROPOSAL_UNDER_ANALYSIS':
        return 'Aguardando análise da seguradora.'
      case 'PROPOSAL_APPROVED':
        return 'Aprovada pelo cliente. Aguardando instalação do rastreador.'
      case 'CANCELLED':
        return 'Cancelado.'
      case 'REJECTED':
        return 'Rejeitado pela seguradora.'
      default:
        return ''
    }
  }

  loadDomainListData() {
    this.statusList = this.utilsService.stringEnumToKeyValue(QuotaStatus)

    this.brokersService.getBrokers().subscribe(brokers => {
      
      if (brokers && brokers.content) {
        this.brokerList = brokers.content
        this.brokerList.push({'name':'ALL','id':0})
        this.brokerList.sort(function(a, b){
          return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });
      }
    })
  }
  

}
