import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteAcceptanceComponent } from './quote-acceptance.component';

describe('QuoteAcceptanceComponent', () => {
  let component: QuoteAcceptanceComponent;
  let fixture: ComponentFixture<QuoteAcceptanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteAcceptanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteAcceptanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
