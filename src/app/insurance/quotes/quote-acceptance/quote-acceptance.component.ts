import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { MatStepper } from '@angular/material';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { BaseComponent } from 'app/_base/base-component.component';
import { QuotePaymentComponent } from '../quote-payment/quote-payment.component';
import { CustomerPersonalComponent } from 'app/customers/customer-personal/customer-personal.component';
import { CustomerDocumentsComponent } from 'app/customers/customer-documents/customer-documents.component';
import { CustomerAddressComponent } from 'app/customers/customer-address/customer-address.component';
import { VehicleDetailsComponent } from 'app/vehicles/vehicle-details/vehicle-details.component';


@Component({
  selector: 'tnm-quote-acceptance',
  templateUrl: './quote-acceptance.component.html',
  styleUrls: ['./quote-acceptance.component.scss'],
  animations: fuseAnimations
})
export class QuoteAcceptanceComponent extends BaseComponent implements OnInit {

  @ViewChild(MatStepper) stepper: MatStepper

  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  @ViewChild(CustomerDocumentsComponent)
  private customerDocumentsComponent: CustomerDocumentsComponent

  @ViewChild(CustomerAddressComponent)
  private customerAddressComponent: CustomerAddressComponent

  @ViewChild(VehicleDetailsComponent)
  private vehicleDetailsComponent: VehicleDetailsComponent  

  @ViewChild(QuotePaymentComponent)
  private quotePaymentComponent: QuotePaymentComponent  

  proposalStepCompleted: Boolean = false
  paymentStepCompleted: Boolean = false


  constructor(private injector : Injector,
              private fuseConfig: FuseConfigService,
              private formBuilder: FormBuilder) {
    super(injector)

    // hide
    this.fuseConfig.config = {
        layout: {
            navbar : { hidden: true },
            toolbar: { hidden: true },
            footer : { hidden: true }
          }
      };
    }

  ngOnInit() {
    
  }
  

  proposalComplete() {

    if (this.customerPersonalComponent.isInvalid() 
        || this.customerDocumentsComponent.isInvalid()
        || this.customerAddressComponent.isInvalid()
        || this.vehicleDetailsComponent.isInvalid()) {

          return;
    }

    this.proposalStepCompleted = true;
    setTimeout(() => {           
      this.stepper.next();
     }, 1);
  }

  onPaymentTypeChanged(type: string) {

    this.paymentStepCompleted = true;
    setTimeout(() => {           
      this.stepper.next();
     }, 1);
  }

}
