import { Component, OnInit, ViewChild, Injector, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar, MatStepper, MatDialog, MatSlideToggleChange } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { merge, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BaseComponent } from 'app/_base/base-component.component';
import { MaskUtils } from 'app/core/mask-utils';
import { VehiclesService } from 'app/core/services/vehicles.service';
import { Vehicle } from 'app/core/models/vehicle.model';
import { CustomerPersonalComponent } from 'app/customers/customer-personal/customer-personal.component';
import { CustomerDocumentsComponent } from 'app/customers/customer-documents/customer-documents.component';
import { CustomerAddressComponent } from 'app/customers/customer-address/customer-address.component';
import { QuoteInstallmentsComponent } from '../quote-installments/quote-installments.component';
import { QuotePaymentComponent } from '../quote-payment/quote-payment.component';
import { Quote, FuelType } from 'app/core/models/quote.model';
import { QuotesSelectedService } from '../quotes.selected.service';
import { QuotesService } from 'app/core/services/quotes.service';
import { Address } from 'app/core/models/address.model';
import { map, startWith } from 'rxjs/operators';
import { UsebensService } from 'app/core/services/usebens.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { Payment, PaymentType, CreditCardBrand } from '../../../core/models/payment.model';
import { UtilsService } from '../../../core/utils.service';
import { Customer, PersonType, MaritalStatus, PersonGender, MaritalStatusUsebens } from '../../../core/models/customer.model';
import { AddressStateUsebens } from '../../../core/models/address.model';
import { QuotePrintComponent } from '../quote-print/quote-print.component';
import { Offer } from '../../../core/models/offer.model';
import { OffersService } from '../../../core/services/offers.service';
import { ProductsService } from '../../../core/services/products.service';
import { ServicesService } from '../../../core/services/services.service';
import { ConfirmDialogComponent } from 'app/dialogs/confirm-dialog/confirm-dialog.component';
import { BrokersService } from '../../../core/services/brokers.service';
import * as moment from 'moment';


@Component({
  selector: 'app-quote-details',
  templateUrl: './quote-details.component.html',
  styleUrls: ['./quote-details.component.scss'],
  animations: fuseAnimations
})
export class QuoteDetailsComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatStepper) stepper: MatStepper

  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  @ViewChild(CustomerDocumentsComponent)
  private customerDocumentsComponent: CustomerDocumentsComponent

  @ViewChild(CustomerAddressComponent)
  private customerAddressComponent: CustomerAddressComponent

  @ViewChild(QuotePaymentComponent)
  private quotePaymentComponent: QuotePaymentComponent

  vehicleForm: FormGroup
  vehicleDetailsForm: FormGroup
  vehicleAdditionalInformation: FormGroup
  coveragesForm: FormGroup
  dataForm: FormGroup
  secureForm: FormGroup
  ownerForm: FormGroup
  driverForm: FormGroup
  quote: Quote
  broker: any
  isLoading: boolean = false
  creditCard: Payment
  manufactureList: Array<any>
  installmentsList: Array<any> = []
  creditCardInstallmentsList: Array<any> = []
  modelList: Array<any>
  yearsList: Array<any>
  offersList: Array<Offer> = []
  servicesList: any
  productsList: any
  registrationYearList: any
  vehicleMonthlyUsageList: Array<any> = [{ 'text': 'até 500 km / mês', 'code': '1' }, { 'text': 'até 1000 km / mês', 'code': '2' }, { 'text': 'até 1250 km / mês', 'code': '3' }, { 'text': 'até 1500 km / mês', 'code': '4' }, { 'text': 'até 1750 km / mês', 'code': '5' }, { 'text': 'acima de 1750 km / mês', 'code': '6' }]
  motoristPersonTypeList: Array<any> = [{ 'text': 'Pessoa Física', 'code': '1' }, { 'text': 'Pessoa Jurídica com condutor principal ', 'code': '2' }, { 'text': 'Pessoa Jurídica sem condutor principal ', 'code': '3' }]
  vehicleModificationTypeList: Array<any> = [{ 'text': 'Não, nenhuma modificação', 'code': '1' }, { 'text': 'Sim, é turbo, rebaixado ou tunado ', 'code': '2' }, { 'text': 'Sim, possui outras modificações', 'code': '3' }]//{'text':'Sim, chassi remarcado','code':'0'},
  relationshipWithVehicleOwnerList: Array<any> = [{ 'text': 'O próprio', 'code': '1' }, { 'text': 'Operação de Leasing', 'code': '2' }, { 'text': 'Veículo comprado recentemente e em transferência', 'code': '3' }, { 'text': 'Veículo em nome de terceiro', 'code': '4' }]
  vehicleUseTypeList: Array<any> = [{ 'text': 'Particular', 'code': '1' }, { 'text': 'Particular Locado', 'code': '2' }, { 'text': 'Motorista de Aplicativo (Uber, Cabify, Pop, outros)', 'code': '3' }, { 'text': 'Representante Comercial / Vendedor', 'code': '4' }, { 'text': 'Serviços Técnicos', 'code': '5' }, { 'text': 'Serviços de Entrega', 'code': '6' }, { 'text': 'Compartilhado (Car Sharing)', 'code': '7' }, { 'text': 'Outros', 'code': '8' }]
  occupationsList: Array<any> = []
  mainActivityList: Array<any> = []
  statusList: Array<any> = [{ 'text': 'Sim', 'type': true }, { 'text': 'Não', 'type': false }]
  garageResidentialList: Array<any> = [{ 'text': 'Sim, externo privado e pago', 'code': "1" }, { 'text': 'Sim, com portão manual', 'code': "2" }, { 'text': 'Sim, com portão automático ou porteiro', 'code': "3" }, { 'text': 'Não ou outro tipo de garagem', 'code': "4" }]
  garageWorkList: Array<any> = [{ 'text': 'Sim, interno da empresa em local fechado', 'code': "5" }, { 'text': 'Sim, externo a empresa, privado e pago', 'code': "6" }, { 'text': 'Não ou Outro tipo de garagem', 'code': "7" }, { 'text': 'Não usa para locomoção ao trabalho', 'code': "8" }, { 'text': 'Não trabalha', 'code': "9" }]
  garageStudyList: Array<any> = [{ 'text': 'Diurno em garagem privada e fechada - paga', 'code': "10" }, { 'text': 'Diurno sem garagem', 'code': "11" }, { 'text': 'Não estuda', 'code': "12" }, { 'text': 'Não utiliza o veiculo para locomoção ao local de estudo', 'code': "13" }, { 'text': 'Noturno em garagem privada e fechada - paga', 'code': "14" }, { 'text': 'Noturno sem garagem', 'code': "15" }]
  vehiclePcdList: Array<any> = [{ 'text': 'Não', 'code': "1" }, { 'text': 'Sim, PCD original de fábrica', 'code': "2" }, { 'text': 'Sim, adaptado para PCD', 'code': "3" }]
  typeVehicleList: Array<any> = [{ id: 'CAR', name: 'Carros e utilitários' }];
  vehicleUseList: Array<any> = [{ id: 'convencional', name: 'Convencional' }, { id: 'taxi', name: 'Táxi' }, { id: 'locadora', name: 'Locadora' }, { id: 'aplicativo', name: 'Transporte de passageiro por aplicativo' }];
  renovationTypeList: Array<any> = [{ 'text': 'Sim, da Usebens', 'code': '1' }, { 'text': 'Sim, de Congênere', 'code': '3' }, { 'text': 'Não (seguro novo)', 'code': '2' }];
  vehicleIncidentsList: Array<any> = [{ 'text': 'Nenhum', 'code': '1' }, { 'text': '1', 'code': '2' }, { 'text': '2', 'code': '3' }, { 'text': '3', 'code': '4' }, { 'text': '4 ou mais', 'code': '5' }];
  currentPolicyBonusIdList: Array<any> = [{ 'text': '1', 'code': '1' }, { 'text': '2', 'code': '2' }, { 'text': '3', 'code': '3' }, { 'text': '4', 'code': '4' }, { 'text': '5', 'code': '5' }, { 'text': '6', 'code': '6' }, { 'text': '7', 'code': '7' }, { 'text': '8', 'code': '8' }, { 'text': '9', 'code': '9' }, { 'text': '10', 'code': '10' }, { 'text': '0', 'code': '11' }];
  insurerPolicyIdList: Array<any> = [{ 'text': 'USEBENS SEGUROS SA', 'code': '339141' }, { 'text': 'AIG SEGUROS BRASIL S.A.', 'code': '3466007' }, { 'text': 'ALFA SEGURADORA S.A.', 'code': '3466008' }, { 'text': 'ALIANÇA DO BRASIL SEGUROS S.A.', 'code': '3466009' }, { 'text': 'ALLIANZ SEGUROS S.A.', 'code': '3466010' }, { 'text': 'BRADESCO VIDA E PREVIDÊNCIA S.A', 'code': '132029' }, { 'text': 'BMG SEGURADORA S.A', 'code': '91317' }, { 'text': 'BRASILVEÍCULOS COMPANHIA DE SEGUROS', 'code': '3466014' },
  { 'text': 'CAIXA SEGURADORA S.A.', 'code': '3466016' }, { 'text': 'CARDIF DO BRASIL SEGUROS E GARANTIAS S.A.', 'code': '3466017' }, { 'text': 'CESCEBRASIL SEGUROS DE GARANTIAS E CRÉDITO S.A.', 'code': '3466018' }, { 'text': 'CHUBB SEGUROS BRASIL S.A.', 'code': '3466019' }, { 'text': 'COMPANHIA DE SEGUROS ALIANÇA DA BAHIA', 'code': '3466020' }, { 'text': 'COMPANHIA DE SEGUROS ALIANÇA DO BRASIL', 'code': '3466021' }, { 'text': 'COMPANHIA DE SEGUROS DO ESTADO DE SÃO PAULO - COSESP', 'code': '3466022' }, { 'text': 'COMPANHIA DE SEGUROS PREVIDÊNCIA DO SUL - PREVISUL', 'code': '3466023' },
  { 'text': 'ESSOR SEGUROS S.A', 'code': '3466024' }, { 'text': 'GENERALI BRASIL SEGUROS S.A.', 'code': '3466025' }, { 'text': 'GENTE SEGURADORA S.A.', 'code': '3466026' }, { 'text': 'HDI GLOBAL SEGUROS S.A.', 'code': '3466027' }, { 'text': 'HDI SEGUROS S.A.', 'code': '3466028' }, { 'text': 'INDIANA SEGUROS S.A.', 'code': '3466029' }, { 'text': 'INVESTPREV SEGURADORA S.A.', 'code': '3466031' }, { 'text': 'ITAU SEGUROS DE AUTO E RESIDÊNCIA', 'code': '3466032' }, { 'text': 'ITAU SEGUROS S.A.', 'code': '3466033' }, { 'text': 'J. MALUCELLI SEGURADORA S/A', 'code': '3466034' }, { 'text': 'LIBERTY SEGUROS S.A.', 'code': '3466035' }, { 'text': 'MAPFRE SEGUROS GERAIS S.A.', 'code': '3466036' }, { 'text': 'MITSUI SUMITOMO SEGUROS S.A.', 'code': '3466037' },
  { 'text': 'PORTO SEGURO CIA DE SEGUROS GERAIS', 'code': '3466038' }, { 'text': 'QBE BRASIL SEGUROS S.A.', 'code': '3466039' }, { 'text': 'SAFRA SEGUROS GERAIS S.A.', 'code': '3466040' }, { 'text': 'SANCOR SEGUROS DO BRASIL S.A.', 'code': '3466041' }, { 'text': 'SEGURADORA BRASILEIRA DE CRÉDITO À EXPORTAÇÃO S.A.', 'code': '3466042' }, { 'text': 'SEGUROS SURA S.A.', 'code': '3466043' }, { 'text': 'SOMPO SEGUROS S.A.', 'code': '3466044' }, { 'text': 'SUHAI SEGURADORA S.A.', 'code': '3466045' }, { 'text': 'SUL AMÉRICA CIA NACIONAL DE SEGUROS', 'code': '189589' }, { 'text': 'TOKIO MARINE SEGURADORA S.A.', 'code': '109647' }, { 'text': 'TOKIO MARINE SEGURADORA S.A', 'code': '3466049' }, { 'text': 'SUL AMERICA CIA NACIONAL DE SEGUROS', 'code': '121234' }, { 'text': 'SEGURADORA LÍDER DOS CONSÓRCIOS DO SEGURO DPVAT S.A.', 'code': '352596' }, { 'text': 'USEBENS SEGUROS S.A.', 'code': '3466046' }];
  colorVehicleList = ["Amarelo", "Azul", "Branco", "Chumbo", "Cinza", "Creme", "Dourado", "Laranja", "Marrom", "Branco", "Prata", "Preto", "Roxo", "Verde", "Vermelho"]
  kindShipWithThirdPartyList: Array<any> = [{ 'text': 'Cônjuge ou União Estável Comprovada', 'code': '1' }, { 'text': 'Pais e Filhos ou Enteados', 'code': '2' }, { 'text': 'Avós e Netos', 'code': '3' }, { 'text': 'Irmãos', 'code': '4' }, { 'text': 'Primos de primeiro grau', 'code': '5' }, { 'text': 'Tio e sobrinho de primeiro grau', 'code': '6' }, { 'text': 'PJ e Sócio / Adm. no Contrato Social / Estatuto', 'code': '7' }, { 'text': 'PJ e Diretor da Empresa', 'code': '8' }, { 'text': 'Espólio do Proprietário Falecido', 'code': '9' }, { 'text': 'Outro', 'code': '10' }];
  politicallyExposedPersonList: Array<any> = ['Sim', 'Não']
  isSamePerson: boolean = true
  manufacture: any
  model: any
  vehicle: Vehicle
  vehiclePrice: string
  selectedVehicle: any
  occupation: any
  licensePlateMask = MaskUtils.licensePlateMask
  chassiMask = MaskUtils.chassiMask
  yearMask = MaskUtils.yearMask
  postalCodeMask = MaskUtils.postalCodeMask
  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask
  dateMask = MaskUtils.dateMask
  phoneMask = MaskUtils.phoneMask
  renavamMask = MaskUtils.renavamMask
  isTuned: Boolean = false
  coveragesList: Array<any> = []
  coveragesSelectedList: Array<any> = []

  maritialStatusList: Array<any> = [{ 'text': 'Solteiro', 'code': '1' }, { 'text': 'Casado', 'code': '2' }, { 'text': 'Divorciado', 'code': '4' }, { 'text': 'Viúvo', 'code': '5' }, { 'text': 'Separado', 'code': '6' }]
  personGenderList: Array<any> = []
  activitiesList: Array<any> = []

  quoteIsChanged = true
  quoteIsLoad = false
  perfilStepCompleted: Boolean = false
  conductorStepCompleted: Boolean = false
  calcStepCompleted: Boolean = false
  proposalStepCompleted: Boolean = false
  paymentStepCompleted: Boolean = false
  personTypeList = [{ 'code': '1', 'name': "Pessoa Física" }, { 'code': '2', 'name': "Pessoa Jurídica" }]
  nMainActivity = null
  nOccupation = null
  modelControl = new FormControl();
  occupationControl = new FormControl();
  mainActivityControl = new FormControl();
  mainActivity: any

  filteredOptions: Observable<string[]>;
  occupationsOptions: Observable<string[]>;
  mainActivityOptions: Observable<string[]>;

  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  primaryColorBrand: string = localStorage.getItem('primaryColorBrand');

  optionsModel = [];

  constructor(private injector: Injector,
    private formBuilder: FormBuilder,
    private vehicleService: VehiclesService,
    private loadingService: FuseSplashScreenService,
    private dialog: MatDialog,
    private quoteSelectedService: QuotesSelectedService,
    private snackbar: MatSnackBar,
    private offersService: OffersService,
    private brokersService: BrokersService,
    private usebensService: UsebensService,
    private quoteService: QuotesService,
    private productsService: ProductsService,
    private servicesService: ServicesService,
    private router: Router,
    private utils: UtilsService) {
    super(injector)

    this.quote = new Quote()
    this.quote.coverages = []
    this.vehicle = new Vehicle()

    
    this.initForms()
  }

  ngOnInit() {

    localStorage.setItem('hiddenLoading', 'true');

    this.filteredOptions = this.modelControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );

    this.occupationsOptions = this.occupationControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterOccupation(value))
    );

    this.mainActivityOptions = this.mainActivityControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterMainActivity(value))
    );

   
   this.customerDocumentsComponent.setRequired()
   this.customerPersonalComponent.setRequired()
  
  
    this.customerPersonalComponent.setHideFields()
   

    this.customerPersonalComponent.personType
      .subscribe(type => {

        let personType = 1;

            if(type == 'Jurídica'){
              personType = 2;
            }

            this.customerDocumentsComponent.setPersonType(type)

            this.dataForm.patchValue({
              personType: personType
            })
      })

    this.getProducts()
      .subscribe(
        products => {
          this.productsList = products.content

          this.getServices()
            .subscribe(
              services => {
                this.servicesList = services.content
                this.getOffers()
              }
            )
        }
      )

    if (this.appContext.loggedUser.profile == "BROKER") {
      this.getBrokerByUser()
    }
  }

  ngAfterViewInit() {

    this.quoteSelectedService.quoteSelected.subscribe(result => {

      if (result != null) {
        this.quote = result

        if (this.quote.coverages == null) {
          this.quote.coverages = []
        }

        if (!this.quoteIsLoad) {
          this.quoteIsLoad = true;

            this.usebensService.getOccupations()
              .subscribe(occupations => {
                if (occupations && occupations.content) {

                  this.occupationsList = occupations.content

                  if (this.quote && this.quote.customer && this.quote.customer.profession) {

                    let ocupation = this._filterOccupationById(this.quote.customer.profession)

                    if (ocupation && ocupation.length > 0) {
                    
                          this.occupation = ocupation[0]['cdProfissao']
                          this.nOccupation = ocupation[0]['nmProfissao']

                          this.loadDomainData()
                          this.loadData()

                          this.quoteIsChanged = false;
                       
                          this.calcStepCompleted = true
                          this.proposalStepCompleted = true
                          this.paymentStepCompleted = true
                    }

                  }else{
                      this.loadDomainData()
                      this.loadData()

                      this.quoteIsChanged = false;
                    
                      this.calcStepCompleted = true
                      this.proposalStepCompleted = true
                      this.paymentStepCompleted = true
                  }

                  
                }
              })

        }

      } else {
        this.loadDomainData()
        this.loadingService.hide()
      }
    })
  }

  private initForms() {

    this.secureForm = this.formBuilder.group({
      renovationType: this.formBuilder.control(null, [Validators.required]),
      vehicleLastYearThefts: this.formBuilder.control(null, [Validators.required]),
      vehicleIncidents: this.formBuilder.control(null, [Validators.required]),
      currentPolicyBonusId: this.formBuilder.control(null, [Validators.required]),
      currentPolicyBonusIdClass: this.formBuilder.control(null, [Validators.required]),
      insurerPolicyId: this.formBuilder.control(null, [Validators.required]),
      currentPolicyEndValidity: this.formBuilder.control(null, [Validators.required]),
      currentPolicyId: this.formBuilder.control(null, [Validators.required]),
    })

    this.dataForm = this.formBuilder.group({
      profession: this.formBuilder.control(null, [Validators.required]),
      motoristPersonType: this.formBuilder.control(null, [Validators.required]),
      vehicleMonthlyUsage: this.formBuilder.control(null, [Validators.required]),
      vehicleOwner: this.formBuilder.control(null, [Validators.required]),
      garageResidential: this.formBuilder.control(null, [Validators.required]),
      garageWork: this.formBuilder.control(null, [Validators.required]),
      garageStudy: this.formBuilder.control(null, [Validators.required]),
      usedUnder25YearsOld: this.formBuilder.control(null, [Validators.required]),
      relationshipWithVehicleOwner: this.formBuilder.control(null, [Validators.required]),
      postalCode: this.formBuilder.control(null, [Validators.required]),
      personType: this.formBuilder.control(null, [Validators.required]),
      birthDate: this.formBuilder.control(null, [Validators.required]),
      maritialStatus: this.formBuilder.control(null, [Validators.required]),
      gender: this.formBuilder.control(null, [Validators.required]),
      boughtDate: this.formBuilder.control(null, [Validators.required]),
      crvSigned: this.formBuilder.control(null, [Validators.required]),
      detranDocumentation: this.formBuilder.control(null, [Validators.required]),
      kindShipWithThirdParty: this.formBuilder.control(null, [Validators.required]),
      kindshipWithVehicleOwner: this.formBuilder.control(null, [Validators.required]),
      mainActivity: this.formBuilder.control(null, [Validators.required]),
      offer: this.formBuilder.control(null, [Validators.required]),
      residencialPhone: this.formBuilder.control(null, [Validators.required])
    })

    this.vehicleForm = this.formBuilder.group({
      type: this.formBuilder.control('CAR', [Validators.required]),
      manufacture: this.formBuilder.control(null, [Validators.required]),
      model: this.formBuilder.control(null, [Validators.required]),
      registrationYear: this.formBuilder.control(null, [Validators.required]),
      modelYear: this.formBuilder.control(null, [Validators.required]),
      vehicleUse: this.formBuilder.control(null, [Validators.required])
    })

  
    this.vehicleAdditionalInformation = this.formBuilder.group({
      vehiclePcd: this.formBuilder.control(null, [Validators.required]),
      vehicleUseType: this.formBuilder.control(null, [Validators.required]),
      vehicleModificationType: this.formBuilder.control(null, [Validators.required]),
      armored: this.formBuilder.control(null, [Validators.required]),
      gnv: this.formBuilder.control(null, [Validators.required]),
      brandNew: this.formBuilder.control(null, [Validators.required])
    })

    this.vehicleDetailsForm = this.formBuilder.group({
      licensePlate: this.formBuilder.control(null, [Validators.required]),
      chassi: this.formBuilder.control(null, [Validators.required]),
      renavam: this.formBuilder.control(null, [Validators.required]),
      color: this.formBuilder.control(null, [Validators.required]),
      mileage: this.formBuilder.control(null, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
      financed: this.formBuilder.control(null, [Validators.required]),
      politicallyExposedPerson: this.formBuilder.control(null, [Validators.required]),
      relationshipWithPoliticallyExposedPerson: this.formBuilder.control(null, [Validators.required])
    })

    this.ownerForm = this.formBuilder.group({
      name: this.formBuilder.control(null, [Validators.required]),
      thirdPartyNationalIdentity: this.formBuilder.control(null, [Validators.required]),
      birthdate: this.formBuilder.control(null, [Validators.required]),
      gender: this.formBuilder.control(null, [Validators.required])
    })

    this.driverForm = this.formBuilder.group({
      name: this.formBuilder.control(null, [Validators.required]),
      registrationNumber: this.formBuilder.control(null, [Validators.required]),
      birthDate: this.formBuilder.control(null, [Validators.required]),
      maritalStatus: this.formBuilder.control(null, [Validators.required]),
      gender: this.formBuilder.control(null, [Validators.required])
    })

    this.coveragesForm = this.formBuilder.group({
            rouboFurtoCoverage: this.formBuilder.control(true),
    })

  }

  private loadDomainData() {

    this.personGenderList = this.utils.stringEnumToKeyValue(PersonGender)

    this.changeManufacture('CAR', null);

    this.loadOccupations()

    this.usebensService.getCoverages()
      .subscribe(coverages => {
        if (coverages && coverages.content) {
          this.coveragesList = coverages.content

          this.loadCoverages()
          this.createControlCoverageForm()
          
        }
      })

    this.loadActivities()

  }

  createControlCoverageForm(){

    if(this.coveragesList && this.coveragesList.length > 0){

      this.coveragesForm = null;

      this.coveragesForm = this.formBuilder.group({
        rouboFurtoCoverage: this.formBuilder.control(true),
        })

      this.coveragesList.forEach(element => {
        if (!this.coveragesForm.get(element.idProdutoCobertura) && element.idProdutoCobertura != "753") {
          
            let idx = this.quote.coverages.findIndex(c => c.id === element.idProdutoCobertura)
            if (idx != -1) {
              this.coveragesForm.addControl(element.idProdutoCobertura, this.formBuilder.control(true));
            }else{
              this.coveragesForm.addControl(element.idProdutoCobertura, this.formBuilder.control(false));
            }
          
        }
      });
    }
  }

  loadOccupations() {

    if (this.occupationsList.length == 0) {
    this.usebensService.getOccupations()
      .subscribe(occupations => {
        if (occupations && occupations.content) {
          this.occupationsList = occupations.content

          if (this.quote && this.quote.customer && this.quote.customer.profession) {

            let ocupation = this._filterOccupationById(this.quote.customer.profession)

            if (ocupation && ocupation.length > 0) {
              this.dataForm.patchValue({
                profession: ocupation[0]['nmProfissao']
              })

              this.occupation = ocupation[0]['cdProfissao']
              this.nOccupation = ocupation[0]['nmProfissao']
            }

          }
        }
      })
    }else{
      
      if (this.quote && this.quote.customer && this.quote.customer.profession) {

          setTimeout(() => {  
            this.dataForm.patchValue({
              profession:  this.nOccupation
            })
            }, 500);
      }

    }

  }

  loadActivities() {
    this.usebensService.getActivities()
      .subscribe(activities => {
        if (activities && activities.content) {
          this.mainActivityList = activities.content

          if (this.quote && this.quote.customer && this.quote.customer.mainActivity) {

            let mainActivity = this._filterMainActivityById(this.quote.customer.mainActivity)

            if (mainActivity && mainActivity.length > 0) {

                this.nMainActivity = mainActivity[0]['nmRamoAtividade']

                this.dataForm.patchValue({
                  mainActivity: mainActivity[0]['nmRamoAtividade']
                })

                this.mainActivity = this.quote.customer.mainActivity;

                if (this.quote.customer.personType == '2') {
                  this.customerDocumentsComponent.setPersonType(PersonType['PERSON'])
                } else {
                  this.customerDocumentsComponent.setPersonType(PersonType['COMPANY'])
                }

            }

          }
        }
      })
  }

  personTypeSelected(event) {

    let type = 'PERSON'

    if (event.value == '2') {
      type = 'COMPANY'
      this.customerDocumentsComponent.setRequiredCompany()
      this.customerPersonalComponent.setRequiredCompany()
    } else {
      this.customerDocumentsComponent.setRequired()
    }

    this.customerDocumentsComponent.setPersonType(PersonType[type])
    this.customerPersonalComponent.initForms()
  }

  getProducts(): Observable<any> {
    return this.productsService.getProducts()
  }

  getServices(): Observable<any> {
    return this.servicesService.getServices()
  }

  getOffers() {
    this.offersService.getOffersByBrand(this.appContext.loggedUser.brand, 'INSURANCE')
      .subscribe(offers => {
        let maxProductLength = 0;
        let maxServiceLength = 0;

        if (offers.content != null) {
          this.offersList = offers.content;
          this.offersList.forEach(offer => {
            if (maxProductLength < offer.products.length) {
              maxProductLength = offer.products.length
            }
            if (maxProductLength < offer.services.length) {
              maxServiceLength = offer.services.length
            }
            offer.products = this.utilsService.getCompleteListByIdsList(offer.products, this.productsList)
            offer.services = this.utilsService.getCompleteListByIdsList(offer.services, this.servicesList)
          });

          this.offersList.forEach(offer => {
            if (maxProductLength > offer.products.length) {
              for (var i = 0; i < (maxProductLength - offer.products.length) + 1; i++) { offer.products.push({}) }
            }
            if (maxServiceLength > offer.services.length) {
              for (var i = 0; i < (maxServiceLength - offer.services.length) + 1; i++) { offer.services.push({}) }
            }
          });

        }

        if (offers && offers.content.length > 0) {
          this.dataForm.patchValue({
            offer: offers.content[0].id
          })
        }

        this.offersList.sort(function (a, b) {
          return a.saleMaxInstallments - b.saleMaxInstallments
        });
      })
  }

  setOffer(stepper: MatStepper, offer) {

    this.dataForm.patchValue({
      offer: offer.id
    })
  }

  changeModificationType(code) {
    if (code == "2") {
      this.isTuned = true
      this.snackbar.open('Veículos turbo, rebaixados ou tunados não são aceitos.', '', { duration: 10000 });
    } else if (code == '0') {
      this.snackbar.open('Veículos com chassi remarcado não são aceitos.', '', { duration: 10000 });
      this.isTuned = true
    } else if (code == '3') {
      this.snackbar.open('Aceitação sujeita a análise de vistoria e documentações (conforme manual do produto / corretor vigente).', '', { duration: 10000 });
      this.isTuned = false
    } else {
      this.isTuned = false
    }
  }

  changeCrvSigned(status) {
    if (status) {
      this.isTuned = true
      this.snackbar.open('A Seguradora somente aceita veículos cujo documento de transferência já esteja assinado em nome do novo proprietário, o segurado. No caso de veículos zero KM com Nota Fiscal emitida, responder SIM.', '', { duration: 10000 });
    } else {
      this.isTuned = false
    }
  }

  changearmored(status) {
    if (status) {
      this.snackbar.open('Fique atento para as informações obrigatórias e documentações necessárias para emitir o seguro em caso de veículos blindados. Não há cobertura para o valor da blindagem. Verifique o percentual FIPE disponibilizado pela Seguradora. Consulte o manual de produto / corretor para certificar-se que todas as documentações exigidas estão disponíveis, antes de seguir com o pedido de formalização da proposta.', '', { duration: 10000 });
    }
  }

  changeCustomerMotorist(status) {
    if (!status) {
      this.snackbar.open('Considerando que o condutor principal e o segurado são pessoas diferentes, será necessário o CPF do condutor principal para elaboração da Proposta. Para a Cotação não solicitaremos essa informação, mas a tenha em mão para a formalização da Proposta.', '', { duration: 10000 });
    }
  }

  changeMotorist25(status) {
    if (!status) {
      this.snackbar.open('Caso ocorra um sinistro e o condutor tenha até 25 anos no momento do sinistro, não haverá cobertura. Favor explicar cuidadosamente ao seu cliente.', '', { duration: 10000 });
    }
  }

  changeKindShipWithThirdParty(code) {
    if (code == "10") {
      this.snackbar.open('Caso o cliente não se enquadre na relação entre o segurado e proprietário desta relação, não haverá aceitação pela Seguradora. Portanto, OUTROS tipos de relação não possuem aceitação, seja ela de qualquer natureza.', '', { duration: 10000 });
    }
  }

  changeManufacture(type, data) {

    this.manufactureList = []
    this.modelList = []
    this.yearsList = []
    this.registrationYearList = []

    this.vehicleService.getManufacturers(type)
      .subscribe(manufactures => {
        this.manufactureList = manufactures.content
        this.manufactureList.sort(function (a, b) {
          return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });
        if (data) {
          manufactures.content.forEach(manufacture => {
            if (manufacture['name'] == data.manufacturerName) {
              this.changeModel(manufacture.id, data)
            }
          });
        }
      })
  }

  changeModel(manufacture, data) {

    this.manufacture = manufacture
    this.modelList = []
    this.yearsList = []
    this.registrationYearList = []
    this.vehicleService.getModels(manufacture)
      .subscribe(models => {
        this.modelList = models.content
        this.modelList.sort(function (a, b) {
          return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });
        models.content.forEach(model => {
          this.optionsModel.push({ name: model.name, id: model.id })
          if (data) {
            if (model['name'] == data.modelName) {
              this.changeYears(model.id, data)
            }
          }
        });

      })
  }


  changeYears(model, data) {
    this.model = model
    this.vehicleService.getYears(this.manufacture, model)
      .subscribe(years => {

        years.content.forEach(model => {
          if (model['name'] != "32000 Gasolina") {
            this.yearsList.push(model)
          }
        })

        this.yearsList.sort(function (a, b) {
          return a['idt'] - b['idt']
        });

        if (data) {
          years.content.forEach(year => {
            if (year['name'].indexOf(data.modelYear) >= 0) {
              this.isLoading = false;
              this.changeVehicle(year.idModeloAno, data)
            }
          });
        }
      })
  }

  changeSelectedOccupation(model) {
    let selected = this._filterOccupation(model)
    this.occupation = selected[0]['cdProfissao']
    this.nOccupation = selected[0]['nmProfissao']
    this.dataForm.patchValue({
      profession: model
    })
  }

  changeSelectedMainActivity(model) {
    let selected = this._filterMainActivity(model)
    this.mainActivity = selected[0]['idRamoAtividade']
    this.dataForm.patchValue({
      mainActivity: model
    })
  }

  changeSelectedYears(model) {

    this.vehicleForm.patchValue({
      model: model
    });

    let selected = this._filter(model)
    if (selected && selected.length > 0) {
      this.model = selected[0]['id']
      this.vehicleService.getYears(this.manufacture, this.model)
        .subscribe(years => {
          years.content.forEach(model => {
            if (model['name'] != "32000 Gasolina") {
              this.yearsList.push(model)
            }
          })

          this.yearsList.sort(function (a, b) {
            return a['idt'] - b['idt']
          });

        })
    }
  }

  changeVehicle(year, data) {
    this.vehicleService.getVehicle(this.manufacture, this.model, year)
      .subscribe(vehicles => {
        vehicles.content.forEach(vehicle => {

          let registrationYear = null;

          if (vehicle.idModeloAno == year) {
            this.selectedVehicle = vehicle
            this.vehicle.model = vehicle.modelo
            this.vehicle.manufacture = vehicle.marca
            this.vehicle.modelYear = vehicle.ano
            this.vehiclePrice = vehicle.preco
            this.registrationYearList = []
            this.registrationYearList.push(vehicle.ano)
            this.registrationYearList.push(((parseInt(vehicle.ano) + 1).toString()))
            this.registrationYearList.sort(function (a, b) {
              return a - b
            });
            registrationYear = vehicle.idModeloAno

            if (data) {

              this.vehicleForm.patchValue({
                type: 'CAR',
                manufacture: this.quote.vehicle.manufacturer || null,
                model: this.quote.vehicle.modelName || null,
                registrationYear: registrationYear || null,
                modelYear: this.quote.vehicle.modelYear || null,
                vehicleUse: 'convencional'
              })

              this.loadingService.hide()
            }
          }


        });
      })
  }

  recalculate() {
    this.quoteIsChanged = false;
    this.calculate(false)
    this.loadCoverages()
  }

  validateVehicleFilds() {

    if ((!this.vehicleForm.valid || !this.vehicleAdditionalInformation.valid) && !this.perfilStepCompleted) {
      this.validateAllFields(this.vehicleForm);
      this.validateAllFields(this.vehicleAdditionalInformation);
      return false;
    }

    if (!this.secureForm.value.renovationType || !this.secureForm.value.vehicleLastYearThefts || !this.secureForm.value.vehicleIncidents) {
      this.validateAllFields(this.secureForm);
      return false
    }

    if (parseInt(this.secureForm.value.renovationType) == 3) {

      if (!this.secureForm.value.currentPolicyBonusId || !this.secureForm.value.insurerPolicyId || !this.secureForm.value.currentPolicyBonusIdClass || !this.secureForm.value.currentPolicyEndValidity || !this.secureForm.value.currentPolicyId) {
        this.validateAllFields(this.secureForm);
        return false
      }
    }
   
    this.perfilStepCompleted = true;
    setTimeout(() => { this.stepper.next(); }, 1);
    
    var scrollDiv = document.getElementById('contentScroll');
        scrollDiv.scrollTop = 0; 
  }

  validateConductorFilds() {

    let isValidate = true

    let customer = this.customerPersonalComponent.getCustomerData()
    //let documents = this.customerDocumentsComponent.getCustomerData()

    if (this.dataForm.value.personType == '2') {

      if (!customer.name || !customer.procurator ||  !customer.mobilePhone || !customer.email || !customer.registrationNumber) {
        this.validateAllFields(this.customerPersonalComponent.getCustomerForm())
        this.snackbar.open('Todos os dados da empresa precisam ser preenchidos.', '', { duration: 6000 });
        isValidate = false
        return;
      }

    }else{

      if (!customer.name || !customer.registrationNumber ||  !customer.mobilePhone || !customer.email) {
        this.validateAllFields(this.customerPersonalComponent.getCustomerForm())
        this.snackbar.open('Todos os dados do segurado precisam ser preenchidos.', '', { duration: 6000 });
        isValidate = false
        return;
      }

    }
   

    if (parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2) {

      if (!this.dataForm.value.kindShipWithThirdParty) {

        this.validateAllFields(this.dataForm)
        isValidate = false
        return false;
      }

      if (!this.ownerForm.value.name || !this.ownerForm.value.thirdPartyNationalIdentity || !this.ownerForm.value.birthdate || !this.ownerForm.value.gender) {

        this.snackbar.open('Os dados do proprietário do veículo devem ser preenchidos', '', { duration: 4000 });
        this.validateAllFields(this.ownerForm)
        isValidate = false
        return false;
      }

      if (this.ownerForm.value.birthdate) {
        try {

          let nDate = moment(this.ownerForm.value.birthdate, "DD-MM-YYYY").toISOString()
          new Date(nDate).toISOString()
          if (!nDate) {
            this.ownerForm.patchValue({
              birthdate: null
            })
            this.validateAllFields(this.ownerForm);
            isValidate = false
            this.snackbar.open('Data inválida .', '', { duration: 4000 });
            return false;
          }
        }
        catch (err) {
          this.ownerForm.patchValue({
            birthdate: null
          })
          this.ownerForm.value.birthdate = null
          this.validateAllFields(this.ownerForm);
          isValidate = false
          this.snackbar.open('Data inválida .', '', { duration: 4000 });
          return false;
        }
      }
    }

    //Validar aqui dados do user customer
    if (!this.dataForm.value.personType || this.dataForm.value.vehicleOwner == null || !this.dataForm.value.relationshipWithVehicleOwner || !this.dataForm.value.motoristPersonType || !this.dataForm.value.postalCode ) {
      this.validateAllFields(this.dataForm);
      isValidate = false
    }


    if (!this.dataForm.value.vehicleOwner) {
      if (!this.driverForm.value.name || !this.driverForm.value.registrationNumber || !this.driverForm.value.birthDate || !this.driverForm.value.maritalStatus || !this.driverForm.value.gender) {
        this.validateAllFields(this.driverForm);
        isValidate = false
      }
    }

    if (parseInt(this.dataForm.value.relationshipWithVehicleOwner) == 3) {

      if (!this.dataForm.value.boughtDate || this.dataForm.value.crvSigned == null || this.dataForm.value.detranDocumentation == null) {
        this.validateAllFields(this.dataForm);
        isValidate = false
      }

    } else if (parseInt(this.dataForm.value.relationshipWithVehicleOwner) == 4) {
     
      if (!this.dataForm.value.kindShipWithThirdParty) {
        this.validateAllFields(this.dataForm);
        isValidate = false
      }

    }

    if (this.dataForm.value.motoristPersonType != '3') {

      if (this.dataForm.value.personType == '1') {
        if (!this.dataForm.value.gender || !this.dataForm.value.profession) {
          this.validateAllFields(this.dataForm);
          isValidate = false
        }
      }

      if (!this.dataForm.value.birthDate || !this.dataForm.value.maritialStatus || !this.dataForm.value.garageWork || !this.dataForm.value.garageStudy || !this.dataForm.value.garageResidential || !this.dataForm.value.vehicleMonthlyUsage || this.dataForm.value.usedUnder25YearsOld == null) {
        this.validateAllFields(this.dataForm);
        isValidate = false
      }

      if (this.dataForm.value.birthDate) {
        try {
          let nDate = moment(this.dataForm.value.birthDate, "DD-MM-YYYY").toISOString()
          new Date(nDate).toISOString()
          if (!nDate) {
            this.dataForm.patchValue({
              birthDate: null
            })
            isValidate = false
            this.validateAllFields(this.dataForm);
            this.snackbar.open('Data inválida .', '', { duration: 4000 });
          }
        }
        catch (err) {
          this.dataForm.patchValue({
            birthDate: null
          })
          this.validateAllFields(this.dataForm);
          isValidate = false
          this.snackbar.open('Data inválida .', '', { duration: 4000 });
        }
      }

    }

    if (isValidate) {
      this.conductorStepCompleted = true;
      //setTimeout(() => { this.stepper.next(); }, 1);
      this.calculate(true)
    }

  }

  getBrokerByUser() {
    this.brokersService.getBrokerByUser().subscribe(brokers => {
      if (brokers && brokers.content) {
        this.broker = brokers.content[0]
      }
    })
  }

  goQuote() {

    this.quoteIsChanged = false;
    this.calcStepCompleted = true
    this.proposalStepCompleted = true
    this.paymentStepCompleted = true

    this.calculate(true)

  }

  calculate(goToNext: boolean = true) {

    this.loadingService.show()

    if (!this.vehicleForm.valid && this.quote.id == null) {
      this.validateAllFields(this.vehicleForm);
      this.loadingService.hide()
      return false;
    }
    else {

      try {

        this.saveQuote().subscribe(result => {

          this.quote =  result 

          this.quote.totalPrice = result.premiumTotalAmount ? result.premiumTotalAmount : 0
          this.quote.premiumTariff = result.premiumTariff ? result.premiumTariff : 0
          this.quote.installments = result.installments ? result.installments : 0
          this.quote.id = result.id

          this.loadingService.hide()

          if (result.name) {
            this.quote.key = result.name
          }
          this.quoteIsChanged = false;
         
          if (goToNext) {
            setTimeout(() => { this.stepper.next(); }, 1);
            var scrollDiv = document.getElementById('contentScroll');
            scrollDiv.scrollTop = 0; 
          }

        }, error => {
          this.getQuote()
          this.loadingService.hide()
          console.log(error)
          this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
        });
      }
      catch (err) {

        // this.loadingService.hide()
        // this.quoteIsChanged = false;
       
        // if (goToNext) {
        //   setTimeout(() => { this.stepper.next(); }, 1);
        // }

      }


    }
  }

  getQuote(){
    this.quoteService.getQuote(this.quote.id).subscribe(result => {
      if (result && result.id) {
        this.quote = result
        this.createControlCoverageForm()
      }
      this.loadingService.hide()
    })
  }

  saveQuote(): Observable<any> {

    if (this.quote.id == null) {

      let usePurpose = null;

      if (this.vehicleAdditionalInformation.value.vehicleUseType) {
        if (parseInt(this.vehicleAdditionalInformation.value.vehicleUseType) == 1 || parseInt(this.vehicleAdditionalInformation.value.vehicleUseType) == 2) {
          usePurpose = '1'
        } else {
          usePurpose = '2'
        }
      }

      let vehicleOwner = false

      if (parseInt(this.dataForm.value.relationshipWithVehicleOwner) < 3) {
        vehicleOwner = true
      }

      let personalData = this.customerPersonalComponent.getCustomerData()

      let quote = {
        "brand": this.appContext.loggedUser.brand,
        "offer": this.dataForm.value.offer ? this.dataForm.value.offer : 5750672456679424,
        "broker": this.broker ? this.broker.id : this.appContext.loggedUser.id,
        "insurer": "USEBENS",
        "status": "QUOTATION",
        "renovationType": this.secureForm.value.renovationType ? this.secureForm.value.renovationType : null,
        "productId": "108",
        "tariffCategory": "10",
        "validityMonths": "12",
        "currentPolicyInsurer": this.secureForm.value.insurerPolicyId ? this.secureForm.value.insurerPolicyId : null,
        "currentPolicyBonusClass": this.secureForm.value.currentPolicyBonusIdClass ? this.secureForm.value.currentPolicyBonusIdClass : null,
        "currentPolicyBonusId": this.secureForm.value.currentPolicyBonusId ? this.secureForm.value.currentPolicyBonusId : null,
        "currentPolicyEndValidity": this.secureForm.value.currentPolicyEndValidity ? this.formatDate(this.secureForm.value.currentPolicyEndValidity) : null,
        "currentPolicyId": this.secureForm.value.currentPolicyId ? this.secureForm.value.currentPolicyId : null,
        "coverages": [{ 'id': '753', 'limit': '0.0' }],
        "customer": {
          "addressPostalCode": this.dataForm.value.postalCode,
          "registrationNumber": personalData.registrationNumber.replace(/\D+/g, ''),
          "addressCity": null,
          "personType": this.dataForm.value.personType ? this.dataForm.value.personType : null,
          "birthdate": this.dataForm.value.birthDate ? this.formatDate(this.dataForm.value.birthDate) : null,
          "gender": this.dataForm.value.gender ? this.dataForm.value.gender == "MALE" ? '1' : '2' : null,
          "maritalStatus": this.dataForm.value.maritialStatus ? this.dataForm.value.maritialStatus : null,
          "vehicleOwner": vehicleOwner,
          "vehicleDriver": this.dataForm.value.vehicleOwner ? this.dataForm.value.vehicleOwner : false,
          "profession": this.occupation ? this.occupation : null,
          "professionName" : this.nOccupation ? this.nOccupation : null,
          "addressComplement": null,
          "nationalIdentity": null,
          "mainActivity": this.mainActivity ? this.mainActivity : null,
          "nationalIdentityExpeditionAgency": null,
          "driverLicense": null,
          "driverLicenseCategory": null,
          "driverLicenseExpiration": null,
          "addressDistrict": null,
          "addressStreet": null,
          "addressNumber": null,
          "email": personalData.email,
          "residencialPhone": this.dataForm.value.residencialPhone,
          "cellphone": personalData.mobilePhone,
          "name": personalData.name,
          "addressState": null,
          "thirdPartyNationalIdentity": this.ownerForm.value.thirdPartyNationalIdentity ? this.ownerForm.value.thirdPartyNationalIdentity.replace(/\D+/g, '') : null,
          "kindShipWithThirdParty": this.dataForm.value.kindShipWithThirdParty ? this.dataForm.value.kindShipWithThirdParty : null,
          "kindshipWithVehicleOwner": this.dataForm.value.kindShipWithThirdParty ? this.dataForm.value.kindShipWithThirdParty : null,
          "relationshipWithVehicleOwner": this.dataForm.value.relationshipWithVehicleOwner ? this.dataForm.value.relationshipWithVehicleOwner : null,
          "age": this.dataForm.value.birthDate ? this.calculateAge(this.dataForm.value.birthDate) : null
        },
        "vehicle": {
          "fipeCode": this.selectedVehicle.fipeCodigo,
          "fipePrice": this.selectedVehicle.preco,
          "registrationYear": this.selectedVehicle.ano,
          "manufacturerName": this.selectedVehicle.marca,
          "manufacturer": this.selectedVehicle.manufacturer,
          "modelName": this.selectedVehicle.name,
          "fuelType": FuelType[this.selectedVehicle.combustivel],
          "useType": this.vehicleAdditionalInformation.value.vehicleUseType ? this.vehicleAdditionalInformation.value.vehicleUseType : null,
          "modificationType": this.vehicleAdditionalInformation.value.vehicleModificationType ? this.vehicleAdditionalInformation.value.vehicleModificationType : null,
          "brandNew": this.vehicleAdditionalInformation.value.brandNew ? this.vehicleAdditionalInformation.value.brandNew : false,
          "garageResidential": this.dataForm.value.garageResidential ? this.dataForm.value.garageResidential : null,
          "garageStudy": this.dataForm.value.garageStudy ? this.dataForm.value.garageStudy : null,
          "garageWork": this.dataForm.value.garageWork ? this.dataForm.value.garageWork : null,
          "model": this.selectedVehicle.model,
          "monthlyUsage": this.dataForm.value.vehicleMonthlyUsage ? this.dataForm.value.vehicleMonthlyUsage : null,
          "modelYear": this.selectedVehicle.anoModelo,
          "vehicleLastYearThefts": this.secureForm.value.vehicleLastYearThefts ? this.secureForm.value.vehicleLastYearThefts : null,
          "vehicleIncidents": this.secureForm.value.vehicleIncidents ? this.secureForm.value.vehicleIncidents : null,
          "licensePlate": this.vehicleDetailsForm.value.licensePlate ? this.vehicleDetailsForm.value.licensePlate : null,
          "chassi": this.vehicleDetailsForm.value.chassi ? this.vehicleDetailsForm.value.chassi : null,
          "renavam": this.vehicleDetailsForm.value.renavam ? this.vehicleDetailsForm.value.renavam : null,
          "pcd": this.vehicleAdditionalInformation.value.vehiclePcd ? this.vehicleAdditionalInformation.value.vehiclePcd : null,
          "detranDocumentation": this.dataForm.value.detranDocumentation ? this.dataForm.value.detranDocumentation : false,
          "crvSigned": this.dataForm.value.crvSigned ? this.dataForm.value.crvSigned : false,
          "boughtDate": this.dataForm.value.boughtDate ? this.formatDate(this.dataForm.value.boughtDate) : null,
          "usePurpose": usePurpose,
          "gnv": this.vehicleAdditionalInformation.value.gnv ? this.vehicleAdditionalInformation.value.gnv : false,
          "financed": this.vehicleDetailsForm.value.financed ? this.vehicleDetailsForm.value.financed : false,
          "mileage": this.vehicleDetailsForm.value.mileage ? this.vehicleDetailsForm.value.mileage : null,
          "armored": this.vehicleAdditionalInformation.value.armored ? this.vehicleAdditionalInformation.value.armored : false,
          "color": this.vehicleDetailsForm.value.color ? this.vehicleDetailsForm.value.color : null,
          "usedUnder25YearsOld": this.dataForm.value.usedUnder25YearsOld ? this.dataForm.value.usedUnder25YearsOld : false
        },
        "driver": {
          "personType": this.dataForm.value.motoristPersonType,
          "name": !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.name : null,
          "nationalIdentity": !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.registrationNumber ? this.driverForm.value.registrationNumber.replace(/\D+/g, '') : personalData.registrationNumber.replace(/\D+/g, '') : null,
          "birthdate": (!this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2') ? this.driverForm.value.birthDate ? this.formatDate(this.driverForm.value.birthDate) : this.formatDate(this.dataForm.value.birthDate) : this.formatDate(this.dataForm.value.birthDate),
          "maritalStatus": !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.maritalStatus : this.dataForm.value.maritialStatus ? this.dataForm.value.maritialStatus : null,
          "gender": !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.gender == "MALE" ? 1 : 2 : this.dataForm.value.gender ? this.dataForm.value.gender == "MALE" ? '1' : '2' : null
        },
        "owner": {
          "name": parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2 ? this.ownerForm.value.name : null,
          "birthdate": parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2 ? this.ownerForm.value.birthdate ? this.formatDate(this.ownerForm.value.birthdate) : null : null,
          "registrationNumber": parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2 ? this.ownerForm.value.thirdPartyNationalIdentity.replace(/\D+/g, '') : null,
          "gender": parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2 ? this.ownerForm.value.gender ? this.ownerForm.value.gender == "MALE" ? '1' : '2' : '1' : null
        }
      }


      let customer = new Customer()
          customer.registrationNumber =  personalData.registrationNumber.replace(/\D+/g, '');
    
      this.customerDocumentsComponent.loadCustomerData(customer)

      return this.quoteService.createQuote(quote)
    } else {
      return this.updateQuote()
    }
  }

  updateQuote() {

    let addressData = this.customerAddressComponent.getData()
    let personalData = this.customerPersonalComponent.getCustomerData()
    let documentsData = this.customerDocumentsComponent.getCustomerData()
    
    let usePurpose = null;

    if (this.vehicleAdditionalInformation.value.vehicleUseType) {
      if (parseInt(this.vehicleAdditionalInformation.value.vehicleUseType) == 1 || parseInt(this.vehicleAdditionalInformation.value.vehicleUseType) == 2) {
        usePurpose = '1'
      } else {
        usePurpose = '2'
      }
    }

    let vehicleOwner = false

    if (parseInt(this.dataForm.value.relationshipWithVehicleOwner) < 3) {
      vehicleOwner = true
    }
    //UPDATE OFFER
    this.quote.offer = this.dataForm.value.offer ? this.dataForm.value.offer : 5750672456679424;

    //UPDATE SECURE
    this.quote.renovationType = this.secureForm.value.renovationType ? this.secureForm.value.renovationType : null;
    this.quote.currentPolicyInsurer = this.secureForm.value.insurerPolicyId ? this.secureForm.value.insurerPolicyId : null;
    this.quote.currentPolicyBonusClass =  this.secureForm.value.currentPolicyBonusIdClass ? this.secureForm.value.currentPolicyBonusIdClass : null;
    this.quote.currentPolicyBonusId =  this.secureForm.value.currentPolicyBonusId ? this.secureForm.value.currentPolicyBonusId : null;
    this.quote.currentPolicyEndValidity =  this.secureForm.value.currentPolicyEndValidity ? this.formatDate(this.secureForm.value.currentPolicyEndValidity) : null;
    this.quote.currentPolicyId =  this.secureForm.value.currentPolicyId ? this.secureForm.value.currentPolicyId : null;
      
    //UPDATE CUSTONER
    this.quote.customer.addressPostalCode = addressData.postalCode ? this.utilsService.stringToNumber(addressData.postalCode) : this.dataForm.value.postalCode
    this.quote.customer.registrationNumber =  personalData.registrationNumber.replace(/\D+/g, '')
    this.quote.customer.addressCity =  addressData.city ? addressData.city : null
    this.quote.customer.personType =  this.dataForm.value.personType ? this.dataForm.value.personType : null
    this.quote.customer.birthdate =  this.dataForm.value.birthDate ? this.formatDate(this.dataForm.value.birthDate) : null
    this.quote.customer.gender =  this.dataForm.value.gender ? this.dataForm.value.gender == "MALE" ? '1' : '2' : null
    this.quote.customer.maritalStatus =  this.dataForm.value.maritialStatus ? this.dataForm.value.maritialStatus : null
    this.quote.customer.vehicleOwner =  vehicleOwner
    this.quote.customer.vehicleDriver =  this.dataForm.value.vehicleOwner ? this.dataForm.value.vehicleOwner : false
    this.quote.customer.profession =  this.occupation ? this.occupation : null
    this.quote.customer.professionName =  this.nOccupation ? this.nOccupation : null
    this.quote.customer.addressComplement =  addressData.complement ? addressData.complement : null
    this.quote.customer.nationalIdentity =  documentsData.nationalIdentity ? documentsData.nationalIdentity : null
    this.quote.customer.nationalIdentityExpeditionAgency =  documentsData.nationalIdentityExpeditionAgency ? documentsData.nationalIdentityExpeditionAgency : null
    this.quote.customer.driverLicense =  documentsData.driverLicense ? documentsData.driverLicense : null
    this.quote.customer.driverLicenseCategory =  documentsData.driverLicenseCategory ? documentsData.driverLicenseCategory : null
    this.quote.customer.driverLicenseExpiration =  documentsData.nationalIdentityExpeditionDate ? this.formatDate(documentsData.nationalIdentityExpeditionDate) : null
    this.quote.customer.addressDistrict =  addressData.district ? addressData.district : null
    this.quote.customer.addressStreet =  addressData.street ? addressData.street : null
    this.quote.customer.addressNumber =  addressData.number ? addressData.number : null
    this.quote.customer.email =  personalData.email ? personalData.email : null
    this.quote.customer.residencialPhone =  this.dataForm.value.residencialPhone.replace(/\D+/g, '') || null
    this.quote.customer.commercialPhone =   null
    this.quote.customer.cellphone = personalData.mobilePhone ? personalData.mobilePhone.replace(/\D+/g, '') : null,
    this.quote.customer.name =  personalData.name ? personalData.name : null
    this.quote.customer.mainActivity =  this.mainActivity ? this.mainActivity : null
    this.quote.customer.addressState =  addressData.state ? AddressStateUsebens[addressData.state] : null
    this.quote.customer.thirdPartyNationalIdentity =  this.ownerForm.value.thirdPartyNationalIdentity ? this.ownerForm.value.thirdPartyNationalIdentity.replace(/\D+/g, '') : null
    this.quote.customer.relationshipWithVehicleOwner =  this.dataForm.value.relationshipWithVehicleOwner ? this.dataForm.value.relationshipWithVehicleOwner : null
    this.quote.customer.age =  this.dataForm.value.birthDate ? this.calculateAge(this.dataForm.value.birthDate) : null
    this.quote.customer.kindShipWithThirdParty =  this.dataForm.value.kindShipWithThirdParty ? this.dataForm.value.kindShipWithThirdParty : null
    this.quote.customer.kindshipWithVehicleOwner =  this.dataForm.value.kindShipWithThirdParty ? this.dataForm.value.kindShipWithThirdParty : null
    this.quote.customer.politicallyExposedPerson =  this.vehicleDetailsForm.value.politicallyExposedPerson ? this.vehicleDetailsForm.value.politicallyExposedPerson : null
    this.quote.customer.relationshipWithPoliticallyExposedPerson =  this.vehicleDetailsForm.value.politicallyExposedPerson ? this.vehicleDetailsForm.value.politicallyExposedPerson : null
    

    //UPDATE VEHICLE
    this.quote.vehicle.fipeCode =  this.selectedVehicle.fipeCodigo
    this.quote.vehicle.fipePrice =  this.selectedVehicle.preco
    this.quote.vehicle.registrationYear =  this.selectedVehicle.ano
    this.quote.vehicle.manufacturerName =  this.selectedVehicle.marca
    this.quote.vehicle.manufacturer =   this.selectedVehicle.manufacturer
    this.quote.vehicle.modelName =   this.selectedVehicle.name
    this.quote.vehicle.fuelType =   FuelType[this.selectedVehicle.combustivel]
    this.quote.vehicle.usePurpose =   usePurpose
    this.quote.vehicle.useType =   this.vehicleAdditionalInformation.value.vehicleUseType ? this.vehicleAdditionalInformation.value.vehicleUseType : null
    this.quote.vehicle.modificationType =   this.vehicleAdditionalInformation.value.vehicleModificationType ? this.vehicleAdditionalInformation.value.vehicleModificationType : null
    this.quote.vehicle.brandNew =   this.vehicleAdditionalInformation.value.brandNew ? this.vehicleAdditionalInformation.value.brandNew : false
    this.quote.vehicle.garageResidential =   this.dataForm.value.garageResidential ? this.dataForm.value.garageResidential : null
    this.quote.vehicle.garageStudy =   this.dataForm.value.garageStudy ? this.dataForm.value.garageStudy : null
    this.quote.vehicle.garageWork =   this.dataForm.value.garageWork ? this.dataForm.value.garageWork : null
    this.quote.vehicle.model =   this.selectedVehicle.model
    this.quote.vehicle.monthlyUsage =   this.dataForm.value.vehicleMonthlyUsage ? this.dataForm.value.vehicleMonthlyUsage : null
    this.quote.vehicle.modelYear =   this.selectedVehicle.anoModelo
    this.quote.vehicle.vehicleLastYearThefts =   this.secureForm.value.vehicleLastYearThefts ? this.secureForm.value.vehicleLastYearThefts : null
    this.quote.vehicle.vehicleIncidents =   this.secureForm.value.vehicleIncidents ? this.secureForm.value.vehicleIncidents : null
    this.quote.vehicle.licensePlate =   this.vehicleDetailsForm.value.licensePlate ? this.vehicleDetailsForm.value.licensePlate : null
    this.quote.vehicle.chassi =   this.vehicleDetailsForm.value.chassi ? this.vehicleDetailsForm.value.chassi : null
    this.quote.vehicle.renavam =   this.vehicleDetailsForm.value.renavam ? this.vehicleDetailsForm.value.renavam : null
    this.quote.vehicle.pcd =   this.vehicleAdditionalInformation.value.vehiclePcd ? this.vehicleAdditionalInformation.value.vehiclePcd : null
    this.quote.vehicle.detranDocumentation =   this.dataForm.value.detranDocumentation ? this.dataForm.value.detranDocumentation : false
    this.quote.vehicle.crvSigned =   this.dataForm.value.crvSigned ? this.dataForm.value.crvSigned : false
    this.quote.vehicle.boughtDate =   this.dataForm.value.boughtDate ? this.formatDate(this.dataForm.value.boughtDate) : null
    this.quote.vehicle.gnv =   this.vehicleAdditionalInformation.value.gnv ? this.vehicleAdditionalInformation.value.gnv : false
    this.quote.vehicle.financed =   this.vehicleDetailsForm.value.financed ? this.vehicleDetailsForm.value.financed : false
    this.quote.vehicle.mileage =   this.vehicleDetailsForm.value.mileage ? this.vehicleDetailsForm.value.mileage : null
    this.quote.vehicle.armored =   this.vehicleAdditionalInformation.value.armored ? this.vehicleAdditionalInformation.value.armored : false
    this.quote.vehicle.color =   this.vehicleDetailsForm.value.color ? this.vehicleDetailsForm.value.color : null
    this.quote.vehicle.usedUnder25YearsOld =   this.dataForm.value.usedUnder25YearsOld ? this.dataForm.value.usedUnder25YearsOld : false
    
    //UPDATE OWNER
    this.quote.owner.name =  parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2 ? this.ownerForm.value.name : personalData.name ? personalData.name : null
    this.quote.owner.birthdate =  parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2 ? this.ownerForm.value.birthdate ? this.formatDate(this.ownerForm.value.birthdate) : null : this.dataForm.value.birthDate ? this.formatDate(this.dataForm.value.birthDate) : null
    this.quote.owner.registrationNumber =  parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2 ? this.ownerForm.value.thirdPartyNationalIdentity.replace(/\D+/g, '') :  personalData.registrationNumber.replace(/\D+/g, '')
    this.quote.owner.gender =  parseInt(this.dataForm.value.relationshipWithVehicleOwner) > 2 ? this.ownerForm.value.gender ? this.ownerForm.value.gender == "MALE" ? '1' : '2' : '1' :  this.dataForm.value.gender ? this.dataForm.value.gender == "MALE" ? '1' : '2' : null
    
    //UPDATE DRIVER
    this.quote.driver.personType =  !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.dataForm.value.motoristPersonType : this.dataForm.value.personType ? this.dataForm.value.personType : null
    this.quote.driver.name =  !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.name : personalData.name ? personalData.name : null
    this.quote.driver.nationalIdentity =  !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.registrationNumber ? this.driverForm.value.registrationNumber.replace(/\D+/g, '') :  personalData.registrationNumber.replace(/\D+/g, '')  : null
    this.quote.driver.birthdate =  !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.birthDate ? this.formatDate(this.driverForm.value.birthDate) :  this.formatDate(this.dataForm.value.birthDate) : this.formatDate(this.dataForm.value.birthDate)
    this.quote.driver.maritalStatus =  !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.maritalStatus : this.dataForm.value.maritialStatus 
    this.quote.driver.gender =  !this.dataForm.value.vehicleOwner || this.dataForm.value.personType == '2' ? this.driverForm.value.gender == "MALE" ? 1 : 2 :  this.dataForm.value.gender ? this.dataForm.value.gender == "MALE" ? '1' : '2' : null
    

    
     //UPDATE PAYMENT
    if (this.creditCard) {

      this.quote.installments = this.creditCard.installments      
      this.quote.payment.type =  this.creditCard.type
      this.quote.payment.creditCardBrand =  this.creditCard.creditCardBrand ? this.creditCard.creditCardBrand : null
      this.quote.payment.creditCardHolderName = this.creditCard.creditCardHolderName ? this.creditCard.creditCardHolderName : null
      this.quote.payment.creditCardNumber =  this.creditCard.creditCardNumber ? this.creditCard.creditCardNumber : null
      this.quote.payment.creditCardExpMonth =  this.creditCard.creditCardExpMonth ? this.creditCard.creditCardExpMonth : null
      this.quote.payment.creditCardExpYear =  this.creditCard.creditCardExpYear ? this.creditCard.creditCardExpYear : null
      this.quote.payment.creditCardSecurityCode =  this.creditCard.creditCardSecurityCode ? this.creditCard.creditCardSecurityCode : null
      this.quote.payment.creditCardPreAuthorizationTransaction = this.quote.payment && this.quote.payment.creditCardPreAuthorizationTransaction ? this.quote.payment.creditCardPreAuthorizationTransaction : null
      this.quote.payment.debitCardBank =  this.creditCard.debitCardBank ? this.creditCard.debitCardBank : null
      this.quote.payment.debitCardBrand =  this.creditCard.debitCardBrand ? this.creditCard.debitCardBrand : null
      this.quote.payment.debitCardHolderName =  this.creditCard.debitCardHolderName ? this.creditCard.debitCardHolderName : null
      this.quote.payment.debitCardNumber =  this.creditCard.debitCardNumber ? this.creditCard.debitCardNumber : null
      this.quote.payment.debitCardSecurityCode =  this.creditCard.debitCardSecurityCode ? this.creditCard.debitCardSecurityCode : null
      this.quote.payment.debitCardExpYear =  this.creditCard.debitCardExpYear ? this.creditCard.debitCardExpYear : null
      this.quote.payment.debitCardExpMonth =  this.creditCard.debitCardExpMonth ? this.creditCard.debitCardExpMonth : null

    }
    
    return this.quoteService.updateQuote(this.quote.id, this.quote)
  }

  calculateAge(birthday) {

    try {
      var ageDifMs = Date.now() - new Date(birthday).getTime();
      var ageDate = new Date(ageDifMs);
      return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    catch (err) {
      this.snackbar.open('Data inválida .', '', { duration: 4000 });
    }

  }

  formatDate(_date) {

    let date = moment(_date, "DD-MM-YYYY").toISOString()

    try {
      let nDate = new Date(date).toISOString()
      return nDate.replace('.000Z', '')
    }
    catch (err) {

      this.snackbar.open('Data inválida .', '', { duration: 4000 });
    }
  }

  loadData() {

    this.isLoading = true

    this.changeManufacture('CAR', this.quote.vehicle)

    this.vehicleDetailsForm.patchValue({
      licensePlate: this.quote.vehicle.licensePlate || null,
      chassi: this.quote.vehicle.chassi || null,
      renavam: this.quote.vehicle.renavam || null,
      color: this.quote.vehicle.color || null,
      mileage: this.quote.vehicle.mileage || null,
      financed: this.quote.vehicle.financed ? this.quote.vehicle.financed : false,
      politicallyExposedPerson: this.quote.customer.politicallyExposedPerson || null,
      relationshipWithPoliticallyExposedPerson: this.quote.customer.politicallyExposedPerson || null
    })

    this.vehicleAdditionalInformation = this.formBuilder.group({
      vehiclePcd: this.quote.vehicle.pcd || null,
      vehicleUseType: this.quote.vehicle.useType || null,
      vehicleModificationType: this.quote.vehicle.modificationType || null,
      armored: this.quote.vehicle.armored ? this.quote.vehicle.armored : false,
      gnv: this.quote.vehicle.gnv ? this.quote.vehicle.gnv : false,
      brandNew: this.quote.vehicle.brandNew ? this.quote.vehicle.brandNew : false,
    })


    this.dataForm.patchValue({
      profession: this.nOccupation,
      motoristPersonType: this.quote.driver.personType || null,
      vehicleMonthlyUsage: this.quote.vehicle.monthlyUsage || null,
      vehicleOwner: this.quote.customer.vehicleOwner,
      garageResidential: this.quote.vehicle.garageResidential || null,
      garageWork: this.quote.vehicle.garageWork || null,
      garageStudy: this.quote.vehicle.garageStudy || null,
      usedUnder25YearsOld: this.quote.vehicle.usedUnder25YearsOld ? this.quote.vehicle.usedUnder25YearsOld : false,
      relationshipWithVehicleOwner: this.quote.customer.relationshipWithVehicleOwner || null,
      postalCode: this.quote.customer.addressPostalCode || null,
      personType: this.quote.customer.personType || null,
      birthDate: this.quote.customer.birthdate ? this.convertLoadDate(this.quote.customer.birthdate) : this.convertLoadDate(this.quote.driver.birthdate),
      maritialStatus: this.quote.customer.maritalStatus || null,
      gender: this.quote.customer.gender == '1' ? 'MALE' : 'FEMALE' || null,
      boughtDate: this.quote.vehicle.boughtDate ? this.convertLoadDate(this.quote.vehicle.boughtDate) : null,
      crvSigned: this.quote.vehicle.crvSigned ? this.quote.vehicle.crvSigned : false,
      detranDocumentation: this.quote.vehicle.detranDocumentation || false,
      kindShipWithThirdParty: this.quote.customer.kindShipWithThirdParty || null,
      kindshipWithVehicleOwner: this.quote.customer.kindshipWithVehicleOwner || null,
      residencialPhone : this.quote.customer.residencialPhone || null,
      mainActivity: this.nMainActivity
    })

    if (this.nOccupation) {
      this.loadOccupations()
    }

    if (this.nMainActivity) {
      this.loadActivities()
    }

    this.driverForm.patchValue({
      name: this.quote.driver.name || null,
      registrationNumber: this.quote.driver.nationalIdentity || null,
      birthDate: this.convertLoadDate(this.quote.driver.birthdate) || null,
      maritalStatus: this.quote.driver.maritalStatus || null,
      gender: this.quote.driver.gender == '1' ? 'MALE' : 'FEMALE'
    })

    this.secureForm.patchValue({
      renovationType: this.quote.renovationType || null,
      vehicleLastYearThefts: this.quote.vehicle.vehicleLastYearThefts || null,
      vehicleIncidents: this.quote.vehicle.vehicleIncidents || null,
      currentPolicyBonusId: this.quote.currentPolicyBonusId || null,
      currentPolicyBonusIdClass: this.quote.currentPolicyBonusId || null,
      insurerPolicyId: this.quote.currentPolicyInsurer || null,
      currentPolicyEndValidity: this.quote.currentPolicyEndValidity ? this.convertLoadDate(this.quote.currentPolicyEndValidity) : null,
      currentPolicyId: this.quote.currentPolicyId || null
    })

    this.ownerForm.patchValue({
      name: this.quote &&  this.quote.owner ? this.quote.owner.name : null,
      thirdPartyNationalIdentity: this.quote &&  this.quote.owner ? this.quote.owner.registrationNumber : null,
      birthdate:this.quote &&  this.quote.owner ?  this.convertLoadDate(this.quote.owner.birthdate) : null,
      gender: this.quote &&  this.quote.owner ? this.quote.owner.gender == '1' ? 'MALE' : 'FEMALE' : null
    })

    let customer = new Customer() 
    customer.registrationNumber = this.quote.customer.registrationNumber || null;
    customer.personType = this.quote.customer.personType == '1' ? 'PERSON' : 'COMPANY';
    //customer.birthDate = this.quote.customer.birthdate ? this.quote.customer.birthdate : this.quote.driver.birthdate;
    //customer.gender = this.quote.customer.gender == '1' ? 'MALE' : 'FEMALE' || null;
    //customer.maritialStatus = this.loadMaritalStatusUsebens(this.quote.customer.maritalStatus) || null;
    customer.brand = this.quote.brand || null;
    customer.name = this.quote.customer.name || null;
    customer.email = this.quote.customer.email || null;
    customer.mobilePhone = this.quote.customer.cellphone || null;
    //customer.residencialPhone = this.quote.customer.residencialPhone || null;
    //customer.occupation = this.nOccupation


    this.customerPersonalComponent.setCustomer(customer)

    let document = new Customer()
        document.registrationNumber = this.quote.customer.registrationNumber || null;
        document.nationalIdentity = this.quote.customer.nationalIdentity || null;
        document.nationalIdentityExpeditionDate = this.quote.customer.driverLicenseExpiration || null;
        document.nationalIdentityExpeditionAgency = this.quote.customer.nationalIdentityExpeditionAgency || null;
        document.stateRegistration = this.quote.customer.stateRegistration || null;
        document.driverLicense = this.quote.customer.driverLicense || null;
        document.driverLicenseCategory = this.quote.customer.driverLicenseCategory || null

    this.customerDocumentsComponent.loadCustomerData(document)

    let address = new Address()
    address.street = this.quote.customer.addressStreet || null;
    address.number = this.quote.customer.addressNumber || null;
    address.complement = this.quote.customer.addressComplement || null;
    address.district = this.quote.customer.addressDistrict || null;
    address.city = this.quote.customer.addressCity || null;
    address.state = this.changeAddressState(this.quote.customer.addressState) || null;
    address.postalCode = this.quote.customer.addressPostalCode || null;
    address.reference = this.quote.customer.addressReference || null;

    if (this.quote.customer.name != 'USER TRACKNME') {
      this.customerAddressComponent.loadData(address)
    }

    this.coveragesForm.patchValue({
      rouboFurtoCoverage: true
    })

    this.quote.totalPrice = this.quote.premiumTotalAmount

    if (this.quote.payment) {
      this.creditCard = this.quote.payment
      this.creditCard.installments = this.quote.installments
      this.quotePaymentComponent.loadPayment(this.creditCard)

      this.quoteIsChanged = false;
    
      this.calcStepCompleted = true
      this.proposalStepCompleted = true
      this.paymentStepCompleted = true

      if (this.creditCard.type == 'CREDIT_CARD') {
        this.calculateInstalment(12, this.quote.installments)
        this.calculateCreditCardInstalment(this.quote.installments)
      } else {
        this.calculateInstalment(this.quote.installments, this.quote.installments)
      }
    }

    this.configByQuoteStatus()
    this.loadCoverages()
  }

  calculateInstalment(installment, parcel) {
    let installments = installment
    let sumPrice = (this.quote.totalPrice + 360)
    let installmentsInstallation = parcel

    this.installmentsList = [];
    for (var i = 0; i < installments; i++) {
        this.installmentsList.push({ 'number': i + 1, 'value': (sumPrice / (installments)).toFixed(2) })
    }
   
  }

  calculateCreditCardInstalment(parcel) {
    this.creditCardInstallmentsList = [];
    for (var i = 0; i < parcel; i++) {
        this.creditCardInstallmentsList.push({ 'number': i + 1, 'value': ( 300 / parcel).toFixed(2) })
    }
  }

  

  convertLoadDate(date) {
    if (date) {
      let _date = new Date(date)
      let day = _date.getDate()
      let month = _date.getMonth() + 1
      return (day < 10 ? '0' + day : day) + '/' + (month < 10 ? '0' + month : month) + '/' + _date.getFullYear()
    }
  }

  configByQuoteStatus() {
    if (this.quote.status == "QUOTATION") {
      this.calcStepCompleted = true
      this.proposalStepCompleted = true
      this.perfilStepCompleted = true

      this.quoteIsChanged = false;
      this.conductorStepCompleted = true
      this.perfilStepCompleted = true
      this.calcStepCompleted = true
      this.proposalStepCompleted = true
      this.paymentStepCompleted = true
      this.vehicleForm.disable()
    }

  }

  contractInsurance() {

    this.calcStepCompleted = true;
    setTimeout(() => {
      this.stepper.next();
    }, 1);
  }

  proposalComplete() {

    let documents = this.customerDocumentsComponent.getCustomerData()

    if(this.vehicleDetailsForm.value.chassi && this.vehicleDetailsForm.value.chassi.length < 17){
      this.snackbar.open('Chassi inválido, insira os 17 caracteres.', '', { duration: 6000 });
      return;
    }
    
    if(this.vehicleDetailsForm.value.renavam && this.vehicleDetailsForm.value.renavam.length < 11){
      this.snackbar.open('Renavam inválido, insira os 11 caracteres.', '', { duration: 6000 });
      return;
    }

    if (this.dataForm.value.personType == '2') {

      if (this.customerAddressComponent.isInvalid()) {
        this.validateAllFields(this.customerAddressComponent.getCustomerForm())
        this.snackbar.open('Todos os campos do endereço precisam ser preenchidos.', '', { duration: 6000 });
        return;
      }

      if (!this.vehicleDetailsForm.value.licensePlate || !this.vehicleDetailsForm.value.chassi  || !this.vehicleDetailsForm.value.renavam  || !this.vehicleDetailsForm.value.color || !this.vehicleDetailsForm.value.mileage) {
        this.validateAllFields(this.vehicleDetailsForm)
        this.snackbar.open('Todos os dados da empresa precisam ser preenchidos.', '', { duration: 6000 });
        return;
      }


    } else {

      if ( this.customerDocumentsComponent.isInvalid()
        || this.customerAddressComponent.isInvalid()
      ) {

        this.validateAllFields(this.customerPersonalComponent.getCustomerForm())
        this.validateAllFields(this.customerDocumentsComponent.getCustomerForm())
        this.validateAllFields(this.customerAddressComponent.getCustomerForm())

        this.snackbar.open('Todos os campos precisam ser preenchidos.', '', { duration: 6000 });

        return;
      }


      try {
        let nDate = moment(documents.nationalIdentityExpeditionDate, "DD-MM-YYYY").toISOString()
        new Date(nDate).toISOString()

        if (!nDate) {
          this.snackbar.open('Data de expedição inválida .', '', { duration: 4000 });
          return
        }
      }
      catch (err) {
        this.snackbar.open('Data de expedição inválida .', '', { duration: 4000 });
        return
      }

      if (!this.vehicleDetailsForm.value.licensePlate || !this.vehicleDetailsForm.value.chassi  || !this.vehicleDetailsForm.value.renavam ||   !this.vehicleDetailsForm.value.color || !this.vehicleDetailsForm.value.mileage || !this.vehicleDetailsForm.value.politicallyExposedPerson || !this.vehicleDetailsForm.value.relationshipWithPoliticallyExposedPerson) {
        this.validateAllFields(this.vehicleDetailsForm)
        this.snackbar.open('Todos os dados da empresa precisam ser preenchidos.', '', { duration: 6000 });
        return;
      }
    }

    this.loadingService.show()

    this.saveQuote().subscribe(result => {
      this.loadingService.hide()
      this.proposalStepCompleted = true;
      setTimeout(() => { this.stepper.next(); }, 1);
    }, error => {
      this.loadingService.hide()
      console.log(error)
      this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
    });

  }

  morteLMI: number = 0
  onMorteCoverageChange(event: any) {

    let lmiValue: number = event.value;

    switch (lmiValue) {
      case 1: this.morteLMI = 3000;
        break;
      case 2: this.morteLMI = 5000;
        break;
      case 3: this.morteLMI = 7000;
        break;
      case 4: this.morteLMI = 10000;
        break;
      default: this.morteLMI = 0;
        break;
    }

    let coverage = { id: 'morteCoverageLMI', value: lmiValue }
    let coverageIdx = this.quote.coverages.findIndex(c => c.id === 'morteCoverageLMI')
    if (coverageIdx > -1) {
      this.quote.coverages[coverageIdx] = coverage
    } else {
      this.quote.coverages.push(coverage)
    }
  }

  invalidezLMI: number = 0
  onInvalidezCoverageChange(event: any) {

    let lmiValue: number = event.value;

    switch (lmiValue) {
      case 1: this.invalidezLMI = 3000;
        break;
      case 2: this.invalidezLMI = 5000;
        break;
      case 3: this.invalidezLMI = 7000;
        break;
      case 4: this.invalidezLMI = 10000;
        break;
      default: this.invalidezLMI = 0;
        break;
    }

    let coverage = { id: 'invalidezCoverageLMI', value: lmiValue }
    let coverageIdx = this.quote.coverages.findIndex(c => c.id === 'invalidezCoverageLMI')
    if (coverageIdx > -1) {
      this.quote.coverages[coverageIdx] = coverage
    } else {
      this.quote.coverages.push(coverage)
    }
  }

  showInstallments() {
    this.dialog.open(QuoteInstallmentsComponent, { data: this.quote,width: '900px',
    height: '800px'})
  }

  onCoverageChangeDefault() {
    this.coveragesForm.patchValue({
      rouboFurtoCoverage: true
    })
  }

  onPaymentTypeChanged(creditCard) {

    if (creditCard.type == 'CREDIT_CARD') {
      this.calculateInstalment(12, creditCard.installments)
      this.calculateCreditCardInstalment(creditCard.installments)
    } else {
      this.calculateInstalment(creditCard.installments, creditCard.installments)
    }

    this.creditCard = creditCard

    //if(!this.paymentStepCompleted){

    this.loadingService.show()
    this.loadCoverages()
    this.saveQuote().subscribe(result => {
      this.paymentStepCompleted = true;
     
      this.quote = result;
      this.quote.totalPrice = result.premiumTotalAmount ? result.premiumTotalAmount : 0
      this.quote.premiumTariff = result.premiumTariff ? result.premiumTariff : 0
      this.quote.installments = result.installments ? result.installments : 0
      this.quote.id = result.id

      this.loadingService.hide()

      if (result.name) {
        this.quote.key = result.name
      }
      setTimeout(() => { this.stepper.next(); }, 1);
      this.loadingService.hide()
    }, error => {
      this.loadingService.hide()
      console.log(error)
      this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
    });

    // }else{
    //   setTimeout(() => { this.stepper.next(); }, 1);
    // }

  }

  loadCoverages(){

    this.coveragesSelectedList = [];

    this.coveragesList.forEach(element => {
      let idx = this.quote.coverages.findIndex(c => c.id === element.idProdutoCobertura)
      
      if (idx != -1) {
        this.coveragesSelectedList.push(element)
      }
    });
  }

  sendProposal() {

    this.loadingService.show()

    this.updateQuote()

    this.quoteService.createPropose(this.quote.id)
      .subscribe(result => {
        this.paymentStepCompleted = true;
        this.configByQuoteStatus()
        this.router.navigate(['/insurance/proposal/'])
        this.loadingService.show()
        this.quote.status = "PROPOSAL"
        this.quote.sentDate = new Date()
        this.loadingService.hide()
      }, error => {
        this.getQuote()
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
      })
  }

  onCoverageChange(event: MatSlideToggleChange, coverageId: string, coverageLimit: string) {

    //let coverageValue = parseFloat(this.coveragesForm.value[nmCoverage])
   
    // if( coverageValue == 0){
    //   let coverageField = [nmCoverage]+'_slide'
    //   this.coveragesForm.patchValue({
    //     [coverageField]: false
    //   })
    //   this.snackbar.open('Insira o valor da cobertura '+nmCoverage,'', { duration: 8000 });
    //   return 
    // }else if(coverageValue > parseFloat(coverageLimit)){
    //   this.snackbar.open('o valor da cobertura é maior que o valor limite','', { duration: 8000 });
    //   return 
    // }

    this.quoteIsChanged = true

    let coverage = { id: coverageId, limit: coverageLimit }

    if(!this.quote.coverages){
      this.quote.coverages =[]
    }

    if (event.checked) {
      let idx = this.quote.coverages.findIndex(c => c.id === coverageId)
      if (idx == -1) {
        this.quote.coverages.push(coverage);
      }
    } else {
      let idx = this.quote.coverages.findIndex(c => c.id === coverageId)
      if (idx != -1) {
        this.quote.coverages.splice(idx, 1)
      }
    }

  }

  convertDate(date) {
    try {
      if (date) {
        let _date = date.split('/');
        _date = _date[1] + '-' + _date[0] + '-' + _date[2]
        return new Date(_date)
      }
    }
    catch (err) {
      this.snackbar.open('Data inválida .', '', { duration: 4000 });
    }
  }

  paymentTypeEnumToString(value): string {
    return PaymentType[value]
  }

  quoteStatusAsString(value): string {
    switch (value) {
      case 'QUOTATION':
        return 'Cotação'
      case 'PROPOSAL':
        return 'Proposta'
      case 'PROPOSAL_SENT':
        return 'Proposta Enviada'
      case 'PROPOSAL_ACCEPTED':
        return 'Proposta Efetivada'
      case 'CANCELLED':
        return 'Cancelado.'
      case 'REJECTED':
        return 'Rejeitado pela seguradora.'
      default:
        return ''
    }
  }

  loadMaritalStatusUsebens(value): string {

    switch (value) {
      case '1':
        return 'SINGLE'
      case '2':
        return 'MARRIED'
      case '4':
        return 'DIVORCED'
      case '5':
        return 'WIDOWED'
      case '6':
        return 'SEPARATED'
      default:
        return ''
    }

  }

  quotePrintDialog() {

    this.dialog
      .open(QuotePrintComponent, {
        width: '900px',
        height: '900px',
        data: this.quote
      })
      .afterClosed()
      .subscribe()
  }

  cancelQuote() {

    this.dialog.open(ConfirmDialogComponent,
      {
      panelClass: 'event-form-dialog',
        data: {
          width: '600px',
          height: '300px',
          title: 'Confirmação',
          message: 'Deseja cancelar essa cotação?'
        }
      })

      .afterClosed()
      .subscribe(response => {
        if (response.data.confirm) {
          this.loadingService.show()
          this.quoteService.cancelPropose(this.quote.id)
            .subscribe(quote => {
              this.router.navigate(['/insurance/quotes/'])
              this.loadingService.hide()
              this.snackbar.open('Cotação cancelada com sucesso', '', {
                duration: 5000
              })
            })
        }

      });

  }

  changeAddressState(id): string {

    switch (id) {
      case '59':
        return "AC";
      case '8':
        return "AL"
      case '61':
        return "AP"
      case '60':
        return "AM";
      case '13':
        return "BA"
      case '62':
        return "CE"
      case '15':
        return "DF";
      case '21':
        return "ES"
      case '22':
        return "GO"
      case '4':
        return "MA";
      case '24':
        return "MT"
      case '3':
        return "MS"
      case '17':
        return "MG";
      case '20':
        return "PA"
      case '18':
        return "PB"
      case '63':
        return "PR";
      case '14':
        return "PE"
      case '12':
        return "PI"
      case '2':
        return "RJ";
      case '64':
        return "RN"
      case '23':
        return "RS"
      case '6':
        return "RO";
      case '26':
        return "RR"
      case '11':
        return "SC"
      case '1':
        return "SP";
      case '9':
        return "SE"
      case '15':
        return "TO"
      default:
        return ''
    }
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.optionsModel.filter(option => option['name'] ? option['name'].toLowerCase().indexOf(filterValue) === 0 : '');
  }

  private _filterOccupation(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.occupationsList.filter(option => option['nmProfissao'] ? option['nmProfissao'].toLowerCase().indexOf(filterValue) === 0 : '');
  }

  private _filterMainActivity(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.mainActivityList.filter(option => option['nmRamoAtividade'] ? option['nmRamoAtividade'].toLowerCase().indexOf(filterValue) === 0 : '');
  }

  private _filterOccupationById(value: string): string[] {

    let ocupations = this.occupationsList.filter(option => option['cdProfissao'] ? option['cdProfissao'].indexOf(value) === 0 : '');

    if (ocupations && ocupations.length > 1) {
      let _ocupation = []
      ocupations.forEach(ocupation => {
        if (ocupation['cdProfissao'] === value) {
          _ocupation = [ocupation]
        }
      });
      return _ocupation
    }

    return ocupations

  }

  private _filterMainActivityById(value: string): string[] {

    let mainActivities = this.mainActivityList.filter(option => option['idRamoAtividade'] ? option['idRamoAtividade'].indexOf(value) === 0 : '');

    if (mainActivities && mainActivities.length > 1) {
      let _mainActivitie = []
      mainActivities.forEach(mainActivitie => {
        if (mainActivitie['idRamoAtividade'] === value) {
          _mainActivitie = [mainActivitie]
        }
      });
      return _mainActivitie
    }

    return mainActivities

  }

}
