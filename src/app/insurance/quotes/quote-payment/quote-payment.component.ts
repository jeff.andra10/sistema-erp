import { Component, OnInit, Output, EventEmitter, Injector, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaymentDebitCardBank } from '../../../core/models/payment.model';
import { MaskUtils } from 'app/core/mask-utils';
import { BaseComponent } from 'app/_base/base-component.component';
import { MatSnackBar} from '@angular/material';

@Component({
  selector: 'tnm-quote-payment',
  templateUrl: './quote-payment.component.html',
  styleUrls: ['./quote-payment.component.scss']
})
export class QuotePaymentComponent extends BaseComponent implements OnInit {

  @Output() onPaymentTypeChanged: EventEmitter<any> = new EventEmitter();

  @Input()
  paymentsTypeAvailable = ['CREDIT_CARD', 'DEBIT_CARD', 'BOOKLET', 'EMAIL']

  creditCardForm: FormGroup
  brandCreditCardList : Array<any>
  isHiddenControls : boolean = false
  listBank  = ['BRADESCO','BANCO DO BRASIL','SANTANDER','ITAÚ','CITIBANK','BRB','CAIXA ECONÔMICA','BANCO OB'];
  installmentsList = []
  installmentsListCreditCard = []

  expMonthMask = MaskUtils.expMonthMask
  expYearMask  = MaskUtils.expYearMask
  cardMask     = [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]
  ccsMask      = [ /\d/ , /\d/, /\d/]  

  constructor(private injector: Injector,
              private snackbar: MatSnackBar,
              private formBuilder: FormBuilder) { 
      super(injector)
  }

  ngOnInit() {

    this.creditCardForm = this.formBuilder.group({
      type: this.formBuilder.control(''),
      installments: this.formBuilder.control(''),
      debitCardBank: this.formBuilder.control(''),
      creditCardBrand: this.formBuilder.control('', [Validators.required]),
      creditCardHolderName: this.formBuilder.control('', [Validators.required]),
      creditCardNumber: this.formBuilder.control('', [Validators.required]),
      creditCardExpMonth: this.formBuilder.control('', []),
      creditCardExpYear: this.formBuilder.control('', [Validators.required]),
      creditCardSecurityCode: this.formBuilder.control('', [Validators.required])
    })

    this.brandCreditCardList = [
      { id: 'Visa', name: 'Visa' },
      { id: 'Mastercard', name: 'Mastercard' },
      { id: 'Amex', name: 'American Express' },
      { id: 'Diners', name: 'Diners Club' },
      { id: 'Elo', name: 'Elo' },
      { id: 'Hipercard', name: 'Hipercard' }
    ]

    this.createInstallmentList(4)
    this.createInstallmentListCreditCard(6)
  }

  public loadPayment(payment){

    this.creditCardForm.patchValue({
      type: payment.type ? payment.type : null ,
      installments: payment.installments ? payment.installments : null,
      debitCardBank: payment.debitCardBank ? payment.debitCardBank : null ,
      creditCardBrand: payment.creditCardBrand ? payment.creditCardBrand  : payment.debitCardBrand ? payment.debitCardBrand: null ,
      creditCardHolderName:  payment.creditCardHolderName ? payment.creditCardHolderName : payment.debitCardHolderName ? payment.debitCardHolderName: null ,
      creditCardNumber:  payment.creditCardNumber ? payment.creditCardNumber : payment.debitCardNumber ? payment.debitCardNumber : null ,
      creditCardExpMonth:  payment.creditCardExpMonth ? payment.creditCardExpMonth : payment.debitCardExpMonth ? payment.debitCardExpMonth : null ,
      creditCardExpYear:  payment.creditCardExpYear ? payment.creditCardExpYear : payment.debitCardExpYear ? payment.debitCardExpYear : null ,
      creditCardSecurityCode:  payment.creditCardSecurityCode ? payment.creditCardSecurityCode :  payment.debitCardSecurityCode ? payment.debitCardSecurityCode : null ,
    })

  }

  selectBank(bank){
    
    bank =  PaymentDebitCardBank[bank]

    if(bank == "Bradesco" || bank == "BancoDoBrasil" ){

      this.brandCreditCardList = [
        { id: 'Visa', name: 'Visa' },
        { id: 'Mastercard', name: 'Mastercard' },
        { id: 'Elo', name: 'Elo' }
      ]
        
    }else if(bank == "Santander" || bank == "Itau" || bank == "CitiBank"){
      
      this.brandCreditCardList = [
        { id: 'Visa', name: 'Visa' },
        { id: 'Mastercard', name: 'Mastercard' }
      ]

    }else if(bank == "BRB" || bank == "CAIXA ECONÔMICA" || bank == "BancooB" ){
      
      this.brandCreditCardList = [
        { id: 'Mastercard', name: 'Mastercard' }
      ]
    }else{
      this.brandCreditCardList = [
        { id: 'Visa', name: 'Visa' },
        { id: 'Mastercard', name: 'Mastercard' },
        { id: 'Amex', name: 'American Express' },
        { id: 'Diners', name: 'Diners Club' },
        { id: 'Elo', name: 'Elo' },
        { id: 'Hipercard', name: 'Hipercard' }
      ]
    }

  }

  setPaymentType(type: string) {

    if(!this.creditCardForm.value.installments){
      this.snackbar.open('Selecione o número de parcelas.', '', { duration: 5000 });
      return false
    }

    this.creditCardForm.patchValue ({
      type: type
    });

    if(type == 'DEBIT_CARD'){
      this.creditCardForm.patchValue ({
        debitCardBank:PaymentDebitCardBank[this.creditCardForm.value.debitCardBank],
        installments: this.creditCardForm.value.installments ? this.creditCardForm.value.installments : 4
      });
    }else if (type == "BOOKLET"){
      this.creditCardForm.patchValue ({
        installments: this.creditCardForm.value.installments ? this.creditCardForm.value.installments : 4
      });
    }

    this.onPaymentTypeChanged.emit(this.creditCardForm.value)
  }

  createInstallmentList(installment) {
    this.installmentsList = []
    for(var i=0; i< installment;i++){
      if(i==0){
        this.installmentsList.push({'number':i+1,'text':'À vista'})
      }else{
        this.installmentsList.push({'number':i+1,'text':'Parcelar em '+(i+1)+'x'})
      }
    }
  } 

  createInstallmentListCreditCard(installment) {
    this.installmentsListCreditCard = []
    for(var i=0; i< installment;i++){
      if(i==0){
        this.installmentsListCreditCard.push({'number':i+1,'text':'À vista'})
      }else{
        this.installmentsListCreditCard.push({'number':i+1,'text':'Parcelar em '+(i+1)+'x'})
      }
    }
  } 

  changeInstallment(installment: number) {

    this.creditCardForm.patchValue ({
      installments: installment
    });

    //this.onPaymentTypeChanged.emit(this.creditCardForm.value)
  }

  hideControls() {
    this.creditCardForm.disable()
    this.isHiddenControls = true
  }
}
