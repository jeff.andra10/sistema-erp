import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotePaymentComponent } from './quote-payment.component';

describe('QuotePaymentComponent', () => {
  let component: QuotePaymentComponent;
  let fixture: ComponentFixture<QuotePaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuotePaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
