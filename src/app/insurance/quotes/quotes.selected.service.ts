import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Quote } from "app/core/models/quote.model";

@Injectable()
export class QuotesSelectedService {

    private quoteSource : BehaviorSubject<Quote> = new BehaviorSubject(null)
    public quoteSelected = this.quoteSource.asObservable();
  
    setSelected(value: Quote) {
      this.quoteSource.next(value)
    }
}