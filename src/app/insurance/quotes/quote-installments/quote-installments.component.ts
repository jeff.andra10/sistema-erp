import { Component, OnInit, Inject, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BaseComponent } from 'app/_base/base-component.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-quote-installments',
  templateUrl: './quote-installments.component.html',
  styleUrls: ['./quote-installments.component.scss']
})
export class QuoteInstallmentsComponent extends BaseComponent implements OnInit {

  constructor(private injector: Injector, 
              private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<QuoteInstallmentsComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) { 
              super(injector)
  }

  paymentForm: FormGroup
  quoteList : Array<any> = []
  installmentsList: Array<any> = []
  paymentList: Array<any> = [{'text':'Cartão de Crédito','number':'6'},{'text':'Cartão de Débito','number':'4'},{'text':'Boleto','number':'4'}]
  installments : number = 0
  totalPrice : any
  installmentsInstallation : number = 0
  installmentsListCreditCard = []

  

   
  ngOnInit() {

    this.paymentForm = this.formBuilder.group({
      payment: this.formBuilder.control('6'),
    })

    this.installments = 12
    this.totalPrice = this.data.totalPrice
    this.installmentsInstallation = 6
    this.calculateInstalment(this.installments)
    this.createInstallmentListCreditCard(6)
  }

  close() {
    this.dialogRef.close();
  }

  paymentSelected(installments){
    this.installmentsInstallation = installments 
    this.calculateInstalment(installments == '6' ? 12 : installments)
    this.createInstallmentListCreditCard(installments)
  }

  calculateInstalment(installments){
    this.installments = installments 
    let sumPrice = (this.data.totalPrice + 360)
    
    this.quoteList = [];
    for(var i=0; i<installments; i++ ){
      if(this.installmentsInstallation == 4 && this.installmentsInstallation > i){
         this.quoteList.push({'number':i+1,'value': (((sumPrice)/(installments))+(300/this.installmentsInstallation)).toFixed(2)})
      }else{
        this.quoteList.push({'number':i+1,'value': (sumPrice/(installments)).toFixed(2)})
      }
    }
  }

  createInstallmentListCreditCard(installment) {
    this.installmentsListCreditCard = []
    for(var i=0; i< installment;i++){
      if(i==0){
        this.installmentsListCreditCard.push({'number':i+1,'text':'À vista'})
      }else{
        this.installmentsListCreditCard.push({'number':i+1,'text':'Parcelar em '+(i+1)+'x'})
      }
    }
  } 

  paymentCreditCardSelected(installment){
    this.installmentsInstallation = installment
  }

  paymentInstallmentSelected(installment){
    this.installmentsInstallation = installment
    this.calculateInstalment(installment)
  }

}
