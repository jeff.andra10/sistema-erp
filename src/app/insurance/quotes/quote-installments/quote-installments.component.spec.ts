import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteInstallmentsComponent } from './quote-installments.component';

describe('QuoteInstallmentsComponent', () => {
  let component: QuoteInstallmentsComponent;
  let fixture: ComponentFixture<QuoteInstallmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteInstallmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteInstallmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
