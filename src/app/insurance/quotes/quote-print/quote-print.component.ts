import { Component, OnInit, Inject, Injector } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { DateHelp } from '../../../core/helps/date.help';


@Component({
  selector: 'quote-print',
  templateUrl: './quote-print.component.html',
  styleUrls: ['../quotes.component.scss'],
})
export class QuotePrintComponent extends BaseComponent implements OnInit {

  quoteDetails : Array<any>
  questionnaireAnswered : Array<any>
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  printQuotetion : Array<any>
  quote : any
  
  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<QuotePrintComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackbar: MatSnackBar,
    private dateHelp: DateHelp
  ) {
    super(injector)
   }

  ngOnInit() {

    this.quote = this.data

    this.quoteDetails = [
        {'detail':'Data de Cotação','value':this.dateHelp.convertDateToString(this.data.lifecycle.quotationDate)},
        {'detail':'Posto de Emissão','value':'-'},
        {'detail':'Cotado por','value':'-'},
        {'detail':'Prêmio Tarifário','value':'R$ '+this.data.premiumAmount},
        {'detail':'IOF','value':'R$ '+this.data.iofAmount},
        {'detail':'Custo do Seguro (Prêmio Tarifário + IOF)','value':'R$ '+this.data.premiumTotalAmount},
        {'detail':'Limite Máximo de Indenização','value':'-'},
        {'detail':'FIPE','value':this.data.vehicle.fipePrice},
        {'detail':'Ano do Veículo','value':this.data.vehicle.modelYear},
        {'detail':'Combustível','value':this.convertToFuelType(this.data.vehicle.fuelType)},
        {'detail':'Vigência','value': '12 meses'},
        {'detail':'Tipo','value': this.data.customer.personType == '1' ? "Pessoa Física" : "Pessoa Jurídica"}
    ]

    this.questionnaireAnswered  = [
        {'detail':'O veículo é turbo, rebaixado, tunado ou possui outras modificações?','value':this.convertToModificationType(this.data.vehicle.modificationType)},
        {'detail':'O segurado é o proprietário do veículo?','value':this.data.customer.vehicleOwner == true ? 'Sim' : 'Não'},
        {'detail':'Qual a utilização do veículo?','value':this.convertToVehicleUseType(this.data.vehicle.useType)},
        {'detail':'O Veículo possui adaptação para pessoa com deficiência (PCD)?','value': this.convertToPcd(this.data.vehicle.pcd)},
        {'detail':'O veículo é modificado com Kit Gás (GNV)?','value':this.data.vehicle.gnv == true ? 'Sim' : 'Não'},
        {'detail':'O veículo é blindado?','value':this.data.vehicle.armoredVehicle == true ? 'Sim' : 'Não'},
        {'detail':'O Segurado é o condutor principal?','value':this.data.customer.vehicleOwner == true ? 'Sim' : 'Não'},
        {'detail':'Número de Roubos ou Furtos de veículos nos últimos 12 meses','value':this.convertToVehicleIncidents(this.data.vehicle.vehicleLastYearThefts)},
        {'detail':'Número de Sinistros cobertos nos últimos 12 meses','value':this.convertToVehicleIncidents(this.data.vehicle.vehicleIncidents)},
        {'detail':'Nenhum Qual a relação do Segurado com o Proprietário do veículo?','value':this.convertToRelationshipWithVehicleOwner(this.data.customer.relationshipWithVehicleOwner)},
        {'detail':'Tipo de Condutor','value':this.data.driver.personType == '1' ? "Pessoa Física" : "Pessoa Jurídica"},
        {'detail':'Data de Nascimento do Condutor','value':this.dateHelp.convertDateToString(this.data.driver.birthdate)},
        {'detail':'Estado Civil do Condutor','value':this.convertToMaritalStatus(this.data.driver.maritalStatus)},
        {'detail':'Sexo do Condutor','value':this.data.driver.gender == '1' ? 'Masculino' : 'Feminino'},
        {'detail':'Deseja cobertura para outros condutores até 25 anos?','value':this.data.vehicle.isVehicleUsedUnder25YearsOld == true ? 'Sim' : 'Não'},
        {'detail':'Qual a quilometragem rodada por mês?','value':this.convertToMiles(this.data.vehicle.monthlyUsage)},
        {'detail':'Possui garagem no local de pernoite do veículo (residência)?','value':this.convertToGarageResidential(this.data.vehicle.garageResidential)},
        {'detail':'Possui garagem no local de trabalho?','value':this.convertToGarageWork(this.data.vehicle.garageWork)},
        {'detail':'Possui garagem no local de estudo?','value':this.convertToGarageStudy(this.data.vehicle.garageStudy)},
        {'detail':'É renovação do seguro?','value':this.convertToRenovationType(this.data.renovationType)}
    ]

    this.printQuotetion = [
        // {'detail':'Descrição','value':''},
        {'detail':'Data de Cotação','value':this.dateHelp.convertDateToString(this.data.lifecycle.quotationDate)},
        {'detail':'Posto de Emissão','value':'-'},
        {'detail':'Cotado por','value':'-'},
        {'detail':'Prêmio Tarifário','value':'R$ '+this.data.premiumAmount},
        {'detail':'IOF','value':'R$ '+this.data.iofAmount},
        {'detail':'Custo do Seguro (Prêmio Tarifário + IOF)','value':'R$ '+this.data.premiumTotalAmount},
        {'detail':'Limite Máximo de Indenização','value':'-'},
        {'detail':'FIPE','value':this.data.vehicle.fipePrice},
        {'detail':'Ano do Veículo','value':this.data.vehicle.modelYear},
        {'detail':'Combustível','value':this.convertToFuelType(this.data.vehicle.fuelType)},
        {'detail':'Vigência','value': '12 meses'},
        {'detail':'Tipo','value': this.data.customer.personType == '1' ? "Pessoa Física" : "Pessoa Jurídica"},
        // {'detail':'Questionário respondido','value':''},
        {'detail':'O veículo é turbo, rebaixado, tunado ou possui outras modificações?','value':this.convertToModificationType(this.data.vehicle.modificationType)},
        {'detail':'O segurado é o proprietário do veículo?','value':this.data.customer.vehicleOwner == true ? 'Sim' : 'Não'},
        {'detail':'Qual a utilização do veículo?','value':this.convertToVehicleUseType(this.data.vehicle.useType)},
        {'detail':'O Veículo possui adaptação para pessoa com deficiência (PCD)?','value': this.convertToPcd(this.data.vehicle.pcd)},
        {'detail':'O veículo é modificado com Kit Gás (GNV)?','value':this.data.vehicle.gnv == true ? 'Sim' : 'Não'},
        {'detail':'O veículo é blindado?','value':this.data.vehicle.armoredVehicle == true ? 'Sim' : 'Não'},
        {'detail':'O Segurado é o condutor principal?','value':this.data.customer.vehicleOwner == true ? 'Sim' : 'Não'},
        {'detail':'Número de Roubos ou Furtos de veículos nos últimos 12 meses','value':this.convertToVehicleIncidents(this.data.vehicle.vehicleLastYearThefts)},
        {'detail':'Número de Sinistros cobertos nos últimos 12 meses','value':this.convertToVehicleIncidents(this.data.vehicle.vehicleIncidents)},
        {'detail':'Nenhum Qual a relação do Segurado com o Proprietário do veículo?','value':this.convertToRelationshipWithVehicleOwner(this.data.customer.relationshipWithVehicleOwner)},
        {'detail':'Tipo de Condutor','value':this.data.driver.personType == '1' ? "Pessoa Física" : "Pessoa Jurídica"},
        {'detail':'Data de Nascimento do Condutor','value':this.dateHelp.convertDateToString(this.data.driver.birthdate)},
        {'detail':'Estado Civil do Condutor','value':this.convertToMaritalStatus(this.data.driver.maritalStatus)},
        {'detail':'Sexo do Condutor','value':this.data.driver.gender == '1' ? 'Masculino' : 'Feminino'},
        {'detail':'Deseja cobertura para outros condutores até 25 anos?','value':this.data.vehicle.isVehicleUsedUnder25YearsOld == true ? 'Sim' : 'Não'},
        {'detail':'Qual a quilometragem rodada por mês?','value':this.convertToMiles(this.data.vehicle.monthlyUsage)},
        {'detail':'Possui garagem no local de pernoite do veículo (residência)?','value':this.convertToGarageResidential(this.data.vehicle.garageResidential)},
        {'detail':'Possui garagem no local de trabalho?','value':this.convertToGarageWork(this.data.vehicle.garageWork)},
        {'detail':'Possui garagem no local de estudo?','value':this.convertToGarageStudy(this.data.vehicle.garageStudy)},
        {'detail':'É renovação do seguro?','value':this.convertToRenovationType(this.data.renovationType)}
    ]
    
  }

  printQuote(printSectionId) {
    var innerContents = document.getElementById(printSectionId).innerHTML;
    var popupWinindow = window.open('', '_blank', 'width=600,height=600,scrollbars=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();

    //window.print();
  }

  convertToRelationshipWithVehicleOwner(type){
    switch (type) {
        case '1':
          return 'O próprio'
        case '2':
          return 'Operação de Leasing'
        case '3':
          return 'Veículo comprado recentemente e em transferência'
        case '4':
          return 'Veículo em nome de terceiro'
        default:
          return '-'
    }
  }

  convertToModificationType(type){
    switch (type) {
        case '1':
          return 'Não, nenhuma modificação'
        case '2':
          return 'Sim, é turbo, rebaixado ou tunado'
        case '0':
          return 'Sim, chassi remarcado'
        case '3':
          return 'Sim, possui outras modificações'
        default:
          return '-'
    }
  }

  convertToFuelType(type){
    switch (type) {
        case '1':
          return 'Gasolina'
        case '2':
          return 'Álcool'
        case '3':
          return 'Diesel'
        case '4':
          return 'BIFUEL'
        case '7':
          return 'TETRAFUEL'
        default:
          return '-'
    }
  }
  
  convertToPcd(type){
    switch (type) {
        case '1':
          return 'Não'
        case '2':
          return 'Sim, PCD original de fábrica'
        case '3':
          return 'Sim, adaptado para PCD'
        default:
          return '-'
    }
  }
 
  convertToVehicleUseType(type){
    switch (type) {
        case '1':
          return 'Particular'
        case '2':
          return 'Particular Locado'
        case '3':
          return 'Motorista de Aplicativo (Uber, Cabify, Pop, outros)'
        case '4':
          return 'Representante Comercial / Vendedor'
        case '5':
          return 'Serviços Técnicos'
        case '6':
          return 'Serviços de Entrega'
        case '7':
          return 'Compartilhado (Car Sharing)'
        case '8':
          return 'Outros'
        default:
          return '-'
    }
  }

  convertToRenovationType(type){
    switch (type) {
        case '1':
          return 'Sim, da Usebens	'
        case '2':
          return 'Sim, de Congênere'
        case '3':
          return 'Não (seguro novo)'
        default:
          return '-'
    }
  }

  convertToVehicleIncidents(type){
    switch (type) {
        case '1':
          return 'Nenhum'
        case '2':
          return '1'
        case '3':
          return '2'
        case '4':
          return '3'
        case '5':
          return '4 ou mais'
        default:
          return '-'
    }
  }
  
  convertToMiles(miles){
    switch (miles) {
        case '1':
          return 'até 500 km / mês'
        case '2':
          return 'até 1000 km / mês'
        case '3':
          return 'até 1250 km / mês'
        case '4':
          return 'até 1500 km / mês'
        case '5':
          return 'até 1750 km / mês'
        case '6':
          return 'acima de 1750 km / mês'
        default:
          return '-'
    }
  }

  convertToMaritalStatus(status){
    switch (status) {
        case '1':
          return 'Solteiro'
        case '2':
          return 'Casado'
        case '4':
          return 'Divorciado'
        case '5':
          return 'Viúvo'
        case '6':
          return 'Separado'
        default:
          return '-'
    }
 }

 convertToGarageResidential(type){
    switch (type) {
        case '1':
          return 'Sim, externo privado e pago'
        case '2':
          return 'Sim, com portão manual'
        case '3':
          return 'Sim, com portão automático ou porteiro'
        case '4':
          return 'Não ou outro tipo de garagem'
        default:
          return '-'
    }
  }
  convertToGarageWork(type){
    switch (type) {
        case '5':
          return 'Sim, interno da empresa em local fechado'
        case '6':
          return 'Sim, externo a empresa, privado e pago'
        case '7':
          return 'Não ou Outro tipo de garagem'
        case '8':
          return 'Não usa para locomoção ao trabalho'
        case '9':
          return 'Não trabalha'
        default:
          return '-'
    }
  }
  convertToGarageStudy(type){
    switch (type) {
        case '10':
          return 'Diurno em garagem privada e fechada - paga'
        case '11':
          return 'Diurno sem garagem'
        case '12':
          return 'Não estuda'
        case '13':
          return 'Não utiliza o veiculo para locomoção ao local de estudo'
        case '14':
          return 'Noturno em garagem privada e fechada - paga'
        case '15':
          return 'Noturno sem garagem'
        
        
        default:
          return ''
    }
  }

}
