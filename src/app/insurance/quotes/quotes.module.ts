import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../angular-material/material.module";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "@fuse/shared.module";

import { QuotesComponent } from "./quotes.component";
import { QuoteDetailsComponent } from './quote-details/quote-details.component';
import { CustomerDocumentsComponent } from "../../customers/customer-documents/customer-documents.component";
import { CustomerPersonalComponent } from "../../customers/customer-personal/customer-personal.component";
import { CustomerAddressComponent } from "../../customers/customer-address/customer-address.component";
import { CustomersModule } from "../../customers/customers.module";
import { QuoteInstallmentsComponent } from './quote-installments/quote-installments.component';
import { VehicleDetailsComponent } from "../../vehicles/vehicle-details/vehicle-details.component";
import { VehiclesModule } from "../../vehicles/vehicles.module";
import { QuotePaymentComponent } from './quote-payment/quote-payment.component';
import { QuotePrintComponent } from './quote-print/quote-print.component';
import { ProposalDetailsComponent } from '../proposal/proposal-details/proposal-details.component'
import { QuoteAcceptanceComponent } from "./quote-acceptance/quote-acceptance.component";
import { QuotesSelectedService } from "./quotes.selected.service";

const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'quotes/new', component: QuoteDetailsComponent },
    { path: 'quotes/:id', component: QuoteDetailsComponent },
    { path: 'proposal/:id', component: ProposalDetailsComponent }
]);

@NgModule({
    declarations:[
        QuotesComponent,
        QuoteDetailsComponent,
        QuoteInstallmentsComponent,
        QuotePaymentComponent,
        QuoteAcceptanceComponent,
        QuotePrintComponent,
        ProposalDetailsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        TextMaskModule,
        FlexLayoutModule,
        RouterModule,
        authRouting,
        CustomersModule,
        VehiclesModule
    ],
    exports: [
        QuotePrintComponent
    ],
    entryComponents: [
        CustomerDocumentsComponent,
        CustomerPersonalComponent,
        CustomerAddressComponent,
        QuoteInstallmentsComponent,
        VehicleDetailsComponent,
        QuotePaymentComponent,
        QuotePrintComponent,
        ProposalDetailsComponent
    ],
    providers: [
        QuotesSelectedService
    ]
})
export class QuotesModule {}