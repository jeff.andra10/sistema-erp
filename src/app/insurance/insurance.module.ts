import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from '../angular-material/material.module';
import { TextMaskModule } from 'angular2-text-mask';
import { PoliciesComponent } from './policies/policies.component';
import { QuotesComponent } from './quotes/quotes.component';
import { ProposalComponent } from './proposal/proposal.component'
import { QuotesModule } from './quotes/quotes.module';
import { QuoteAcceptanceComponent } from './quotes/quote-acceptance/quote-acceptance.component';
import { PolicyDetailsComponent } from './policies/policy-details/policy-details.component';
import { PoliciesSelectedService } from './policies/policies.selected.service';
//import { ConfirmDialogComponent } from 'app/dialogs/confirm-dialog/confirm-dialog.component';

const authRouting: ModuleWithProviders = RouterModule.forChild([
  { path: '',  
    children : [
      { path: 'quotes', component: QuotesComponent },
      { path: 'policies', component: PoliciesComponent },
      { path: 'policies/:id', component: PolicyDetailsComponent },
      { path: 'quote-acceptance', component: QuoteAcceptanceComponent },
      { path: 'proposal', component: ProposalComponent },
    ]
  }
]);

@NgModule({
  imports: [
    authRouting,
    CommonModule,
    FuseSharedModule,
    MaterialModule,
    TextMaskModule,
    QuotesModule,
  ],
  declarations: [
    PoliciesComponent,
    PolicyDetailsComponent,
    ProposalComponent
  ],
  exports: [
  ],
  providers: [
    PoliciesSelectedService
  ],
  entryComponents: [
   // ConfirmDialogComponent
  ]
  
})
export class InsuranceModule { }
