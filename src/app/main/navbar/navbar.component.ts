import { Component, Input, OnDestroy, ViewChild, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { AppContext } from '../../core/appcontext';
import { FuseNavigationComponent } from '@fuse/components/navigation/navigation.component';
import { BrandsService } from "../../../app/core/services/brands.service";

@Component({
    selector     : 'fuse-navbar',
    templateUrl  : './navbar.component.html',
    styleUrls    : ['./navbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FuseNavbarComponent implements OnDestroy, AfterViewInit
{
    @ViewChild('navbarVertical') navbarVertical: FuseNavigationComponent
    @ViewChild('navbarHorizontal') navbarHorizontal: FuseNavigationComponent

    private fusePerfectScrollbar: FusePerfectScrollbarDirective;

    urlLogo : string = 'assets/images/logos/logo-tracknme.svg'

    @ViewChild(FusePerfectScrollbarDirective) set directive(theDirective: FusePerfectScrollbarDirective)
    {
        if ( !theDirective )
        {
            return;
        }

        this.fusePerfectScrollbar = theDirective;

        this.navigationServiceWatcher =
            this.navigationService.onItemCollapseToggled.subscribe(() => {
                this.fusePerfectScrollbarUpdateTimeout = setTimeout(() => {
                    this.fusePerfectScrollbar.update();
                }, 310);
            });
    }

    @Input() layout;
    navigation: any;
    navigationServiceWatcher: Subscription;
    fusePerfectScrollbarUpdateTimeout;

    appContextOnInitWatcher: Subscription;
    appContextBrandOnInitWatcher: Subscription;

    constructor(
        private sidebarService: FuseSidebarService,
        private navigationService: FuseNavigationService,
        private appContext: AppContext,
        private brandsService: BrandsService
    )
    {
        // Default layout
        this.layout = 'vertical';
    }

    ngAfterViewInit(): void {

        setTimeout(() => {
            this.urlLogo = localStorage.getItem('logoBrand');
        });

        // Watch context init
        this.appContextOnInitWatcher = this.appContext.onContextInit.subscribe((session)=>{

            if (session != null) {
              //this.loadMenu()
              this.urlLogo = localStorage.getItem('logoBrand');
            }

            this.setUrlLogo(localStorage.getItem('logoBrand'))
            
        })

        
        
    }

    setUrlLogo(url){
        this.urlLogo = url
    }
    
    ngOnDestroy()
    {
        if ( this.fusePerfectScrollbarUpdateTimeout )
        {
            clearTimeout(this.fusePerfectScrollbarUpdateTimeout);
        }

        if ( this.navigationServiceWatcher )
        {
            this.navigationServiceWatcher.unsubscribe();
        }
    }

    toggleSidebarOpened(key)
    {
        this.sidebarService.getSidebar(key).toggleOpen();
    }

    toggleSidebarFolded(key)
    {
        this.sidebarService.getSidebar(key).toggleFold();
    }

    private loadMenu() {

        // get menu
        this.navigation = this.buildMenu(this.appContext.isLoggedUserTracknme());

        // update navigators
        if (this.navbarHorizontal) {
            setTimeout(() => {
                this.navbarHorizontal.navigation = this.navigation
            }, 300);
        }

        if (this.navbarVertical) {
            setTimeout(() => {
                this.navbarVertical.navigation = this.navigation
            }, 300);
        }
    }

    private buildMenu(isTracknme: Boolean) : any {

        var menu : any = []

        if (isTracknme) {

            menu = [{
                'id' : 'applications', 'title': '', 'translate': 'NAV.APPLICATIONS', 'type': 'group',
                'children': [ 
                    { 'id': 'dashboard', 'title': 'Dashboard', 'type': 'item', 'icon': 'dashboard', 'url': '/dashboard' },
                    { 'id': 'customers', 'title': 'Clientes', 'type': 'item', 'icon': 'group', 'url': 'customers' },
                    { 'id': 'vehicles', 'title': 'Veículos', 'type': 'item', 'icon': 'directions_car', 'url': 'vehicles' },
                    { 'id': 'contracts', 'title': 'Contratos', 'type': 'item', 'icon': 'description', 'url': 'contracts' }, 
                    { 'id': 'transactions', 'title': 'Financeiro', 'type': 'item', 'icon': 'bar_chart', 'url': 'transactions' },   
                    { 'id': 'sale', 'title': 'Vendas', 'type': 'item', 'icon': 'local_offer','url': '/sales'},    
                    { 'id': 'serviceOrders', 'title': 'Ordens de Serviço', 'type': 'item', 'icon': 'assignment', 'url': 'service-orders'},
                    { 'id': 'admin', 'title': 'Administração', 'type': 'collapse', 'icon': 'settings',
                        'children': [
                            { 'id': 'offers', 'title': 'Ofertas', 'type': 'item', 'icon': 'library_books', 'url': '/admin/offers'},
                            { 'id': 'products', 'title': 'Produtos', 'type': 'item', 'icon': 'apps', 'url': '/admin/products' },
                            { 'id': 'services', 'title': 'Serviços', 'type': 'item', 'icon': 'headset_mic', 'url': '/admin/services' },  
                            { 'id': 'simcards', 'title': 'Sim Cards', 'type': 'item', 'icon': 'sim_card', 'url': '/admin/simcards' },     
                            { 'id': 'brands', 'title': 'Marcas', 'type': 'item', 'icon': 'bookmarks', 'url': '/admin/brands' },
                            { 'id': 'devices', 'title': 'Rastreadores', 'type': 'item', 'icon': 'mobile_friendly', 'url': '/admin/devices'},
                            { 'id': 'suppliers', 'title': 'Fornecedores', 'type': 'item', 'icon': 'store', 'url': '/admin/suppliers'}
                        ]
                    }
                ]
            }]

        } else {

            menu = [{
                'id' : 'applications', 'title': '', 'translate': 'NAV.APPLICATIONS', 'type': 'group',
                'children': [ 
                    { 'id': 'dashboard', 'title': 'Dashboard', 'type': 'item', 'icon': 'dashboard', 'url': '/dashboard' },
                    { 'id': 'customers', 'title': 'Clientes', 'type': 'item', 'icon': 'group', 'url': 'customers' },
                    { 'id': 'vehicles', 'title': 'Veículos', 'type': 'item', 'icon': 'directions_car', 'url': 'vehicles' },
                    { 'id': 'contracts', 'title': 'Contratos', 'type': 'item', 'icon': 'description', 'url': 'contracts' }, 
                    // { 'id': 'transactions', 'title': 'Financeiro', 'type': 'item', 'icon': 'bar_chart', 'url': 'transactions' },  
                    { 'id': 'sale', 'title': 'Vendas', 'type': 'item', 'icon': 'local_offer','url': '/sales'},   
                    { 'id': 'purchases', 'title': 'Compras', 'type': 'item', 'icon': 'shopping_cart', 'url': 'purchases'},
                    { 'id': 'serviceOrders', 'title': 'Ordens de Serviço', 'type': 'item', 'icon': 'assignment', 'url': 'service-orders'},
                    { 'id': 'admin', 'title': 'Administração', 'type': 'collapse', 'icon': 'settings',
                        'children': [
                            { 'id': 'offers', 'title': 'Ofertas', 'type': 'item', 'icon': 'library_books', 'url': '/admin/offers'},
                            { 'id': 'products', 'title': 'Ativação Produtos', 'type': 'item', 'icon': 'apps', 'url': '/admin/products' },
                            { 'id': 'services', 'title': 'Ativação Serviços', 'type': 'item', 'icon': 'headset_mic', 'url': '/admin/services' },  
                            { 'id': 'brands', 'title': 'Marcas', 'type': 'item', 'icon': 'bookmarks', 'url': '/admin/brands' },
                            { 'id': 'devices', 'title': 'Rastreadores', 'type': 'item', 'icon': 'mobile_friendly', 'url': '/admin/devices'},
                        ]
                    }
                ]
            }]
        }
        
        return menu
    }
}
