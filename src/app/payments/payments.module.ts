import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { PaymentsComponent } from "./payments.component";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../angular-material/material.module";
import { FuseSharedModule } from "@fuse/shared.module";
import { NewCreditCardComponent } from './new-credit-card/new-credit-card.component';


@NgModule({
    declarations:[
        PaymentsComponent,
        NewCreditCardComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        FlexLayoutModule,
        TextMaskModule
    ],
    exports: [
        PaymentsComponent,
        NewCreditCardComponent
    ],
    entryComponents: []
})
export class PaymentsModule {}