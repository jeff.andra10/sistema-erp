import { Component, OnInit, Inject, ViewEncapsulation} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Payment } from '../../core/models/payment.model';
import { PaymentsService } from '../../core/services/payments.service';
import { MaskUtils } from 'app/core/mask-utils';
import { UtilsService } from '../../core/utils.service';
import { CONSTANTS } from '../../core/common-data-domain';
import { Month,PaymentDebitCardBank } from '../../core/models/payment.model';
import { PaymentCardService } from '../../core/services/payment-card.service';
import { CardType } from '../../core/models/card-type';
import { MatSnackBar, MatStepper, MatDialog } from '@angular/material';



@Component({
  selector: 'app-new-credit-card',
  templateUrl: './new-credit-card.component.html',
  styleUrls: ['./new-credit-card.component.scss'],
  providers: [PaymentCardService],
  encapsulation: ViewEncapsulation.None
})
export class NewCreditCardComponent implements OnInit {

  creditCardForm: FormGroup
  user: number
  business: number
  expMonthMask = MaskUtils.expMonthMask
  expYearMask  = MaskUtils.expYearMask
  cardMask     = [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]
  ccsMask      = [ /\d/ , /\d/, /\d/]
  title : string 
  listBank  = ['BRADESCO','BANCO DO BRASIL','SANTANDER','ITAÚ','CITIBANK','BRB','CAIXA ECONÔMICA','BANCO OB'];
  listCard = ['Amex', 'Diners', 'Discover','JCB','Mastercard','Visa','Aura','Elo','Hipercard'];

  
  /**
   * List of months
   */
  public months: Array<Month> = [];

  /**
   * List of years
   */
  public years: Array<number> = [];


  constructor(
    public dialogRef: MatDialogRef<NewCreditCardComponent>,
    private formBuilder: FormBuilder,
    private paymentsService: PaymentsService,
    private utils: UtilsService,
    private snackbar: MatSnackBar,
    private _ccService: PaymentCardService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) 
  {
    if(data.user){
      this.user = data.user
    }else if(data.business){
      this.business = data.business
    }

    if(data.type == 'CREDIT_CARD'){
      this.title = 'Novo Cartão de Crédito';
    }else{
      this.title = 'Novo Cartão de Débito';
    }
    
  }

  ngOnInit() {
    this.initForms();
    this.assignDateValues();
  }

  initForms() {
    
    this.creditCardForm = this.formBuilder.group({
      debitCardBank: this.formBuilder.control(''),
      creditCardBrand: this.formBuilder.control('', [Validators.required]),
      creditCardHolderName: this.formBuilder.control('', [Validators.required]),
      creditCardNumber: this.formBuilder.control('', [Validators.required]),
      creditCardExpMonth: this.formBuilder.control('', []),
      creditCardExpYear: this.formBuilder.control('', [Validators.required]),
      creditCardSecurityCode: this.formBuilder.control('', [Validators.required])
    })
  }

  public loadPaymentData(payment: Payment) {

    this.creditCardForm.setValue ({
      debitCardBank: payment.debitCardBank || null,
      creditCardBrand: payment.creditCardBrand || null,
      creditCardHolderName: payment.creditCardHolderName || null,
      creditCardNumber: payment.creditCardNumber || null,
      creditCardExpMonth: payment.creditCardExpMonth || null,
      creditCardExpYear: payment.creditCardExpYear || null,
      creditCardSecurityCode: payment.creditCardSecurityCode || null
    })
  } 

  
  save() {

    localStorage.setItem('hiddenLoading','false');

    let payment = new Payment

    let number =  this.creditCardForm.value.creditCardNumber

    if(this.user){
      payment.user = this.user
    }else if(this.business){
      payment.business = this.business
    }

    if(this.data.type == 'CREDIT_CARD'){

      payment.type = 'CREDIT_CARD'
      payment.creditCardBrand = this.creditCardForm.value.creditCardBrand
      payment.creditCardHolderName = this.utils.removeCharSpecial(this.creditCardForm.value.creditCardHolderName)
      payment.creditCardNumber = number.replace(/\s/g,'')
      payment.creditCardExpMonth = this.creditCardForm.value.creditCardExpMonth
      payment.creditCardExpYear = this.creditCardForm.value.creditCardExpYear
      payment.creditCardSecurityCode = this.creditCardForm.value.creditCardSecurityCode

    }else{

      payment.type = 'DEBIT_CARD'
      payment.debitCardBrand = this.creditCardForm.value.creditCardBrand
      payment.debitCardHolderName = this.utils.removeCharSpecial(this.creditCardForm.value.creditCardHolderName)
      payment.debitCardExpMonth = this.creditCardForm.value.creditCardExpMonth
      payment.debitCardExpYear = this.creditCardForm.value.creditCardExpYear
      payment.debitCardSecurityCode = this.creditCardForm.value.creditCardSecurityCode
      payment.debitCardBank = PaymentDebitCardBank[this.creditCardForm.value.debitCardBank]
      payment.debitCardNumber = number.replace(/\s/g,'')


      if((payment.debitCardBank == "Bradesco" || payment.debitCardBank == "BancoDoBrasil")  &&  (payment.debitCardBrand != 'Visa' &&  payment.debitCardBrand != 'Mastercard' && payment.debitCardBrand != 'Elo') ){
        
        this.snackbar.open('O '+this.creditCardForm.value.debitCardBank+' só aceita cartão de débito das bandeiras Visa , MasterCard e Elo', '', {
          duration: 9000
        })

        return false
    
      }

      if((payment.debitCardBank == "Santander" || payment.debitCardBank == "Itau" || payment.debitCardBank == "CitiBank")  &&  (payment.debitCardBrand != 'Visa' &&  payment.debitCardBrand != 'Mastercard' ) ){
        
        this.snackbar.open('O '+this.creditCardForm.value.debitCardBank+' só aceita cartão de débito das bandeiras Visa , MasterCard ', '', {
          duration: 9000
        })

        return false
      }

     if((payment.debitCardBank == "BRB" || payment.debitCardBank == "Caixa" || payment.debitCardBank == "BancooB")  &&  payment.debitCardBrand != 'Mastercard' ){
        
        this.snackbar.open('O '+this.creditCardForm.value.debitCardBank+' só aceita cartão de débito da bandeira MasterCard ', '', {
          duration: 9000
        })

        return false
      }

    }


    if( (this.creditCardForm.value.creditCardBrand == 'Visa' || this.creditCardForm.value.creditCardBrand == 'Mastercard' || this.creditCardForm.value.creditCardBrand == 'Elo') || this.data.type == 'CREDIT_CARD' ){
  
      if(this.user || this.business){
        this.paymentsService.create(payment)
        .subscribe(result => {
          this.dialogRef.close({data : result})
          localStorage.setItem('hiddenLoading','true');
        }, error => {
           this.snackbar.open(error.error ? error.error.message : error.message, '', { duration: 5000 });
           localStorage.setItem('hiddenLoading','true');
        })
      }else{
        payment.id = CONSTANTS.TRACKNME_CONSTANT_ID
        this.dialogRef.close({data : payment})
        localStorage.setItem('hiddenLoading','true');
      }
  }else{

    this.snackbar.open('Só é possível cadastrar cartão de débito das bandeiras Visa e MasterCard', '', {
      duration: 9000
    })

  }

  }
  
   selectMask(brand){
      switch(brand){
        case 'Visa':
          this.cardMask  =  [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]
          this.ccsMask   = [ /\d/ , /\d/, /\d/]
          break;
        case 'Mastercard':
           this.cardMask  =  [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]
           this.ccsMask   = [ /\d/ , /\d/, /\d/]
           break;
        case 'Diners':
          this.cardMask  =  [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/]
          this.ccsMask   = [ /\d/ , /\d/, /\d/]
          break;
        case 'Hipercard':
          this.cardMask  =  [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]
          this.ccsMask   = [ /\d/ , /\d/, /\d/]
          break;
        case 'Amex':
          this.cardMask =  [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/]
          this.ccsMask   = [ /\d/ , /\d/, /\d/, /\d/]
          break;
        case 'Elo':
          this.cardMask =  [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]
          this.ccsMask   = [ /\d/ , /\d/, /\d/]
          break;
        case 'Aura':
          this.cardMask =  [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/ ,' ', /\d/, /\d/, /\d/]
          this.ccsMask   = [ /\d/ , /\d/, /\d/]
          break;
        case 'Discover':
          this.cardMask =  [ /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/ , /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]
          this.ccsMask   = [ /\d/ , /\d/, /\d/, /\d/]
          break;
    }
  }

  selectedCreditCard(brand){
    
    if(brand){

      if(brand == 'VISA' || brand == 'Visa Electron'){
        brand = 'Visa'
      }else if (brand == 'American Express'){
        brand = 'Amex'
      }else if (brand == 'Discover Club'){
        brand = 'Discover'
      }else if (brand == 'Diners Carte Blanche'){
        brand = 'Diners'
      }

      this.selectMask(brand)
      
      this.creditCardForm.patchValue ({
        creditCardBrand: brand
      });
    }
    
  }

  /**
   * Populate months and years
   */
  private assignDateValues(): void {
    this.months = PaymentCardService.getMonths();
    this.years = PaymentCardService.getYears();
  }

   /**
   * Returns payment card type based on payment card number
   */
  public getCardType(ccNum: string): string | null {
    if(ccNum){
      let brand = PaymentCardService.getCardType(ccNum)
      //this.selectedCreditCard(brand)
      return brand;
    }
  }

}


