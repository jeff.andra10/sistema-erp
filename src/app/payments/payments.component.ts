import { Component, OnInit, ViewChild, Output, EventEmitter, Injector } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';

import { MaskUtils } from '../core/mask-utils';
import { Payment, PaymentType } from '../core/models/payment.model';
import { MatTabGroup, MatDialog, MatTableDataSource } from '@angular/material';
import { BaseComponent } from '../_base/base-component.component';
import { NewCreditCardComponent } from './new-credit-card/new-credit-card.component';
import { PaymentsService } from '../core/services/payments.service';
import { Customer } from '../core/models/customer.model';
import { fuseAnimations } from '@fuse/animations/index';
import { CONSTANTS } from '../core/common-data-domain';



@Component({
  selector: 'tnm-payments',
  templateUrl: './payments.component.html',
  styleUrls: [ './payments.component.scss' ],
  animations : fuseAnimations
})
export class PaymentsComponent extends BaseComponent implements OnInit {

  @ViewChild('tabGroup') tabGroup: MatTabGroup;
  @Output() onPaymentTypeChanged: EventEmitter<any> = new EventEmitter();


  creditCardForm: FormGroup
  paymentBooklet : Payment
  paymentSelected : Payment
  paymentsList : Array<Payment> = []
  dueDayList : any
  addingNewCreditCard : boolean = false
  showSelectCreditCard : boolean = false
  customerSelected : Customer
  displayedColumns = ['icon', 'card'];
  installmentsList: Array<object>
  installmentSelected : 1
  dueDaySelected : 1
  installmentNumber : Number = 1
  typePayment : String = 'Cartão de Crédito'
  paymentsType = ['CREDIT_CARD','BOOKLET','DEBIT_CARD']
  constant_id = CONSTANTS.TRACKNME_CONSTANT_ID
  isBusiness : boolean = false
  cardType : string = 'CREDIT_CARD'

  displayedPaymentColumns = ['type', 'brand', 'card']
  dataSource: MatTableDataSource<Payment>

  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  
  selectedRowIndex: number = -1;

  isHideFields : boolean = false

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private paymentsService: PaymentsService
  ) {
    super(injector)
  }

  viewDetails(row){

  }

  ngOnInit() {

    this.initForms()
    
    this.showSelectCreditCard = false;

    this.paymentsList = [];
    
    this.createInstallmentList(this.installmentNumber)

    this.dueDayList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]
  }

  createInstallmentList(installment) {

    this.installmentsList = []
    for(var i=0; i< installment;i++){
      if(i==0){
        this.installmentsList.push({'number':i+1,'text':'À vista'})
      }else{
        this.installmentsList.push({'number':i+1,'text':'Parcelar em '+(i+1)+'x'})
      }
    }
  }

  lastFourNumbers(cardNumber){
    return cardNumber.substr(cardNumber.length - 4); 
  }

  private initForms() {
    this.creditCardForm = this.formBuilder.group({
      creditCardSelected: ['', Validators.required],
      installments: ['', Validators.required ],
      dueDay: ['', Validators.required]
    })
  }

  public loadData(payment: Payment) {
    this.tabGroup.selectedIndex = (payment.type == 'CREDIT_CARD' || payment.type == 'DEBIT_CARD')? 0 : 1
  }

  public getData() : any {
    return this.paymentSelected
  }

  public getTypePayment() : any {
    return this.typePayment
  }

  public getInstallments() : any {
    return this.installmentSelected['number'] ? this.installmentSelected['number'] : 1
  }

  public getDueDay() : any {
    return this.dueDaySelected
  }

  public hideFields(){
    this.isHideFields = true;
  }

  
  public isInvalid() {
    return this.paymentSelected != null
  }

  public setReadOnly() {
    // this.newCreditCardForm.disable()
  }

  public setPaymentsType(types){
    this.paymentsType = types
  }


  public setCustomer(customer: Customer) {
 
    this.customerSelected = customer
    this.isBusiness = false

    if(customer.user != null){
      this._getUserPayments(customer.user)
    }else{
      this.showSelectCreditCard = true;
      this.paymentsList = [];
    }
       
  }

  public setBusiness(business) {

    this.isBusiness = true

    if(business.id != null){
      this._getBusinessPayments(business.id)
    }
       
  }

  highlight(row){
    this.selectedRowIndex = row.id;
  }


  public setPlanInstallment(installment: Number) {
    this.installmentNumber = installment
    this.createInstallmentList(this.installmentNumber)
  }

  public setPaymentType(type: string) {
    
    this.installmentSelected = this.creditCardForm.value.installments ? this.creditCardForm.value.installments : 1
    this.dueDaySelected = this.creditCardForm.value.dueDay
  
    if (type == "CREDIT_CARD") {

      this.typePayment = this.cardType
      this.onPaymentTypeChanged.emit(type)

    } else if (type == "BOOKLET"){

      this.typePayment = "BOOKLET"
      
      if (this.paymentBooklet != null) {

        this.paymentSelected = this.paymentBooklet
        this.onPaymentTypeChanged.emit(type)

      } else {

        this.paymentSelected = new Payment()
        this.paymentSelected.type = 'BOOKLET'
        if(this.isBusiness){
          this.paymentSelected.business = this.appContext.session.user.brand
        }else{
          this.paymentSelected.user = this.appContext.session.user.id
        }
        

        this.paymentsService.create(this.paymentSelected)
        .subscribe(result => {
          this.paymentSelected.id = result.id
          this.onPaymentTypeChanged.emit(type)
        })
      }
    }
  }

  public _getUserPayments(id) {
    
    if(id){
      
      this.showSelectCreditCard = false;
      this.paymentsList = [];
      
      this.paymentsService.getPaymentsByUser(id)
        .subscribe(result=>{
          if (result.content != null) {
              result.content.forEach(element => {
                if (element.type == 'CREDIT_CARD' || element.type == 'DEBIT_CARD') {
                  this.paymentsList.push(element);
                } else if (element.type == 'BOOKLET'){
                  this.paymentBooklet = element
                }
              });

              if(this.paymentsList.length > 0){
                this.showSelectCreditCard = true;
                
                this.dataSource = new MatTableDataSource<Payment>(this.paymentsList);
                this.creditCardSelectChange(this.paymentsList[0])
              }

              
          }
      })

    }
    
  }


  public _getBusinessPayments(id) {
    
    if(id){
      
      this.showSelectCreditCard = false;
      this.paymentsList = [];
      
      this.paymentsService.getPaymentsByBrand(id)
        .subscribe(result=>{
          if (result.content != null) {
              result.content.forEach(element => {
                if (element.type == 'CREDIT_CARD' || element.type == 'DEBIT_CARD') {
                  this.paymentsList.push(element);
                } else if (element.type == 'BOOKLET'){
                  this.paymentBooklet = element
                }
              });

              if(this.paymentsList.length > 0){
                this.showSelectCreditCard = true;
                
                this.dataSource = new MatTableDataSource<Payment>(this.paymentsList);
                this.creditCardSelectChange(this.paymentsList[0])
              }

              
          }
      })

    }
    
  }

  addCreditCard(type) {

    let id = this.appContext.session.user.id;

    let _data = null;

    if(this.customerSelected){
      id = this.customerSelected.user;
      _data = { user :  id, type: type}
      this._getUserPayments(id)
      if(!this.customerSelected.id){
        this.showSelectCreditCard = false;
      }
    }else{
      if(this.isBusiness){
        _data = { business : this.appContext.session.user.brand,type: type}
        this._getBusinessPayments(this.appContext.session.user.brand)
      }else{
        _data = { user :  id, type: type}
      }
    }

    this.dialog
    .open(NewCreditCardComponent, { width:'1100px',height:'550px', data: _data })
    .afterClosed()
    .subscribe(response => {
    
      if(response != undefined){
          this.paymentSelected = response.data
          this.paymentsList.push(this.paymentSelected)
          
          if(this.paymentsList.length > 0){
            this.showSelectCreditCard = true;
            
            this.dataSource = new MatTableDataSource<Payment>(this.paymentsList);
            this.creditCardSelectChange(this.paymentsList[0])
          }
      }
      
    })
  }

  creditCardSelectChange(event) {

    this.creditCardForm.patchValue ({
      creditCardSelected: event || null
    })

    this.selectedRowIndex = event.id;

    this.cardType = event.type
    this.typePayment = this.cardType

    this.createInstallmentList(this.installmentNumber)
    
    if(this.creditCardForm && this.creditCardForm.value && this.creditCardForm.value.creditCardSelected && (!this.paymentSelected || this.paymentSelected.id != this.constant_id)){
      
      this.paymentSelected = new Payment()
      this.paymentSelected.id = this.creditCardForm.value.creditCardSelected.id
      
    }

  }
}
