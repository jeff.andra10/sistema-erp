import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseLoginComponent } from './login.component';
import { MaterialModule } from '../../angular-material/material.module';
import { CommonModule } from '@angular/common';
import { FirstAccessComponent } from './first-access/first-access.component';


const routes = [
    {
        path     : 'login',
        component: FuseLoginComponent,
    }
];

@NgModule({
    declarations: [
        FuseLoginComponent,
        FirstAccessComponent
    ],
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule,
        CommonModule,
        MaterialModule
    ],
    exports: [
        FirstAccessComponent
    ],
    entryComponents: [
        FirstAccessComponent
    ]
})
export class LoginModule{}
