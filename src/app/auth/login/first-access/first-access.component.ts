import { Component, Inject, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { FuseConfig } from '@fuse/types';
import { UserService } from '../../../core/services/users.service';
import { BaseComponent } from '../../../_base/base-component.component';

@Component({
  selector: 'tnm-first-access-product',
  templateUrl: './first-access.component.html',
  styleUrls: ['./first-access.component.scss'],
})
export class FirstAccessComponent extends BaseComponent implements OnInit {

  contractForm: FormGroup;
  suppliers: object[];
  brandList: any[];
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  primaryColor: string = localStorage.getItem('primaryColorBrand');
  contract: string;
  firstAccess: boolean;


  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private fuseConfig: FuseConfigService,
    private dialogRef: MatDialogRef<FirstAccessComponent>,
    private snackbar: MatSnackBar
  ) {
    super(injector);
  }

  ngOnInit() {
    this.firstAccess = this.data.firstAccess;
    this.contract = this.data.term.replace(/<(?:.|\n)*?>/gm, '');
    this.createForm();
  }

  createForm() {
    this.contractForm = this.formBuilder.group({
      contract: this.formBuilder.control({ value: this.contract, disabled: true }),
      check: this.formBuilder.control('', [Validators.required])
    });
  }

  activiteContract() {

    const editUser = [{ 'op': 'add', 'path': '/terms', 'value': 'true' }];
    this.userService.update(this.appContext.loggedUser.id, editUser)
                    .subscribe(result => { this.loadDashboard(); });

  }

  cancelContract() {
    this.router.navigate(['login']);
    this.dialogRef.close();
  }

  loadDashboard() {
    this.setColorBrand();

    if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile === 'OPERATOR') {
      this.router.navigate(['sales']);
    } else if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile === 'BROKER') {
      this.router.navigate(['insurance/quotes']);
    } else {
      this.router.navigate(['dashboard']);
    }

    this.dialogRef.close()
  }

  closeDialog() {
    this.dialogRef.close();
    if (this.data.firstAccess) {
      this.router.navigate(['login']);
    }
  }

  setColorBrand() {

    const fuse: FuseConfig = {
      layout: {
        style: 'vertical-layout-1',
        width: 'fullwidth',
        navbar: {
          hidden: false,
          position: 'left',
          folded: false,
          background: this.primaryColor
        },
        toolbar: {
          hidden: false,
          position: 'below-static',
          background: this.primaryColor
        },
        footer: {
          hidden: false,
          position: 'below-static',
          background: this.primaryColor
        }
      },
      customScrollbars: true
    };

    this.fuseConfig.setConfig(fuse);
    this.fuseConfig.setDefault(fuse);
  }

}
