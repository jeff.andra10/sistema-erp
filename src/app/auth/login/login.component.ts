import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Title } from '@angular/platform-browser';

import { FuseConfig } from '@fuse/types';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';

import { Subject } from 'rxjs';

import { AppContext } from '../../core/appcontext';
import { UtilsService } from '../../core/utils.service';
import { Brand } from '../../core/models/brand.model';
import { SessionModel } from '../../core/models/session.model';
import { DocumentsService } from '../../core/services/documents.service';
import { SessionService } from '../../core/services/session.service';
import { PalletColors } from '../../../app/core/models/pallet.model';
import { BrandsService } from '../../../app/core/services/brands.service';

import { FirstAccessComponent } from './first-access/first-access.component';

declare var require: any;

@Component({
    selector: 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: fuseAnimations
})
export class FuseLoginComponent implements OnInit, OnDestroy {
    allBrands: Array<object> = [];
    loginForm: FormGroup;
    loginFormErrors: any;
    session: SessionModel;
    primaryColor: string;
    terms: any;
    aliasArray = [];
    version: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    urlLogo: String = 'assets/images/logos/fuse.svg';

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private router: Router,
        private snackbar: MatSnackBar,
        private sessionService: SessionService,
        private brandsService: BrandsService,
        private titleService: Title,
        private dialog: MatDialog,
        private documentsService: DocumentsService,
        private appContext: AppContext,
        private loadingService: FuseSplashScreenService,
        private utilsService: UtilsService
    ) {

        this.fuseConfig.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                }
            }
        };

        this.loginFormErrors = {
            email: {},
            password: {}
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();

    }

    ngOnInit() {

        const appVersion = require('../../../../version.json');

        if (appVersion.version.indexOf('development') !== -1) {
            this.version = appVersion.version.replace('-development', '');
        } else if (appVersion.version.indexOf('master') !== -1) {
            this.version = appVersion.version.replace('-master', '');
        }

        localStorage.setItem('hiddenLoading', 'true');


        const href = this.router['location']['_platformStrategy']['_platformLocation']['_doc']['baseURI'];

        const hrefArray = href.split('//');
        this.aliasArray = hrefArray[1].split('.');

        if (this.aliasArray[0] !== 'localhost:4200/') {
            this.getBrandByAlias(this.aliasArray[0]);
        } else {
            this.getBrandByAlias('dev');
        }

        this.urlLogo = localStorage.getItem('logoBrand');

        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.loginForm.valueChanges.subscribe(() => {
            this.onLoginFormValuesChanged();
        });


    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onLoginFormValuesChanged() {
        for (const field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }

    login() {

        this.loadingService.show();

        const loginBody = {
            'login': this.loginForm.value.email,
            'password': this.loginForm.value.password,
            'persistent': false,
            'type': 'DEFAULT'
        };

        this.sessionService.create(loginBody)
            .subscribe(
                session => {
                    console.log(session, 'session');
                    

                    this.session = session;
                    this.snackbar.open(`Bem vindo(a) ${session.user.name}`, '', {
                        duration: 8000
                    });

                },
                response => {
                    
                    this.loadingService.hide();
                    this.snackbar.open(response.error.message, '', {
                        duration: 8000
                    });
                },
                () => {

                    this.appContext.initContext(this.session);

                    if (this.appContext && this.appContext.loggedUser &&
                        this.appContext.loggedUser.profile === 'OPERATOR'
                            || this.appContext.loggedUser.profile === 'BROKER' ||
                                this.appContext.loggedUser.profile === 'ADMINISTRATOR') {

                        this.getBrands();

                        if (this.appContext && this.appContext.isLoggedUserTracknme()) {
                            localStorage.setItem('isUserTracknme', 'true');
                        } else {
                            localStorage.setItem('isUserTracknme', 'false');
                        }

                        if (this.session.user.terms) {

                            if (this.aliasArray[0] === 'localhost:4200/' || this.aliasArray[0] === 'dev') {
                                this.loadConfig(true);
                            } else {
                                this.setColorBrand();
                            }

                        } else {

                            if (this.aliasArray[0] === 'localhost:4200/' || this.aliasArray[0] === 'dev') {
                                this.loadConfig(false);
                            }

                            this.firstAccessDialog();
                        }
                    } else {
                        this.loadingService.hide();
                        this.snackbar.open('Esse perfil não possui acesso ao sistema', '', {
                            duration: 8000
                        });
                    }

                }
            );

    }

    loadConfig(status: boolean) {

        this.brandsService.getBrands().subscribe(brands => {

            let indexBrandUser = 0;

            if (this.appContext && this.appContext.brandTracknme && this.appContext.loggedUser && this.appContext.loggedUser.brand && brands.content.length > 0) {
                indexBrandUser = brands.content.map((item: { id: any; }) => item.id).indexOf(this.appContext.loggedUser.brand);
                this.setTitle(brands.content[indexBrandUser].name + ' - ERP');
            } else {
                indexBrandUser = brands.content.map((item: { id: any; }) => item.id).indexOf(6137206846521344);
            }

            const brandPallet = brands.content[indexBrandUser].palette ? brands.content[indexBrandUser].palette : 'blue-grey';
            const logoBrand = brands.content[indexBrandUser]['logo'];

            if (logoBrand && logoBrand != undefined) {
                localStorage.setItem('logoBrand', logoBrand);
            } else {
                localStorage.setItem('logoBrand', 'assets/images/logos/logo-tracknme.svg');
            }

            this.urlLogo = localStorage.getItem('logoBrand');

            this.primaryColor = PalletColors[brandPallet] + '-A700-bg';
            const secondColorBrand = PalletColors[brandPallet] + '-A400-bg';

            localStorage.setItem('primaryColorBrand', this.primaryColor);
            localStorage.setItem('secondColorBrand', secondColorBrand);

            if (status) {
                this.setColorBrand();
            } else {

                if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'OPERATOR') {
                    this.router.navigate(['sales']);
                } else if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'BROKER') {
                    this.router.navigate(['insurance/quotes']);
                } else {
                    this.router.navigate(['dashboard']);
                }

            }

        });

    }

    setColorBrand() {

        const fuse: FuseConfig = {
            layout: {
                style: 'vertical-layout-1',
                width: 'fullwidth',
                navbar: {
                    hidden: false,
                    position: 'left',
                    folded: false,
                    background: this.primaryColor
                },
                toolbar: {
                    hidden: false,
                    position: 'below-static',
                    background: this.primaryColor
                },
                footer: {
                    hidden: false,
                    position: 'below-static',
                    background: this.primaryColor
                }
            },
            customScrollbars: true
        };

        this.fuseConfig.setConfig(fuse);
        this.fuseConfig.setDefault(fuse);

        if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'OPERATOR') {
            this.router.navigate(['sales']);
        } else if (this.appContext && this.appContext.loggedUser && this.appContext.loggedUser.profile == 'BROKER') {
            this.router.navigate(['insurance/quotes']);
        } else {
            this.router.navigate(['dashboard']);
        }


    }

    getBrandByAlias(alias) {

        this.brandsService.getBrandByAlias(alias).subscribe(brand => {

            if (brand) {

                let brandPallet = brand.palette ? brand.palette : 'blue-grey';
                let logoBrand = brand.logo;

                this.setTitle(brand.name + ' - ERP');

                if (logoBrand && logoBrand != undefined) {
                    localStorage.setItem('logoBrand', logoBrand);
                } else {
                    localStorage.setItem('logoBrand', 'assets/images/logos/logo-tracknme.svg');
                }

                this.urlLogo = localStorage.getItem('logoBrand');

                this.primaryColor = PalletColors[brandPallet] + '-A700-bg';
                let secondColorBrand = PalletColors[brandPallet] + '-A400-bg';

                localStorage.setItem('primaryColorBrand', this.primaryColor);
                localStorage.setItem('secondColorBrand', secondColorBrand);
            }

        });

    }

    //TODO: mover para classe de inicializacao do app
    getBrands() {

        if (this.appContext.brands != null && this.appContext.brands.length > 0) {
            return;
        }

        this.brandsService.getBrandsByTree('', this.appContext.loggedUser.brand)
            .subscribe(brands => {
                if (brands.content && brands.content.length > 0) {
                    this.makeBrandList(brands.content);
                    this.appContext.brands = <[Brand]>this.allBrands;
                }
            });
    }

    //TODO: mover para classe de inicializacao do app
    private makeBrandList(brandList) {

        let filteredBrands: Array<object>;

        brandList.forEach(brand => {
            this.allBrands.push(brand);
            if (brand.brandChildren) {
                this.makeBrandList(brand.brandChildren);
            }
        });
        this.utilsService.arrayRemoveDuplicates(this.allBrands, 'id');
    }

    public setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }

    firstAccessDialog() {
        this.loadingService.hide();
        this.documentsService.getDocumentsTerms().subscribe(terms => {
            this.dialog
                .open(FirstAccessComponent, {
                    width: '800px',
                    data: {
                        title: 'Termos do Primeiro Acesso',
                        term: terms.term,
                        firstAccess: true
                    }
                });
        });
    }

}
