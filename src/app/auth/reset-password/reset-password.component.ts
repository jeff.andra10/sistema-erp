import { Component, OnInit, OnDestroy } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';

import { UserService } from '../../core/services/users.service'
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector   : 'fuse-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls  : ['./reset-password.component.scss'],
    animations : fuseAnimations
})
export class FuseResetPasswordComponent implements OnInit, OnDestroy
{
    resetPasswordForm: FormGroup;
    resetPasswordFormErrors: any;

     // Private
     private _unsubscribeAll: Subject<any>;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private snackbar: MatSnackBar,
        private router: Router
    )
    {
        
        this.fuseConfig.config = {
            layout: {
                navbar : {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer : {
                    hidden: true
                }
            }
        };

        this.resetPasswordFormErrors = {
            password       : {},
            passwordConfirm: {}
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this.resetPasswordForm = this.formBuilder.group({
            password       : ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPassword]]
        });

        this.resetPasswordForm.valueChanges.subscribe(() => {
            this.onResetPasswordFormValuesChanged();
        });
    }

     /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onResetPasswordFormValuesChanged()
    {
        for ( const field in this.resetPasswordFormErrors )
        {
            if ( !this.resetPasswordFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.resetPasswordFormErrors[field] = {};

            // Get the control
            const control = this.resetPasswordForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.resetPasswordFormErrors[field] = control.errors;
            }
        }
    }

    recoveryPassword(){

        let href = this.router['location']['_platformStrategy']['_platformLocation']['_doc']['baseURI'];
        
        let token = href.split("=");
      
        let body = {
            "token": token[1],
            "password": this.resetPasswordForm.value.password,
            "confirmPassword": this.resetPasswordForm.value.confirmPassword,
          }
 
         this.userService.changePassword(body)
         .subscribe(result => {
             
             this.snackbar.open('Senha atualizada com sucesso', '', {
                 duration: 5000
             })
 
             this.router.navigate(['/login']);
         })
     }
}

function confirmPassword(control: AbstractControl)
{
    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}
