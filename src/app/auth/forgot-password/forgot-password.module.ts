import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';

import { FuseForgotPasswordComponent } from './forgot-password.component';
import { MaterialModule } from '../../angular-material/material.module';
import { CommonModule } from '@angular/common';

const routes = [
    {
        path     : 'forgot-password',
        component: FuseForgotPasswordComponent
    }
];

@NgModule({
    declarations: [
        FuseForgotPasswordComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MaterialModule,
        CommonModule,

        FuseSharedModule,
    ]
})
export class ForgotPasswordModule
{
}
