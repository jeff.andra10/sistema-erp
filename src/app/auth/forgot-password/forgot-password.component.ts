import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { UserService } from '../../core/services/users.service'
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
    selector: 'fuse-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    animations: fuseAnimations
})
export class FuseForgotPasswordComponent implements OnInit, OnDestroy {
    forgotPasswordForm: FormGroup;
    forgotPasswordFormErrors: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private snackbar: MatSnackBar,
        private router: Router
    ) {

        this.fuseConfig.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                }
            }
        };


        this.forgotPasswordFormErrors = {
            email: {}
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
        this.forgotPasswordForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });

        this.forgotPasswordForm.valueChanges.subscribe(() => {
            this.onForgotPasswordFormValuesChanged();
        });
    }

    /**
    * On destroy
    */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onForgotPasswordFormValuesChanged() {
        for (const field in this.forgotPasswordFormErrors) {
            if (!this.forgotPasswordFormErrors.hasOwnProperty(field)) {
                continue;
            }

            // Clear previous errors
            this.forgotPasswordFormErrors[field] = {};

            // Get the control
            const control = this.forgotPasswordForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.forgotPasswordFormErrors[field] = control.errors;
            }
        }
    }

    recoveryPassword() {

        const body = {
            'login': this.forgotPasswordForm.value.email,
            'expiryTime': 4320
        };

        this.userService.passwordRecovery(body)
            .subscribe(result => {

                this.snackbar.open('Um email com instruções de recuperação foi enviado para ' + this.forgotPasswordForm.value.email, '', {
                    duration: 5000
                });

                this.router.navigate(['/login']);
            });
    }
}
