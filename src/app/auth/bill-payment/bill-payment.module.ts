import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { BillPayment } from './bill-payment.component';


const routes = [
    {
        path     : 'bill/payment',
        component: BillPayment
    }
];

@NgModule({
    declarations: [
        BillPayment
    ],
    imports     : [
        RouterModule.forChild(routes),

        FuseSharedModule
    ]
})
export class BillPaymentModule{}
