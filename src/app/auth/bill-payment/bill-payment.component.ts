import { Component } from '@angular/core';

import { FuseConfigService } from '@fuse/services/config.service';

@Component({
    selector   : 'bill-payment',
    templateUrl: './bill-payment.component.html',
    styleUrls  : ['./bill-payment.component.scss']
})
export class BillPayment
{
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     */
    constructor(
        private _fuseConfigService: FuseConfigService
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar : {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer : {
                    hidden: true
                }
            }
        };
    }
}
