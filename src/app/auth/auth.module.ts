import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MaterialModule } from '../angular-material/material.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseLoginComponent } from './login/login.component';
import { FuseForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { ResetPasswordModule } from './reset-password/reset-password.module'
import { LoginModule } from './login/login.module';
import { BillPaymentModule } from './bill-payment/bill-payment.module'
const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'login', component: FuseLoginComponent }
    //{ path: '', redirectTo:'login', pathMatch: 'full' }
    //{ path: '**', redirectTo: '' }
  ]);

@NgModule({
    declarations: [
    ],
    imports     : [
        authRouting,
        MaterialModule,
        FuseSharedModule,
        LoginModule,
        ForgotPasswordModule,
        BillPaymentModule,
        ResetPasswordModule
    ]
})
export class AuthModule {}