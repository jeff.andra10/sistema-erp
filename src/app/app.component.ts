import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { FuseConfig } from '@fuse/types';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';

import { TranslateService } from '@ngx-translate/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

import { locale as navigationEnglish } from 'app/navigation/i18n/en';
import { locale as navigationTurkish } from 'app/navigation/i18n/tr';
import {
  navigation,
  navigationAssociation,
  navigationBroker,
  navigationOperator
} from 'app/navigation/navigation';

import { AppContext } from './core/appcontext';

@Component({
  selector: 'fuse-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  navigation: any;
  fuseConfig: any;

  // Private
  private _unsubscribeAll: Subject<any>;
  appContextOnInitWatcher: Subscription;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FuseNavigationService} _fuseNavigationService
   * @param {FuseSidebarService} _fuseSidebarService
   * @param {FuseSplashScreenService} _fuseSplashScreenService
   * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
   * @param {TranslateService} _translateService
   * @param {Router} _router
   * @param {AppContext} _appContext
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _fuseNavigationService: FuseNavigationService,
    private _fuseSidebarService: FuseSidebarService,
    private _fuseSplashScreenService: FuseSplashScreenService,
    private _fuseTranslationLoaderService: FuseTranslationLoaderService,
    private _translateService: TranslateService,
    private _router: Router,
    private _appContext: AppContext
  ) {
    this.navigation = navigationAssociation;
    // Register the navigation to the service
    this._fuseNavigationService.register('main', this.navigation);

    // Set the main navigation as our current navigation
    this._fuseNavigationService.setCurrentNavigation('main');

    // Add languages
    this._translateService.addLangs(['en', 'tr']);

    // Set the default language
    this._translateService.setDefaultLang('en');

    // Set the navigation translations
    this._fuseTranslationLoaderService.loadTranslations(
      navigationEnglish,
      navigationTurkish
    );

    // Use a language
    this._translateService.use('en');

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this._fuseConfigService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config: any) => {
        console.log(config, 'config');

        this.fuseConfig = config;
      });
  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngAfterViewInit(): void {
    if (this._appContext.isLoggedIn()) {
      this.loadMenu();
    }

    // Watch context init
    this.appContextOnInitWatcher = this._appContext.onContextInit.subscribe(
      session => {
        if (session != null) {
          this.loadMenu();
        }
      }
    );
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggle sidebar open
   *
   * @param key
   */
  toggleSidebarOpen(key: any): void {
    this._fuseSidebarService.getSidebar(key).toggleOpen();
  }

  private loadMenu() {
    // Get default navigation

    if (
      this._appContext &&
      this._appContext.loggedUser &&
      this._appContext.loggedUser.profile === 'BROKER'
    ) {
      this.navigation = navigationBroker;
    } else if (
      this._appContext &&
      this._appContext.loggedUser &&
      this._appContext.loggedUser.profile === 'OPERATOR'
    ) {
      this.navigation = navigationOperator;
    } else if (this._appContext && this._appContext.isLoggedUserTracknme()) {
      this.navigation = navigation;
    } else {
      this.navigation = navigationAssociation;
    }

    // UnRegister the navigation to the service
    this._fuseNavigationService.unregister('main');
    // Register the navigation to the service
    this._fuseNavigationService.register('main', this.navigation);

    // Set the main navigation as our current navigation
    this._fuseNavigationService.setCurrentNavigation('main');

    const _routerUrl = this._router['location']['_platformStrategy'][
      '_platformLocation'
    ]['location']['pathname'];

    if (
      _routerUrl !== '/login' &&
      _routerUrl !== '/' &&
      _routerUrl !== '/erp/index.html' &&
      _routerUrl !== '/erp/login'
    ) {
      this.setColorBrand();
    }
  }

  setColorBrand() {
    let primaryColor = localStorage.getItem('primaryColorBrand');

    if (!primaryColor || primaryColor === undefined) {
      primaryColor = 'mat-fuse-dark-700-bg';
    }

    const fuse: FuseConfig = {
      layout: {
        style: 'vertical-layout-1',
        width: 'fullwidth',
        navbar: {
          hidden: false,
          position: 'left',
          folded: false,
          background: primaryColor
        },
        toolbar: {
          hidden: false,
          position: 'below-static',
          background: primaryColor
        },
        footer: {
          hidden: false,
          position: 'below-static',
          background: primaryColor
        }
      },
      customScrollbars: true
    };

    this._fuseConfigService.setConfig(fuse);
    this._fuseConfigService.setDefault(fuse);
  }
}
