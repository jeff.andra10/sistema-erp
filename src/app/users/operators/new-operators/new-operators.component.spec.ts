import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewOperatorsComponent } from './new-operators.component';

describe('NewOperatorsComponent', () => {
  let component: NewOperatorsComponent;
  let fixture: ComponentFixture<NewOperatorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewOperatorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewOperatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
