import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../angular-material/material.module";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "@fuse/shared.module";
import { CustomerPersonalComponent } from "../../customers/customer-personal/customer-personal.component";
import { CustomersModule } from "../../customers/customers.module";
import { OperatorsComponent } from "../operators/operators.component";
import { NewOperatorsComponent } from "../operators/new-operators/new-operators.component";
import { EditOperatorsComponent } from "../operators/edit-operators/edit-operators.component"
const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'users/operators/new', component: NewOperatorsComponent },
    { path: 'users/operators/:id', component: EditOperatorsComponent }
]);

@NgModule({
    declarations:[
        OperatorsComponent,
        NewOperatorsComponent,
        EditOperatorsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        TextMaskModule,
        FlexLayoutModule,
        RouterModule,
        authRouting,
        CustomersModule
    ],
    exports: [

    ],
    entryComponents: [
        CustomerPersonalComponent,
        
    ],
    providers: [
        
    ]
})
export class OperatorsModule {}