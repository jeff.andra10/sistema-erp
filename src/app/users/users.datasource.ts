import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";

import { UtilsService } from "app/core/utils.service";
import { PaginatedContent } from "app/core/models/paginatedContent.model";
import { User } from "../core/models/user.model";
import { UserService } from "../core/services/users.service";
import { Brand } from "../core/models/brand.model";

export class UsersDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<User[]>([]);
    private totalElementsSubject = new BehaviorSubject<number>(0)
    public totalElements$ = this.totalElementsSubject.asObservable()
   
    constructor(
        private userService : UserService, 
        private utilsService : UtilsService,
        private brands: Array<Brand>) 
    {}

    load(filter:string,
        status: string,
        profile: string,
        sortField:string,
        sortDirection:string,
        pageIndex:number,
        pageSize:number) {

        this.userService.findUsers(
            filter, 
            status,
            profile,
            sortField,
            sortDirection,
            pageIndex, 
            pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe((result : PaginatedContent<User>) => {

                if (result.content && this.brands) {
                    result.content.forEach(obj => {
                        obj.brandObj = this.utilsService.getItemById(obj.brand, this.brands)
                    });
                }
                
                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<User[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
