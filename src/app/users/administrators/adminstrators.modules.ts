import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../angular-material/material.module";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "@fuse/shared.module";
import { CustomerPersonalComponent } from "../../customers/customer-personal/customer-personal.component";
import { CustomersModule } from "../../customers/customers.module";
import { AdminstratorsComponent } from "../administrators/administrators.component";
import { NewAdminstratorsComponent } from "../administrators/new-administrators/new-adminstrators.component";
import { EditAdminstratorsComponent } from "../administrators/edit-adminstrators/edit-adminstrators.component"

const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'users/administrators/new', component: NewAdminstratorsComponent },
    { path: 'users/administrators/:id', component: EditAdminstratorsComponent }
]);

@NgModule({
    declarations:[
        AdminstratorsComponent,
        NewAdminstratorsComponent,
        EditAdminstratorsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        TextMaskModule,
        FlexLayoutModule,
        RouterModule,
        authRouting,
        CustomersModule
    ],
    exports: [

    ],
    entryComponents: [
        CustomerPersonalComponent,
        
    ],
    providers: [
        
    ]
})
export class AdminstratorsModule {}