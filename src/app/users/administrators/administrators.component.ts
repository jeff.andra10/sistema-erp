import { Component, OnInit, Injector, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';
import { MatTableDataSource, MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA, MatSort, MatPaginator} from '@angular/material';
import { Router } from '@angular/router';
import { BaseComponent } from '../../_base/base-component.component';
import { UserService } from '../../core/services/users.service';
import { CustomersService } from '../../core/services/customers.service';
import { UserStatus, UserColorStatus, UserColorStatusLight } from '../../core/models/user.model';
import { fuseAnimations } from '@fuse/animations';
import { Subject } from 'rxjs';
import { UsersDataSource } from "../users.datasource"
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";

@Component({
  selector: 'tnm-administrators',
  templateUrl: './administrators.component.html',
  styleUrls: ['./administrators.component.scss'],
  animations : fuseAnimations
})
export class AdminstratorsComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  brandList: Array<any>
  displayedColumns  = ['statusColor','space','name', 'status', 'email', 'brand' ]
  dataSource: UsersDataSource
  
  resultLength: number = 0

  searchSubject = new Subject()
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private userService: UserService,
    private customersService: CustomersService,
    private loadingService: FuseSplashScreenService,
    private router: Router,
  ) { 
    super(injector)
    
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  selectCustomer(row){
    this.loadingService.show()
    this.router.navigate(['/users/administrators/', row.id])
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading','false');
    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadCustomers(val)
    })

    this.brandList = this.appContext.brands

    this.dataSource = new UsersDataSource(this.userService, this.utilsService, this.brandList)
    this.dataSource.load('', '', 'ADMINISTRATOR','name', 'asc', 0, 10)   
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    if(this.dataSource && this.dataSource.totalElements$){
      this.dataSource.totalElements$.subscribe( value => {
        this.resultLength = value
      })
    }
    
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadCustomers())
    )
    .subscribe()
  }

  loadCustomers(filter? : string) {

    this.dataSource.load(
      filter, 
      '',
      'ADMINISTRATOR',
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  }

  createAdmin(){
    this.router.navigate(['/administrators/new'])
  }

  userStatusEnumToString(status) : string {
    return UserStatus[status]
  }

  userColorStatusEnumToString(value) : string {
    return UserColorStatus[value]
  }

  userColorStatusLightEnumToString(value) : string {
    return UserColorStatusLight[value]
  }
  
}