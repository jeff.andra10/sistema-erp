import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminstratorsComponent } from './administrators.component';

describe('AdminstratorsComponent', () => {
  let component: AdminstratorsComponent;
  let fixture: ComponentFixture<AdminstratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminstratorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminstratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
