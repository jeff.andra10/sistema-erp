import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAdminstratorsComponent } from './new-adminstrators.component';

describe('NewAdminstratorsComponent', () => {
  let component: NewAdminstratorsComponent;
  let fixture: ComponentFixture<NewAdminstratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAdminstratorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAdminstratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
