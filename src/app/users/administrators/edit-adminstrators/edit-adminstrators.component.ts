import { Component, OnInit, Injector } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder, AbstractControl } from '@angular/forms';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerPersonalComponent } from '../../../customers/customer-personal/customer-personal.component';
import { Customer } from '../../../core/models/customer.model';
import { UserStatus } from '../../../core/models/user.model'
import { UserService } from '../../../core/services/users.service';
import { UtilsService } from '../../../core/utils.service';
import { BaseComponent } from 'app/_base/base-component.component';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";

@Component({
  selector: 'tnm-edit-adminstrators',
  templateUrl: './edit-adminstrators.component.html',
  styleUrls: [ './edit-adminstrators.component.scss' ]
})
export class EditAdminstratorsComponent extends BaseComponent implements OnInit, AfterViewInit {
  
  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  generalForm: FormGroup
  customer: Customer
  userId: number
  statusList: any
  
  constructor(
    private injector: Injector,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private snackbar: MatSnackBar,
    private loadingService: FuseSplashScreenService,
    private router: Router,
    private utils: UtilsService
  ) {
    super(injector)
   }

  ngOnInit() {
    localStorage.setItem('hiddenLoading','true');
    this.loadingService.show()
    this.userId = this.activatedRoute.snapshot.params['id']
    this.loadCustomerData()
    this.loadDomainListData()
  }

  loadDomainListData() {
    this.initiForm()
    this.statusList = this.utilsService.stringEnumToKeyValue(UserStatus)
  }

  initiForm(){
    this.generalForm = this.formBuilder.group({
      status:this.formBuilder.control(''),
      login:this.formBuilder.control('',[Validators.required]),
      password:this.formBuilder.control('',[Validators.required, Validators.minLength(8)]),
      confirmPassword:this.formBuilder.control('',[Validators.required, checkConfirmPassword])
    })
  }

  ngAfterViewInit() {

    setTimeout(() => {
      this.customerPersonalComponent.setHideFields()
    }, 10)
    
  }

  cancel() {
    this.router.navigate(['/users/adminstrators'])
  }

  isCustomerInvalid()  {
    return this.customerPersonalComponent.isInvalid()        
  }

  updateCustomer() {
    
    let updatePassword = false;
    if(this.generalForm.value.password && this.generalForm.value.password.length > 0){
      
      if(this.generalForm.value.password.length < 8){
        this.snackbar.open('A senha deve ter no mínimo 8 caracteres.', '', { duration: 6000 });
        updatePassword = false;
        return;
      }

      if(this.generalForm.value.confirmPassword && this.generalForm.value.confirmPassword.length > 0){

        updatePassword = true;
        if(this.generalForm.value.confirmPassword != this.generalForm.value.password){
          this.snackbar.open('As senhas devem ser iguais.', '', { duration: 6000 });
          updatePassword = false;
          return;
        }
      }else{
        updatePassword = false;
        this.snackbar.open('A confimação de senha deve ser  preenchida.', '', { duration: 6000 });
        return;
      }

    }

    this.loadingService.show()

    let personalData = this.customerPersonalComponent.getCustomerData()
     
    let editData = []
        
    let editItem = {'op': 'add', 'path': '/name', 'value': personalData.name};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/mobile', 'value': this.utils.stringToNumber(personalData.mobilePhone)};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/email', 'value': personalData.email};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/login', 'value': this.generalForm.value.login};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/cpfCnpj', 'value': this.utils.stringToNumber(personalData.registrationNumber)};
        editData.push(editItem);

    this.userService.update(this.userId,editData)
    .subscribe(response => {
      this.snackbar.open('Administrador atualizado com sucesso', '', { duration: 8000 })
      
        if(updatePassword){
            this.changePassword()
        }else{
            this.router.navigate(['/users/adminstrators'])
        }
      },
      response => {
        this.loadingService.hide()
        this.snackbar.open(response.error.message,'', { duration: 8000 })
    })
  }

  isInvalid() {
    return this.generalForm ? this.generalForm.invalid : false
  }

  onListValueChanged(status){

    this.loadingService.show()

    let editData = []
        
    let editItem = {'op': 'add', 'path': '/status', 'value': status};
        editData.push(editItem);
        

    this.userService.update(this.userId,editData)
    .subscribe(response => {
      this.snackbar.open('Administrador atualizado com sucesso', '', { duration: 8000 })
      this.loadingService.hide()

      },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000 })
        this.loadingService.hide()

    })

  }

  convertDate(date){
    if(date){
      let  _date = date.split('/');
          _date =_date[1] +'-'+_date[0]+'-'+_date[2]
      return new Date(_date)
    }
      
  }

  changePassword(){
  
    let body = {
        "token": '',
        "user":this.userId,
        "password": this.generalForm.value.password,
        "confirmPassword": this.generalForm.value.confirmPassword,
      }

     this.userService.changePassword(body)
     .subscribe(result => {
        this.router.navigate(['/users/adminstrators'])
     })
 }

  loadCustomerData() {

    this.userService.getOne(this.userId)
    .subscribe(user => {

      let customer = new Customer

      customer.name = user.name
      customer.email = user['email']
      customer.registrationNumber = this.utils.stringToNumber(user.cpfCnpj)
      customer.mobilePhone = this.utils.stringToNumber(user.mobile)
      customer.personType = 'PERSON'

      this.generalForm.patchValue({
        'status':user['status'],
        'login': user.login
      })

      this.customerPersonalComponent.loadCustomerData(customer)
      this.loadingService.hide()
    })
  }

}

function checkConfirmPassword(control: AbstractControl)
{

    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('confirmPassword');


    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}
