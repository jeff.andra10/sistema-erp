import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAdminstratorsComponent } from './edit-adminstrators.component';

describe('EditAdminstratorsComponent', () => {
  let component: EditAdminstratorsComponent;
  let fixture: ComponentFixture<EditAdminstratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAdminstratorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAdminstratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
