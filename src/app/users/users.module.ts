import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { UsersSearchComponent } from "./users-search/users-search.component";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../angular-material/material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { FuseSharedModule } from "@fuse/shared.module";
import { RouterModule } from "@angular/router";
import { AdminstratorsComponent } from "./administrators/administrators.component";
import { TechnicalComponent } from "./technical/technical.component";
import { OperatorsComponent } from "./operators/operators.component";
import { CustomerPersonalComponent } from "../customers/customer-personal/customer-personal.component";
import { CustomersModule } from "../customers/customers.module";
import { AdminstratorsModule } from './administrators/adminstrators.modules';
import { OperatorsModule } from './operators/operators.modules';
import { TechnicalModule } from './technical/technical.modules';
import { BrokersModule } from './brokers/brokers.modules';
import { BrokersComponent } from './brokers/brokers.component'

const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'users',  
    children : [
        { path: 'adminstrators', component: AdminstratorsComponent },
        { path: 'installers', component: TechnicalComponent },
        { path: 'operators', component: OperatorsComponent },
        { path: 'brokers', component: BrokersComponent }
    ]
  }
    
  ]);

@NgModule({
    declarations:[
        UsersSearchComponent
    ],
    imports: [
        authRouting,
        CommonModule,
        TextMaskModule,
        MaterialModule,
        CustomersModule,
        ReactiveFormsModule,
        FuseSharedModule,
        AdminstratorsModule,
        OperatorsModule,
        TechnicalModule,
        BrokersModule,
       
    ],
    exports: [
        UsersSearchComponent
    ],
    entryComponents: [
        CustomerPersonalComponent
    ]
})
export class UsersModule {}