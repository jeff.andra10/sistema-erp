import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { Observable } from 'rxjs/Observable';
import { MatTableDataSource, MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { UserService } from 'app/core/services/users.service';
import {  ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'tnm-users-search',
  templateUrl: './users-search.component.html',
  styleUrls    : ['./users-search.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class UsersSearchComponent implements OnInit {

  searchForm: FormGroup
  searchField: FormControl
  usersList: Array<any>

  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  secondColorBrand : string = localStorage.getItem('secondColorBrand');

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UsersSearchComponent>,
    private usersService: UserService
  ) { }

  ngOnInit() {
    
    this.initForms()
  }

  initForms() {
    this.searchField = this.formBuilder.control('')

    this.searchForm = this.formBuilder.group({
      searchField: this.searchField
    });

    this.searchField.valueChanges
    .debounceTime(500)
    .distinctUntilChanged()
    .switchMap(searchTerm =>

      this.usersService.getUsersByProfileAndName("(ADMINISTRATOR,OPERATOR,INSTALLER)", searchTerm)
      .catch(error => Observable.from([])))
      .subscribe(users => {
        
        this.usersList = users.content
      })
  }

  setUser(value) {

    this.dialogRef.close({user: value})
  }
  
}