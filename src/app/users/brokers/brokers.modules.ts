import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../angular-material/material.module";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "@fuse/shared.module";
import { CustomerPersonalComponent } from "../../customers/customer-personal/customer-personal.component";
import { CustomersModule } from "../../customers/customers.module";
import { BrokersComponent } from './brokers.component';
import { NewBrokersComponent } from "../brokers/new-brokers/new-brokers.component";
import { EditBrokersComponent } from "../brokers/edit-brokers/edit-brokers.component"

const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'users/brokers/new', component: NewBrokersComponent },
    { path: 'users/brokers/:id', component: EditBrokersComponent }
]);

@NgModule({
    declarations:[
        BrokersComponent,
        NewBrokersComponent,
        EditBrokersComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        TextMaskModule,
        FlexLayoutModule,
        RouterModule,
        authRouting,
        CustomersModule
    ],
    exports: [

    ],
    entryComponents: [
        CustomerPersonalComponent,
        
    ],
    providers: [
        
    ]
})
export class BrokersModule {}