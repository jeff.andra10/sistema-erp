import { Component, OnInit, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { BaseComponent } from '../../_base/base-component.component';
import { fuseAnimations } from '@fuse/animations';
import { BrokersDataSource } from './brokers.datasource';
import { BrokersService } from '../../core/services/brokers.service';
//import { BrokerDetailsComponent } from './broker-details/broker-details.component';

@Component({
  selector: 'app-brokers',
  templateUrl: './brokers.component.html',
  styleUrls: ['./brokers.component.scss'],
  animations : fuseAnimations
})
export class BrokersComponent  extends BaseComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  brandList: Array<any>
  displayedColumns = ['name', 'email', 'registrationNumber'];
  dataSource: BrokersDataSource
  
  resultLength: number = 0
  searchSubject = new Subject()

  constructor(
    private injector: Injector,
    private brokersService: BrokersService,
    private router: Router,
    public dialog: MatDialog
  ) {
    super(injector)
   }

   searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  ngOnInit() {

    localStorage.setItem('hiddenLoading','false');
    this.brandList = this.appContext.brands
    
    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadData(val)
    })

    this.dataSource = new BrokersDataSource(this.brokersService)
    this.dataSource.load('', 'name', 'asc', 0, 10)
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    if(this.dataSource && this.dataSource.totalElements$){
      this.dataSource.totalElements$.subscribe( value => {
        this.resultLength = value
      })
    }
    
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadData())
    )
    .subscribe()
  }

  loadData(filter? : string) {

    this.dataSource.load(
      filter, 
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  } 

  selectCustomer(row){
    this.router.navigate(['/users/brokers/', row.id])
  }

}
