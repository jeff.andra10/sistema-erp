import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";

import { BrokersService } from "app/core/services/brokers.service";

export class BrokersDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private totalElementsSubject = new BehaviorSubject<number>(0)

    public totalElements$ = this.totalElementsSubject.asObservable()

    constructor(
        private brokersService : BrokersService) {
    }

    load(filter:string,
            sortField: string,
            sortDirection:string,
            pageIndex:number,
            pageSize:number) {

        this.brokersService.findBrokers(
            filter, 
            sortField,
            sortDirection,
            pageIndex, 
            pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe(result => {
               
                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
