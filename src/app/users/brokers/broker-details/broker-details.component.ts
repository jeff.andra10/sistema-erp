// import { Component, OnInit, Injector, Inject } from '@angular/core';
// import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
// import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
// import { BaseComponent } from 'app/_base/base-component.component';
// import { MaskUtils } from 'app/core/mask-utils';
// import { BrokersService } from 'app/core/services/brokers.service';
// import { updateLocale } from 'moment';
// import { ConfirmDialogComponent } from 'app/dialogs/confirm-dialog/confirm-dialog.component';


// @Component({
//   selector: 'app-broker-details',
//   templateUrl: './broker-details.component.html',
//   styleUrls: ['./broker-details.component.scss']
// })
// export class BrokerDetailsComponent extends BaseComponent implements OnInit {

//   dataForm: FormGroup
//   broker: any
//   isNewBroker : boolean = true
//   occupationsList : Array<any>

//   phoneMask = MaskUtils.phoneMask
//   cpfMask = MaskUtils.cpfMask
//   cnpjMask = MaskUtils.cnpjMask
  
//   constructor(
//     private injector: Injector,
//     private brokersService: BrokersService,
//     private formBuilder: FormBuilder,
//     private dialogRef: MatDialogRef<BrokerDetailsComponent>,
//     private deleteDialog: MatDialog,
//     private snackbar: MatSnackBar,
//     @Inject(MAT_DIALOG_DATA) private data: any,
//   ) { 
//     super(injector)

//     if (data != null) {
//       this.broker = data
//       this.isNewBroker = false
//     }
//   }

//   ngOnInit() {

//     this.initForms()

//     if (this.broker) {
//       this.loadData()
//     } else {
//       this.broker = {}
//     }
//   }

//   initForms() {
    
//     this.dataForm = this.formBuilder.group({
//       personType: this.formBuilder.control('PERSON', [Validators.required]),
//       registrationNumberPerson: this.formBuilder.control('', [Validators.required]),
//       registrationNumberCompany: this.formBuilder.control(''),
//       name: this.formBuilder.control('', [Validators.required]),
//       tradingName: this.formBuilder.control(''),
//       contactPerson: this.formBuilder.control(''),
//       email: this.formBuilder.control('', [Validators.required])
//     })

//     this.dataForm.valueChanges.subscribe((newForm) => {
      
//       if(newForm.personType == 'PERSON'){
//         this.dataForm.controls.tradingName.setValidators(null)
//         this.dataForm.controls.contactPerson.setValidators(null)
//         this.dataForm.controls.registrationNumberCompany.setValidators(null)
//         this.dataForm.controls.registrationNumberPerson.setValidators([Validators.required])
//       }else{
//         this.dataForm.controls.tradingName.setValidators([Validators.required])
//         this.dataForm.controls.contactPerson.setValidators([Validators.required])
//         this.dataForm.controls.registrationNumberPerson.setValidators(null)
//         this.dataForm.controls.registrationNumberCompany.setValidators([Validators.required])
//       } 
//     });
//   }

//   loadData() {

//     this.dataForm.patchValue ({
//       personType: this.broker.personType || null,
//       registrationNumberPerson: this.broker.registrationNumber || null,
//       registrationNumberCompany: this.broker.registrationNumber || null,
//       name: this.broker.name || null,
//       tradingName: this.broker.tradingName || null,
//       contactPerson: this.broker.contactPerson || null,
//       email: this.broker.email || null
//     })
//   }

//   save(body) {
    
   
    
//     this.broker.brand = this.appContext.session.user.brand
//     this.broker.name = body.name
//     this.broker.companyName = body.companyName
//     this.broker.tradingName = body.tradingName
//     this.broker.personType = body.personType
//     this.broker.registrationNumber = this.utilsService.stringOnlyDigits((body.personType == 'PERSON' ? body.registrationNumberPerson : body.registrationNumberCompany))
//     this.broker.contactPerson = body.contactPerson
//     this.broker.email = body.email
    
    
//     if (this.isNewBroker) {

//       this.brokersService.registerBroker(this.broker)
//         .subscribe(result => {
//           this.snackbar.open('Corretor criado com sucesso', '', {
//             duration: 8000
//           })
//           this.dialogRef.close()
//         }, error => {
//           console.log(error)
//           this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
//         })

//     } else {
     
//       this.updateBroker(this.broker)
//     }
//   }

//   delete() {

//     this.deleteDialog.open(ConfirmDialogComponent, 
//       { 
//         panelClass: 'event-form-dialog',
//         data: { 
//           width: '600px',
//           height: '300px',
//           title: 'Confirmação', 
//           message: `Deseja excluir o corretor ${this.broker.name}?` 
//         }
//       })
//       .afterClosed()
//       .subscribe(response=>{

//         if (response.data.confirm == true) {
//           this.brokersService.deleteBroker(this.broker.id).subscribe(result => {
//             this.snackbar.open('Corretor excluído com sucesso', '', {
//               duration: 8000
//             })
//             this.dialogRef.close()
//           })
//         }
//       });

//   }

//   updateBroker(editForm) {
//     let editData = []
    
//     for(let item in editForm){
//       let editItem = {'op': 'add', 'path': '/'+item, 'value': editForm[item]};
//       editData.push(editItem);
//     }
    
//     this.brokersService.update(this.broker.id, editData)
//       .subscribe(device => {
//         this.snackbar.open('Corretor atualizado com sucesso', '', {
//           duration: 8000
//         })
//         this.dialogRef.close()
//       })
//   }
// }
