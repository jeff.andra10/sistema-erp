import { Component, OnInit, Injector } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder, AbstractControl } from '@angular/forms';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { CustomerPersonalComponent } from '../../../customers/customer-personal/customer-personal.component';
import { Customer } from '../../../core/models/customer.model';
import { MaskUtils } from 'app/core/mask-utils';
import { UtilsService } from '../../../core/utils.service';
import { UserService } from '../../../core/services/users.service';
import { BaseComponent } from 'app/_base/base-component.component';
import { BrokersService } from 'app/core/services/brokers.service';

@Component({
  selector: 'tnm-new-brokers',
  templateUrl: './new-brokers.component.html',
  styleUrls: ['./new-brokers.component.scss']
})
export class NewBrokersComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  customer: Customer

  dataForm: FormGroup
  broker: any
  isNewBroker: boolean = true
  occupationsList: Array<any>

  phoneMask = MaskUtils.phoneMask
  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask

  constructor(
    private injector: Injector,
    private brokersService: BrokersService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private snackbar: MatSnackBar,
    private router: Router,
    private utils: UtilsService
  ) {
    super(injector)
  }

  ngOnInit() {
    this.initForms()
  }

  initForms() {

    this.dataForm = this.formBuilder.group({
      personType: this.formBuilder.control('PERSON', [Validators.required]),
      registrationNumberPerson: this.formBuilder.control('', [Validators.required]),
      registrationNumberCompany: this.formBuilder.control(''),
      name: this.formBuilder.control('', [Validators.required]),
      tradingName: this.formBuilder.control(''),
      contactPerson: this.formBuilder.control(''),
      email: this.formBuilder.control('', [Validators.required]),
      password:this.formBuilder.control('',[Validators.required, Validators.minLength(8)]),
      confirmPassword:this.formBuilder.control('',[Validators.required, checkConfirmPassword])
    })

    this.dataForm.valueChanges.subscribe((newForm) => {

      if (newForm.personType == 'PERSON') {
        this.dataForm.controls.tradingName.setValidators(null)
        this.dataForm.controls.contactPerson.setValidators(null)
        this.dataForm.controls.registrationNumberCompany.setValidators(null)
        this.dataForm.controls.registrationNumberPerson.setValidators([Validators.required])
      } else {
        this.dataForm.controls.tradingName.setValidators([Validators.required])
        this.dataForm.controls.contactPerson.setValidators([Validators.required])
        this.dataForm.controls.registrationNumberPerson.setValidators(null)
        this.dataForm.controls.registrationNumberCompany.setValidators([Validators.required])
      }
    });
  }

  ngAfterViewInit() {

  }

  cancel() {
    this.router.navigate(['/users/brokers'])
  }

  isCustomerInvalid() {
    return this.customerPersonalComponent.isInvalid()
  }

  createCustomer() {

    let createPassword = false;

    if(this.dataForm.value.password && this.dataForm.value.password.length > 0){
    
      if(this.dataForm.value.password.length < 8){
        this.snackbar.open('A senha deve ter no mínimo 8 caracteres.', '', { duration: 6000 });
        createPassword = false;
        return;
      }

      if(this.dataForm.value.confirmPassword && this.dataForm.value.confirmPassword.length > 0){
        createPassword = true;
        if(this.dataForm.value.confirmPassword != this.dataForm.value.password){
          this.snackbar.open('As senhas devem ser iguais.', '', { duration: 6000 });
          createPassword = false;
          return;
        }
      }else{
        this.snackbar.open('A confimação de senha deve ser  preenchida.', '', { duration: 6000 });
        createPassword = false;
        return;
      }

    }else{
      this.snackbar.open('A senha deve ser  preenchida.', '', { duration: 6000 });
      createPassword = false;
      return;
    }

    let broker ={
      brand : this.appContext.session.user.brand,
      name : this.dataForm.value.name,
      companyName : this.dataForm.value.companyName,
      tradingName : this.dataForm.value.tradingName,
      personType : this.dataForm.value.personType,
      registrationNumber : this.utilsService.stringOnlyDigits((this.dataForm.value.personType == 'PERSON' ? this.dataForm.value.registrationNumberPerson : this.dataForm.value.registrationNumberCompany)),
      contactPerson : this.dataForm.value.contactPerson,
      email : this.dataForm.value.email
    }
    
    this.brokersService.registerBroker(broker)
      .subscribe(result => {
        this.snackbar.open('Corretor criado com sucesso', '', {
          duration: 8000
        })
        if(createPassword){
          this.changePassword(result.user)
      }else{
        this.router.navigate(['/users/brokers'])
      }
        
      }, error => {
        console.log(error)
        this.snackbar.open(error.error.statusCode == 400 ? error.error.message : 'Ocorreu um erro na sua requisição. tente novamente.', '', { duration: 8000 });
      })

  }

  changePassword(userId){
  
    let body = {
        "token": '',
        "user":userId,
        "password": this.dataForm.value.password,
        "confirmPassword": this.dataForm.value.confirmPassword,
      }

     this.userService.changePassword(body)
     .subscribe(result => {
       this.router.navigate(['/users/brokers'])
     })
 }
}


function checkConfirmPassword(control: AbstractControl)
{

    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('confirmPassword');


    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}
