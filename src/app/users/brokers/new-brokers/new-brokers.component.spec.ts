import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBrokersComponent } from './new-brokers.component';

describe('NewBrokersComponent', () => {
  let component: NewBrokersComponent;
  let fixture: ComponentFixture<NewBrokersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBrokersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBrokersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
