import { Component, OnInit, Injector } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder, AbstractControl } from '@angular/forms';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerPersonalComponent } from '../../../customers/customer-personal/customer-personal.component';
import { Customer } from '../../../core/models/customer.model';
import { MaskUtils } from 'app/core/mask-utils';
import { UtilsService } from '../../../core/utils.service';
import { UserService } from '../../../core/services/users.service';
import { BaseComponent } from 'app/_base/base-component.component';
import { BrokersService } from 'app/core/services/brokers.service';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";


@Component({
  selector: 'tnm-edit-brokers',
  templateUrl: './edit-brokers.component.html',
  styleUrls: ['./edit-brokers.component.scss']
})
export class EditBrokersComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  customer: Customer

  dataForm: FormGroup
  broker: any
  isNewBroker: boolean = true
  customerId: number
  occupationsList: Array<any>
  user: any
  phoneMask = MaskUtils.phoneMask
  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask
  generalForm: FormGroup

  constructor(
    private injector: Injector,
    private brokersService: BrokersService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private loadingService: FuseSplashScreenService,
    private snackbar: MatSnackBar,
    private router: Router,
    private utils: UtilsService
  ) {
    super(injector)
  }

  ngOnInit() {

    localStorage.setItem('hiddenLoading','true');
    this.loadingService.show()

    this.customerId = this.activatedRoute.snapshot.params['id']
    this.initForms()
  }

  initForms() {

    this.generalForm = this.formBuilder.group({
        status:this.formBuilder.control(''),
        login:this.formBuilder.control('',[Validators.required]),
        password:this.formBuilder.control('',[Validators.required, Validators.minLength(8)]),
        confirmPassword:this.formBuilder.control('',[Validators.required, checkConfirmPassword])
    })

    this.dataForm = this.formBuilder.group({
      personType: this.formBuilder.control('PERSON', [Validators.required]),
      registrationNumberPerson: this.formBuilder.control('', [Validators.required]),
      registrationNumberCompany: this.formBuilder.control(''),
      name: this.formBuilder.control('', [Validators.required]),
      tradingName: this.formBuilder.control(''),
      contactPerson: this.formBuilder.control(''),
      email: this.formBuilder.control('', [Validators.required])
    })

    this.dataForm.valueChanges.subscribe((newForm) => {

      if (newForm.personType == 'PERSON') {
        this.dataForm.controls.tradingName.setValidators(null)
        this.dataForm.controls.contactPerson.setValidators(null)
        this.dataForm.controls.registrationNumberCompany.setValidators(null)
        this.dataForm.controls.registrationNumberPerson.setValidators([Validators.required])
      } else {
        this.dataForm.controls.tradingName.setValidators([Validators.required])
        this.dataForm.controls.contactPerson.setValidators([Validators.required])
        this.dataForm.controls.registrationNumberPerson.setValidators(null)
        this.dataForm.controls.registrationNumberCompany.setValidators([Validators.required])
      }
    });
  }

  ngAfterViewInit() {
    this.loadCustomerData()
  }

  cancel() {
    this.router.navigate(['/users/brokers'])
  }

  isCustomerInvalid() {
    return this.customerPersonalComponent.isInvalid()
  }

  loadData() {

    this.dataForm.patchValue ({
      personType: this.broker.personType || null,
      registrationNumberPerson: this.broker.registrationNumber || null,
      registrationNumberCompany: this.broker.registrationNumber || null,
      name: this.broker.name || null,
      tradingName: this.broker.tradingName || null,
      contactPerson: this.broker.contactPerson || null,
      email: this.broker.email || null
    })
  }


  updateCustomer() {

    let updatePassword = false;
    if(this.generalForm.value.password && this.generalForm.value.password.length > 0){
      
      if(this.generalForm.value.password.length < 8){
        this.snackbar.open('A senha deve ter no mínimo 8 caracteres.', '', { duration: 6000 });
        updatePassword = false;
        return;
      }

      if(this.generalForm.value.confirmPassword && this.generalForm.value.confirmPassword.length > 0){

        updatePassword = true;
        if(this.generalForm.value.confirmPassword != this.generalForm.value.password){
          this.snackbar.open('As senhas devem ser iguais.', '', { duration: 6000 });
          updatePassword = false;
          return;
        }
      }else{
        updatePassword = false;
        this.snackbar.open('A confimação de senha deve ser  preenchida.', '', { duration: 6000 });
        return;
      }

    }
     
    this.loadingService.show()

    let editData = []
        
    let editItem = {'op': 'add', 'path': '/name', 'value': this.dataForm.value.name};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/companyName', 'value': this.dataForm.value.companyName ? this.dataForm.value.companyName : null};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/email', 'value': this.dataForm.value.email};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/tradingName', 'value': this.dataForm.value.tradingName ? this.dataForm.value.tradingName : null};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/registrationNumber', 'value': this.utilsService.stringOnlyDigits((this.dataForm.value.personType == 'PERSON' ? this.dataForm.value.registrationNumberPerson : this.dataForm.value.registrationNumberCompany))};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/personType', 'value': this.dataForm.value.personType};
        editData.push(editItem);
        editItem = {'op': 'add', 'path': '/contactPerson', 'value': this.dataForm.value.contactPerson};
        editData.push(editItem);

    this.brokersService.update(this.customerId,editData)
    .subscribe(response => {
      this.snackbar.open('Corretor atualizado com sucesso', '', { duration: 8000 })
      
        if(this.user.login != this.generalForm.value.login){
          this.changeLoginUser(this.user.id)
        }
         
        if(updatePassword){
            this.changePassword(this.user.id)
        }else{
            this.router.navigate(['/users/brokers'])
        }
      },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000 })
        this.loadingService.hide()
    })
  }

  changeLoginUser(userId){

    let editData = []
        
    let editItem = {'op': 'add', 'path': '/login', 'value': this.generalForm.value.login};
        editData.push(editItem);
        
    this.userService.update(userId,editData)
    .subscribe(response => {
      
      },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000 })
    })
  
  }

  changePassword(userId){
  
    let body = {
        "token": '',
        "user":userId,
        "password": this.generalForm.value.password,
        "confirmPassword": this.generalForm.value.confirmPassword,
      }

     this.userService.changePassword(body)
     .subscribe(result => {
       this.router.navigate(['/users/brokers'])
     })
 }

 loadCustomerData() {

    this.brokersService.getOne(this.customerId)
    .subscribe(broker => {
        this.broker = broker

        this.userService.getOne(broker.user)
        .subscribe(user => {
          
          this.user = user
          this.generalForm.patchValue({
            'login':user['login']
          })

          this.loadingService.hide()

        })

        this.loadData()
    })
  }
}


function checkConfirmPassword(control: AbstractControl)
{

    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('confirmPassword');


    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}
