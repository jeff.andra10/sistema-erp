import { Component, OnInit, Injector } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { CustomerPersonalComponent } from '../../../customers/customer-personal/customer-personal.component';
import { Customer } from '../../../core/models/customer.model';
import { UserService } from '../../../core/services/users.service';
import { UtilsService } from '../../../core/utils.service';
import { BaseComponent } from 'app/_base/base-component.component';

@Component({
  selector: 'tnm-new-technical',
  templateUrl: './new-technical.component.html',
  styleUrls: [ './new-technical.component.scss' ]
})
export class NewTechnicalComponent extends BaseComponent implements OnInit, AfterViewInit {
  
  @ViewChild(CustomerPersonalComponent)
  private customerPersonalComponent: CustomerPersonalComponent;

  customer: Customer
  
  constructor(
    private injector: Injector,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private router: Router,
    private utils: UtilsService
  ) {
    super(injector)
   }

  ngOnInit() {
   
  }

  ngAfterViewInit() {

    setTimeout(() => {
      this.customerPersonalComponent.setHideFields()
      this.customerPersonalComponent.setCreatUserAdmin()
    }, 10)
    
  }

  cancel() {
    this.router.navigate(['/users/installers'])
  }

  isCustomerInvalid()  {

    return this.customerPersonalComponent.isInvalid() 
            
  }

  createCustomer() {

    let personalData = this.customerPersonalComponent.getCustomerData()
     
    if (!personalData.name || !personalData.mobilePhone || !personalData.registrationNumber || !personalData.email || !personalData.password || !personalData.confirmPassword  || this.customerPersonalComponent.getCustomerForm().controls.registrationNumber.status == 'INVALID'  || this.customerPersonalComponent.getCustomerForm().controls.password.status == 'INVALID'  || this.customerPersonalComponent.getCustomerForm().controls.confirmPassword.status == 'INVALID'){
      this.validateAllFields(this.customerPersonalComponent.getCustomerForm())
      this.snackbar.open('Todos os dados precisam ser preenchidos.', '', { duration: 6000 });
      return;
    }

    
    let user = {
        'brand' : personalData.brand ,
        'name' : personalData.name,
        'mobile' : this.utils.stringToNumber(personalData.mobilePhone),
        'email' : personalData.email,
        'login' : personalData.email,
        'profile' : 'INSTALLER',
        'cpfCnpj' : this.utils.stringToNumber(personalData.registrationNumber),
        'gender' : null,
        'status' : "ACTIVE",
        'password' : personalData.password,
        'confirmPassword' : personalData.confirmPassword,
        'birthDate' : this.convertDate(personalData.birthDate)
    }
        
    let wrapper = {
      "user":user,
        "origin":"ERP",
        "addresses":[],
        "contacts":[]
    }

    this.userService.createAdmin(wrapper)
    .subscribe(response => {
      this.snackbar.open('Instalador criado com sucesso', '', { duration: 8000 })
      this.router.navigate(['/users/installers'])
      },
      response => {
        this.snackbar.open(response.error.message,'', { duration: 8000 })
    })
  }

  convertDate(date){
    if(date){
      let  _date = date.split('/');
          _date =_date[1] +'-'+_date[0]+'-'+_date[2]
      return new Date(_date)
    }
      
  }

}
