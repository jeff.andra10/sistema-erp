import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTechnicalComponent } from './new-technical.component';

describe('NewTechnicalComponent', () => {
  let component: NewTechnicalComponent;
  let fixture: ComponentFixture<NewTechnicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTechnicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTechnicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
