import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTechnicalComponent } from './edit-technical.component';

describe('EditTechnicalComponent', () => {
  let component: EditTechnicalComponent;
  let fixture: ComponentFixture<EditTechnicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTechnicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTechnicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
