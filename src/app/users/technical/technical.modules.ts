import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../angular-material/material.module";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "@fuse/shared.module";
import { CustomerPersonalComponent } from "../../customers/customer-personal/customer-personal.component";
import { CustomersModule } from "../../customers/customers.module";
import { TechnicalComponent } from "../technical/technical.component";
import { NewTechnicalComponent } from "../technical/new-technical/new-technical.component";
import { EditTechnicalComponent } from "../technical/edit-technical/edit-technical.component"

const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'users/installers/new', component: NewTechnicalComponent },
    { path: 'users/installers/:id', component: EditTechnicalComponent }
]);

@NgModule({
    declarations:[
        TechnicalComponent,
        NewTechnicalComponent,
        EditTechnicalComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FuseSharedModule,
        TextMaskModule,
        FlexLayoutModule,
        RouterModule,
        authRouting,
        CustomersModule
    ],
    exports: [

    ],
    entryComponents: [
        CustomerPersonalComponent,
        
    ],
    providers: [
        
    ]
})
export class TechnicalModule {}