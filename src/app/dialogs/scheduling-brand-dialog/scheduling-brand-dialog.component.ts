import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {  ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'tnm-scheduling-dialog',
  templateUrl: './scheduling-brand-dialog.component.html',
  styleUrls    : ['./scheduling-brand-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class SchedulingDialogComponent implements OnInit {

  modelSelected: String
  dialogForm: FormGroup
  brandSelected: String
  pendencyType:String
  pendencyDescription:String

  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  constructor(
    private dialogRef: MatDialogRef<SchedulingDialogComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    
   }

  ngOnInit() {

    console.log('this.data:',this.data)

    this.pendencyType = this.data.pendencyType ? this.data.pendencyType : null
    this.pendencyDescription = this.data.pendencyDescription ? this.data.pendencyDescription : null

    this.dialogForm = this.formBuilder.group({
      discription: this.formBuilder.control(''),
      brand: this.formBuilder.control(''),
      model: this.formBuilder.control(''),
    })
  }

  onSubmit() {
    this.dialogRef.close({data : {confirm: true, brandSelected:this.brandSelected, pendencyType: this.pendencyType,pendencyDescription:this.pendencyDescription }})
  }

  onNoClick() {
    this.dialogRef.close({data: {confirm: false}})
  }
  
}