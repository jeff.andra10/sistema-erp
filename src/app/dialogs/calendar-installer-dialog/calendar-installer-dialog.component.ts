import { Component, OnInit, Injector, AfterViewInit, ViewChild, ViewEncapsulation,Inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
//import { Subject } from 'rxjs';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { BaseComponent } from '../../_base/base-component.component';
import { StateAddress } from '../../core/common-data-domain';
import { MaskUtils } from '../../core/mask-utils';
import {  MatSnackBar } from '@angular/material';
import {  AssignServiceOrder } from '../../core/models/service-orders.model';
import { ServiceOrdersService } from '../../core/services/service-orders.service';
import * as moment from 'moment';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { ServiceOrdersSelectedService } from '../../service-orders/service-orders.selected.service';
import { dataBinding } from '@syncfusion/ej2-schedule';



@Component({
  selector: 'tnm-calendar-installer-dialog',
  templateUrl: './calendar-installer-dialog.component.html',
  styleUrls    : ['./calendar-installer-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CalendarInstallerDialogComponent extends BaseComponent implements OnInit {


  inputText: String
  scheduleForm: FormGroup
  configForm: FormGroup
  stateAddressList: any
  public searchControl: FormControl;
  public zoom: number;
  postalCodeMask = MaskUtils.postalCodeMask

  technicianList = []
  technician : number
  hours: string
  date: Date

  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  constructor(
    private injector: Injector,
    private dialogRef: MatDialogRef<CalendarInstallerDialogComponent>,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private serviceOrderSelectedService: ServiceOrdersSelectedService,
    private loadingService: FuseSplashScreenService,
    private serviceOrdersService: ServiceOrdersService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(injector)
   }

  ngOnInit() {

    localStorage.setItem('hiddenLoading','true');

   
    if(this.data && this.data.installers && this.data.edit){
      this.technicianList = this.data.installers;
      this.technician = this.data.installer
    }

    this.configForm = this.formBuilder.group({
      deslocationTime: this.formBuilder.control('', [Validators.required]),
      serviceLenght: this.formBuilder.control('', [Validators.required]),
      duringBusinessHours: this.formBuilder.control('')
    })

    this.scheduleForm = this.formBuilder.group({
      installer: this.formBuilder.control({value:'', disabled: true}),
      scheduledDate: this.formBuilder.control({ value: '', disabled: true }, [Validators.required]),
      time: this.formBuilder.control('', [Validators.required]),
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', [])
    })

    
    if(this.data && this.data.address){
      let address = this.data.address.street + ' ,' + this.data.address.number + ' ,' + this.data.address.district + ' ,' + this.data.address.city + ' - ' + this.data.address.state + ' ,' + this.data.address.postalCode;
      this.loadAddressFields(this.data.address)
    }
    
    
    this.stateAddressList = this.utilsService.stringEnumToKeyValue(StateAddress)

  }

  onSubmit() {
    this.dialogRef.close({data : {confirm: true, inputText : this.inputText}})
  }

  onNoClick() {
    this.dialogRef.close({data: {confirm: false}})
  }

  

  loadAddressFields(address){

    this.scheduleForm.patchValue({
      street: address.street || null,
      number: address.number || null,
      complement: address.complement || null,
      district: address.district || null,
      city: address.city || null,
      state: address.state || null,
      postalCode: address.postalCode || null,
      reference:  address.reference || null
    })

  }

  

  assignServiceOrder() {

    if (!this.scheduleForm.value.street) {
      this.snackbar.open('O endereço informado não possui nome da rua.', '', {
        duration: 8000
      })
      return false
    }

    if (!this.scheduleForm.value.number) {
      this.snackbar.open('O endereço informado não possui número.', '', {
        duration: 8000
      })
      return false
    }

    if (!this.scheduleForm.value.district) {
      this.snackbar.open('O endereço informado não possui o nome do bairro.', '', {
        duration: 8000
      })
      return false
    }

    if (!this.scheduleForm.value.city) {
      this.snackbar.open('O endereço informado não possui o nome da cidade.', '', {
        duration: 8000
      })
      return false
    }

    if (!this.scheduleForm.value.state) {
      this.snackbar.open('O endereço informado não possui o nome do estado.', '', {
        duration: 8000
      })
      return false
    }

    if (!this.scheduleForm.value.state) {
      this.snackbar.open('O endereço informado não possui o número do cep.', '', {
        duration: 8000
      })
      return false
    }

    if (!this.configForm.value.deslocationTime) {
      this.snackbar.open('Selecione o tempo de deslocamento.', '', {
        duration: 8000
      })
      return false
    }

    if (!this.configForm.value.serviceLenght) {
      this.snackbar.open('Selecione a duração da instalação.', '', {
        duration: 8000
      })
      return false
    }

    var currentDate = new Date()

    if(!this.data.edit){

      if (currentDate > this.data.selectedTime) {

        this.snackbar.open('Informe uma data superior a data atual', '', {
          duration: 8000
        })
        return false
      }

    }
    

    let assignData = new AssignServiceOrder()

    this.loadingService.show()

        assignData.loggedUser = this.appContext.session.user.id
        assignData.street = this.scheduleForm.value.street
        assignData.number = this.scheduleForm.value.number
        assignData.complement = this.scheduleForm.value.complement
        assignData.district = this.scheduleForm.value.district
        assignData.city = this.scheduleForm.value.city
        assignData.state = this.scheduleForm.value.state
        assignData.postalCode = this.utilsService.stringToNumber(this.scheduleForm.value.postalCode),
        assignData.reference = this.scheduleForm.value.reference
        assignData.beta = true

    if(this.data.edit){

      let _date =  moment(this.date).toISOString().split('T')
      let _hours = parseInt(this.hours) > 9 ? this.hours+':00' : '0'+this.hours+':00'

      assignData.technician = this.technician
      assignData.scheduledDate = _date[0]+'T'+_hours+':00.000Z'
      assignData.duringBusinessHours = this.configForm.value.duringBusinessHours ? true : false
      assignData.deslocationTime = (parseInt(this.configForm.value.deslocationTime) * 60).toString()
      assignData.serviceLenght = this.configForm.value.serviceLenght
      assignData.period = parseInt(this.hours) > 12 ? "AFTERNOON" : "MORNING"
    }else{
      assignData.technician = this.data.installer.Id
      assignData.scheduledDate = this.data.date
      assignData.duringBusinessHours = this.configForm.value.duringBusinessHours ? true : false
      assignData.deslocationTime = (parseInt(this.configForm.value.deslocationTime) * 60).toString()
      assignData.serviceLenght = this.configForm.value.serviceLenght
      assignData.period = this.data.period.indexOf('AM') > -1 ? "MORNING" : "AFTERNOON"

    }
    
    this.serviceOrdersService.assignServiceOrder(this.data.serviceOrder.id, assignData)
      .subscribe(result => {

        this.dialogRef.close({data : {confirm: true, inputText : this.inputText}})
        if(!this.data.edit){
          this.snackbar.open('Ordem de Serviço agendada com sucesso', '', {
            duration: 8000
          })
        }else{
          this.snackbar.open('Ordem de Serviço reagendada com sucesso', '', {
            duration: 8000
          })
        }
       
        this.serviceOrderSelectedService.reloadCalendar(this.data.selectedTime)
      
      },
        response => {
          this.snackbar.open(response.error.message, '', { duration: 8000 })
        })
  }

  

  
}