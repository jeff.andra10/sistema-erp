import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { FlexLayoutModule } from '@angular/flex-layout'
import { ConfirmDialogComponent } from "./confirm-dialog/confirm-dialog.component";
import { InputDialogComponent } from "./input-dialog/input-dialog.component";
import { SchedulingDialogComponent } from "./scheduling-brand-dialog/scheduling-brand-dialog.component";
import { CalendarInstallerDialogComponent } from "./calendar-installer-dialog/calendar-installer-dialog.component"
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../angular-material/material.module";
import { CalendarModule as AngularCalendarModule } from 'angular-calendar';
import { AgmCoreModule,GoogleMapsAPIWrapper } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { ReactiveFormsModule } from "@angular/forms";
import { FuseSharedModule } from "@fuse/shared.module";

@NgModule({
    declarations:[
        ConfirmDialogComponent,
        InputDialogComponent,
        SchedulingDialogComponent,
        CalendarInstallerDialogComponent
    ],
    imports: [
        TextMaskModule,
        FlexLayoutModule,
        FormsModule,
        MaterialModule,
        CommonModule,
        // AngularCalendarModule.forRoot(),
        // AgmCoreModule.forRoot({
        //     apiKey: 'AIzaSyBKaSWoT0uDq74-a2j3n1EnBp1wmDtw3yM'
        // }),
        // AgmDirectionModule,
        ReactiveFormsModule,
        FuseSharedModule
    ],
    exports: [
        ConfirmDialogComponent,
        InputDialogComponent,
        SchedulingDialogComponent,
        CalendarInstallerDialogComponent
    ],
    entryComponents: [
        ConfirmDialogComponent,
        InputDialogComponent,
        SchedulingDialogComponent,
        CalendarInstallerDialogComponent
    ],
    providers:[GoogleMapsAPIWrapper]
})
export class DialogsModule {}