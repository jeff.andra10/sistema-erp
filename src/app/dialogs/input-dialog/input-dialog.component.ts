import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'tnm-input-dialog',
  templateUrl: './input-dialog.component.html',
})

export class InputDialogComponent implements OnInit {

  inputText: String
  
  constructor(
    private dialogRef: MatDialogRef<InputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.dialogRef.close({data : {confirm: true, inputText : this.inputText}})
  }

  onNoClick() {
    this.dialogRef.close({data: {confirm: false}})
  }
  
}