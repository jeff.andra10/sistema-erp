import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {  ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'tnm-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls    : ['./confirm-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ConfirmDialogComponent implements OnInit {

  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  constructor(
    private dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  onYesClick() {
    this.dialogRef.close({data : {confirm: true}})
  }

  onNoClick() {
    this.dialogRef.close({data: {confirm: false}})
  }
  
}