import { Injectable } from "@angular/core";

@Injectable()
export class UtilsService {

    constructor(){}

    getItemById(id, list): object {
        
        if (list == null) return null

        let completeItem
        list.forEach(item =>{
            
            if(id === item.id){
              completeItem = item
            }
        })
       
        return completeItem
    }

    getItemByColumnNameAndValue(columnName, columnValue, list): object {

        if (list == null) return null

        let completeItem
        list.forEach(item =>{
            if(item[columnName] == columnValue){
              completeItem = item
            }
        })
        return completeItem
    }    

    getCompleteListByIdsList(idsList, completeList): Array<object> {

        if (completeList == null) return null

        let filteredList = []
        idsList.forEach(id =>{
            completeList.forEach(obj => {
            if(id === obj.id){
                filteredList.push(obj)
            }
            })
        })
        return filteredList
    }

    stringEnumToKeyValue(stringEnum) {
        var keyValue = []
        var keys = Object.keys(stringEnum)
        
        for (let k of keys) {
            keyValue.push({key: k, value: stringEnum[k]})
        }
        return keyValue
    }

    stringToNumber(value : any) : number {

        if (!value) return null
        
        return value.toString().replace(/\D+/g, '')
     
    }

    stringOnlyDigits(value : any) : string {

        if (!value) return null

        return value.toString().replace(/\D+/g, '')
    }

    numberOnlyDigits(value : any) : number {

        if (!value) return null

        return value.replace(/\D+/g, '')
    }

    arrayRemoveDuplicates(originalArray, prop) {
        let newArray = [];
        let lookupObject = {};
    
        for (let i in originalArray) {
          lookupObject[originalArray[i][prop]] = originalArray[i];
        }
    
        for (let i in lookupObject) {
          newArray.push(lookupObject[i]);
        }
    
        return newArray;
      }

    removeCharSpecial(value : any) : string{
        var map={"â":"a","Â":"A","à":"a","À":"A","á":"a","Á":"A","ã":"a","Ã":"A","ê":"e","Ê":"E","è":"e","È":"E","é":"e","É":"E","î":"i","Î":"I","ì":"i","Ì":"I","í":"i","Í":"I","õ":"o","Õ":"O","ô":"o","Ô":"O","ò":"o","Ò":"O","ó":"o","Ó":"O","ü":"u","Ü":"U","û":"u","Û":"U","ú":"u","Ú":"U","ù":"u","Ù":"U","ç":"c","Ç":"C"};
        return value.replace(/[\W\[\] ]/g,function(a){return map[a]||a}) 
    }

    sortByField(list : Array<any>, fieldName: string) : Array<any>{
        if(list && list.length > 0){
            return list.sort(function(a,b) {
                return a[fieldName] < b[fieldName] ? -1 : a[fieldName] > b[fieldName] ? 1 : 0;
            });
        }else{
            return list
        }
    }

    objectKeyIndexedAsArray(objectKey) : Array<any> {

        return Object.keys(objectKey).map(function(namedIndex){
            objectKey[namedIndex].key = namedIndex
            return objectKey[namedIndex];
        });
    }

    randomId() {
        return Math.floor(Math.random() * 100000);
    }

    
}