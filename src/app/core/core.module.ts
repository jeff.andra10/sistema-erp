import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionService } from './services/session.service';
import { AppContext } from './appcontext';
import { UtilsService } from './utils.service';
import { BrandsService } from './services/brands.service';
import { SuppliersService } from './services/suppliers.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/auth.interceptor';
import { SimCardsService } from './services/sim-cards.service';
import { DevicesService } from './services/devices.service';
import { VehiclesService } from './services/vehicles.service';
import { ManufacturesService } from './services/manufactures.service';
import { ProductsService } from './services/products.service';
import { ServicesService } from './services/services.service';
import { OffersService } from './services/offers.service';
import { CustomersService } from './services/customers.service';
import { UserService } from './services/users.service';
import { PlansService } from './services/plans.service';
import { ServiceOrdersService } from './services/service-orders.service';
import { DocumentsService } from './services/documents.service';
import { SalesService } from './services/sales.service';
import { PurchasesService } from './services/purchases.service';
import { PaymentsService } from './services/payments.service';
import { MonitoringService } from './services/monitoring.service';
import { AccouttingsService } from './services/accouttings.service';
import { QuotesService } from './services/quotes.service';
import { CoveragesService } from './services/coverages.service';
import { BrokersService } from './services/brokers.service';
import { PoliciesService } from './services/policies.service';
import { UsebensService } from './services/usebens.service';
import { DateHelp } from './helps/date.help';
import { GoogleMapsService } from './services/googlemaps.service'
import { CarService } from './services/car.service'
import { InstallersService } from './services/installers.service'

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
      return {
          ngModule: CoreModule,
          providers: [
              AppContext,
              SessionService,
              UtilsService,
              { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi:true },
              BrandsService,
              SuppliersService,
              SimCardsService,
              DevicesService,
              VehiclesService,
              ManufacturesService,
              ProductsService,
              ServicesService,
              OffersService,
              CustomersService,
              UserService,
              PlansService,
              ServiceOrdersService,
              DocumentsService,
              SalesService,
              PurchasesService,
              PaymentsService,
              MonitoringService,
              AccouttingsService,
              QuotesService,
              CoveragesService,
              BrokersService,
              PoliciesService,
              UsebensService,
              DateHelp,
              GoogleMapsService,
              CarService,
              InstallersService
          ]
      }
  }
}
