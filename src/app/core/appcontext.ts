import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "../../../node_modules/rxjs";

import { User } from "./models/user.model";
import { Brand } from "./models/brand.model";
import { SessionModel } from "./models/session.model";

@Injectable()
export class AppContext {
    
    private _brands: Array<Brand> = []
    private _session: SessionModel
    private _loggedUser: User
    private _brandTracknme: any = null
    private _acessToken: string

    private onContextInitSubjet : BehaviorSubject<SessionModel> = new BehaviorSubject(null)
    public onContextInit = this.onContextInitSubjet.asObservable();

    private onContextLoadBrands : BehaviorSubject<any> = new BehaviorSubject(null)
    public onLoadBrands = this.onContextLoadBrands.asObservable();


    constructor(
    ) {}

    public isLoggedIn(): boolean {
        return this.session == null ? false : true
    }

    public isLoggedUserTracknme(): boolean {
        if (this.session == null) return false;

        return (this.loggedUser && (this.loggedUser.brand === this.brandTracknme))
    }

    public changeBrandLoggedUserTracknme(session: SessionModel) {
        
        this._session = session
        localStorage.setItem('session', JSON.stringify({session}))

        this._loggedUser = session.user
        let user = session.user
        localStorage.setItem('loggedUser', JSON.stringify({user}))

        this._acessToken = session.accessToken
        localStorage.setItem('tkn', this._acessToken);
        this.onContextInitSubjet.next(this.session)
    }

    public get loggedUser() : User {
    
        if (this._loggedUser == null && localStorage.getItem('loggedUser')) {
            this._loggedUser = JSON.parse(localStorage.getItem('loggedUser')).user;
        }

        return this._loggedUser
    }

    public get brandTracknme() : any {
    
        if (this._brandTracknme == null) {
            this._brandTracknme = 6137206846521344;
        }

        return this._brandTracknme
    }

    public get session() : SessionModel {
        if (this._session == null) {
            
            let jsonObj: any = JSON.parse(localStorage.getItem('session'));
            if (jsonObj) {
                this._session = new SessionModel().deserialize(jsonObj.session)
            }
        }

        return this._session
    }

    public get brands() : Array<Brand> {
        if (this._brands && this._brands.length == 0) {
            
            let jsonObj: any = JSON.parse(localStorage.getItem('brandList'));
            if (jsonObj) {
                this._brands =  jsonObj.map(brand => new Brand().deserialize(brand))
            }
        }
        return this._brands
    }
    public set brands(v : Array<Brand>) {
        this._brands = v
        localStorage.setItem('brandList', JSON.stringify(v));
    }

    public get accessToken() : string {
    
        if (this._acessToken == null && localStorage.getItem('tkn')) {
            this._acessToken = localStorage.getItem('tkn');
        }
        return this._acessToken
    }

    public set accessToken(value: string) {
    
        this._acessToken = value;
        localStorage.setItem('tkn', value);
    }    

    public initContext(session: SessionModel) {

        this._session = session
        localStorage.setItem('session', JSON.stringify({session}))

        this._loggedUser = session.user
        let user = session.user
        localStorage.setItem('loggedUser', JSON.stringify({user}))

        this._acessToken = session.accessToken
        localStorage.setItem('tkn', this._acessToken);

        this.onContextInitSubjet.next(this.session)
    }

    public destroyContext() {

        localStorage.clear()
        sessionStorage.clear()
        this._brands = null
        this._session = null
        this._loggedUser = null
        
    }
}