import { Injectable, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs/Observable";

import { BaseRestService } from "../services/baserest.service";
import { PaginatedContent } from "../models/paginatedContent.model";
import { Purchase, DonePurchase } from "../models/purchase.model";
import { AppContext } from "../appcontext";

@Injectable()
export class PurchasesService extends BaseRestService<Purchase> {
    
    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ){
        super(injector, 'purchases')
    }

    findPurchases(filter = '',
                status = '',
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 10):  Observable<PaginatedContent<Purchase>> {

        var params = ''
        params += 'by=default-search'
        if (filter != '') {
            params += '&fields=user.name,id'
            params += '&term=' + filter
        }

        if (status != '') { params += '&status=' + status }

        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        params +='&brand='+this.appContext.session.user.brand
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        params += '&fetch=eager'

        return this.http.get<PaginatedContent<Purchase>>(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }

    createNew(body: any): Observable<any> {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/create`, body)
    }

    approve(id: number, loggedUser: number): Observable<any> {

        let body = {loggedUser: loggedUser}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/approve/${id}`, body)
    }

    reject(id: number, loggedUser: number): Observable<any> {

        let body = {loggedUser: loggedUser}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/reject/${id}`, body)
    }    

    done(id: number, body: DonePurchase): Observable<any> {

        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/done/${id}`, body)
    }

    shipped(id: number, loggedUser: number): Observable<any> {

        let body = {loggedUser: loggedUser}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/shipped/${id}`, body)
    }    

    inpreparation(id: number, loggedUser: number): Observable<any> {

        let body = {loggedUser: loggedUser}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/inpreparation/${id}`, body)
    }
}