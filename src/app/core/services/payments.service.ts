import { Injectable, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";
import { Payment } from "../models/payment.model";

@Injectable()
export class PaymentsService extends BaseRestService<Payment> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ) {
        super(injector, 'payments')
    }

    getPaymentsByUser(user): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/payments?by=user&user=${user}`)
    }

    getPaymentsByBrand(business): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/payments?by=default-search&business=${business}`)
    }

    getPaymentById(id): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/payments/${id}`)
    }

    getChargesByPaymentId(id): Observable<any> {

     var params = ''
         params += '?by=default-search'
         params += '&payment=' + id
         params += '&order=sortingOrder'
         return this.http.get<any>(`${this.serverURL}/charges${params}`)
    }

    getChargesBySaleId(id): Observable<any> {

        var params = ''
         params += '?by=default-search&limit=100&page=0'
         params += '&sale=' + id
         params += '&order=sortingOrder'
         return this.http.get<any>(`${this.serverURL}/charges${params}`)
    }

    getChargesRetryById(id): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/charges/retry/${id}`, body)
    }

    getChargesSendBookletById(id): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/charges/send-booklet/${id}`, body)
    }
    
}