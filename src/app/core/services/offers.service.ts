import { Observable } from "rxjs/Observable"
import { Injectable, Injector } from "@angular/core";
import { BaseRestService } from "./baserest.service";
import { Offer } from "../models/offer.model";
import { AppContext } from "../appcontext";
import { map } from "rxjs/operators";

@Injectable()
export class OffersService extends BaseRestService<Offer> {

    constructor(protected injector: Injector,
                private appContext: AppContext){

            super(injector, 'offers')
    }

    getOffers(params? : any): Observable<any> {

        var defaultParams = `?by=default-search&brand=${this.appContext.session.user.brand}`
        if (params) {
            defaultParams += `&limit=${params.limit}&page=${params.page}`
            if(params.segment){
                defaultParams += `&segment=${params.segment}`
            }
        }
        
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)
    }

    getTracknmeOffers(){
        var defaultParams = `?by=default-search&brand=${this.appContext.brandTracknme}&segment=BUSINESS&agreement=TRACKING&order=recurrencePrice`
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)
    }

    getAvailableForAllBrands(params? : any): Observable<any> {
        var defaultParams = `&limit=${params.limit}&page=${params.page}&segment=CUSTOMERS`
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&availableForAllBrands=true&fetch=eager${defaultParams}`)
    } 

    getOffersBySimCard(user,simCard): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&brand=${user}&products=${simCard}&fetch=eager`)
    }   
    
    getOffersByBrand(user,agreement): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&brand=${user}&agreement=${agreement}&limit=200&page=0`)
    }    

    getOffersById(id): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/${id}`)
    }

    findOffers(filter = '', 
                brandId = 0,
                segment = '',
                agreement = '',
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 3):  Observable<any> {

        var params = ''
        params += 'by=default-search'
        if (filter != '') {
            params += '&fields=name'
            params += '&term=' + filter
        }

        if (segment != '') {
            params += '&segment=' + segment
        } 

        if (agreement != '') {
            params += '&agreement=' + agreement
        } 

        if (brandId != 0) {
            params += '&brand=' + brandId.toString()
        } else {
            params += '&brand=' + this.appContext.loggedUser.brand
        }
        
        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        
        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }   
  
}