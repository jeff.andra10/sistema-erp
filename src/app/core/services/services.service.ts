import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";
import { BaseRestService } from "./baserest.service";
import { Service } from "../models/service.model";
import { AppContext } from "../appcontext";

@Injectable()
export class ServicesService extends BaseRestService<Service> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ){
        super(injector, 'services')
    }

    getServices(params? : any): Observable<any> {
        
        var defaultParams = `?by=default-search&brand=${this.appContext.brandTracknme}`
        if (params) {
            defaultParams += `&limit=${params.limit}&page=${params.page}`
        }

        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)        
    }

    getMyServices(): Observable<any> {
        
        var defaultParams = `?by=default-search&brand=${this.appContext.loggedUser.brand}`
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)        
    }

    getCommodities(params? : any): Observable<any> {

        var defaultParams = `?by=brand&brand=${this.appContext.session.user.brand}&limit=1000`

        return this.http.get<any>(`${this.serverURL}/commodities${defaultParams}&fetch=eager`)
    }

    enableCommoditie(params? : any): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/commodities/enable`,params)
    }

    disableCommoditie(params? : any): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/commodities/disable`,params)
    }

    findServices(filter = '', 
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 3,
                brand):  Observable<any> {

        var params = ''
        if(brand){
            params += 'by=brand&brand='+brand
        }else{
            params += 'by=default-search'
        }
       
        if (filter != '') {
            params += '&fields=name'
            params += '&term=' + filter
        }
        
        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }

        
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        
        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }    
}