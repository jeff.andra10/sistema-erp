import { Injectable, Injector } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";
import { Brand } from "../models/brand.model";
import { AppContext } from "../appcontext";

@Injectable()
export class BrandsService extends BaseRestService<Brand> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ){
        super(injector, 'brands')
    }

    create(body: any): Observable<any> {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}`, body)
    }

    delete(id: number): Observable<any> {
        return this.http.delete<any>( `${this.serverURL}/${this.baseURL}/${id}`)
    } 

    getBrandsTreeById(id): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=brandTree&brand=${id}`)
    }

    getBrands(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&limit=1000`)
    }

    getBrandByAlias(alias): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/${alias}`)
    }

    getBrandAddress(id): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/addresses?by=default-search&business=${id}`)
    }

    createBrandAddress(body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/addresses`, body)
    }

    updateBrandAddress(id, body): Observable<any> {
        return this.http.put<any>(`${this.serverURL}/addresses/${id}`, body)
    }

    setPictureBrand(formData: FormData,id): Observable<any> {

        let _headers = new HttpHeaders()
                            .set('enctype', 'multipart/form-data')
                            .set('Accept', 'application/json;charset=UTF-8');

        return this.http.post<any>( `${this.serverURL}/${this.baseURL}?id=${id}`, 
                                    formData, 
                                    { headers: _headers });
    }

    getBrandsByTree(filter : string,parent: number): Observable<any> {

        var params = ''
            
        if (filter != '') {
            params += 'by=default-search'
            params += '&parent='+parent
            params += '&fields=name'
            params += '&term=' + filter
        }else{
            params += 'by=brandTree&brand='+this.appContext.loggedUser.brand
        }
        
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?${params}`)
    }

    findBrands(filter,parent,sortOrder = 'asc',pageNumber = 0,pageSize = 3) : Observable<any> {
        var params = ''
            params += 'by=default-search'

        if (filter != '') {
            params += '&fields=name'
            params += '&term=' + filter
        }

        if(parent && parent != 0){
            params += '&parent='+parent
        }else{
            params += '&brand=' + this.appContext.loggedUser.brand
        }

        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        params += '&fetch=eager'

        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?${params}`
    )}

}