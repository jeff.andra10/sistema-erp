import { Injectable, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators";
import { BaseRestService } from "./baserest.service";
import { Customer } from "../models/customer.model";
import { AppContext } from "../appcontext";
import { PaginatedContent } from "../models/paginatedContent.model";

@Injectable()
export class CustomersService extends BaseRestService<Customer> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ){
        super(injector, 'customers')
    }

    findCustomers(
                filter = '',
                status = '',
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 10) : Observable<PaginatedContent<Customer>> {

        var params = ''
        params += 'by=default-search'
        if (filter != '') {
            params += '&fields=name,email,registrationNumber,nationalIdentity'
            params += '&term=' + filter
        }

        if (status != '') { params += '&status=' + status }

        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }

        params += '&brand='+ this.appContext.session.user.brand

        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        params += '&fetch=eager'

        return this.http.get<PaginatedContent<Customer>>(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }

    getCustomersBySearchTerms(term): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&fields=name,email,registrationNumber,nationalIdentity&term=${term}&brand=${this.appContext.session.user.brand}`)
    }

    getCustomersByEmail(email): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&email=${email}&brand=${this.appContext.session.user.brand}`)
    }

    registerCustomer(body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/register`, body)
    }    

    getCustomerByUser(id: number): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&user=${id}`)
    }

    //TODO: refatorar
    getCustomerAddress(id): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/addresses?by=default-search&user=${id}`)
    }

    createCustomerAddress(body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/addresses`, body)
    }

    updateCustomerAddress(id, body): Observable<any> {
        return this.http.put<any>(`${this.serverURL}/addresses/${id}`, body)
    }

    deleteCustomerAddress(id): Observable<any> {
        return this.http.delete<any>(`${this.serverURL}/addresses/${id}`)
    }

    //TODO: refatorar
    getCustomerContacts(id): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/contacts?by=user&user=${id}`)
    }

    createCustomerContact(body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/contacts`, body)
    }

    updateCustomerContact(id, body): Observable<any> {
        return this.http.put<any>(`${this.serverURL}/contacts/${id}`, body)
    }

    deleteCustomerContact(id): Observable<any> {
        return this.http.delete<any>(`${this.serverURL}/contacts/${id}`)
    }  

    getCustomerPlans(id): Observable<any> {
        var params = ''
         params += '?by=default-search'
         params += '&user=' + id
         params += '&fetch=eager'
         params += '&limit=100'
         
         return this.http.get<any>(`${this.serverURL}/plans${params}`)
     } 
     
     getPlansByBrand(id): Observable<any> {
        var params = ''
         params += '?by=default-search'
         params += '&brand=' + id
         params += '&limit=1000'
         params += '&fetch=eager'
         
         return this.http.get<any>(`${this.serverURL}/plans${params}`)
     }

     getPlanById(id): Observable<any> {
         return this.http.get<any>(`${this.serverURL}/plans/${id}`)
     }
     
     getCustomerPlanCharges(id): Observable<any> {
     var params = ''
         params += '?by=default-search'
         params += '&plan=' + id
         params += '&order=sortingOrder'
         params += '&sort=desc' 
         params += '&limit=100'
         params += '&fetch=eager'
         
         return this.http.get<any>(`${this.serverURL}/charges${params}`)
     }     
     
     
     getCustomerChargeLogs(id): Observable<any> {
     var params = ''
         params += '?by=default-search'
         params += '&charging=' + id
         params += '&limit=100'
         params += '&fetch=eager'
         
         return this.http.get<any>(`${this.serverURL}/charges/logs${params}`)
     }
    
}