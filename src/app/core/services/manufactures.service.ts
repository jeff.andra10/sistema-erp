import { Injectable, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";

@Injectable()
export class ManufacturesService extends BaseRestService<any> {

    VEHICLES_API: string = 'vehicles'
    MANUFACTURERS_API: string = 'manufacturers'
    MODELS_API: string = 'models'

    constructor(
        protected injector: Injector
    ){
        super(injector, 'manufacturers')
    }

    getManufacturerByType(type) {
        return this.http.get<any>(`${this.serverURL}/${this.VEHICLES_API}/${this.MANUFACTURERS_API}/${type}`)
    }

    getModelByManufacturer(manufacturerId) {
        return this.http.get<any>(`${this.serverURL}/${this.VEHICLES_API}/${this.MODELS_API}/${manufacturerId}`)
    }

}