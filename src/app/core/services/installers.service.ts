import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";


@Injectable()
export class InstallersService extends BaseRestService<any> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ) {
        super(injector, 'installers')
    }


    getSchedules(date): Observable<any> {
        let params = `?by=default-search&brand=${this.appContext.session.user.brand}&limit=1000&date=${date}&fetch=eager`
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/schedule${params}`)
    }

    deleteAppointment(id): Observable<any> {
       return this.http.delete<any>( `${this.serverURL}/${this.baseURL}/schedule/appointment/${id}`, {})
    }

    updateAppointment(id, body: any): Observable<any> {
        return this.http.put<any>( `${this.serverURL}/${this.baseURL}/schedule/appointment/${id}`, body)
    }

}