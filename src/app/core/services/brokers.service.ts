import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { AppContext } from "../appcontext";
import { map } from "rxjs/operators";

import { BaseRestService } from "./baserest.service";
import { UtilsService } from "app/core/utils.service";
import { PaginatedContent } from "app/core/models/paginatedContent.model";

@Injectable()
export class BrokersService extends BaseRestService<any> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext,
        private utilsService: UtilsService
    ){
        super(injector, 'brokers')

        // override backend to mock url
        //this.serverURL = 'https://erp-quote.firebaseio.com'
    }

    getBrokers(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&brand=${this.appContext.session.user.brand}`)
    }

    getBrokerByUser(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&brand=${this.appContext.session.user.brand}&user=${this.appContext.session.user.id}`)
    }

    findBrokers(filter = '', 
                        sortField = '', 
                        sortOrder = '', 
                        pageNumber = 0, 
                        pageSize = 3):  Observable<any> {

        var params = ''
        params += 'by=default-search'
        params += '&brand='+this.appContext.session.user.brand

        if (filter != '') {
            params += '&fields=name,email'
            params += '&term=' + filter
        }
        
        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        
        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }  

    registerBroker  (body: any): Observable<any> {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/register`, body)
    }

    updateBroker(key: string, body: any): Observable<any> {
        return this.http.put<any>( `${this.serverURL}/${this.baseURL}/${key}`, body)
    }

    deleteBroker(id): Observable<any> {
        return this.http.delete<any>( `${this.serverURL}/${this.baseURL}/${id}`)
    }    
}