import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";
import { Vehicle } from "../models/vehicle.model";
import { AppContext } from "../appcontext";
import { map } from "rxjs/operators";

@Injectable()
export class VehiclesService extends BaseRestService<Vehicle> {

    CARS_API: string = 'vehicles'

    constructor(protected injector: Injector,
                private appContext: AppContext){

                super(injector, 'vehicles')
    }

    getVehicles(params): Observable<any> {
        let order = '&order=dateCreation&desc'
        let defaultParams = `?by=default-search&brand=${this.appContext.session.user.brand}&fetch=eager&limit=${params.limit}&page=${params.page}${order}`
        return this.http.get<any>(`${this.serverURL}/${this.CARS_API}${defaultParams}`)
    }

    getManufacturers(type): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/vehicles/manufacturers/${type}`)
    }

    getModels(manufacture): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/vehicles/manufacturers/${manufacture}/models`)
    }

    getYears(manufacture,model): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/vehicles/manufacturers/${manufacture}/models/${model}/years`)
    }

    getVehicle(manufacture,model,year): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/vehicles/manufacturers/${manufacture}/models/${model}/years/${year}/vehicles`)
    }

    getVehicleById(id): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/vehicles/${id}`)
    }

    getVehiclesByUser(userId): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/vehicles?by=default-search&user=${userId}`)
    }

    findVehicles(filter = '', 
                brandId = 0,
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 3):  Observable<any> {

        var params = ''
        params += 'by=default-search'
        if (filter != '') {
            params += '&fields=manufacturer,licensePlate,model'
            params += '&term=' + filter
        }
        
        if (brandId != 0) {
            params += '&brand=' + brandId.toString()
        } else {
            params += '&brand=' + this.appContext.loggedUser.brand
        }

        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        
        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }  
}