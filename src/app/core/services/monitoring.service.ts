import { HttpClient } from "@angular/common/http"
import { Observable } from "rxjs/Observable"
import { Injectable, Injector } from "@angular/core";
import { BaseRestService } from "./baserest.service";
import { Offer } from "../models/offer.model";
import { AppContext } from "../appcontext";


@Injectable()
export class MonitoringService extends BaseRestService<Offer> {

    constructor(protected injector: Injector,
                private appContext: AppContext){

            super(injector, 'booklet/monitoring')
    }

    getMonitoring(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}`)
    }

    
}