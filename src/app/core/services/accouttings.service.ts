import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BaseRestService } from './baserest.service';
import { Accouttings } from '../models/accouttings.modal';
import { AppContext } from '../appcontext';
import { PaginatedContent } from '../models/paginatedContent.model';
import { map } from 'rxjs/operators';

@Injectable()
export class AccouttingsService extends BaseRestService<Accouttings> {

    constructor(
        protected injector: Injector, 
        private appContext: AppContext
    ){
        super(injector, 'accountings')
    }

    findAccouttings(
        filter = '',
        status = '',
        sortField = '', 
        sortOrder = '', 
        pageNumber = 0, 
        pageSize = 10) : Observable<PaginatedContent<Accouttings>> {

        var params = ''
        params += 'by=default-search'

        params += '&brand='+ this.appContext.session.user.brand

        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }

        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        params += '&fetch=eager'

        return this.http.get<PaginatedContent<Accouttings>>(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }

    getAccouttingsByBrand(): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&brand=${this.appContext.session.user.brand}&page=0&limit=10`)
    }

    getFactByBrand(): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/reports/fact/accounting?by=factAccountingBrand&brand=${this.appContext.session.user.brand}`)
    }

    getFactByInterval(startDate,endDate): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/reports/fact/accounting?by=factAccountingBrand&brand=${this.appContext.session.user.brand}&start=${startDate}&end=${endDate}`)
    }

    getFactByYear(startDate,endDate): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}
        /reports/fact/accounting?by=factAccountingBrandMonth&brand=${this.appContext.session.user.brand}&start=${startDate}&end=${endDate}`)
    }

    
}