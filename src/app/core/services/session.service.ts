import { Injectable, Injector } from '@angular/core';
import { BaseRestService } from './baserest.service';
import { SessionModel } from '../models/session.model';

@Injectable()
export class SessionService extends BaseRestService<SessionModel> {

  constructor(protected injector: Injector) {
    super(injector, 'sessions')
  }

}
