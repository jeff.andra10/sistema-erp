import { Observable } from "rxjs/Observable"
import { Injectable, Injector } from "@angular/core";
import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";
import { Quote } from "../models/quote.model";
import { UtilsService } from "app/core/utils.service";

@Injectable()
export class UsebensService extends BaseRestService<Quote> {

    constructor(protected injector: Injector,
                private appContext: AppContext,
                private utilsService: UtilsService){

            super(injector, 'usebens')
    }


    getOccupations(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/occupations`)
    }

    getCoverages(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/coverages`)
    }

    getActivities(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/activities`)
    }

    printPolicy(id): Observable<any> {
        return this.http.get(`${this.serverURL}/${this.baseURL}/reports/policy/${id}`,{  responseType: 'blob' as 'json' })
    }

    printProposal(id): Observable<any> {
        return this.http.get(`${this.serverURL}/${this.baseURL}/reports/proposal/${id}`,{  responseType: 'blob' as 'json' })
    }

}