import { Observable } from "rxjs/Observable"
import { Injectable, Injector } from "@angular/core";
import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";
import { map } from "rxjs/operators";
import { Quote } from "../models/quote.model";
import { PaginatedContent } from "../models/paginatedContent.model";
import { UtilsService } from "app/core/utils.service";

@Injectable()
export class QuotesService extends BaseRestService<Quote> {

    constructor(protected injector: Injector,
                private appContext: AppContext){

            super(injector, 'quotations')
    }

    findQuotes(filter = '', 
                brandId = 0,
                status = 'ALL',
                broker = 0,
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 10):  Observable<any> {
        

        var params = ''
        params += 'by=default-search'

        if (filter != '') {
            params += '&fields=customer.name,customer.registrationNumber,id,status'
            params += '&term=' + filter
        }

        if (brandId != 0) {
            params += '&brand=' + brandId.toString()
        } else {
            params += '&brand=' + this.appContext.loggedUser.brand
        }

        if(broker != 0){
            params += '&broker=' + broker
        }

        // if(this.appContext.loggedUser.profile ==  "BROKER"){
        //     params += '&broker=' + this.appContext.loggedUser.id
        // }

        if (status != 'ALL'){
            params += '&status=' + status
        }
        
        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        
        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }   
  
    createQuote(body: any): Observable<any> {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/quotation/create`, body)
    }

    updateQuote(id: number, body: any): Observable<any> {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/quotation/update/${id}`, body)
    }

    getQuote(id: number): Observable<any> {
        return this.http.get<any>( `${this.serverURL}/${this.baseURL}/${id}`)
    }

    createPropose(id: number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/proposal/${id}`,body)
    }

    createPolicy(id: number, body: any): Observable<any> {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/policy/${id}`, body)
    }

    analyzePropose(id: number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/analyze/${id}`, body)
    }

    approvePropose(id: number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/approve/${id}`, body)
    }

    cancelPropose(id: number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/cancel/${id}`, body)
    }

    deleteQuote(key: string): Observable<any> {
        return this.http.delete<any>( `${this.serverURL}/${this.baseURL}/${key}`)
    }

}