import { Observable } from "rxjs/Observable"
import { Injectable, Injector } from "@angular/core";
import { map } from "rxjs/operators";

import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";
import { UtilsService } from "app/core/utils.service";
import { Policy } from "app/core/models/policy.model";
import { PaginatedContent } from "app/core/models/paginatedContent.model";

@Injectable()
export class PoliciesService extends BaseRestService<Policy> {

    constructor(protected injector: Injector,
                private appContext: AppContext,
                private utilsService: UtilsService){

            super(injector, 'policies')

            // override backend to mock url
            //this.serverURL = 'https://erp-quote.firebaseio.com'
    }

    findPolicies(filter = '', 
                brandId = 0,
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 10):  Observable<any> {
        
        return this.http.get(`${this.serverURL}/${this.baseURL}.json`)
        .pipe( map((result : any)=>  {

            let resultArray = this.utilsService.objectKeyIndexedAsArray(result);
            let pagedResult = new PaginatedContent<any>();
            pagedResult.totalElements = resultArray.length;
            pagedResult.content = resultArray;
            return pagedResult;
        }));

        // var params = ''
        // params += 'by=default-search'
        // if (filter != '') {
        //     params += '&fields=name'
        //     params += '&term=' + filter
        // }

        // if (brandId != 0) {
        //     params += '&brand=' + brandId.toString()
        // } else {
        //     params += '&brand=' + this.appContext.loggedUser.brand
        // }
        
        // if (sortField != '') {
        //     params += '&order=' + sortField
        //     if (sortOrder != '') {
        //         params += '&' + sortOrder
        //     }
        // }
        
        // params += '&page=' + pageNumber.toString()
        // params += '&limit=' + pageSize.toString()
        
        // return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        // .pipe( map(res =>  res) );
    }   
  
}