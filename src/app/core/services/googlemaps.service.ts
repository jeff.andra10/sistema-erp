import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";
import { HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { UtilsService } from "app/core/utils.service";

@Injectable()
export class GoogleMapsService extends BaseRestService<any> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext,
        private utilsService: UtilsService
        
    ) {
        super(injector, 'geocode')
    }

    getLocationForAddress(address:string): Observable<any> {
        return this.http.get<any>(`https://maps.googleapis.com/maps/api/${this.baseURL}/json?address=${address}&key=AIzaSyBKaSWoT0uDq74-a2j3n1EnBp1wmDtw3yM`)
    }
}
