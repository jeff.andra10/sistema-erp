import { Injectable, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs/Observable";

import { BaseRestService } from "../services/baserest.service";
import { PaginatedContent } from "../models/paginatedContent.model";
import { Sale } from "../models/sale.model";
import { AppContext } from "../appcontext";

@Injectable()
export class SalesService extends BaseRestService<Sale> {
    
    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ){
        super(injector, 'sales')
    }

    findSales(filter = '',
                status = '',
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 10):  Observable<PaginatedContent<Sale>> {

        var params = ''
        params += 'by=default-search'
        params += '&brand='+this.appContext.session.user.brand
        if (filter != '') {
            params += '&fields=user.name,id'
            params += '&term=' + filter
        }

        if (status != '') { params += '&status=' + status }

        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        params += '&fetch=eager'

        return this.http.get<PaginatedContent<Sale>>(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }    

    createNew(body: any): Observable<any> {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/sale-to-customers/create`, body)
    }

    approve(id: number, loggedUser: number): Observable<any> {

        let body = {loggedUser: loggedUser}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/sale-to-business/approve/${id}`, body)
    }

    approveToCustomer(id: number, loggedUser: number): Observable<any> {
        let body = {loggedUser: loggedUser}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/sale-to-customers/approve/${id}`, body)
    }

    reject(id: number, loggedUser: number): Observable<any> {

        let body = {loggedUser: loggedUser}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/sale-to-business/reject/${id}`, body)
    }

    rejectToCustomer(id: number, loggedUser: number): Observable<any> {

        let body = {loggedUser: loggedUser}
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/sale-to-customers/reject/${id}`, body)
    }

    cancel(id: number, loggedUser: number,segment: string): Observable<any> {

        let body = {loggedUser: loggedUser}

        if(segment == "BUSINESS"){
            return this.http.post<any>( `${this.serverURL}/${this.baseURL}/sale-to-business/cancel/${id}`, body)
        }else{
            return this.http.post<any>( `${this.serverURL}/${this.baseURL}/sale-to-customers/cancel/${id}`, body)
        }
        
    }

    calculateFreight(params? : any): Observable<any> {

        let defaultParams = '?destinationPostalCode='+params
            defaultParams += '&weight=1'

        return this.http.get<any>(`${this.serverURL}/shipping/postalservice${defaultParams}`)
    }

    getSaleById(id): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/${id}`)
    }
    
}