import { HttpClient } from '@angular/common/http';
import { Injector } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';

export abstract class BaseRestService<T> {

  protected serverURL: string;
  protected http: HttpClient;
  
  constructor(
    protected injector: Injector,
    protected baseURL: string
  ) { 

    this.http = injector.get(HttpClient);
    this.serverURL = environment.serverUrl;
  }

  createAdmin(body: any): Observable<T> {
    return this.http.post<T>( `${this.serverURL}/signin`, body);
   }

  getOne(id: number): Observable<T> {
    return this.http.get<T>(`${this.serverURL}/${this.baseURL}/${id}`);
  }

  create(body: any): Observable<T> {
      return this.http.post<T>( `${this.serverURL}/${this.baseURL}`, body);
  }

  update(id: number, body: any): Observable<any> {
      return this.http.put<any>( `${this.serverURL}/${this.baseURL}/${id}`, body);
  }

  delete(id: number): Observable<any> {
      return this.http.delete<any>( `${this.serverURL}/${this.baseURL}/${id}`);
  } 
}
