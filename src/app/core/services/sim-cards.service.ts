import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { SimCard } from "../models/sim-card.model";
import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";
import { map } from "rxjs/operators";

@Injectable()
export class SimCardsService extends BaseRestService<SimCard> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ){
        super(injector, 'simcards')
    }

    getSimCards(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&brand=${this.appContext.session.user.brand}`)
    }

    getSimCardsBySearchTerms(term): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&fields=operator,iccid,msisdn&term=${term}&brand=${this.appContext.session.user.brand}`)
    }

    findSimCards(filter = '', 
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 3):  Observable<any> {

        var params = ''
        params += 'by=default-search'
        if (filter != '') {
            params += '&fields=iccid,msisdn'
            params += '&term=' + filter
        }
        
        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        
        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    } 
}