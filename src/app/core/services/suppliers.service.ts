import { Injectable, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Supplier } from "../models/supplier.model";

import { Observable } from "rxjs/Observable";
import { AppContext } from "../appcontext";
import { BaseRestService } from "./baserest.service";
import { map } from "rxjs/operators";

@Injectable()
export class SuppliersService extends BaseRestService<Supplier> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ){
        super(injector, 'suppliers')
    }

    getSuppliers(): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&brand=${this.appContext.session.user.brand}`)
    }

    findSuppliers(filter = '', 
                        sortField = '', 
                        sortOrder = '', 
                        pageNumber = 0, 
                        pageSize = 3):  Observable<any> {

        var params = ''
        params += 'by=default-search'
        if (filter != '') {
            params += '&fields=companyName,tradingName,email'
            params += '&term=' + filter
        }
        
        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        
        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }  
}