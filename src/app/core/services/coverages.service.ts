import { Observable } from "rxjs/Observable"
import { Injectable, Injector } from "@angular/core";
import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";
import { map } from "rxjs/operators";
import { Quote } from "../models/quote.model";
import { PaginatedContent } from "../models/paginatedContent.model";
import { UtilsService } from "app/core/utils.service";

@Injectable()
export class CoveragesService extends BaseRestService<Quote> {

    constructor(protected injector: Injector,
                private appContext: AppContext,
                private utilsService: UtilsService){

            super(injector, 'coverages')

            // override backend to mock url
            this.serverURL = 'https://erp-quote.firebaseio.com'
    }

    findCoverages():  Observable<any> {
        
        return this.http.get(`${this.serverURL}/${this.baseURL}.json`)
        .pipe( map((result : any)=>  {

            let resultArray = this.utilsService.objectKeyIndexedAsArray(result);
            let pagedResult = new PaginatedContent<any>();
            pagedResult.totalElements = resultArray.length;
            pagedResult.content = resultArray;
            return pagedResult;
        }));
    }   
  
}