import { Injectable, Injector } from "@angular/core";
import { map } from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";
import { User } from "../models/user.model";
import { AppContext } from "../appcontext";
import { PaginatedContent } from "../models/paginatedContent.model";

@Injectable()
export class UserService extends BaseRestService<User> {

    constructor(
        protected injector: Injector, 
        private appContext: AppContext){
            super(injector, 'users');
        }


    findUsers(
            filter = '',
            status = '',
            profile = '',
            sortField = '', 
            sortOrder = '', 
            pageNumber = 0, 
            pageSize = 10): Observable<PaginatedContent<User>> {

    let params = '';
    params += 'by=default-search';
    if (filter !== '') {
        params += '&fields=name,email';
        params += '&term=' + filter;
    }

    if (status != '') { params += '&status=' + status }

    if (sortField != '') {
        params += '&order=' + sortField
        if (sortOrder != '') {
            params += '&' + sortOrder
        }
    }

    if(profile != ''){ params += '&profile=' + profile }

    params += '&brand='+ this.appContext.session.user.brand

    params += '&page=' + pageNumber.toString()
    params += '&limit=' + pageSize.toString()
    params += '&fetch=eager'

    return this.http.get<PaginatedContent<User>>(`${this.serverURL}/${this.baseURL}?${params}`)
    .pipe( map(res =>  res) );
}

    getUserByRegistrationNumber(regNumber): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&fields=name&term=${regNumber}&brand=${this.appContext.session.user.brand}`)
    }

    getUserById(id): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/${id}`)
    }

    getUsersByProfileAndName(profile: string, userName: string): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&fields=name&term=${userName}&brand=${this.appContext.session.user.brand}&profile=${profile}`)
    }    

    getTechnician(profile: string): Observable<any>  {

        var params = ''
        params += 'by=default-search'
        if(this.appContext.session.user.brand != this.appContext.brandTracknme){
            params +='&brand='+ this.appContext.session.user.brand
        }
        params += '&profile=' + profile
        params += '&limit=1000'
       
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?${params}`)
    }   
    
    getTechnicianByBrand(): Observable<any>  {

        var params = ''
        params += 'by=default-search'
        params +='&brand='+ this.appContext.session.user.brand
        params += '&profile=INSTALLER'
        params += '&limit=1000'
       
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?${params}`)
    } 

    getUsersByName(userName: string): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&fields=name&term=${userName}&brand=${this.appContext.session.user.brand}`)
    }

    passwordRecovery(body: any): Observable<any>  {
        return this.http.post<any>( `${this.serverURL}/password/token`, body)
    }

    changePassword(body: any): Observable<any>  {
        return this.http.post<any>( `${this.serverURL}/password/change`, body)
    }

    migration(body: any): Observable<any>  {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/migrate`, body)
    }
    

}