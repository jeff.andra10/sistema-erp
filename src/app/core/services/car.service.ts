import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";
import { Vehicle } from "../models/vehicle.model";
import { AppContext } from "../appcontext";
import { map } from "rxjs/operators";

@Injectable()
export class CarService extends BaseRestService<Vehicle> {

    CARS_API: string = 'cars'

    constructor(protected injector: Injector,
                private appContext: AppContext){

                super(injector, 'cars')
    }

    getCar(id): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/cars/${id}`)
    }

}