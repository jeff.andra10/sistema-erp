import { Injectable, Injector } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Observable } from "rxjs/Observable";


import { BaseRestService } from "./baserest.service";
import { Plan } from "../models/plan.model";
import { AppContext } from "../appcontext";

@Injectable()
export class PlansService extends BaseRestService<Plan> {

    private plan = new BehaviorSubject<any>({});
    currentPlan = this.plan.asObservable();
  
    constructor(
      protected injector: Injector,
      private appContext: AppContext
    ){
      super(injector, 'plans')
    }

    setPlan(plan: any) {
      this.plan.next(plan)
    }

    cancelPlan(id, body): Observable<any> {
      return this.http.post<any>(`${this.serverURL}/${this.baseURL}/cancel/${id}`, body)
    }

    paymentChangePlan(id, body): Observable<any> {
      return this.http.post<any>(`${this.serverURL}/${this.baseURL}/payment-change/${id}`, body)
    }

    reactivatePlan(id, body): Observable<any> {
      return this.http.post<any>(`${this.serverURL}/${this.baseURL}/reactivate/${id}`, body)
    }

    getActivePlansBySearchTerms(licensePlate): Observable<any> {

      var q = ''

      return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&fields=device.car.licensePlate,user.name&term=${licensePlate}&brand=${this.appContext.session.user.brand}&status=ACTIVE&fetch=eager`)
  }

  getPlansByUser(user): Observable<any> {
      return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=user&user=${user}&fetch=eager`)
  }    
  
  getPlansByDevice(device): Observable<any> {
    return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=device&device=${device}&fetch=eager`)
  }

  getPlansById(id): Observable<any> {
    return this.http.get<any>(`${this.serverURL}/offers?by=default-search&id=${id}`)
  }

  getPlanById(id): Observable<any> {
    return this.http.get<any>(`${this.serverURL}/plans/?by=default-search&id=${id}&fetch=eager`)
  }
  
  findCanceledPlans(filter = '', 
                    sortOrder = 'asc', 
                    pageNumber = 0, 
                    pageSize = 3) : Observable<any> {

      var params = ''
      params += 'by=default-search'
      if (filter != '') {
          params += '&fields=user.name'
          params += '&term=' + filter
      }
      params += '&status=CANCELED'
      params += '&brand=' + this.appContext.session.user.brand
      //params += '&sort=' + sortOrder
      params += '&page=' + pageNumber.toString()
      params += '&limit=' + pageSize.toString()
      params += '&fetch=eager'

      return this.http.get<any>(`${this.serverURL}/${this.baseURL}?${params}`)
  } 

  planOffersChange(id,body): Observable<any> {
    return this.http.post<any>(`${this.serverURL}/${this.baseURL}/offer-change/${id}`, body)
  }


  findPlans(filter,status, 
                    statusPayment,
                    brand,
                    sortField = '', 
                    sortOrder = 'asc', 
                    pageNumber = 0, 
                    pageSize = 3) : Observable<any> {


      var params = ''
      params += 'by=default-search'
      if (filter != '') {
          params += '&fields=user.name,user.login,vehicle.licensePlate'
          params += '&term=' + filter
      }
      if(status && status != 'ALL'){
          params += '&status='+status
      }
      if(statusPayment && statusPayment != 'ALL'){
        params += '&debt='+statusPayment
      }

      if(brand == 0){
        params += '&brand=' + this.appContext.session.user.brand
      }else{
        params += '&brand=' + brand
      }

      if (sortField != '') {
        params += '&order=' + sortField
        if (sortOrder != '') {
            params += '&' + sortOrder
        }
    }

      params += '&page=' + pageNumber.toString()
      params += '&limit=' + pageSize.toString()
      params += '&fetch=eager'

      return this.http.get<any>(`${this.serverURL}/${this.baseURL}?${params}`)
  } 
}