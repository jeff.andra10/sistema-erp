import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/do';
import { Injectable, Injector } from "@angular/core";
import { MatSnackBar} from '@angular/material';
import { AppContext } from "../appcontext";
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { Router } from "../../../../node_modules/@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    private appContext : AppContext
    private router: Router
    private alias: string = 'tracknme'
    

    constructor(private injector: Injector,
                private loadingService: FuseSplashScreenService,
                private snackbar: MatSnackBar ){
                    
                this.appContext = this.injector.get(AppContext);
                this.router = this.injector.get(Router);

                if(this.router && this.router['location']){
                    let href = this.router['location']['_platformStrategy']
                    ['_platformLocation']['_doc']['baseURI'];
                    const hrefArray = href.split("//");
                    const aliasArray = hrefArray[1].split(".");
                    const aliasRef = aliasArray[0];

                    if(aliasRef != 'localhost:4200/' && aliasRef != 'dev' && aliasRef != 'wwww'){
                        this.alias = aliasRef;
                    }

                 }
                
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
            
        if(this.alias != 'tracknme'){
            let serverUrl = request.url
            serverUrl = serverUrl.replace('www.tracknme',this.alias+'.tracknme')
            request = request.clone(
                {url: serverUrl}
            )
        }

        if(!localStorage.getItem('hiddenLoading') || localStorage.getItem('hiddenLoading') == 'false'){
            if(!this.loadingService.isLoading){
                this.loadingService.show()
            }
        }
        
        let requestWithHeaders : HttpRequest<any> = this.handleRequestHeaders(request)

        return next.handle(requestWithHeaders).do((event: HttpEvent<any>) => {
            // success
            if (event instanceof HttpResponse) {

                if(!localStorage.getItem('hiddenLoading') || localStorage.getItem('hiddenLoading') == 'false'){
                    this.loadingService.hide();
                }
                
                this.handleResponseHeaderAcessToken(event);
            }

          }, (err: any) => {
            
            // error
            this.loadingService.hide();

            if (err instanceof HttpErrorResponse) {
              if (err.status === 401) {
                  this.appContext.destroyContext();
                  this.router.navigate(['/login']);
                  this.snackbar.open('Sua sessão expirou, logue novamente.', '', {
                    duration: 5000
                  })
              }
            }
          });
    }

    private handleResponseHeaderAcessToken(event: HttpResponse<any>) {

        if (event.headers.has('x-access-token')) {
            let tnk = event.headers.get('x-access-token');
            this.appContext.accessToken = tnk;
        }
    }

    private handleRequestHeaders(request: HttpRequest<any>) : HttpRequest<any> {

        let requestWithHeaders : HttpRequest<any>

        if(this.appContext.isLoggedIn() && 
        request.url.toLocaleLowerCase().startsWith('https://erp-quote.firebaseio.com/') == false
        ){


            if (request.headers.has('Content-Type') ||
                request.headers.has('enctype')) {

                requestWithHeaders = request.clone(
                    {
                        setHeaders: {
                            'Authorization': this.appContext.accessToken,
                        }
                    }
                )

            } else {

                requestWithHeaders = request.clone(
                    {
                        setHeaders: {
                            'Content-Type': 'application/json',
                            'Authorization': this.appContext.accessToken,
                        }
                    }
                )
            }
                                                                                       
            if(request.url.indexOf('/reports/proposal/') != -1 || request.url.indexOf('/reports/policy/') != -1 ){

                requestWithHeaders = request.clone(
                    {
                        setHeaders: {
                            'Authorization': this.appContext.accessToken,
                            'responseType': 'arraybuffer'
                        }
                    }
                )

            }

        } else {

            requestWithHeaders = request.clone(
                {
                    setHeaders: {
                        'Content-Type': 'application/json'
                    }
                }
            )
        }


       

        return requestWithHeaders;
    }
}