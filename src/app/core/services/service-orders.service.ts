import { Injectable, Injector } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { BaseRestService } from "./baserest.service";
import { ServiceOrder, AssignServiceOrder, RejectServiceOrder, DonePurchaseServiceOrder } from "../models/service-orders.model";
import { AppContext } from "../appcontext";
import * as moment from 'moment';

@Injectable()
export class ServiceOrdersService extends BaseRestService<ServiceOrder> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ) {

        super(injector, 'service/orders')
    }

    getServiceOrdersBySearchTerms(type? : string, status? : string, term?: string): Observable<any> {
        
        var q = ''

        if (term) { q += '&fields=user.name&term=' + term }
        if (type) { q += '&type=' + type }
        if (status) { q += '&status=' + status }
        q +='&brand='+this.appContext.session.user.brand
        q += '&fetch=eager'

        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/?by=default-search${q}&order=dateTime&desc&limit=100`)
    }

    findServiceOrders(brand,
                        filter = '', 
                        serviceOrderType = '',
                        serviceOrderStatus = '',
                        technician,
                        sortField = '', 
                        sortOrder = '', 
                        pageNumber = 0, 
                        pageSize = 3):  Observable<any> {

        var params = ''
        params += 'by=default-search'
        
        if (filter != '') {
            params += '&fields=installation.user,uninstallation.user,maintenance.user,id'
            params += '&term=' + filter
        }
        if (serviceOrderType != '') { params += '&type=' + serviceOrderType }
        if (serviceOrderStatus != '') { params += '&status=' + serviceOrderStatus }        
        
        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
 
        params +='&brand='+ ((brand != 0) ? brand : this.appContext.session.user.brand)
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        params += '&fetch=eager'

        if(technician && technician > 0){
            params += '&technician=' + technician
        }

        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
        .pipe( map(res =>  res) );
    }    

    findServiceOrdersByType(brand,
        filter = '', 
        serviceOrderType = '',
        serviceOrderStatus = '',
        technician,
        sortField = '', 
        sortOrder = '', 
        pageNumber = 0, 
        pageSize = 3,
        type='',
        fieldType=''):  Observable<any> {

            var params = ''
             params += 'by=default-search'
            
            if (filter != '') {
                params += '&type=' + type
                params += '&fields='+fieldType+'.user.login,'+fieldType+'.customer.email,'+fieldType+'.customer.name,'+fieldType+'.customer.registrationNumber,'+fieldType+'.vehicle.licensePlate'
                params += '&term=' + filter
            }

            if (serviceOrderType != '') { params += '&type=' + serviceOrderType }
            if (serviceOrderStatus != '') { params += '&status=' + serviceOrderStatus }        
            
            if (sortField != '') {
                params += '&order=' + sortField
                if (sortOrder != '') {
                    params += '&' + sortOrder
                }
            }

    
            params +='&brand='+ ((brand != 0) ? brand : this.appContext.session.user.brand)
            params += '&page=' + pageNumber.toString()
            params += '&limit=' + pageSize.toString()
            params += '&fetch=eager'

            if(technician && technician > 0){
                params += '&technician=' + technician
            }

            return this.http.get(`${this.serverURL}/${this.baseURL}?${params}`)
            .pipe( map(res =>  res) );

    }

    openServiceOrder(body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/open`, body)
    }

    assignServiceOrder(id, body: AssignServiceOrder): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/assign/${id}`, body)
    }

    inprogressServiceOrder(id, body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/inprogress/${id}`, body)
    }

    rejectServiceOrder(id, body : RejectServiceOrder): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/cancel/${id}`, body)
    }

    closeServiceOrder(id, body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/close/${id}`, body)
    }

    doneServiceOrder(id, body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/done/${id}`, body)
    }

    addServiceOrderComment(body): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/comments`, body)
    }

    getServiceOrderComments(id: number): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/comments?by=default-search&serviceOrder=${id}&order=dateTime&desc&limit=1000&fetch=eager`)
    }

    donePurchaseServiceOrder(id, body: DonePurchaseServiceOrder): Observable<any> {
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/done/${id}`, body)
    }

    getServiceOrderImages(id: number): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/documents/devices/${id}`)
    }

    setPictureDevice(formData: FormData,type, deviceId): Observable<any> {

        var params = ''
        params += '?type='+type;
        params += '&id='+deviceId;

        let _headers = new HttpHeaders()
                            .set('enctype', 'multipart/form-data')
                            .set('Accept', 'application/json;charset=UTF-8');
         
        return this.http.post<any>( `${this.serverURL}/documents/devices${params}`, 
                                    formData, 
                                    { headers: _headers });
    }

    getSchedulesTodayByBrand(day : Date): Observable<any> {
        
        let startDate = moment(day).startOf('day');
        let endDate   = moment(day).endOf('day');

        let startArray = startDate.toISOString().split('T')
        let endArray = endDate.toISOString().split('T')

        let brandId = this.appContext.session.user.brand;

        let start = startArray[0] + 'T00:00:00'
        let end = endArray[0] + 'T00:00:00'


        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&limit=1000&fields=installation.scheduledDate,uninstallation.scheduledDate,maintenance.scheduledDate&fetch=eager&brand=${brandId}&term=[${start},${end}]`)
    }

}