import { Injectable, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { AppContext } from "app/core/appcontext";
import { BaseRestService } from "app/core/services/baserest.service";

@Injectable()
export class DocumentsService extends BaseRestService<any> {

    constructor(protected injector: Injector,
                private appContext: AppContext){

                super(injector, 'documents')
    }

    getDocumentsDevice(deviceId): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/devices/${deviceId}`)
    }
    
    getDocumentsTerms(): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/terms/TERM_USER_TERMS_USE`)
    }

    getDocumentsPrivacy(): Observable<any>  {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/terms/TERM_USER_PRIVACY_POLICY`)
    }
}