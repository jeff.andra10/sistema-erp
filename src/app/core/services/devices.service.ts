import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Device } from "../models/device.model"
import { BaseRestService } from "./baserest.service";
import { AppContext } from "../appcontext";
import { map } from "rxjs/operators";

@Injectable()
export class DevicesService extends BaseRestService<Device> {

    constructor(
        protected injector: Injector,
        private appContext: AppContext
    ) {
        super(injector, 'devices')
    }

    createDevice(body: any): Observable<any> {
        return this.http.post<any>( `${this.serverURL}/${this.baseURL}/creation`, body)
    }

    testDevices(id): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/test/${id}`, body)
    }

    getDevices(params): Observable<any> {
        let defaultParams = `?by=default-search&brand=${this.appContext.session.user.brand}&limit=${params.limit}&page=${params.page}`
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)
    }

    getDevicesByModel(params): Observable<any> {
        let defaultParams = `?by=default-search&brand=${this.appContext.session.user.brand}&model=${params.model}&limit=${params.limit}&page=${params.page}`
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)
    }

    getByLicensePlate(licensePlate): Observable<any> {
        let defaultParams = `?by=customOperatorFilter&fetch=eager&licensePlate=${licensePlate}&brand=${this.appContext.session.user.brand}`
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)
    }

    getDevicesByIds(ids:Array<number>): Observable<any> {

        let defaultParams = `?by=ids&ids=${ids.join(",")}&limit=1000 `
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)
    }

    findDevicesByIds( ids,
                        sortField = '', 
                        sortOrder = '', 
                        pageNumber, 
                        pageSize ):  Observable<any> {

         var params = `?by=ids&ids=${ids.join(",")}`

        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()
        
        return this.http.get(`${this.serverURL}/${this.baseURL}${params}`)
        .pipe( map(res =>  res) );
    }    

    getDevicesById(id:number): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}/${id}`)
    }

    discardDevices(id:number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/discard/${id}`, body)
    }

    cancelDevices(id:number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/cancel/${id}`, body)
    }

    activateDevices(id:number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/activate/${id}`, body)
    }

    reactivateDevices(id:number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/reactivate/${id}`, body)
    }

    deactivateDevices(id:number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/deactivate/${id}`, body)
    }

    maintenanceDevices(id:number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/under-maintenance/${id}`, body)
    }

    returnedDevice(id:number): Observable<any> {
        let  body = {"loggedUser": this.appContext.loggedUser.id}
        return this.http.post<any>(`${this.serverURL}/${this.baseURL}/returned/${id}`, body)
    }

    getDevicesByCarId(id:number): Observable<any> {
        let defaultParams = `?by=default-search&car=${id}`
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}${defaultParams}`)
    }

    getDevicesBySearchTerms(term): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/${this.baseURL}?by=default-search&fields=imei,model&term=${term}&brand=${this.appContext.session.user.brand}`)
    }

    findDevices(filter = '', 
                brand,
                model = '',
                status = '',
                startDate = '',
                endDate = '',
                sortField = '', 
                sortOrder = '', 
                pageNumber = 0, 
                pageSize = 3):  Observable<any> {

        var params = ''
        params += 'by=default-search'
        if (filter != '') {
            params += '&fields=identifier,imei,simCard,number'
            params += '&term=' + filter
        }

        if (model != '') {
            params += '&model='+model
        }

        if (status != '') {
            params += '&status='+status
        }

        if (startDate != '' && endDate != '') {
            let date = []; 
            date.push(startDate);
            date.push(endDate);
            params += '&lifecycle.returnedDate='+date
        }

        if(brand != 0 && brand != undefined){
            params += '&brand='+brand
        }else{
            params += '&brand=' + this.appContext.session.user.brand
        }
        
        if (sortField != '') {
            params += '&order=' + sortField
            if (sortOrder != '') {
                params += '&' + sortOrder
            }
        }
        
        
        params += '&page=' + pageNumber.toString()
        params += '&limit=' + pageSize.toString()

        
        return this.http.get(`${this.serverURL}/${this.baseURL}?${params}&fetch=eager&relationships=vehicle`)
        .pipe( map(res =>  res) );
    }   
    
    getReportDevicesByBrand(id:number): Observable<any> {
        return this.http.get<any>(`${this.serverURL}/reports/fact/devices?by=brand&brand=${id}`)
    }
}