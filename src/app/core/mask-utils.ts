export class MaskUtils {

    public static get phoneMask() {
        return ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    } 

    public static get mobilePhoneMask() {
        return ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    } 

    public static get postalCodeMask() {
        return [ /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]
    } 

    public static get cnpjMask() {
        return [ /\d/ , /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/ , /\d/, /\d/, '/', /\d/, /\d/,/\d/, /\d/, '-', /\d/, /\d/,]
    } 

    public static get cpfMask() {
        return [ /\d/ , /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/ , /\d/, /\d/, '-', /\d/, /\d/,]
    }     

    public static get rgMask() {
        return [ /\d/ , /\d/ , /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]
    }

    public static get cnhMask() {
        return [ /\d/ , /\d/ , /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
    }

    public static get renavamMask() {
        return [ /[0-9]/, /[0-9]/ , /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/,/[0-9]/, /[0-9]/]
    }

    public static get simCardMask() {
        return [/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]
    }

    public static get licensePlateMask() {
        //return [/[a-zA-Z]/, /[a-zA-Z]/, /[a-zA-Z]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/]
        return [/[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[a-zA-Z0-9]/, /[0-9]/]
        
    }

    public static get chassiMask() {
        return [/[0-9]/,/[a-zA-Z]/, /[a-zA-Z]/, /[a-zA-Z]/, /[a-zA-Z]/, /[a-zA-Z]/,/[0-9]/, /[0-9]/, /[0-9]/, /[a-zA-Z]/, /[a-zA-Z]/,/\d/ , /\d/ , /\d/, /\d/, /\d/, /\d/]
    }

    public static get dateMask() {
        return [/[0-9]/, /[0-9]/, '/' ,/[0-9]/,/[0-9]/, '/' ,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]
    }

    public static get yearMask() {
        return [/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]
    }

    public static get expMonthMask() {
        return [/[0-9]/, /[0-9]/]
    }

    public static get expYearMask() {
        return [/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]
    }
    
 }