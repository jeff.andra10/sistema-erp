
export class DateHelp  {

    public convertStringToDate(date){
        try {
            if(date){
              let  _date = date.split('/');
                  _date =_date[1] +'-'+_date[0]+'-'+_date[2]
              return new Date(_date)
            }
        }
        catch(err) {
            return new Date()
        }
    }

    public convertDateToString(date){
        if(date){
          let _date = new Date(date)
          let day = _date.getDate()
          let month = _date.getMonth()+1
          return (day < 10 ? '0'+day : day) +'/'+ ( month < 10 ? '0'+month : month) +'/' + _date.getFullYear()
        }
      }
}

