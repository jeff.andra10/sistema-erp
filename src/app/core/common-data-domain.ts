
export enum StateAddress {
    AC = "AC", 
    AL = "AL", 
    AM = "AM", 
    AP = "AP", 
    BA = "BA", 
    CE = "CE",
    DF = "DF",
    ES = "ES",
    GO = "GO",  
    MA = "MA", 
    MG = "MG", 
    MS = "MS", 
    MT = "MT", 
    PA = "PA", 
    PB = "PB", 
    PE = "PE", 
    PI = "PI", 
    PR = "PR", 
    RJ = "RJ", 
    RN = "RN", 
    RO = "RO", 
    RR = "RR", 
    RS = "RS", 
    SC = "SC", 
    SE = "SE", 
    SP = "SP", 
    TO = "TO"
}

export class CONSTANTS {

    static TRACKNME_SUPPLIER : number = 5578010453344256
    static TRACKNME_CONSTANT_ID : number = 111111110
    static VERSION_ID : number = 18112213
}

export class DataDomain {

    static getBanks() {

        let list = []

        list.push({key: '246', value: '246 - Banco ABC Brasil S.A.'})
        list.push({key: '75', value: '75 - Banco ABN AMRO S.A.'})
        list.push({key: '121', value: '121 - Banco Agibank S.A.'})
        list.push({key: '25', value: '25 - Banco Alfa S.A.'})
        list.push({key: '641', value: '641 - Banco Alvorada S.A.'})
        list.push({key: '65', value: '65 - Banco Andbank (Brasil) S.A.'})
        list.push({key: '24', value: '24 - Banco BANDEPE S.A.'})
        list.push({key: '96', value: '96 - Banco BM&FBOVESPA de Serviços de Liquidação e Custódia S.A'})
        list.push({key: '318', value: '318 - Banco BMG S.A.'})
        list.push({key: '752', value: '752 - Banco BNP Paribas Brasil S.A.'})
        list.push({key: '248', value: '248 - Banco Boavista Interatlântico S.A.'})
        list.push({key: '107', value: '107 - Banco BOCOM BBM S.A.'})
        list.push({key: '63', value: '63 - Banco Bradescard S.A.'})
        list.push({key: '36', value: '36 - Banco Bradesco BBI S.A.'})
        list.push({key: '204', value: '204 - Banco Bradesco Cartões S.A.'})
        list.push({key: '394', value: '394 - Banco Bradesco Financiamentos S.A.'})
        list.push({key: '237', value: '237 - Banco Bradesco S.A.'})
        list.push({key: '218', value: '218 - Banco BS2 S.A.'})
        list.push({key: '208', value: '208 - Banco BTG Pactual S.A.'})
        list.push({key: '263', value: '263 - Banco Cacique S.A.'})
        list.push({key: '473', value: '473 - Banco Caixa Geral - Brasil S.A.'})
        list.push({key: '40', value: '40 - Banco Cargill S.A.'})
        list.push({key: '739', value: '739 - Banco Cetelem S.A.'})
        list.push({key: '233', value: '233 - Banco Cifra S.A.'})
        list.push({key: '745', value: '745 - Banco Citibank S.A.'})
        list.push({key: '95', value: '95 - Banco Confidence de Câmbio S.A.'})
        list.push({key: '756', value: '756 - Banco Cooperativo do Brasil S.A. - BANCOOB'})
        list.push({key: '748', value: '748 - Banco Cooperativo Sicredi S.A.'})
        list.push({key: '222', value: '222 - Banco Credit Agricole Brasil S.A.'})
        list.push({key: '505', value: '505 - Banco Credit Suisse (Brasil) S.A.'})
        list.push({key: '3', value: '3 - Banco da Amazônia S.A.'})
        list.push({key: '83', value: '83 - Banco da China Brasil S.A.'})
        list.push({key: '707', value: '707 - Banco Daycoval S.A.'})
        list.push({key: '1', value: '1 - Banco do Brasil S.A.'})
        list.push({key: '47', value: '47 - Banco do Estado de Sergipe S.A.'})
        list.push({key: '37', value: '37 - Banco do Estado do Pará S.A.'})
        list.push({key: '41', value: '41 - Banco do Estado do Rio Grande do Sul S.A.'})
        list.push({key: '4', value: '4 - Banco do Nordeste do Brasil S.A.'})
        list.push({key: '265', value: '265 - Banco Fator S.A.'})
        list.push({key: '224', value: '224 - Banco Fibra S.A.'})
        list.push({key: '94', value: '94 - Banco Finaxis S.A.'})
        list.push({key: '612', value: '612 - Banco Guanabara S.A.'})
        list.push({key: '12', value: '12 - Banco INBURSA de Investimentos S.A.'})
        list.push({key: '604', value: '604 - Banco Industrial do Brasil S.A.'})
        list.push({key: '653', value: '653 - Banco Indusval S.A.'})
        list.push({key: '77', value: '77 - Banco Inter S.A.'})
        list.push({key: '249', value: '249 - Banco Investcred Unibanco S.A.'})
        list.push({key: '184', value: '184 - Banco Itaú BBA S.A.'})
        list.push({key: '29', value: '29 - Banco Itaú Consignado S.A.'})
        list.push({key: '479', value: '479 - Banco ItauBank S.A'})
        list.push({key: '376', value: '376 - Banco J. P. Morgan S.A.'})
        list.push({key: '74', value: '74 - Banco J. Safra S.A.'})
        list.push({key: '217', value: '217 - Banco John Deere S.A.'})
        list.push({key: '600', value: '600 - Banco Luso Brasileiro S.A.'})
        list.push({key: '389', value: '389 - Banco Mercantil do Brasil S.A.'})
        list.push({key: '370', value: '370 - Banco Mizuho do Brasil S.A.'})
        list.push({key: '746', value: '746 - Banco Modal S.A.'})
        list.push({key: '456', value: '456 - Banco MUFG Brasil S.A.'})
        list.push({key: '735', value: '735 - Banco Neon S.A.'})
        list.push({key: '169', value: '169 - Banco Olé Bonsucesso Consignado S.A.'})
        list.push({key: '212', value: '212 - Banco Original S.A.'})
        list.push({key: '623', value: '623 - Banco PAN S.A.'})
        list.push({key: '611', value: '611 - Banco Paulista S.A.'})
        list.push({key: '643', value: '643 - Banco Pine S.A.'})
        list.push({key: '747', value: '747 - Banco Rabobank International Brasil S.A.'})
        list.push({key: '633', value: '633 - Banco Rendimento S.A.'})
        list.push({key: '120', value: '120 - Banco Rodobens S.A.'})
        list.push({key: '422', value: '422 - Banco Safra S.A.'})
        list.push({key: '33', value: '33 - Banco Santander  (Brasil)  S.A.'})
        list.push({key: '743', value: '743 - Banco Semear S.A.'})
        list.push({key: '366', value: '366 - Banco Société Générale Brasil S.A.'})
        list.push({key: '464', value: '464 - Banco Sumitomo Mitsui Brasileiro S.A.'})
        list.push({key: '82', value: '82 - Banco Topázio S.A.'})
        list.push({key: '634', value: '634 - Banco Triângulo S.A.'})
        list.push({key: '655', value: '655 - Banco Votorantim S.A.'})
        list.push({key: '610', value: '610 - Banco VR S.A.'})
        list.push({key: '119', value: '119 - Banco Western Union do Brasil S.A.'})
        list.push({key: '21', value: '21 - BANESTES S.A. Banco do Estado do Espírito Santo'})
        list.push({key: '719', value: '719 - Banif-Banco Internacional do Funchal (Brasil)S.A.'})
        list.push({key: '755', value: '755 - Bank of America Merrill Lynch Banco Múltiplo S.A.'})
        list.push({key: '81', value: '81 - BBN Banco Brasileiro de Negócios S.A.'})
        list.push({key: '250', value: '250 - BCV - Banco de Crédito e Varejo S.A.'})
        list.push({key: '144', value: '144 - BEXS Banco de Câmbio S.A.'})
        list.push({key: '17', value: '17 - BNY Mellon Banco S.A.'})
        list.push({key: '125', value: '125 - Brasil Plural S.A. - Banco Múltiplo'})
        list.push({key: '70', value: '70 - BRB - Banco de Brasília S.A.'})
        list.push({key: '104', value: '104 - Caixa Econômica Federal'})
        list.push({key: '320', value: '320 - China Construction Bank (Brasil) Banco Múltiplo S.A.'})
        list.push({key: '477', value: '477 - Citibank N.A.'})
        list.push({key: '487', value: '487 - Deutsche Bank S.A. - Banco Alemão'})
        list.push({key: '64', value: '64 - Goldman Sachs do Brasil Banco Múltiplo S.A.'})
        list.push({key: '62', value: '62 - Hipercard Banco Múltiplo S.A.'})
        list.push({key: '492', value: '492 - ING Bank N.V.'})
        list.push({key: '652', value: '652 - Itaú Unibanco Holding S.A.'})
        list.push({key: '341', value: '341 - Itaú Unibanco S.A.'})
        list.push({key: '488', value: '488 - JPMorgan Chase Bank, National Association'})
        list.push({key: '399', value: '399 - Kirton Bank S.A. - Banco Múltiplo'})
        list.push({key: '128', value: '128 - MS Bank S.A. Banco de Câmbio'})
        list.push({key: '254', value: '254 - Paraná Banco S.A.'})
        list.push({key: '751', value: '751 - Scotiabank Brasil S.A. Banco Múltiplo'})
        list.push({key: '118', value: '118 - Standard Chartered Bank (Brasil) S/A–Bco Invest.'})
        list.push({key: '129', value: '129 - UBS Brasil Banco de Investimento S.A.'})

        return list
    }


    static getLeaseContractPeriod() {
        return [
            { key: 12, value: '12 meses' },
            { key: 24, value: '24 meses' },
            { key: 36, value: '36 meses' }
          ];
    }

    static getInstallments() {
        return [
            { key: 1, value: 'Á vista' },
            { key: 2, value: '2x sem juros' },
            { key: 3, value: '3x sem juros' },
            { key: 4, value: '4x sem juros' },
            { key: 5, value: '5x sem juros' },
            { key: 6, value: '6x sem juros' },
            { key: 7, value: '7x com juros' },
            { key: 8, value: '8x com juros' },
            { key: 9, value: '9x com juros' },
            { key: 10, value: '10x com juros' },
            { key: 11, value: '11x com juros' },
            { key: 12, value: '12x com juros' }
          ];
    }

}
