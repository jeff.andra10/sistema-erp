import { Deserializable } from "./deserializable.model";

export class Address implements Deserializable<Address> {
    
    id: number
    name: string
    postalCode: number
    state: string
    city: string
    district: string
    street: string
    complement: string
    number: string
    reference: string
    user: number
    business: number

    deserialize(input: any): Address {
        Object.assign(this, input)
        return this
    }
}

export enum AddressStateUsebens {
    AC = "59",
    AL =  "8",
    AP =  "61",
    AM =  "60",
    BA =  "13",
    CE =  "62",
    DF =  "15",
    ES =  "21",
    GO =  "22",
    MA =  "4",
    MT =  "24",
    MS =  "3",
    MG =  "17",
    PA = "20",
    PB = "18",
    PR =  "63",
    PE =  "14",
    PI = "12",
    RJ =  "2",
    RN =  "64",
    RS =  "23",
    RO =  "6",
    RR =  "26",
    SC =  "11",
    SP =  "1",
    SE =  "9",
    TO =  "5"
}