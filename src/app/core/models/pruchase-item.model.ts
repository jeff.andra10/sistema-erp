import { Deserializable } from "./deserializable.model";


export class PurchaseItem implements Deserializable<PurchaseItem> {
    
    id: number
    description: string
    amount: number
    price: number
    offer: number

    deserialize(input: any): PurchaseItem {
        Object.assign(this, input);
        return this;
    }
}


