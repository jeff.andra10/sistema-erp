import { Deserializable } from "./deserializable.model";

export class Contact implements Deserializable<Contact> {
    
    id: number
    name: string
    value: string
    type: string
    user: number
    
    deserialize(input: any): Contact {
        Object.assign(this, input)
        return this
    }
}

export enum ContactType {
    MOBILE = "Celular",
    EMAIL = "E-mail"
}