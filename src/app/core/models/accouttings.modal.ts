import { Deserializable } from "./deserializable.model";

export class Accouttings implements Deserializable<Accouttings> {

    id: number;
    brand: number;
    description: string;
    amount: number;
    deductedAmount: number;
    remainingAmount: number;
    creationDateTime: Date;
    type: string;
    origin: string;
    reference: number;
    brandObj: any;

    deserialize(input: any): Accouttings {
        Object.assign(this, input);
        return this;
    }
}

export enum AccouttingsOrigin {
    PURCHASE = "Compra",
    SALE  = "Venda",
    RECURRENCE = "Recorrente",
    ACCESSION = "Adesão"
}
