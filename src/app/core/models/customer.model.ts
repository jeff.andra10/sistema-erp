import { Deserializable } from "./deserializable.model";
import { Brand } from "./brand.model";

export class Customer implements Deserializable<Customer> {
    id: number
    login: string
    name: string
    email: string
    registrationNumber: number
    stateRegistration: number
    gender: string
    status: string
    profile: string
    brand: number
    password: string
    confirmPassword: string
    birthDate: Date
    nationalIdentity: string
    nationalIdentityExpeditionAgency: string
    nationalIdentityExpeditionDate: Date
    procurator: string
    occupation: string
    driverLicense: string
    driverLicenseCategory: string
    maritialStatus: string
    residencialPhone: number
    commercialPhone: number
    mobilePhone: number
    user: number
    personType: string

    brandObj: any

    deserialize(input: any): Customer {
        Object.assign(this, input)
        return this
    }
}

export enum MaritalStatus {
    SINGLE = "Solteiro", 
    MARRIED = "Casado", 
    SEPARATED = "Separado", 
    DIVORCED = "Divorciado", 
    WIDOWED = "Outro"
}

export enum MaritalStatusUsebens {
    SINGLE = "1", 
    MARRIED = "2", 
    SEPARATED = "6", 
    DIVORCED = "4", 
    WIDOWED = "5"
}

export enum DriverLicenseCategory {
    A = "A", 
    B = "B", 
    C = "C", 
    D = "D",
    E = "E",
    AB = "AB", 
    ACC = "ACC"
}

export enum PersonType {
    PERSON = "Fisica", 
    COMPANY = "Jurídica"
}

export enum PersonGender {
    MALE = "Masculino", 
    FEMALE = "Feminino", 
}




