export interface SimCard {
    id: number
    brand: number
    ICCID: string
    MSISDN: string
}