import { Deserializable } from "./deserializable.model";


export class SaleItem implements Deserializable<SaleItem> {
    
    type: string
    reference: number
    description: string
    price: number
    amount: number

    deserialize(input: any): SaleItem {
        Object.assign(this, input);
        return this;
    }
}


export enum SaleItemType {
    OFFER = "Oferta"
}


