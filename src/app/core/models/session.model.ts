import { Deserializable } from './deserializable.model';
import { User } from './user.model';

export class SessionModel implements Deserializable<SessionModel> {
  sessionId: string;
  accessToken: string;
  user: User;

  deserialize(input: any): SessionModel {
    Object.assign(this, input);
    console.log(input, 'input');
    
    this.user = new User().deserialize(input);
    return this;
  }
}
