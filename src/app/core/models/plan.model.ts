import { Deserializable } from "./deserializable.model";

import { Customer } from "./customer.model";
import { Offer } from "./offer.model";
import { User } from "./user.model";
import { Payment } from "./payment.model";
import { Vehicle } from "./vehicle.model";
import { Device } from "./device.model";

export class Plan implements Deserializable<Plan> {

    id: number;
    brand: number;
    user: number;
    payment: number;
    vehicle: number;
    device: number;
    offer: number;
    status: string;
    recurrence: string;
    nextRecurrenceCharging: Date;
    version: number;

    relationships? : {
        customer?: Customer
        user: User
        payment: Payment
        offer: Offer
        vehicle: Vehicle
        device: Device
    }

    deserialize(input: any): Plan {
        Object.assign(this, input);
        return this;
    }
}

export enum PlanStatus {
    ALL = "Todos",
    //CREATED  = "Criado",
    INACTIVE = "Inativo",
    ACTIVE = "Ativo",
    CANCELED = "Cancelado",
    //UNINSTALLED = "Desinstalado"//"Desinstalado"
}

export enum PaymentStatus {
    ALL = "Todos",
    DUE = "Em dia",
    OVERDUE = "Em atraso"
}
export enum PlanRecurrence {
    DAILY = 'Diária',
    MONTHLY = 'Mensal',
    QUARTERLY = 'Trimestral',
    SEMIANNUALLY = 'Semestral',
    YEARLY = 'Anual',
}     



export enum PlanColorStatus {
    CREATED  = "mat-blue-500-bg",
    INACTIVE = "mat-orange-500-bg",
    ACTIVE = "mat-green-500-bg",
    CANCELED = "mat-red-900-bg",
    UNINSTALLED = "mat-primary-400-bg"
}

export enum PlanColorStatusLight {
    CREATED  = "mat-blue-100-bg",
    INACTIVE = "mat-orange-100-bg",
    ACTIVE = "mat-green-100-bg",
    CANCELED = "mat-red-100-bg",
    UNINSTALLED = "mat-primary-100-bg"
}