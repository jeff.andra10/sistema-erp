export class Device {
    accuracy: number
    activity: string
    angle: number
    battery: number
    batteryDTO: object
    brand: number
    car: number
    hidden: boolean
    carDTO: object
    engineFaultCode: Array<any>
    geolocate: boolean
    gsmSignal: number
    id: number
    identifier: number
    imei: number
    interval: number
    intervalDTO: object
    lastMovement: string
    lastTracking: string
    lastValid: number
    latitude: number
    longitude: number
    model: string
    movement: string
    number: number
    odometer: number
    operator: string
    profileDownload: string
    satellitesAvailable: number
    sensors: Array<any>
    simCard: string
    speed: number
    status: string
    user: number
    velocity: number
    velocityDTO: object
}

export enum DevicesModel {
    Y2  = "Y2",
    A9 = "A9",
    PT60 = "PT60",
    PT36 = "PT36",
    VT200 = "VT200",
    TNMC101 = "TNM-C101",
    SUNTECH100 = "ST100",
    SUNTECH210 = "ST210",
    SUNTECH215  = "ST215",
    SUNTECH215H  = "ST215 H",
    SUNTECH240 = "ST240",
    SUNTECH300  = "ST300",
    SUNTECH310U = "ST310 U",
    SUNTECH340  = "ST340",
    SUNTECH340RB = "ST340 RB",
    SUNTECH350  = "ST350",
    SUNTECH380  = "ST380",
    SUNTECH910  = "ST910",
    SUNTECH940  = "ST940",
    MXT120 = "MXT120",
    MXT140  = "MXT140",
    MXT141 = "MXT141",
    MXT150 = "MXT150",
    MXT151 = "MXT151",
    GV55 = "GV55",
    TK103 = "TK103",
    TK06B = "TK06B"
}


export enum DeviceStatus {
    CREATED  = "Criado",
    INACTIVE = "Inativo",
    ACTIVE = "Ativo",
    CANCELED = "Cancelado",
    UNINSTALLED = "Desinstalado",
    TESTED = "Testado",
    RECEIVED = "Recebido",
    DISCARDED = "Descartado",
    UNDER_MAINTENANCE = "Em Manutenção",
    RETURNED = "Devolvido"
}

export enum DeviceStatusDate {
    CREATED  = "Data da Criação",
    INACTIVE = "Data da Criação",
    ACTIVE = "Data da Ativação",
    CANCELED = "Data do Cancelamento",
    UNINSTALLED = "Data da Desinstalação",
    TESTED = "Date do Teste",
    RECEIVED = "Data do Recebimento",
    DISCARDED = "Data do Descarte",
    UNDER_MAINTENANCE = "Data da Manutenção",
    RETURNED = "Data do Devolução"
}

export enum DeviceStatusLifecycle {
    CREATED  = "inactiveDate",
    INACTIVE = "inactiveDate",
    ACTIVE = "activeDate",
    CANCELED = "canceledDate",
    UNINSTALLED = "uninstalledDate",
    TESTED = "testedDate",
    RECEIVED = "receivedDate",
    DISCARDED = "discardedDate",
    UNDER_MAINTENANCE = "underMaintenanceDate",
    RETURNED = "returnedDate"
}

export enum DeviceColorStatus {
    CREATED  = "mat-blue-500-bg",
    INACTIVE = "mat-orange-500-bg",
    ACTIVE = "mat-green-500-bg",
    CANCELED = "mat-red-900-bg",
    UNINSTALLED = "mat-primary-400-bg",
    TESTED = "mat-blue-800-bg",
    RECEIVED = "mat-purple-300-bg",
    DISCARDED = "mat-primary-400-bg",
    UNDER_MAINTENANCE = "mat-primary-400-bg",
    RETURNED = "mat-primary-400-bg"
}

export enum DeviceColorStatusLight {
    CREATED  = "mat-blue-100-bg",
    INACTIVE = "mat-orange-100-bg",
    ACTIVE = "mat-green-100-bg",
    CANCELED = "mat-red-100-bg",
    UNINSTALLED = "mat-primary-100-bg",
    TESTED = "mat-blue-100-bg",
    RECEIVED = "mat-purple-100-bg",
    DISCARDED = "mat-primary-100-bg",
    UNDER_MAINTENANCE = "mat-primary-100-bg",
    RETURNED = "mat-primary-100-bg"
}