import { Address } from "./address.model";
import { Brand } from "./brand.model";

export class ServiceOrder {
   
    id: number
    brand: number
    status: string
    type: string
    description: string
    technician: number
    brandObj : Brand
    lifecycle : {
        openDate: Date,
        openLoggedUser: number,
        cancelDate?: Date,
        cancelLoggedUser?: number
        assignDate?: Date,
        assignLoggedUser?: number
        rejectDate?: Date,
        rejectLoggedUser?: number
        inprogressDate?: Date,
        inprogressLoggedUser?: number
        doneDate?: Date,
        doneLoggedUser?: number  
    }

    installation?: Installation
    uninstallation?: Uninstallation
    maintenance?: Maintenance
    purchase?: {
        purchase: number
        devices: Array<number>
    } 
    relationships? : any
    serviceOrderStatus : ServiceOrderStatus
    serviceOrderType : ServiceOrderType
    
}

export class Installation {
    user:          number;
    customer:      number;
    inspection:    boolean;
    plan:          number;
    scheduledDate: string;
    address:       Address;
    checkList:     any[];
    installationSpot: string;
    pendencyType : string
    pendencyDescription: string
    priority : string
    schedulingConfirmation : boolean
    period: string
    vehicle: string
}

export class Uninstallation {
    user:          number;
    customer:      number;
    plan:          number;
    device:        number;
    scheduledDate: string;
    address:       Address;
    checkList:     any[];
    installationSpot: string;
    pendencyType : string
    pendencyDescription: string
    priority : string
    schedulingConfirmation : boolean
    period: string
    vehicle: string
}

export class Maintenance {
    user:          number;
    customer:      number;
    plan:          number;
    device:        number;
    scheduledDate: string;
    address:       Address;
    checkList:     any[];
    installationSpot: string;
    pendencyType : string
    pendencyDescription: string
    priority : string
    schedulingConfirmation : boolean
    period: string
    vehicle: string
}

export class Purchase {

    purchase: number
    devices: Array<number>
}

export class AssignServiceOrder {
    loggedUser:    number;
    comment:       string;
    technician:    number;
    scheduledDate: any;
    postalCode:    number;
    state:         string;
    city:          string;
    district:      string;
    street:        string;
    number:        string;
    complement:    string;
    reference:     string;
    duringBusinessHours:     boolean;
    deslocationTime:     string;
    serviceLenght: string;
    period: string;
    beta: boolean;
}

export class Appointment {
    customerConfirmation: boolean
    deslocationTime: number;
    duringBussinessHours: boolean;
    id: number;
    installer: number;
    installerConfirmation: boolean;
    lenght:string;
    period: string;
    relationships: any;
    scheduledDay: number;
    serviceOrder: number;
    startDate: any;
}

export class RejectServiceOrder {
    loggedUser: number;
    comment:    string;
}

export class DonePurchaseServiceOrder {
    loggedUser: number;
    skipTests : boolean;
    devices:    Array<number>;
}

export enum ServiceOrderStatus {
    OPEN = "Aberta",
    ASSIGNED = "Agendada",
    CANCELED = "Cancelada",
    CLOSED = "Finalizada",
    DONE = "Realizada",
    IN_PROGRESS = "Em andamento",
    REJECTED = "Rejeitada",
    PENDENCY = "Pendência"

}

export enum ServiceOrderType {
    INSTALLATION = "Instalação",
    UNINSTALLATION = "Desinstalação",
    PURCHASE = "Compra",
    MAINTENANCE = "Manutenção"
}
export enum ServiceOrderCheckList {
    NOT_INFORMED = "Não Tem",
    OK = "Bom",
    DAMAGED = "Ruim",
}
export enum ServiceOrderMaintenanceType {
    DEVICE_EXCHANGE = "Troca de Dispositivo",
    DEVICE_LOCAL_CHANGE = "Troca de local do Dispositivo",
    DEVICE_CANCELLATION = "Cancelamento de Dispositivo"
}

export enum ServiceOrderColorStatus {
    "Aberta" = "mat-blue-500-bg",
    "Agendada" = "mat-orange-500-bg",
    "Cancelada" = "mat-red-900-bg",
    "Finalizada" = "mat-primary-500-bg",
    "Realizada" = "mat-green-500-bg",
    "Em andamento" = "mat-purple-300-bg",
    "Rejeitada" = "mat-primary-400-bg",
    "Pendência" = "mat-red-900-bg"
}

export enum ServiceOrderColorStatusLight {
    "Aberta" = "mat-blue-100-bg",
    "Agendada" = "mat-orange-100-bg",
    "Cancelada" = "mat-red-100-bg",
    "Finalizada" = "mat-primary-100-bg",
    "Realizada" = "mat-green-100-bg",
    "Em andamento" = "mat-purple-100-bg",
    "Rejeitada" = "mat-primary-100-bg",
    "Pendência" = "mat-red-900-bg"
}

