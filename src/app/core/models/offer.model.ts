import { Deserializable } from "./deserializable.model";

export class Offer implements Deserializable<Offer> {

    id: number
    name: string    
    description: string
    accession: number
    recurrence: number
    products: Array<any>
    services: Array<any>
    brand: number

    agreement: string
    chargingRecurrence: string
    recurrencePrice: number
    salePrice: number 
    saleMaxInstallments: number
    leaseInstallmentPrice: number
    leaseContractPeriod: number
    leaseAccessionPrice: number
    leaseGracePeriod: number
    availableForBrands: Array<number>
    availablePaymentMethods: Array<string>
    availableForAllBrands: boolean
    segment: string

    deserialize(input: any): Offer {
        Object.assign(this, input);
        return this;
    }
}
