import { Deserializable } from "./deserializable.model";

export class Payment implements Deserializable<Payment> {
    
    id: number
    type: string
    creditCardBrand: string
    creditCardHolderName: string
    creditCardNumber: string
    creditCardExpMonth: string
    creditCardExpYear: string
    creditCardSecurityCode: string
    user: number
    business: number
    debitCardHolderName: string
    debitCardBrand: string
    debitCardNumber: string
    debitCardSecurityCode: number
    debitCardExpYear: number
    debitCardExpMonth: number
    debitCardBank: string
    installments: number

    deserialize(input: any): Payment {
        Object.assign(this, input)
        return this
    }
}

export enum PaymentType {
    CREDIT_CARD = "Cartão de Crédito",
    BOOKLET = "Boleto",
    DEBIT_CARD = "Cartão de Débito",
    ELETRONIC_TRANSFER = "Transferência Eletrônica",
    BANK_DEPOSIT = "Deposito Bancario"
}
export enum PaymentCreditCard {
    WAITING = "Aguardando",
    PROCESSING = "Processando",
    DEFERRED = "Diferido",
    FAILED = "Falhou",
    FAIL = "Falhou",
    SUCCESS = "Sucesso",
    DROPPED = "Desistiu",
    PENDING = "Pendente de Pagamento",
    SCHEDULED = "Programado",
    APPROVING = "Processando Pagamento",
    WAITING_FOR_CUSTOMER = "Aguardando Confirmação",
    DELAYED = "Atrasado"
}

export enum ChargeType {
    PLAN_ACCESSION = "Adesão",
    PLAN_RECURRENCE = "Mensalidade",
    CUSTOMER_CONTRACT_ACCESSION = "Contrato de Adesão",
    CUSTOMER_CONTRACT_LEASE_INSTALLMENT = "Contrato de Instalação",
    CUSTOMER_CONTRACT_RECURRENCE = "Contrato de Mensalidade",
    BUSINESS_CONTRACT_ACCESSION = "Contrato de Adesão",
    BUSINESS_CONTRACT_LEASE_INSTALLMENT = "Contrato de Instalação",
    BUSINESS_CONTRACT_RECURRENCE = "Contrato de Mensalidade"
}

export enum CreditCardBrand {
    Visa = "fab fa-cc-visa",
    Mastercard =  "fab fa-cc-mastercard",
    Diners = "fab fa-cc-diners-club",
    Hipercard = "fas fa-credit-card",
    Amex = "fab fa-cc-amex",
    Elo = "fas fa-credit-card",
    Aura = "fas fa-credit-card",
    Discover = "fab fa-cc-discover"
}

export enum Month {
    JANUARY = '01',
    FEBRUARY = '02',
    MARCH = '03',
    APRIL = '04',
    MAY = '05',
    JUNE = '06',
    JULY = '07',
    AUGUST = '08',
    SEPTEMBER = '09',
    OCTOBER = '10',
    NOVEMBER = '11',
    DECEMBER = '12',
  }

  export enum PaymentDebitCardBank {
	'BRADESCO' = 'Bradesco',
	'BANCO DO BRASIL' = 'BancoDoBrasil',
	'SANTANDER' = 'Santander',
	'ITAÚ' = 'Itau',
	'CITIBANK' = 'CitiBank',
	'BRB' = 'BRB',
	'CAIXA ECONÔMICA' = 'Caixa',
	'BANCO OB' = 'BancooB',
    
}
