import { Deserializable } from "./deserializable.model";
import { SaleItem } from "./sale-item.model";
import { Customer } from "./customer.model";
import { Offer } from "./offer.model";
import { User } from "./user.model";
import { Plan } from "./plan.model";
import { Sale } from "./sale.model";
import { PurchaseItem } from "./pruchase-item.model";
import { Payment } from "./payment.model";
import { Supplier } from "./supplier.model";

export class Purchase implements Deserializable<Purchase> {
    
    id: number
    brand: number
    supplier: number
    description: string
    status: PurchaseStatus
    user: number
    payment: number
    total: number
    discount: number
    lifecycle? : {
        createDate: Date
        createLoggedUser: number

        approveDate?: Date
        approveLoggedUser?: number

        cancelDate?: Date
        cancelLoggedUser?: number

        paidDate?: Date
        paidLoggedUser?: number        

        doneDate?: Date
        doneLoggedUser?: number

        shippedDate?: Date
        shippedLoggedUser?: number        

        rejectDate?: Date
        rejectLoggedUser?: number        

        inpreparationDate?: Date
        inpreparationLoggedUser?: number                
    }
    itens: [PurchaseItem]
    devices: Array<number>

    relationships : {
        customer: Customer
        user: User
        supplier: Supplier
        payment: Payment
        offer: Offer
        sale: Sale
    }

    deserialize(input: any): Purchase {
        
        Object.assign(this, input)

        if (input.itens) {
            this.itens = input.itens.map(item => new PurchaseItem().deserialize(item))
        }

        if (input.relationships) {

            if (input.relationships.customer) {
                this.relationships.customer = new Customer().deserialize(input.relationships.customer)
            }

            if (input.relationships.user) {
                this.relationships.user = new User().deserialize(input.relationships.user)
            }

            if (input.relationships.supplier) {
                this.relationships.supplier = new Supplier().deserialize(input.relationships.user)
            }   
            
            if (input.relationships.payment) {
                this.relationships.payment = new Payment().deserialize(input.relationships.payment)
            }
        }
        
        return this;
    }
}

export enum PurchaseStatus {
    OPEN = "Aberta",
    APPROVED = "Efetuada",
    REJECTED = "Rejeitada",
    SHIPPED = "Entregue",
    CANCELED = "Cancelada",
    PAID = "Pagamento Aprovado",
    IN_PREPARATION = "Em preparação",
    DONE = "Finalizada"
}

export enum PurchaseColorStatus {
    OPEN = "mat-blue-200-bg",
    APPROVED = "mat-green-500-bg",
    REJECTED = "mat-orange-500-bg",
    SHIPPED = "mat-brown-500-bg",
    CANCELED = "mat-red-900-bg",
    PAID = "mat-teal-500-bg",
    IN_PREPARATION = "mat-purple-400-bg",
    DONE = "mat-primary-400-bg"
}

export enum PurchaseColorStatusLight {
    OPEN = "mat-blue-100-bg",
    APPROVED = "mat-green-100-bg",
    REJECTED = "mat-orange-100-bg",
    SHIPPED = "mat-brown-100-bg",
    CANCELED = "mat-red-100-bg",
    PAID = "mat-teal-100-bg",
    IN_PREPARATION = "mat-purple-100-bg",
    DONE = "mat-primary-100-bg"
}


export class DonePurchase {
    loggedUser: number;
    devices:    Array<number>;
}