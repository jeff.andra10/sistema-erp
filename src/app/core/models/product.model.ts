export class Product {
    id: number
    name: string    
    description: string
    accession: number
    recurrence: number
    supplier: number
    brand: number
    brandObj: any
}
