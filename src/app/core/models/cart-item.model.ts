import { Deserializable } from "./deserializable.model";
import { Offer } from "./offer.model";

export class CartItem implements Deserializable<CartItem> {

    offer: Offer
    quantity: number
    
    constructor(offer?: Offer, quantity?: number) {

        this.offer = offer
        this.quantity = quantity
    }

    deserialize(input: any): CartItem {
        Object.assign(this, input);
        return this;
    }
}