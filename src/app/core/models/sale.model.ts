import { Deserializable } from "./deserializable.model";
import { SaleItem } from "./sale-item.model";
import { Customer } from "./customer.model";
import { Offer } from "./offer.model";
import { User } from "./user.model";
import { Plan } from "./plan.model";
import { Payment } from "./payment.model";

export class Sale implements Deserializable<Sale> {
    
    id: number
    brand: number
    status: SalesStatus
    description: string
    
    totalPrice: number
    user: number
    broker: number
    customer: number
    plan: number
    itens: [SaleItem]
    segment: string
    purchase: number
    payment: number
    brandObj: any

    lifecycle? : {
        createDate: Date
        createLoggedUser: number

        approveDate?: Date
        approveLoggedUser?: number

        cancelDate?: Date
        cancelLoggedUser?: number

        rejectDate?: Date
        rejectLoggedUser?: number
    }

    relationships? : {
        customer?: Customer
        user: User
        payment: Payment
        offer: Offer
        sale: Sale
    }

    deserialize(input: any): Sale {
        
        Object.assign(this, input)

        if (input.itens) {
            this.itens = input.itens.map(item => new SaleItem().deserialize(item))
        }

        if (input.relationships) {

            // if (input.relationships.customer) {
            //     this.customer = new Customer().deserialize(input.relationships.customer)
            // }

            // if (input.relationships.plan) {
            //     this.plan = new Plan().deserialize(input.relationships.plan)
            // }
        }
        
        return this;
    }
}

export enum SalesStatus {
    OPEN = "Venda em Aberto",
    APPROVED = "Venda com pagamento pendente",
    REJECTED = "Pagamento em atraso",
    CANCELED = "Venda cancelada",
    PAID = "Venda concluída",
    WAITING_FOR_CUSTOMER = "Aguardando Confirmação"
}

export enum SaleColorStatus {
    OPEN = "mat-blue-200-bg",
    APPROVED = "mat-green-500-bg",
    REJECTED = "mat-orange-500-bg",
    CANCELED = "mat-red-900-bg",
    PAID = "mat-blue-500-bg",
    WAITING_FOR_CUSTOMER = "mat-primary-400-bg"
}


export enum SaleColorStatusLight {
    OPEN = "mat-blue-100-bg",
    APPROVED = "mat-green-100-bg",
    REJECTED = "mat-orange-100-bg",
    CANCELED = "mat-red-100-bg",
    PAID = "mat-blue-100-bg",
    WAITING_FOR_CUSTOMER = "mat-primary-100-bg"
}




