import { Deserializable } from "./deserializable.model";

export class Policy implements Deserializable<Policy> {

    id: number
    cpfCnpj: string    
    customerNanem: string
    createdDate: Date
    status: string
    
    deserialize(input: any): Policy {
        Object.assign(this, input);
        return this;
    }
}
