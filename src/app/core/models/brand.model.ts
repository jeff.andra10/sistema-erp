import { Deserializable } from "./deserializable.model";

export class Brand implements Deserializable<Brand> {
    
    id: number
    name: string
    children : [Brand]

    deserialize(input: any): Brand {
        Object.assign(this, input);
        return this;
    }  
}