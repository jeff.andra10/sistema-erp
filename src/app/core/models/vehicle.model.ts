export class Vehicle {
    chassi: string
    color: string
    dateCreation: string
    description: string
    driver: string
    driverLicense: string
    id: number
    installation: string
    installer: string
    licensePlate: string
    manufacture: string
    model: string
    modelYear: string
    name: string
    registrationYear: string
    type: string
}