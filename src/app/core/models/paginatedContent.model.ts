export class PaginatedContent<T> {

    totalElements: number
    last: boolean
    totalPages: number
    first: boolean
    sort: string
    numberOfElements: number
    size: number
    number: number

    content: Array<T>
}