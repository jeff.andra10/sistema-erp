import { Deserializable } from "./deserializable.model";

export class Supplier implements Deserializable<Supplier> {

    id: number
    brand: number
    companyName: string
    tradingName: string
    personType: string
    contactPerson: string
    registrationNumber: string
    email: string
    phone: string
    address: string
    bankCode: string
    bankBranch: string
    bankAccount: string
    merchantId: string
    merchantKey: string

    deserialize(input: any): Supplier {
        Object.assign(this, input);
        return this;
    }    
}