import { Deserializable } from "./deserializable.model";

export class Coverage implements Deserializable<Coverage> {

    id: number
    code: string    
    name: string
    description: string
    lmi?: number
    
    deserialize(input: any): Coverage {
        Object.assign(this, input);
        return this;
    }
}
