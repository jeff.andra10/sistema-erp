import { Deserializable } from "./deserializable.model";

export class User implements Deserializable<User> {
    
    id: number
    login: string
    name: string
    mobile: number
    cpfCnpj: string
    gender: string
    birthDate: string
    brand: number
    profile: string
    terms: boolean
    profileDownload: string
    brandObj: any

    deserialize(input: any): User {
        Object.assign(this, input);
        return this;
    }    
}

export enum UserStatus {
    NOT_CONFIRMED = "Não Confirmado",
    ACTIVE = "Ativo", 
    CANCELED = "Cancelado", 
    BLOCKED = "Bloqueado"
}

export enum UserColorStatus {
    NOT_CONFIRMED = "mat-orange-500-bg",
    ACTIVE = "mat-green-500-bg", 
    CANCELED = "mat-red-900-bg", 
    BLOCKED = "mat-primary-400-bg"
}

export enum UserColorStatusLight {
    NOT_CONFIRMED = "mat-orange-100-bg",
    ACTIVE = "mat-green-100-bg", 
    CANCELED = "mat-red-100-bg", 
    BLOCKED = "mat-primary-100-bg"
}