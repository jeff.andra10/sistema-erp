import { Deserializable } from "./deserializable.model";

export class Quote implements Deserializable<Quote> {

    id: number
    cpfCnpj: string    
    createdDate: Date
    sentDate: Date
    status: string

    
    brand: number;
    offer: number;
    broker: number;
    sale: number;
   
    insurerQuotationId: string    
    insurerProposalId: string    
    insurerEndorsementId: string    
    insurerPolicyId: string    
    insurerStatusId: string    
    productId: string    
    insurerSusepId: string
    insurerStatusDescription: string
    renovationType: string    
    currentPolicyInsurer: string    
    currentPolicyBonusClass: string    
    currentPolicyBonusId: string    
    currentPolicyEndValidity: string    
    currentPolicyId: string    
    tariffCategory: string    
    fipeAcceptanceRate: number;
    compensationLimitAmount: number;
    deductibleAmount: number;
    iofAmount: number;
    interestAmount: number;
    premiumAmount: number;
    premiumTotalAmount: number;
    installments: number;
    deviceInstallationDate: Date  
    deviceActivationDate: Date  

    key: string
    vehicle: any
    customer: any
    address: any
    payment: any
    driver: any
    owner: any
    lifecycle: any
    coverages: Array<any>
    totalPrice: number
    premiumTariff: number  
    

    deserialize(input: any): Quote {
        Object.assign(this, input);
        return this;
    }
}



export enum FuelType {
    TRIFUEL = "TRIFUEL",
    "álcool" = "2",
    TETRAFUEL = "7",
    "Diesel" = "3",
    BIFUEL = "4",
    "gnv" = "GNV",
    "Gasolina" = "1" 
}

export enum QuotaStatus {
    QUOTATION = "Cotação",
    PROPOSAL = "Proposta", 
    ANALYZE = "Analise", 
    POLICY = "Apolice",
    CANCELLED = "Cancelada", 
    REJECTED = "Rejeitada"
}
/*

QuotationDTO {
    Long id;
    Long brand;
    Long offer;
    Long broker;
    QuotationStatus status;
    String insurerQuotationId;
    String insurerProposalId;
    String insurerEndorsementId;
    String insurerPolicyId;
    String insurerSusepId;
    String productId;
    List<QuotationCoverageDTO> coverages = [
        {
            String id;
            String description;
            limit;
        }
    ]
    String renovationType;
    String currentPolicyInsurer;
    String currentPolicyBonusClass;
    String currentPolicyBonusId;
    Date currentPolicyEndValidity;
    String currentPolicyId;
    String tariffCategory;
    fipeAcceptanceRate;
    compensationLimitAmount;
    deductibleAmount;
    iofAmount;
    interestAmount;
    premiumAmount;
    premiumTotalAmount;
    Integer installments;
    Date deviceInstallationDate;
    Date deviceActivationDate;
    QuotationVehicleDTO vehicle = {
        Long manufacturer;
        String manufacturerName;
        Long model;
        String modelName;
        String color;
        String registrationYear;
        String modelYear;
        String fipeCode;
        String fipePrice;
        String licensePlate;
        String chassi;
        boolean gnv = false;
        boolean armoredVehicle = false;
        Boolean brandNew;
        String fuelType;
        String monthlyUsage;
        String useType;
        String modificationType;
        String pcd;
        String garageWork;
        String garageStudy;
        String garageResidential;
        String vehicleLastYearThefts;
        String vehicleIncidents;
        Boolean isVehicleFinanced;
        String mileage;
        String renavam;
        String usePurpose;
        String ownerKinship;
        String ownerName;
        Date ownerBirthdate;
        String ownerNationalIdentity;
        String ownerGender;
        Date boughtDate;
        Boolean crvSigned;
        Boolean detran;
        Boolean isVehicleUsedUnder25YearsOld;
    }
    QuotationDriverDTO driver = {
        String personType;
        String gender;
        String name;
        String nationalIdentity;
        Date birthdate;
        String maritalStatus;
    }
    QuotationCustomerDTO customer = {
        String name;
        String email;
        String personType;
        String ocupation;
        Date birthdate;
        Integer age;
        String gender;
        String maritalStatus;
        String nationalIdentity;
        String nationalIdentityExpeditionAgency;
        String registrationNumber;
        String driverLicense;
        String driverLicenseCategory;
        Date driverLicenseExpiration;
        String residencialPhone;
        String commercialPhone;
        String cellphone;
        Boolean vehicleOwner;
        Boolean vehicleDriver;
        String profession;
        String relationshipWithVehicleOwner;
        String kindshipWithVehicleOwner;
        String kindShipWithThirdParty;
        String thirdPartyNationalIdentity;
        String politicallyExposedPerson;
        String relationshipWithPoliticallyExposedPerson;
        // Address
        String addressStreet;
        String addressNumber;
        String addressDistrict;
        String addressPostalCode;
        String addressCity;
        String addressState;
        String addressLandmark;
        String addressComplement;
    }
    QuotationPaymentDTO payment = {
        ...
    }
}
*/