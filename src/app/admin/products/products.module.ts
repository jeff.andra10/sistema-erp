import { NgModule, ModuleWithProviders } from "@angular/core";
import { NewProductComponent } from "./new-product/new-product.component";
import { ProductsComponent } from "./products.component";
import { EditProductComponent } from './edit-product/edit-product.component';
import { DeleteProductComponent } from './delete-product/delete-product.component';
import { ActivateProductComponent }from './activate-product/activate-product.component';
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../angular-material/material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { FuseSharedModule } from "@fuse/shared.module";
import { ActivationProductComponent } from "./activation/activation.component";


@NgModule({
    declarations:[
        ProductsComponent,
        NewProductComponent,
        EditProductComponent,
        DeleteProductComponent,
        ActivateProductComponent,
        ActivationProductComponent
    ],
    imports: [
        FlexLayoutModule,
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        FuseSharedModule
    ],
    exports: [
        ProductsComponent,
        NewProductComponent,
        EditProductComponent,
        DeleteProductComponent,
        ActivateProductComponent,
        ActivationProductComponent
    ],
    entryComponents: [
        NewProductComponent,
        EditProductComponent,
        DeleteProductComponent,
        ActivateProductComponent,
        ActivationProductComponent
    ]
})
export class ProductsModule {}