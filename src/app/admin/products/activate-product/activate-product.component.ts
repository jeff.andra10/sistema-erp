import { Component, OnInit, Inject , Injector } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { ProductsService } from '../../../core/services/products.service';


@Component({
  selector: 'tnm-activate-product',
  templateUrl: './activate-product.component.html',
  styleUrls: ['../products.component.scss'],
})
export class ActivateProductComponent extends BaseComponent implements OnInit {

  productForm: FormGroup
  suppliers: Array<object>
  brandList: Array<any>
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<ActivateProductComponent>,
    private snackbar: MatSnackBar
  ) {
    super(injector)
   }

  ngOnInit() {
    this.createForm(this.data)
  }

  createForm(data){
    this.productForm = this.formBuilder.group({
      contract: this.formBuilder.control({value : data.relationships ? data.relationships.product.contract ? data.relationships.product.contract : '' : data.contract, disabled: true}),
      check: this.formBuilder.control('', [Validators.required])
    })
  }

  activiteProduct(body) {

    let params : any

    if(this.data['id']){
      params ={
        "commodity": this.data['id'],
        "loggedUser":this.appContext.loggedUser.id
      }
    }else{
      params ={
        "brand": this.appContext.loggedUser.brand,
        "product": this.data['product'],
        "loggedUser":this.appContext.loggedUser.id
      }
    }

    this.productsService.enableCommoditie(params)
    .subscribe(product => {
      this.snackbar.open('Produto ativado com sucesso', '', {
        duration: 8000
      })
      this.dialogRef.close()
    })
  }


  closeDialog(){
    this.dialogRef.close()
  }

}
