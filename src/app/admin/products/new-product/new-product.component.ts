import { Component, OnInit, Injector } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { ProductsService } from '../../../core/services/products.service';
import { AppContext } from '../../../core/appcontext';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';


@Component({
  selector: 'tnm-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['../products.component.scss'],
})
export class NewProductComponent extends BaseComponent implements OnInit {

  productForm: FormGroup
  suppliers: Array<object>
  brandList: Array<any>
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  isAssociation : Boolean = false
  

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private dialogRef: MatDialogRef<NewProductComponent>,
    private snackbar: MatSnackBar
  ) {
    super(injector)
   }

  ngOnInit() {

    if(this.appContext.brandTracknme == this.appContext.loggedUser.brand){
      this.isAssociation = false;

      this.productForm = this.formBuilder.group({
        name: this.formBuilder.control('', [Validators.required]),
        description: this.formBuilder.control('', [Validators.required]),
        accession: this.formBuilder.control('', [Validators.required]),
        recurrence: this.formBuilder.control('', [Validators.required]),
        supplier: this.formBuilder.control(''),
        contract: this.formBuilder.control('', [Validators.required]),
        brand: this.formBuilder.control({value : this.appContext.brandTracknme, disabled: true})
      })

    }else{
      this.isAssociation = true;

      this.productForm = this.formBuilder.group({
        name: this.formBuilder.control('', [Validators.required]),
        description: this.formBuilder.control('', [Validators.required]),
        accession: this.formBuilder.control('', [Validators.required]),
        recurrence: this.formBuilder.control('', [Validators.required]),
        supplier: this.formBuilder.control('', ),
        contract: this.formBuilder.control('', ),
        brand: this.formBuilder.control({value : this.appContext.loggedUser.brand, disabled: true})
      })
    }

    this.brandList = this.appContext.brands
    this.suppliers = JSON.parse(sessionStorage.getItem('suppliers'))
    
    
  }

  createProduct(body) {
    body.brand = this.appContext.loggedUser.brand;

    body.accession = body.accession.replace(',','.')
    body.recurrence = body.recurrence.replace(',','.')

    this.productsService.create(body)
    .subscribe(product => {
      this.snackbar.open('Produto criado com sucesso', '', {
        duration: 8000
      })
      this.dialogRef.close()
    })
  }

}
