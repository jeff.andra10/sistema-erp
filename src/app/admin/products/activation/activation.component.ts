import { Component, OnInit, Injectable, Injector, ViewChild, AfterViewInit } from '@angular/core'
import { MatTableDataSource, MatSnackBar, MatDialog, PageEvent, MatSort, MatPaginator } from '@angular/material'
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Subject } from 'rxjs';


import { ActivateProductComponent }from '../activate-product/activate-product.component';
import { BaseComponent } from '../../../_base/base-component.component';
import { Product } from '../../../core/models/product.model';
import { ProductsService } from '../../../core/services/products.service';
import { UtilsService } from '../../../core/utils.service';
import { fuseAnimations } from '@fuse/animations';
import { ProductsDataSource } from '../products.datasource';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

//@Injectable()
@Component({
  selector: 'tnm-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['../products.component.scss'],
  animations: fuseAnimations
})
export class ActivationProductComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  
  dataSource: ProductsDataSource
  resultLength: number = 0
  searchSubject = new Subject()

  pageSize : number = 10
  isAssociation : Boolean = false

  brandList: any
  suppliersList = JSON.parse(sessionStorage.getItem('suppliers'))
  productsList = []
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');

  
  constructor(
    private injector: Injector,
    private productsService: ProductsService,
    private dialog: MatDialog,
    private utils: UtilsService) {
    super(injector)
   }

  ngOnInit() {

    this.brandList = this.appContext.brands

    this.getCommodities()
    
  }

  ngAfterViewInit() {

  }

  getCommodities(){

    sessionStorage.setItem('productsList',null)
    this.productsList = []

    this.productsService.getCommodities()
      .subscribe(products => {

        products.content.forEach(commoditie => {
          if(commoditie.product){
            this.productsList.push(commoditie)
          }
        });

        this.productsList.sort(function(a, b){
          return a['product'] - b['product']
        });

        sessionStorage.setItem('productsList', JSON.stringify(this.productsList))
      })

  }

  activateProductDialog(product) {
    
    this.dialog
      .open(ActivateProductComponent, {
        width: '800px',
        data: product
      })
      .afterClosed()
        .subscribe(() => this.getCommodities())
  }

  enableProduct(row){

    let params : any

    if(row['id']){
      params ={
        "commodity": row['id'],
        "loggedUser":this.appContext.loggedUser.id
      }
    }else{
      params ={
        "brand": this.appContext.loggedUser.brand,
        "product": row['product'],
        "loggedUser":this.appContext.loggedUser.id
      }
    }

    this.productsService.enableCommoditie(params)
      .subscribe(result => {
        this.getCommodities()
    });

  }

  disableProduct(row){

    let params ={
      "commodity": row['id'],
      "loggedUser":this.appContext.loggedUser.id
    }

    this.productsService.disableCommoditie(params)
      .subscribe(result => {
        this.getCommodities()
    });

  }

}