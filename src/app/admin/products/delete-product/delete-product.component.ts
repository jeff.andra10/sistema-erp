import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Product } from '../../../core/models/product.model';
import { ProductsService } from '../../../core/services/products.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-delete-product',
  templateUrl: './delete-product.component.html'
})
export class DeleteProductComponent implements OnInit {

  product: Product
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(
    private dialogRef: MatDialogRef<DeleteProductComponent>,
    private productsService: ProductsService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackbar: MatSnackBar

  ) { }

  ngOnInit() {
    this.product = this.data
  }

  deleteProduct(){
    this.productsService.delete(this.product.id)
      .subscribe(product => {
        this.dialogRef.close()
        this.snackbar.open('Produto excluído com sucesso', '', {
          duration: 8000
        })

      })
  }


}
