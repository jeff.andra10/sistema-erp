import { Component, Inject, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { ProductsService } from '../../../core/services/products.service';
import { BaseComponent } from '../../../_base/base-component.component';

@Component({
  selector: 'tnm-edit-product',
  templateUrl: './edit-product.component.html'
})
export class EditProductComponent extends BaseComponent implements OnInit {

  productForm: FormGroup;
  currentId: number;
  suppliers: object[];
  brandList: any[];
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  isAssociation: Boolean = false;


  constructor(
    private injector: Injector,
    private dialogRef: MatDialogRef<EditProductComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private productsService: ProductsService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar
  ) {
    super(injector)
  }

  ngOnInit() {
    this.currentId = this.data.id;
    this.brandList = this.appContext.brands;
    this.suppliers = JSON.parse(sessionStorage.getItem('suppliers'));

    if (this.appContext.brandTracknme === this.appContext.loggedUser.brand) {
      this.isAssociation = false;

      this.productForm = this.formBuilder.group({
        name: this.formBuilder.control(this.data.name, [Validators.required]),
        description: this.formBuilder.control(this.data.description, [Validators.required]),
        accession: this.formBuilder.control(this.data.accession, [Validators.required]),
        recurrence: this.formBuilder.control(this.data.recurrence, [Validators.required]),
        supplier: this.formBuilder.control(this.data.supplier ? this.data.supplier.id : ''),
        contract: this.formBuilder.control(this.data.contract ? this.data.contract : '', [Validators.required]),
        brand: this.formBuilder.control({ value: this.data.brandObj.id, disabled: true })
      });

    } else {
      this.isAssociation = true;

      this.productForm = this.formBuilder.group({
        name: this.formBuilder.control(this.data.name, [Validators.required]),
        description: this.formBuilder.control(this.data.description, [Validators.required]),
        accession: this.formBuilder.control(this.data.accession, [Validators.required]),
        recurrence: this.formBuilder.control(this.data.recurrence, [Validators.required]),
        supplier: this.formBuilder.control(this.data.supplier ? this.data.supplier.id : ''),
        contract: this.formBuilder.control(this.data.contract ? this.data.contract : ''),
        brand: this.formBuilder.control({ value: this.data.brandObj.id, disabled: true })
      });
    }
  }


  updateProduct(editForm) {

    editForm.accession = editForm.accession.toString().replace(',', '.');
    editForm.recurrence = editForm.recurrence.toString().replace(',', '.');

    const editData = [];

    for (let item in editForm) {
      let editItem = { 'op': 'add', 'path': '/' + item, 'value': editForm[item] };
      editData.push(editItem);
    }

    this.productsService.update(this.currentId, editData)
      .subscribe(product => {
        this.snackbar.open('Produto atualizado com sucesso', '', {
          duration: 8000
        });
        this.dialogRef.close();
      });
  }

}
