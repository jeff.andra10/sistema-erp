import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import { ProductsService } from '../../core/services/products.service';
import { UtilsService } from "../../core/utils.service";

export class ProductsDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private totalElementsSubject = new BehaviorSubject<number>(0)

    public totalElements$ = this.totalElementsSubject.asObservable()

    constructor(
        private productsService : ProductsService,
        private utilsService: UtilsService,
        private brandList: Array<any>, 
        private suppliersList: Array<any>) {}

    load(filter:string,
            sortField: string,
            sortDirection:string,
            pageIndex:number,
            pageSize:number,
            brand:number) {

        this.productsService.findProducts(
            filter, 
            sortField,
            sortDirection,
            pageIndex, 
            pageSize,
            brand).pipe(
                catchError(() => of([]))
            )
            .subscribe(result => {
               
                if (result.content) {
                    result.content.forEach(product => {
                        product.brandObj =  this.utilsService.getItemById(product.brand, this.brandList)
                        //product.supplier =  this.utilsService.getItemById(product.supplier, this.suppliersList)
                    });
                }

                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)

                sessionStorage.setItem('productsList', JSON.stringify(result.content))
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
