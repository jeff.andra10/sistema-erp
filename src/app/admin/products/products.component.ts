import { Component, OnInit, Injectable, Injector, ViewChild, AfterViewInit } from '@angular/core'
import { MatTableDataSource, MatSnackBar, MatDialog, PageEvent, MatSort, MatPaginator } from '@angular/material'
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Subject } from 'rxjs';

import { NewProductComponent } from './new-product/new-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { DeleteProductComponent } from './delete-product/delete-product.component';
import { ActivateProductComponent }from './activate-product/activate-product.component';
import { BaseComponent } from '../../_base/base-component.component';
import { Product } from '../../core/models/product.model';
import { ProductsService } from '../../core/services/products.service';
import { UtilsService } from '../../core/utils.service';
import { fuseAnimations } from '@fuse/animations';
import { ProductsDataSource } from './products.datasource';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Injectable()
@Component({
  selector: 'tnm-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  animations: fuseAnimations
})
export class ProductsComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  displayedColumns = ['id', 'name', 'brand', 'edit', 'delete']
  
  dataSource: ProductsDataSource
  resultLength: number = 0
  searchSubject = new Subject()

  pageSize : number = 10
  isAssociation : Boolean = false

  brandList: any
  suppliersList = JSON.parse(sessionStorage.getItem('suppliers'))
  productsList = []
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');



  constructor(
    private injector: Injector,
    private productsService: ProductsService,
    private dialog: MatDialog,
    private utils: UtilsService) {
    super(injector)
   }

  ngOnInit() {
      localStorage.setItem('hiddenLoading','false');
      this.brandList = this.appContext.brands

      this.searchSubject
      .debounceTime(500)
      .subscribe((val : string) => {
        this.loadProducts(val)
      })

      this.dataSource = new ProductsDataSource(this.productsService, this.utilsService, this.brandList, this.suppliersList)
      this.dataSource.load('', 'name', 'asc', 0, 10,this.appContext.loggedUser.brand)

  }

  ngAfterViewInit() {

      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

      if(this.dataSource && this.dataSource.totalElements$){
        this.dataSource.totalElements$.subscribe( value => {
          this.resultLength = value
        })
      }
      
      merge(this.sort.sortChange, this.paginator.page)
      .pipe(
          tap(() => this.loadProducts())
      )
      .subscribe()
    
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  loadProducts(filter? : string) {

    this.dataSource.load(
      filter, 
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize,
      this.appContext.loggedUser.brand)
  } 

  newProductDialog() {
    this.dialog
      .open(NewProductComponent, {
        width: '800px',
      })
      .afterClosed()
        .subscribe(() => this.loadProducts())
  }

  editProductDialog(row) {
    this.dialog
      .open(EditProductComponent, {
        width: '800px',
        data: row
      })
      .afterClosed()
        .subscribe(() => this.loadProducts())
  }  

  deleteProductDialog(row) {
    this.dialog
      .open(DeleteProductComponent, {
        width: '400px',
        data: row
      })
      .afterClosed()
        .subscribe(() => this.loadProducts())
  }  

}