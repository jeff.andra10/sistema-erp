import { NgModule, ModuleWithProviders } from "@angular/core";
import { TextMaskModule } from 'angular2-text-mask';
import { NewSimCardComponent } from './new-sim-card/new-sim-card.component';
import { EditSimCardComponent } from './edit-sim-card/edit-sim-card.component';
import { DeleteSimCardComponent } from './delete-sim-card/delete-sim-card.component';
import { SimCardsComponent } from "./sim-cards.component";
import { FlexLayoutModule } from "@angular/flex-layout";
import { MaterialModule } from "../../angular-material/material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { FuseSharedModule } from "@fuse/shared.module";

@NgModule({
    declarations:[
        SimCardsComponent,
        NewSimCardComponent,
        EditSimCardComponent,
        DeleteSimCardComponent],
    imports: [
        TextMaskModule,
        FlexLayoutModule,
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        FuseSharedModule
    ],
    exports: [
        NewSimCardComponent,
        EditSimCardComponent,
        DeleteSimCardComponent
    ],
    entryComponents: [
        NewSimCardComponent,
        EditSimCardComponent,
        DeleteSimCardComponent
    ]
})
export class SimCardsModule{}