import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSimCardComponent } from './edit-sim-card.component';

describe('EditSimCardComponent', () => {
  let component: EditSimCardComponent;
  let fixture: ComponentFixture<EditSimCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSimCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSimCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
