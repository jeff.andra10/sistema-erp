import { Component, OnInit, Inject, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { SimCardsService } from '../../../core/services/sim-cards.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-edit-sim-card',
  templateUrl: './edit-sim-card.component.html',
  styleUrls: ['./edit-sim-card.component.scss']
})
export class EditSimCardComponent extends BaseComponent implements OnInit {

  simCardForm: FormGroup
  currentId: number
  suppliers: Array<object>
  brandList: Array<any>
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  //TODO - Put on shared file
  
  phoneMask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  simCardMask = [/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]



  constructor(
    private injector: Injector,
    private dialogRef: MatDialogRef<EditSimCardComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private simCardsService: SimCardsService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar
  ) { 
    super(injector)
  }

  ngOnInit() {
    this.currentId = this.data.id
    this.brandList = this.appContext.brands
    this.suppliers = JSON.parse(sessionStorage.getItem('suppliers'))
    this.createForm(this.data)
  }

  createForm(data){
    this.simCardForm = this.formBuilder.group({
      iccid: this.formBuilder.control(data.iccid, [Validators.required]),
      msisdn: this.formBuilder.control(data.msisdn, [Validators.required]),
      brand: this.formBuilder.control(data.brand.id, [Validators.required]),
      operator: this.formBuilder.control(data.operator ? data.brand : '', [Validators.required]),
      supplier: this.formBuilder.control(data.supplier ? data.supplier : '', [Validators.required])
    })
  }

  updateSimCard(editForm) {
    let editData = []
    
    editForm.iccid = editForm.iccid.replace(/\D+/g, '')
    editForm.msisdn = editForm.msisdn.replace(/\D+/g, '')

    for(let item in editForm){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': editForm[item]};
      editData.push(editItem);
    }

    this.simCardsService.update(this.currentId, editData)
      .subscribe(simCard => {
        this.snackbar.open('Sim Card atualizado com sucesso', '', {
          duration: 8000
        })
        this.dialogRef.close()
      })
  }
}
