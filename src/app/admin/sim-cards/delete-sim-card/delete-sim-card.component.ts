import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { SimCard } from '../../../core/models/sim-card.model';
import { SimCardsService } from '../../../core/services/sim-cards.service';


@Component({
  selector: 'tnm-delete-sim-card',
  templateUrl: './delete-sim-card.component.html',
  styleUrls: ['./delete-sim-card.component.scss']
})
export class DeleteSimCardComponent implements OnInit {

  simCard: SimCard

  constructor(
    private dialogRef: MatDialogRef<DeleteSimCardComponent>,
    private simCardsService: SimCardsService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackbar: MatSnackBar

  ) { }

  ngOnInit() {
    this.simCard = this.data
  }

  deleteSimCard(){
    this.simCardsService.delete(this.simCard.id)
      .subscribe(suppliers => {
        this.dialogRef.close()
        this.snackbar.open('SimCard excluído com sucesso', '', {
          duration: 8000
        })

      })
  }

}
