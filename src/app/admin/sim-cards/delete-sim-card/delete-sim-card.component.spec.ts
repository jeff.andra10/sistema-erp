import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSimCardComponent } from './delete-sim-card.component';

describe('DeleteSimCardComponent', () => {
  let component: DeleteSimCardComponent;
  let fixture: ComponentFixture<DeleteSimCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteSimCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSimCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
