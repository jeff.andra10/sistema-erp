import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSimCardComponent } from './new-sim-card.component';

describe('NewSimCardComponent', () => {
  let component: NewSimCardComponent;
  let fixture: ComponentFixture<NewSimCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewSimCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSimCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
