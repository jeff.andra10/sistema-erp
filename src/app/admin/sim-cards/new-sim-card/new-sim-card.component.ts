import { Component, OnInit, Injector } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BaseComponent } from '../../../_base/base-component.component';
import { SimCardsService } from '../../../core/services/sim-cards.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-new-sim-card',
  templateUrl: './new-sim-card.component.html',
  styleUrls: ['./new-sim-card.component.scss']
})
export class NewSimCardComponent extends BaseComponent implements OnInit {

  simCardForm: FormGroup
  currentId: number
  brandList: Array<any>
  suppliers: Array<object>
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  


  //TODO - Put on shared file
  phoneMask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  simCardMask = [/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]


  constructor(
    private injector: Injector,
    private dialogRef: MatDialogRef<NewSimCardComponent>,
    private simCardsService: SimCardsService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar
  ) { 
    super(injector)
  }

  ngOnInit() {
    this.brandList = this.appContext.brands
    this.suppliers = JSON.parse(sessionStorage.getItem('suppliers'))
    this.createForm()
  }

  createForm(){
    this.simCardForm = this.formBuilder.group({
      iccid: this.formBuilder.control('', [Validators.required]),
      msisdn: this.formBuilder.control('', [Validators.required]),
      brand: this.formBuilder.control('', [Validators.required]),
      operator: this.formBuilder.control('', [Validators.required]),
      supplier: this.formBuilder.control('', [Validators.required])
    })
  }

  createSimCard(body) {
    body.iccid = body.iccid.replace(/\D+/g, '')
    body.msisdn = body.msisdn.replace(/\D+/g, '')
    this.simCardsService.create(body)
      .subscribe(simCard => {
        this.snackbar.open('SimCard criado com sucesso', '', {
          duration: 8000
        })
        this.dialogRef.close()
      })
  }

}
