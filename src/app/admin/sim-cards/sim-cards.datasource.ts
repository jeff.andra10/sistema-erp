import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import { SimCardsService } from '../../core/services/sim-cards.service';
import { UtilsService } from "../../core/utils.service";

export class SimCardsDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private totalElementsSubject = new BehaviorSubject<number>(0)

    public totalElements$ = this.totalElementsSubject.asObservable()

    constructor(
        private simCardsService: SimCardsService, 
        private utilsService: UtilsService,
        private brandList: Array<any>) {
    }

    load(filter:string,
            sortField: string,
            sortDirection:string,
            pageIndex:number,
            pageSize:number) {

        this.simCardsService.findSimCards(
            filter, 
            sortField,
            sortDirection,
            pageIndex, 
            pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe(result => {
               
                if (result.content) {
                    result.content.forEach(obj => {
                        obj.brand =  this.utilsService.getItemById(obj.brand, this.brandList)
                    });
                }

                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
