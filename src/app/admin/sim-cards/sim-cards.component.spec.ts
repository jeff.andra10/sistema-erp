import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimCardsComponent } from './sim-cards.component';

describe('SimCardsComponent', () => {
  let component: SimCardsComponent;
  let fixture: ComponentFixture<SimCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
