import { Component, OnInit, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog, MatSort, MatPaginator } from '@angular/material';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Subject } from 'rxjs';

import { EditSimCardComponent } from './edit-sim-card/edit-sim-card.component';
import { NewSimCardComponent } from './new-sim-card/new-sim-card.component';
import { DeleteSimCardComponent } from './delete-sim-card/delete-sim-card.component';
import { BaseComponent } from '../../_base/base-component.component';
import { SimCardsService } from '../../core/services/sim-cards.service';
import { fuseAnimations } from '@fuse/animations';
import { SimCardsDataSource } from './sim-cards.datasource';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';


@Component({
  selector: 'tnm-sim-cards',
  templateUrl: './sim-cards.component.html',
  styleUrls: ['./sim-cards.component.scss'],
  animations: fuseAnimations
})
export class SimCardsComponent extends BaseComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  brandList: Array<any>
  displayedColumns = ['iccid', 'msisdn', 'supplier', 'edit', 'delete'];
  dataSource: SimCardsDataSource
  resultLength: number = 0
  searchSubject = new Subject()
  isAssociation : Boolean = false
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  
  

  constructor(
    private injector: Injector,
    private simCardsService: SimCardsService,
    public dialog: MatDialog
  ) {
    super(injector)
   }

  ngOnInit() {

    localStorage.setItem('hiddenLoading','false');
    
    if(this.appContext.brandTracknme != this.appContext.loggedUser.brand){
      this.isAssociation = true
    }

    this.brandList = this.appContext.brands

    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadSimCards(val)
    })

    this.dataSource = new SimCardsDataSource(this.simCardsService, this.utilsService, this.brandList)
    this.dataSource.load('', 'iccid', 'asc', 0, 10)
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    if(this.dataSource && this.dataSource.totalElements$){
      this.dataSource.totalElements$.subscribe( value => {
        this.resultLength = value
      })
    }
    
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadSimCards())
    )
    .subscribe()
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  loadSimCards(filter? : string) {

    this.dataSource.load(
      filter, 
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  } 
  
  editSimCardDialog(row) {
    this.dialog
      .open(EditSimCardComponent, {
        data: row,
        width: '600px',
      })
      .afterClosed()
        .subscribe(() => this.loadSimCards())
  }

  newSimCardDialog() {
    this.dialog
      .open(NewSimCardComponent, {
        width: '600px',
      })
      .afterClosed()
        .subscribe(() => this.loadSimCards())
  }

  deleteSimCardDialog(row){
    this.dialog
      .open(DeleteSimCardComponent, {
        data: row,
        width: '500px',
      })
      .afterClosed()
        .subscribe(() => this.loadSimCards())
  }

}
