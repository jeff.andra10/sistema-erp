import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from '../angular-material/material.module';
import { SimCardsComponent } from './sim-cards/sim-cards.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { SuppliersModule } from './suppliers/suppliers.module';
import { SimCardsModule } from './sim-cards/sim-cards.module';
import { ProductsComponent } from './products/products.component';
import { ProductsModule } from './products/products.module';
import { ServicesModule } from './services/services.module';
import { ServicesComponent } from './services/services.component';
import { OffersModule } from './offers/offers.module';
import { OffersComponent } from './offers/offers.component';
import { BrandsModule } from './brands/brands.module';
import { BrandsComponent } from './brands/brands.component';
import { BrandDetailsComponent } from "./brands/brand-details/brand-details.component";
import { ActivationProductComponent } from "./products/activation/activation.component";
import { ActivationServiceComponent } from './services/activation/activation.component';
import { PermissionsModule } from './permissions/permissions-group/permissions.module'
import { PermissionsComponent } from './permissions/permissions-group/permissions.component'

const authRouting: ModuleWithProviders = RouterModule.forChild([
  { path: 'admin',  
    children : [
      { path: 'suppliers', component: SuppliersComponent },
      { path: 'simcards', component: SimCardsComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'activation/product', component: ActivationProductComponent },
      { path: 'services', component: ServicesComponent },
      { path: 'activation/service', component: ActivationServiceComponent },
      { path: 'offers', component: OffersComponent },
      { path: 'brands', component: BrandsComponent },
      { path: 'brands/:id', component: BrandDetailsComponent },
      { path: 'permissions', component: PermissionsComponent },
      { path: 'permissions/profile', component: PermissionsComponent }

    ]
  }
]);

@NgModule({
  imports: [
    authRouting,
    CommonModule,
    FuseSharedModule,
    MaterialModule,
    SuppliersModule,
    SimCardsModule,
    //DevicesModule,
    ProductsModule,
    ServicesModule,
    OffersModule,
    BrandsModule,
    PermissionsModule
  ],
  declarations: []
})
export class AdminModule { }
