import { Component, OnInit, Inject , Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { ServicesService } from '../../../core/services/services.service';


@Component({
  selector: 'tnm-activate-service',
  templateUrl: './activate-service.component.html'
})
export class ActivateServiceComponent extends BaseComponent implements OnInit {

  serviceForm: FormGroup
  brandList: Array<any>
  suppliers: Array<object>

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private servicesService: ServicesService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<ActivateServiceComponent>,
    private snackbar: MatSnackBar
  ) {
    super(injector)
   }

  ngOnInit() {
    this.createForm(this.data)
  }

  createForm(data){
    this.serviceForm = this.formBuilder.group({
      contract: this.formBuilder.control({value : data.relationships ? data.relationships.service.contract ? data.relationships.service.contract : '' : data.contract, disabled: true}),
      check: this.formBuilder.control('', [Validators.required])
    })
  }

  activateService(body) {

    let params : any

    if(this.data['id']){
      params ={
        "commodity": this.data['id'],
        "loggedUser":this.appContext.loggedUser.id
      }
    }else{
      params ={
        "brand": this.appContext.loggedUser.brand,
        "service": this.data['service'],
        "loggedUser":this.appContext.loggedUser.id
      }
    }

    this.servicesService.enableCommoditie(params)
    .subscribe(result => {
      this.snackbar.open('Serviço ativado com sucesso', '', {
        duration: 8000
      })
      this.dialogRef.close()
    })
  }

  closeDialog(){
    this.dialogRef.close()
  }

}
