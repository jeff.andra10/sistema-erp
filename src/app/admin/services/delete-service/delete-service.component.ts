import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Service } from '../../../core/models/service.model';
import { ServicesService } from '../../../core/services/services.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-delete-service',
  templateUrl: './delete-service.component.html'
})
export class DeleteServiceComponent implements OnInit {

  service: Service
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(
    private dialogRef: MatDialogRef<DeleteServiceComponent>,
    private servicesService: ServicesService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackbar: MatSnackBar

  ) {
   }

  ngOnInit() {
    this.service = this.data
  }

  deleteService(){
    this.servicesService.delete(this.service.id)
      .subscribe(product => {
        this.dialogRef.close()
        this.snackbar.open('Serviço excluído com sucesso', '', {
          duration: 8000
        })
     })
  }


}
