import { Component, OnInit, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatDialog, PageEvent, MatSort, MatPaginator } from '@angular/material';
import { Subject } from 'rxjs';

import { ActivateServiceComponent } from '../activate-service/activate-service.component';
import { BaseComponent } from '../../../_base/base-component.component';
import { Service } from '../../../core/models/service.model';
import { ServicesService } from '../../../core/services/services.service';
import { UtilsService } from '../../../core/utils.service';
import { fuseAnimations } from '@fuse/animations';
import { ServicesDataSource } from '../services.datasource';

@Component({
  selector: 'tnm-activation-services',
  templateUrl: './activation.component.html',
  styleUrls: ['../services.component.scss'],
  animations: fuseAnimations
})
export class ActivationServiceComponent extends BaseComponent implements OnInit, AfterViewInit{

  dataSource: ServicesDataSource

  resultLength: number = 0
  searchSubject = new Subject()

  pageSize: number = 10
  
  brandList: any
  servicesList: Array<Service>
  
  isAssociation : Boolean = false
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  
  constructor(
    private injector: Injector,
    private servicesService: ServicesService,
    private dialog: MatDialog,
    private utils: UtilsService
  ) { 
    super(injector)
  }

  ngOnInit() {

    this.brandList = this.appContext.brands

    this.getCommodities()
    
  }

  ngAfterViewInit() {

  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  

  getCommodities(pageIndex?: number, pageSize?: number){

    let params = {
      limit: pageSize || this.pageSize,
      page: pageIndex || 0
    }

    sessionStorage.setItem('productsList',null)
    this.servicesList = []

    this.servicesService.getCommodities(params)
      .subscribe(services => {
       
        services.content.forEach(commoditie => {
          if(commoditie.service){
            this.servicesList.push(commoditie)
          }
        });
        
        this.servicesList.sort(function(a, b){
          return a['service'] - b['service']
       });

        sessionStorage.setItem('servicesList', JSON.stringify(this.servicesList))
      })
  }

  activiteServiceDialog(service) {
    this.dialog
      .open(ActivateServiceComponent, {
        width: '800px',
        data: service
      })
      .afterClosed()
        .subscribe(() => this.getCommodities())
  }  

  enableService(row){
    
    let params : any

  if(row['id']){
    params ={
      "commodity": row['id'],
      "loggedUser":this.appContext.loggedUser.id
    }
  }else{
    params ={
      "brand": this.appContext.loggedUser.brand,
      "service": row['service'],
      "loggedUser":this.appContext.loggedUser.id
    }
  }

    this.servicesService.enableCommoditie(params)
      .subscribe(result => {
        this.getCommodities()
    });

  }

  disableService(row){

    let params ={
      "commodity": row['id'],
      "loggedUser":this.appContext.loggedUser.id
    }

    this.servicesService.disableCommoditie(params)
      .subscribe(result => {
        this.getCommodities()
    });

  }
}
