import { NgModule, ModuleWithProviders } from "@angular/core";
import { ServicesComponent } from "./services.component";
import { NewServiceComponent } from "./new-service/new-service.component";
import { EditServiceComponent } from "./edit-service/edit-service.component";
import { DeleteServiceComponent } from "./delete-service/delete-service.component";
import { ActivateServiceComponent } from './activate-service/activate-service.component';
import { ActivationServiceComponent } from './activation/activation.component';

import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../angular-material/material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { FuseSharedModule } from "@fuse/shared.module";

@NgModule({
    declarations:[
        ServicesComponent,
        NewServiceComponent,
        EditServiceComponent,
        DeleteServiceComponent,
        ActivateServiceComponent,
        ActivationServiceComponent
    ],
    imports: [
        FlexLayoutModule,
        CommonModule,
        MaterialModule,
        ReactiveFormsModule,
        FuseSharedModule
    ],
    exports: [
        ServicesComponent,
        NewServiceComponent,
        EditServiceComponent,
        DeleteServiceComponent,
        ActivateServiceComponent,
        ActivationServiceComponent
    ],
    entryComponents: [
        NewServiceComponent,
        EditServiceComponent,
        DeleteServiceComponent,
        ActivateServiceComponent,
        ActivationServiceComponent
    ]
})
export class ServicesModule {}