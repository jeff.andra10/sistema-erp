import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import { UtilsService } from "../../core/utils.service";
import { ServicesService } from "../../core/services/services.service";

export class ServicesDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private totalElementsSubject = new BehaviorSubject<number>(0)

    public totalElements$ = this.totalElementsSubject.asObservable()

    constructor(
        private servicesService: ServicesService,
        private utilsService: UtilsService,
        private brandList: Array<any>) {}

    load(filter:string,
            sortField: string,
            sortDirection:string,
            pageIndex:number,
            pageSize:number,
            brand:number) {

        this.servicesService.findServices(
            filter, 
            sortField,
            sortDirection,
            pageIndex, 
            pageSize,
            brand).pipe(
                catchError(() => of([]))
            )
            .subscribe(result => {
               
                if (result.content) {
                    result.content.forEach(service => {
                        service.brandObj =  this.utilsService.getItemById(service.brand, this.brandList)
                    });
                }

                this.totalElementsSubject.next(result.totalElements)
                this.dataSubject.next(result.content)

                sessionStorage.setItem('servicesList', JSON.stringify(result.content))
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}
