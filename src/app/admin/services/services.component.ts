import { Component, OnInit, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatDialog, PageEvent, MatSort, MatPaginator } from '@angular/material';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Subject } from 'rxjs';

import { NewServiceComponent } from './new-service/new-service.component';
import { EditServiceComponent } from './edit-service/edit-service.component';
import { DeleteServiceComponent } from './delete-service/delete-service.component';
import { BaseComponent } from '../../_base/base-component.component';
import { Service } from '../../core/models/service.model';
import { ServicesService } from '../../core/services/services.service';
import { UtilsService } from '../../core/utils.service';
import { fuseAnimations } from '@fuse/animations';
import { ServicesDataSource } from './services.datasource';


@Component({
  selector: 'tnm-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
  animations: fuseAnimations
})
export class ServicesComponent extends BaseComponent implements OnInit, AfterViewInit{

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  displayedColumns = ['id','name', 'brand', 'edit', 'delete']
  dataSource: ServicesDataSource

  resultLength: number = 0
  searchSubject = new Subject()

  pageSize: number = 10
  
  brandList: any
  servicesList: Array<Service>
  
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  
  
  constructor(
    private injector: Injector,
    private servicesService: ServicesService,
    private dialog: MatDialog,
    private utils: UtilsService
  ) { 
    super(injector)
  }

  ngOnInit() {
      localStorage.setItem('hiddenLoading','false');
      this.brandList = this.appContext.brands

      this.searchSubject
      .debounceTime(500)
      .subscribe((val : string) => {
        this.loadServices(val)
      })

      this.dataSource = new ServicesDataSource(this.servicesService, this.utilsService, this.brandList)
      this.dataSource.load('', 'name', 'asc', 0, 10, this.appContext.loggedUser.brand)

  }

  ngAfterViewInit() {

      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

      if(this.dataSource && this.dataSource.totalElements$){
        this.dataSource.totalElements$.subscribe( value => {
          this.resultLength = value
        })
      }
      
      merge(this.sort.sortChange, this.paginator.page)
      .pipe(
          tap(() => this.loadServices())
      )
      .subscribe()
  
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  loadServices(filter? : string) {

    this.dataSource.load(
      filter, 
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize,
      this.appContext.loggedUser.brand)
  } 

  newServiceDialog() {
    this.dialog
      .open(NewServiceComponent, {
        width: '800px',
      })
      .afterClosed()
        .subscribe(() => this.loadServices())
  }

  editserviceDialog(row) {
    this.dialog
      .open(EditServiceComponent, {
        width: '800px',
        data: row
      })
      .afterClosed()
        .subscribe(() => this.loadServices())
  }  
  
  deleteServiceDialog(row) {
    this.dialog
      .open(DeleteServiceComponent, {
        width: '400px',
        data: row
      })
      .afterClosed()
        .subscribe(() => this.loadServices())
  }  
}
