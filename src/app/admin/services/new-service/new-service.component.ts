import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { ServicesService } from '../../../core/services/services.service';


@Component({
  selector: 'tnm-new-service',
  templateUrl: './new-service.component.html',
})
export class NewServiceComponent extends BaseComponent implements OnInit {

  serviceForm: FormGroup
  brandList: Array<any>
  suppliers: Array<object>
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  isAssociation : Boolean = false
  segmentList = [{'name':'B2B','id':'CUSTOMERS'},{'name':'B2C','id':'BUSINESS'},{'name':'Serviços de Plataforma','id':'DEVICE'}]
  

  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder,
    private servicesService: ServicesService,
    private dialogRef: MatDialogRef<NewServiceComponent>,
    private snackbar: MatSnackBar
  ) {
    super(injector)
   }

  ngOnInit() {
    this.brandList = this.appContext.brands
    this.suppliers = JSON.parse(sessionStorage.getItem('suppliers'))

    if(this.appContext.brandTracknme == this.appContext.loggedUser.brand){
      this.isAssociation = false;

      this.serviceForm = this.formBuilder.group({
        name: this.formBuilder.control('', [Validators.required]),
        description: this.formBuilder.control('', [Validators.required]),
        accession: this.formBuilder.control('', [Validators.required]),
        recurrence: this.formBuilder.control('', [Validators.required]),
        brand: this.formBuilder.control({value : this.appContext.brandTracknme, disabled: true}),
        supplier: this.formBuilder.control(''),
        segment: this.formBuilder.control(''),
        contract: this.formBuilder.control('', [Validators.required])
      })

    }else{
      this.isAssociation = true;

      this.serviceForm = this.formBuilder.group({
        name: this.formBuilder.control('', [Validators.required]),
        description: this.formBuilder.control('', [Validators.required]),
        accession: this.formBuilder.control('', [Validators.required]),
        recurrence: this.formBuilder.control('', [Validators.required]),
        brand: this.formBuilder.control({value : this.appContext.loggedUser.brand, disabled: true}),
        supplier: this.formBuilder.control(''),
        segment: this.formBuilder.control(''),
        contract: this.formBuilder.control('')
      })
    }
    
    
  }

  createService(body) {
    body.accession = body.accession.replace(',','.')
    body.recurrence = body.recurrence.replace(',','.')
    
    body.brand = this.appContext.loggedUser.brand;
    this.servicesService.create(body)
    .subscribe(service => {
      this.snackbar.open('Serviço criado com sucesso', '', {
        duration: 8000
      })
      this.dialogRef.close()
    })
  }

}
