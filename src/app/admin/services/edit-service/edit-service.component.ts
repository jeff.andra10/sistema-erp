import { Component, OnInit, Inject, Injector } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '../../../_base/base-component.component';
import { ServicesService } from '../../../core/services/services.service';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-edit-service',
  templateUrl: './edit-service.component.html',
})
export class EditServiceComponent extends BaseComponent implements OnInit {
  
  serviceForm: FormGroup
  currentId: number
  suppliers: Array<object>
  brandList: Array<any>
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  isAssociation : Boolean = false
  

  constructor(
    private injector: Injector,
    private dialogRef: MatDialogRef<EditServiceComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private servicesService: ServicesService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar
  ) { 
    super(injector)
  }

  ngOnInit() {
    this.currentId = this.data.id
    this.brandList = this.appContext.brands
    this.suppliers = JSON.parse(sessionStorage.getItem('suppliers'))
    
    if(this.appContext.brandTracknme == this.appContext.loggedUser.brand){
      this.isAssociation = false;

      this.serviceForm = this.formBuilder.group({
        name: this.formBuilder.control(this.data.name, [Validators.required]),
        description: this.formBuilder.control(this.data.description, [Validators.required]),
        accession: this.formBuilder.control(this.data.accession, [Validators.required]),
        recurrence: this.formBuilder.control(this.data.recurrence, [Validators.required]),
        supplier: this.formBuilder.control(this.data.supplier ? this.data.supplier : ''),
        contract: this.formBuilder.control(this.data.contract ? this.data.contract : '' , [Validators.required]),
        brand: this.formBuilder.control({value : this.data.brandObj.id, disabled: true})
      })

    }else{
      this.isAssociation = true;

      this.serviceForm = this.formBuilder.group({
        name: this.formBuilder.control(this.data.name, [Validators.required]),
        description: this.formBuilder.control(this.data.description, [Validators.required]),
        accession: this.formBuilder.control(this.data.accession, [Validators.required]),
        recurrence: this.formBuilder.control(this.data.recurrence, [Validators.required]),
        supplier: this.formBuilder.control(this.data.supplier ? this.data.supplier : ''),
        contract: this.formBuilder.control(this.data.contract ? this.data.contract : ''),
        brand: this.formBuilder.control({value : this.data.brandObj.id, disabled: true})
      })
    }
  }

  updateService(editForm) {

    editForm.accession = editForm.accession.toString().replace(',','.')
    editForm.recurrence = editForm.recurrence.toString().replace(',','.')

    let editData = []
    for(let item in editForm){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': editForm[item]};
      editData.push(editItem);
    }
    this.servicesService.update(this.currentId, editData)
      .subscribe(service => {
        this.snackbar.open('Serviço atualizado com sucesso', '', {
          duration: 8000
        })
        this.dialogRef.close()
      })
  }
}
