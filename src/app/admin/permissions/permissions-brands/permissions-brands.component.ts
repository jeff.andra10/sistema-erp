import { Component, OnInit, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { SuppliersService } from '../../../core/services/suppliers.service';
import { UtilsService } from '../../../core/utils.service';
import { BaseComponent } from '../../../_base/base-component.component';
import { fuseAnimations } from '@fuse/animations';
import { PermissionsBrandsDataSource } from './permissions-brands.datasource';


@Component({
  selector: 'tnm-permissions-brands',
  templateUrl: './permissions-brands.component.html',
  styleUrls: ['./permissions-brands.component.scss'],
  animations : fuseAnimations
})
export class PermissionsBrandsComponent extends BaseComponent implements OnInit, AfterViewInit {

  brandList: Array<any>
  displayedColumns = ['companyName', 'email', 'edit', 'delete'];
  dataSource: PermissionsBrandsDataSource
  searchForm: FormGroup
  resultLength: number = 0
  searchSubject = new Subject()
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  
  
  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  constructor(
    private injector: Injector,
    private suppliersService: SuppliersService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router,
    private utils: UtilsService
  ) {
    super(injector)
   }

   searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  ngOnInit() {

    localStorage.setItem('hiddenLoading','false');
    this.brandList = this.appContext.brands
    
    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadSuppliers(val)
    })

   this.dataSource = new PermissionsBrandsDataSource(this.suppliersService)

    this.searchForm = this.formBuilder.group({
      brand: this.formBuilder.control('')
    });
    
  }

  onListValueChanged() {
    let brand = this.searchForm.value.brand ? this.searchForm.value.brand : 0
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    if(this.dataSource && this.dataSource.totalElements$){
      this.dataSource.totalElements$.subscribe( value => {
        this.resultLength = value
      })
    }
    
   
  }

  loadSuppliers(filter? : string) {

    this.dataSource.load(
      filter, 
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  } 

}
