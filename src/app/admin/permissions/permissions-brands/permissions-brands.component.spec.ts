import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionsBrandsComponent } from './permissions-brands.component';

describe('PermissionsBrandsComponent', () => {
  let component: PermissionsBrandsComponent;
  let fixture: ComponentFixture<PermissionsBrandsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionsBrandsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionsBrandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
