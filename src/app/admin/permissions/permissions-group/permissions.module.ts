import { NgModule, ModuleWithProviders } from "@angular/core";
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import { MaterialModule } from "../../../angular-material/material.module";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { NewPermissionComponent } from "./new-permission/new-permission.component";
import { PermissionsComponent } from "./permissions.component";
import { PermissionsBrandsComponent } from "../permissions-brands/permissions-brands.component";
import { FuseSharedModule } from "@fuse/shared.module";
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatTreeModule } from '@angular/material/tree';

const authRouting: ModuleWithProviders = RouterModule.forChild([
    { path: 'admin/permissions/group', component: PermissionsComponent },
    { path: 'admin/permissions/brands', component: PermissionsBrandsComponent },
    { path: 'admin/permissions/new', component: NewPermissionComponent }
  ]);

@NgModule({
    declarations:[
        PermissionsComponent,
        NewPermissionComponent,
        PermissionsBrandsComponent
    ],
    imports: [
        TextMaskModule,
        FlexLayoutModule,
        CommonModule,
        ReactiveFormsModule,
        MaterialModule,
        FuseSharedModule,
        CdkTreeModule,
        MatTreeModule,
        authRouting
    ],
    exports: [
        NewPermissionComponent
    ],
    entryComponents: [
        NewPermissionComponent
    ]
})
export class PermissionsModule {}