import { Component, OnInit, Injector } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { BaseComponent } from '../../../../_base/base-component.component';
import { SuppliersService } from '../../../../core/services/suppliers.service';
import { Supplier } from '../../../../core/models/supplier.model';
import { MaskUtils } from '../../../../core/mask-utils';
import { DataDomain } from '../../../../core/common-data-domain';
import {  ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
declare var require: any;


@Component({
  selector: 'tnm-new-permission',
  templateUrl: './new-permission.component.html'
})
export class NewPermissionComponent extends BaseComponent implements OnInit {
  
  supplierForm: FormGroup
  usersList: Array<any>
  statusList: Array<any>
  
  bankList: any
  panelOpenState: boolean = false;
  phoneMask = MaskUtils.phoneMask
  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');

  permissionsArray: any

  typeDashboard: string[] = ['Dashboard'];
  typeCustomers: string[] = ['Clientes', 'Cadastrar Clientes', 'Editar Clientes'];
  typeDevices: string[] = ['Rastreadores', 'Cadastrar Rastreador', 'Editar Rastreador', 'Vincular Cliente', 'Vincular Veículo', 'Adicionar Imagens de Instalação'];
  typeSales: string[] = ['Sales', 'Criar Nova Venda', 'Editar Venda', 'Vincular Cliente', 'Vincular Veículo', 'Adicionar Imagens de Instalação'];
  
  constructor(
    private injector: Injector,
    private suppliersService: SuppliersService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private router: Router
  ) { 
    super(injector)    
  }

  ngOnInit() {

    let permissonFile = require('../../permission.json');

    this.permissionsArray = permissonFile//JSON.parse(permissonFile);
    this.createForm()
    this.usersList  = [{'name':'Administrador',id:'ADMINISTRATOR'},{'name':'Operador',id:'OPERATOR'},{'name':'Instalador',id:'INSTALLER'},{'name':'Corretor',id:'BROKER'}]
    this.statusList = [{'name':'Ativo',id:'ACTIVE'},{'name':'Inativo',id:'INACTIVE'}]
  }

  createForm() {
    this.supplierForm = this.formBuilder.group({
      categorie: this.formBuilder.control('', [Validators.required]),
      name: this.formBuilder.control('', [Validators.required]),
      permission: this.formBuilder.control('', [Validators.required]),
      status: this.formBuilder.control('ACTIVE', [Validators.required])
    })

    
  }

  createPermission(body) {
    
    
  }

}
