import { NgModule, ModuleWithProviders } from "@angular/core";
import { FlexLayoutModule } from '@angular/flex-layout'
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../../angular-material/material.module";
import { ReactiveFormsModule } from "@angular/forms";
import { FuseSharedModule } from "@fuse/shared.module";
import { RouterModule } from "@angular/router";
import { NgxDnDModule } from '@swimlane/ngx-dnd';

import { SelectAgreementTypeComponent } from './select-agreement-type/select-agreement-type.component';
import { OffersComponent } from "./offers.component";
import { DetailsOfferComponent } from "./details-offer/details-offer.component";
import { OfferCardComponent } from "./details-offer/offer-card/offer-card.component";
import { EditOfferNameComponent } from "./details-offer/edit-offer-name/edit-offer-name.component";
import { OffersSelectedService } from "./offers.selected.service";
import { CanDeactivateGuard } from "../../core/can-deactivate-guard.service"
import { ConfirmDialogComponent } from "../../dialogs/confirm-dialog/confirm-dialog.component";
import { OfferSelectBusinessSegmentComponent } from "./select-business-segment/select-business-segment.component";

const authRouting: ModuleWithProviders = RouterModule.forChild([
  {path: 'admin/offers/new/business-segment', component: OfferSelectBusinessSegmentComponent },
  {path: 'admin/offers/new/agreement-type', component: SelectAgreementTypeComponent },
  {path: 'admin/offers/new', component: DetailsOfferComponent , canDeactivate: [CanDeactivateGuard]},
  {path: 'admin/offers/:id', component: DetailsOfferComponent , canDeactivate: [CanDeactivateGuard]}
]);

@NgModule({
  imports: [
    FlexLayoutModule,
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FuseSharedModule,
    authRouting,
    NgxDnDModule
  ],
  declarations: [
    OffersComponent, 
    DetailsOfferComponent, 
    SelectAgreementTypeComponent,
    OfferCardComponent,
    EditOfferNameComponent,
    OfferSelectBusinessSegmentComponent
    
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  providers: [
    OffersSelectedService,
    CanDeactivateGuard
  ]
})
export class OffersModule { }
