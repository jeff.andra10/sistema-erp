import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs";
import { Offer } from "../../core/models/offer.model";

@Injectable()
export class OffersSelectedService {

    private offerSource : BehaviorSubject<Offer> = new BehaviorSubject(null)
    public offerSelected = this.offerSource.asObservable();
  
    setSelected(value: Offer) {
      this.offerSource.next(value)
    }
}