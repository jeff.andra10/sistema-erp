import { Component, OnInit, ViewChild, AfterViewInit, Injector } from '@angular/core'
import { MatSnackBar, MatDialog, MatSelectionList,  MatSlideToggle } from '@angular/material'
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subject, Subscription } from 'rxjs';
import { FuseConfigService } from '@fuse/services/config.service';

import { BaseComponent } from '../../../_base/base-component.component';
import { ProductsService } from '../../../core/services/products.service';
import { ServicesService } from '../../../core/services/services.service';
import { OffersService } from '../../../core/services/offers.service';
import { OffersSelectedService } from '../offers.selected.service';
import { DataDomain } from '../../../core/common-data-domain';
import { ConfirmDialogComponent } from 'app/dialogs/confirm-dialog/confirm-dialog.component';
import { Brand } from '../../../core/models/brand.model';
import { Offer } from '../../../core/models/offer.model';
import { BrandsService } from "../../../core/services/brands.service";
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-details-offer',
  templateUrl: './details-offer.component.html',
  styleUrls: ['./details-offer.component.scss'],
  animations: fuseAnimations
})
export class DetailsOfferComponent extends BaseComponent implements OnInit, AfterViewInit {

  offerForm: FormGroup

  @ViewChild('brandsSelectionList')
  brandsSelectionList: MatSelectionList

  @ViewChild('allBrandsSlideToggle')
  allBrandsSlideToggle: MatSlideToggle

  offer: Offer = new Offer()
  hasChanges: Boolean = false
  currentUserIsTracknme : Boolean = false
  leaseContractPeriodList: Array<any> = []
  installmentsList: Array<any> = []
  
  productsServicesList: Array<any> = []
  myProductsServicesList: Array<any> = []
  commoditiesOfferList: Array<any> = []
  brandsList: Array<Brand> = []
  availableForBrands: Array<any> = []
  productsServicesUnavaiable : Boolean = false
  myProductsServicesUnavaiable : Boolean = false
  brandName : any
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  dialogRef: any;
  
  constructor(
    private injector: Injector,
    private formBuilder: FormBuilder, 
    private snackbar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private productsService: ProductsService,
    private servicesService: ServicesService,
    private offersService: OffersService,
    private brandsService: BrandsService,
    private offersSelectedService: OffersSelectedService) { 
      super(injector)
    }

  ngOnInit() {

    this.currentUserIsTracknme = this.appContext.isLoggedUserTracknme()
    
    this.getBrands()
    this.loadDomainData();
    this.createForm();
   
  }

  
  getBrands(){
    this.brandsService.getBrandsByTree('',0).subscribe(brands => {
      if(brands && brands.content){
        this.brandsList = brands.content[0].brandChildren
        this.brandsList.push(brands.content[0])
        this.brandName = brands.content[0].name
      }
    })
  }

  ngAfterViewInit() {

    this.offersSelectedService.offerSelected.subscribe(offerSubscribed => {
      
      if (offerSubscribed != null) {
        this.offer = offerSubscribed
      
        this.createForm();

        let creditCardPayment : Boolean = (
          this.offer.availablePaymentMethods != null &&
          this.offer.availablePaymentMethods.indexOf('CREDIT_CARD') > -1
        ) 

        let debitCardPayment : Boolean = (
          this.offer.availablePaymentMethods != null &&
          this.offer.availablePaymentMethods.indexOf('DEBIT_CARD') > -1
        ) 

        let bookletPayment : Boolean = (
          this.offer.availablePaymentMethods != null &&
          this.offer.availablePaymentMethods.indexOf('BOOKLET') > -1
        ) 

       
        // load form data
        if (this.offer.agreement == 'SALE') {

          this.offerForm.patchValue ({
            description: this.offer.description || null,
            salePrice: this.offer.salePrice || null,
            saleMaxInstallments: this.offer.saleMaxInstallments || null,
            creditCardPayment: creditCardPayment,
            bookletPayment: bookletPayment,
            debitCardPayment:debitCardPayment,
            recurrencePrice: this.offer.recurrencePrice || null,
          });
        } 
        else if (this.offer.agreement == 'LEASE') {
          
          this.offerForm.patchValue ({
            description: this.offer.description || null,
            leaseAccessionPrice: this.offer.leaseAccessionPrice || null,
            leaseGracePeriod: this.offer.leaseGracePeriod || null,
            leaseContractPeriod: this.offer.leaseContractPeriod || null,
            leaseInstallmentPrice: this.offer.leaseInstallmentPrice || null,
            creditCardPayment: creditCardPayment,
            bookletPayment: bookletPayment,
            debitCardPayment:debitCardPayment,
            saleMaxInstallments: this.offer.saleMaxInstallments || null,
            recurrencePrice: this.offer.recurrencePrice || null,
          });
        } 
        else if (this.offer.agreement == 'RENT') {
          this.offerForm.patchValue ({
            description: this.offer.description || null,
            leaseContractPeriod: this.offer.leaseContractPeriod || null,
            creditCardPayment: creditCardPayment,
            bookletPayment: bookletPayment,
            debitCardPayment:debitCardPayment,
            saleMaxInstallments: this.offer.saleMaxInstallments || null,
            recurrencePrice: this.offer.recurrencePrice || null,
          });
        } 
        else if (this.offer.agreement == 'INSURANCE') {
          this.offerForm.patchValue ({
            description: this.offer.description || null
          });
        } 
        else if (this.offer.agreement == 'FREE') {
          this.offerForm.patchValue ({
            description: this.offer.description || null,
          });
        }

        this.availableForBrands = this.offer.availableForBrands
      }
    })
  }

  canDeactivate() : Observable<boolean> | Boolean {

    const subject = new Subject<boolean>();

    if (this.offerForm.dirty || this.hasChanges) {

      this.dialog.open(ConfirmDialogComponent, {
            panelClass: 'event-form-dialog',
            data: { 
                    width: '600px',
                    height: '300px',
                    title: 'Confirmação', 
                    message: 'Deseja sair sem salvar as alterações?' 
                  }
        })
        .afterClosed()
        .subscribe(response=>{
          subject.next(response.data.confirm);
          subject.complete();
        });

      return subject.asObservable();
    }

    return true
  }

  createForm() {

    if (this.offer.agreement == 'SALE') {

      this.offerForm = this.formBuilder.group({
        description: this.formBuilder.control('', [Validators.required]),
        salePrice: this.formBuilder.control('', [Validators.required]),
        saleMaxInstallments: this.formBuilder.control('', [Validators.required]),
        creditCardPayment: this.formBuilder.control('', []),
        bookletPayment: this.formBuilder.control('', []),
        debitCardPayment: this.formBuilder.control('', []),
        recurrencePrice: this.formBuilder.control('', []),
      });
    } else if (this.offer.agreement == 'LEASE') {

      this.offerForm = this.formBuilder.group({
        description: this.formBuilder.control('', [Validators.required]),
        leaseAccessionPrice: this.formBuilder.control('', []),
        leaseGracePeriod: this.formBuilder.control('', []),
        leaseContractPeriod: this.formBuilder.control('', [Validators.required]),
        leaseInstallmentPrice: this.formBuilder.control('', [Validators.required]),
        creditCardPayment: this.formBuilder.control('', []),
        bookletPayment: this.formBuilder.control('', []),
        debitCardPayment: this.formBuilder.control('', []),
        recurrencePrice: this.formBuilder.control('', [])
      });
    }else if (this.offer.agreement == 'RENT') {

      this.offerForm = this.formBuilder.group({
        description: this.formBuilder.control('', [Validators.required]),
        leaseContractPeriod: this.formBuilder.control('', [Validators.required]),
        creditCardPayment: this.formBuilder.control('', []),
        bookletPayment: this.formBuilder.control('', []),
        debitCardPayment: this.formBuilder.control('', []),
        recurrencePrice: this.formBuilder.control('', [])
      });
    }else if (this.offer.agreement == 'INSURANCE') {

      this.offerForm = this.formBuilder.group({
        description: this.formBuilder.control('', [Validators.required])
      });

    } else if (this.offer.agreement == 'FREE') {

      this.offerForm = this.formBuilder.group({
        description: this.formBuilder.control('', [Validators.required])
      });
    }
  }
  
  loadDomainData() {

    this.leaseContractPeriodList = DataDomain.getLeaseContractPeriod();
    this.installmentsList = DataDomain.getInstallments();

    if (this.currentUserIsTracknme) {
      this.loadProductsServicesForTracknme();
    } else {
      this.loadProductsServicesForAssociation();
    }
  }

  private loadProductsServicesForTracknme() {

    // get products/services
    Observable.forkJoin([
      this.productsService.getProducts(),
      this.servicesService.getServices()
    ]).subscribe(result => {

      let productsServices: Array<any> = [];

      // add products
      result[0].content.forEach(item => {
        let wrapper: any = {};
        wrapper.product = item;
        productsServices.push(wrapper);
      });

      // add services
      result[1].content.forEach(item => {
        let wrapper: any = {};
        wrapper.service = item;
        productsServices.push(wrapper);
      });

      // handle offer edit
      if (!this.offer.id) {
        this.productsServicesList = productsServices
      } else {

        this.commoditiesOfferList = [];
        productsServices.forEach(commoditie => {
          
                if ((commoditie.product && this.offer.products.map(function(product) { return product.id; }).indexOf(commoditie.product.id) > -1)
                || (commoditie.service && this.offer.services.map(function(service) { return service.id; }).indexOf(commoditie.service.id) > -1)) {

                  if(this.offer.products){
                      let index = this.offer.products.map((product) => { return product.id;}).indexOf(commoditie.product.id)
                                  commoditie.product.recurrence = this.offer.products[index].recurrence
                                  commoditie.product.accession  = this.offer.products[index].accession
                  }else if(this.offer.services){
                      let index = this.offer.services.map((service) => { return service.id;}).indexOf(commoditie.service.id)
                                  commoditie.service.recurrence = this.offer.services[index].recurrence
                                  commoditie.service.accession  = this.offer.services[index].accession
                  }
                  
                  this.commoditiesOfferList.push(commoditie);
                    
                }else {
                  this.productsServicesList.push(commoditie);
                }
        });
      }
    })
  }

  private loadProductsServicesForAssociation() {

    let productsServices: Array<any> = [];
    this.commoditiesOfferList = [];

    // get commmodities
    this.productsService.getCommodities()
      .subscribe(commodities => {
        
        this.productsServicesUnavaiable = commodities.content
                                          .filter((v) => v.status != 'INACTIVE')
                                          .length == 0;

        // add products/services
        commodities.content.forEach(commoditie => {
          let wrapper: any = {};
          
          if(commoditie.status != 'INACTIVE'){
            if (commoditie.product) {
              wrapper.product = commoditie.relationships.product;
            }
            else if (commoditie.service) {
              wrapper.service = commoditie.relationships.service;
            }
            productsServices.push(wrapper);
          }
          
        });

        // handle offer edit
        if (!this.offer.id) {
          this.productsServicesList = productsServices;
        }
        else {
          
          productsServices.forEach(commoditie => {
           
              if ((commoditie.product && this.offer.products.map(function(product) { return product.id; }).indexOf(commoditie.product.id) > -1)
              || (commoditie.service && this.offer.services.map(function(service) { return service.id; }).indexOf(commoditie.service.id) > -1)) {

                  if(this.offer.products){
                      let index = this.offer.products.map((product) => { return product.id;}).indexOf(commoditie.product.id)
                                  commoditie.product.recurrence = this.offer.products[index].recurrence
                                  commoditie.product.accession  = this.offer.products[index].accession
                  }else if(this.offer.services){
                      let index = this.offer.services.map((service) => { return service.id;}).indexOf(commoditie.service.id)
                                  commoditie.service.recurrence = this.offer.services[index].recurrence
                                  commoditie.service.accession  = this.offer.services[index].accession
                  }
                  this.commoditiesOfferList.push(commoditie);
              }else {
                this.productsServicesList.push(commoditie);
              }
            
        });

        }
      });

      this.productsService.getMyProducts()
      .subscribe(products => {
        products.content.forEach(product => {
          let wrapper: any = {};
          wrapper.product = product
          
          if (!this.offer.id) {
            this.myProductsServicesList.push(wrapper)
          }else {
            if(this.offer.products[0] && this.offer.products[0].id){
                if (this.offer.products.indexOf(product.id) > -1){
                  this.commoditiesOfferList.push(wrapper);
                }else {
                  this.myProductsServicesList.push(wrapper)
                }
            }else{
                if (this.offer.products.map(function(product) { return product.id; }).indexOf(product.id) > -1){
                  this.commoditiesOfferList.push(wrapper);
                }else {
                  this.myProductsServicesList.push(wrapper)
                }
            }
          }
        })
        this.myProductsServicesUnavaiable = true
        
      })

      this.servicesService.getMyServices()
      .subscribe(services => {
        services.content.forEach(service => {
          let wrapper: any = {};
          wrapper.service = service
          
          if (!this.offer.id) {
            this.myProductsServicesList.push(wrapper)
          }else {
            if(this.offer.services[0] && this.offer.services[0].id){
              if (this.offer.services.indexOf(service.id) > -1){
                this.commoditiesOfferList.push(wrapper);
              }else {
                this.myProductsServicesList.push(wrapper)
              }
          }else{
              if (this.offer.services.map(function(service) { return service.id; }).indexOf(service.id) > -1){
                this.commoditiesOfferList.push(wrapper);
              }else {
                this.myProductsServicesList.push(wrapper)
              }
          }
            
          }
        })
        this.myProductsServicesUnavaiable = true
        
      })

  }

  onOfferNameChanged(newName) {
    this.offer.name = newName
    this.hasChanges = true
  }

  onLisDrop(event) {
    this.hasChanges = true
  }

  saveOffer() {

    if (!this.offer.name) {
      this.snackbar.open('Informe o nome da Oferta.', '', { duration: 8000 })
      return
    }

    if (this.commoditiesOfferList.length == 0) {
      this.snackbar.open('Escolha os Produtos e Serviços.', '', { duration: 8000 })
      return
    }

   
    if (this.offerForm.invalid) {
      this.validateAllFields(this.offerForm)
      return false
    }

    if (this.offer.agreement == 'LEASE' || this.offer.agreement == 'RENT' || this.offer.agreement == 'SALE') {
    
      if (!(this.offerForm.controls.creditCardPayment.value == true ||
          this.offerForm.controls.bookletPayment.value == true ||  this.offerForm.controls.debitCardPayment.value == true
        )) {
        this.snackbar.open('Escolha ao menos uma forma de Pagamento.', '', { duration: 8000 })
        return
      }
    }

    if (this.currentUserIsTracknme) {
        if (!this.allBrandsSlideToggle.checked && 
            (this.availableForBrands == null || this.availableForBrands.length == 0)) {

            this.snackbar.open('Escolha para quais marcas a Oferta será disponibilizada.', '', { duration: 8000 });
            return
        }
    }else{
        if ( this.availableForBrands == null || this.availableForBrands.length == 0){

          this.snackbar.open('Escolha para quais marcas a Oferta será disponibilizada.', '', { duration: 8000 });
          return
        }
    }

    this.bindOfferModel();

    let call : Observable<any>

    if (this.offer.id) {
      call = this.updateOffer()
    } else {
      call = this.createNewOffer()
    }

    call.subscribe(offer => {
        
      this.snackbar.open('Oferta salva com sucesso', '', { duration: 8000 });
      this.offerForm.reset()
      this.hasChanges = false
      this.navigateToOffers()

    }, error => {
      console.log(error)
        this.snackbar.open(error.error ? error.error.message : error.message, '', { duration: 8000 });
    });
  }

  private bindOfferModel() {

    this.offer.brand = this.appContext.session.user.brand;
    this.offer.description = this.offerForm.controls.description.value;

    if (this.offer.agreement == 'SALE') {
      this.offer.salePrice = this.offerForm.controls.salePrice.value;
      this.offer.saleMaxInstallments = this.offerForm.controls.saleMaxInstallments.value;
    }
    else if (this.offer.agreement == 'LEASE') {

      this.offer.leaseAccessionPrice = this.offerForm.controls.leaseAccessionPrice.value;
      this.offer.leaseGracePeriod = this.offerForm.controls.leaseGracePeriod.value;
      this.offer.leaseContractPeriod = this.offerForm.controls.leaseContractPeriod.value;
      this.offer.leaseInstallmentPrice = this.offerForm.controls.leaseInstallmentPrice.value;
    }
    else if (this.offer.agreement == 'RENT') {
      this.offer.leaseContractPeriod = this.offerForm.controls.leaseContractPeriod.value;
    }
  

    if (this.offer.agreement == 'LEASE' || this.offer.agreement == 'RENT' || this.offer.agreement == 'SALE') {

      this.offer.chargingRecurrence = 'MONTHLY';
      this.offer.recurrencePrice = this.offerForm.controls.recurrencePrice.value != null ? this.offerForm.controls.recurrencePrice.value : 0

      // payment methods
      this.offer.availablePaymentMethods = [];
      if (this.offerForm.controls.creditCardPayment.value == true) {
        this.offer.availablePaymentMethods.push('CREDIT_CARD');
      }
      if (this.offerForm.controls.debitCardPayment.value == true) {
        this.offer.availablePaymentMethods.push('DEBIT_CARD');
      }
      if (this.offerForm.controls.bookletPayment.value == true) {
        this.offer.availablePaymentMethods.push('BOOKLET');
      }
    }

    // products
    if (this.offer.products == null) {
      this.offer.products = [];
    }
    
    this.offer.products = this.commoditiesOfferList
      .filter((v) => v.product != null)
      .map(commoditie => this.converteCommoditie(commoditie.product));

    // services
    if (this.offer.services == null) {
      this.offer.services = [];
    }
    this.offer.services = this.commoditiesOfferList
      .filter((v) => v.service != null)
      .map(commoditie => this.converteCommoditie(commoditie.service));

    // brands
    if (this.currentUserIsTracknme && this.allBrandsSlideToggle && this.allBrandsSlideToggle.checked) {
      this.offer.availableForAllBrands = true;
    }
    else {
      this.offer.availableForAllBrands = false;
      if (this.offer.availableForBrands == null) {
        this.offer.availableForBrands = [];
      }
      this.offer.availableForBrands = this.availableForBrands;
    }
    //if (this.currentUserIsTracknme == false) {
      //this.offer.availableForBrands = [this.appContext.loggedUser.brand];
    //}
  }

  converteCommoditie(commoditie){
    return {"id" : commoditie.id,"accession":commoditie.accession,"recurrence":commoditie.recurrence}
  }

  navigateToOffers() {
    this.router.navigate(['admin/offers/'])
  }

  createNewOffer() {
    return this.offersService.create(this.offer)
  }

  updateOffer() : Observable<any> {

    let updateData = []

    updateData.push({'op' : 'add', 'path' : '/name', 'value' : this.offer.name})
    updateData.push({'op' : 'add', 'path' : '/description', 'value' : this.offer.description})
    updateData.push({'op' : 'add', 'path' : '/availableForBrands', 'value' : this.offer.availableForBrands})
    updateData.push({'op' : 'add', 'path' : '/availableForAllBrands', 'value' : this.offer.availableForAllBrands})
    updateData.push({'op' : 'add', 'path' : '/products', 'value' : this.offer.products})
    updateData.push({'op' : 'add', 'path' : '/services', 'value' : this.offer.services})
    
    if (this.offer.agreement == 'SALE') {
      
      updateData.push({'op' : 'add', 'path' : '/salePrice', 'value' : this.offer.salePrice})
      updateData.push({'op' : 'add', 'path' : '/saleMaxInstallments', 'value' : this.offer.saleMaxInstallments})
    }
    else if (this.offer.agreement == 'LEASE'){

      updateData.push({'op' : 'add', 'path' : '/leaseAccessionPrice', 'value' : this.offer.leaseAccessionPrice})
      updateData.push({'op' : 'add', 'path' : '/leaseGracePeriod', 'value' : this.offer.leaseGracePeriod})
      updateData.push({'op' : 'add', 'path' : '/leaseInstallmentPrice', 'value' : this.offer.leaseInstallmentPrice})
      updateData.push({'op' : 'add', 'path' : '/leaseContractPeriod', 'value' : this.offer.leaseContractPeriod})
    }

    if (this.offer.agreement == 'LEASE' || this.offer.agreement == 'RENT' || this.offer.agreement == 'SALE'){
      updateData.push({'op' : 'add', 'path' : '/availablePaymentMethods', 'value' : this.offer.availablePaymentMethods})
      updateData.push({'op' : 'add', 'path' : '/recurrencePrice', 'value' : this.offer.recurrencePrice})
    }

    return this.offersService.update(this.offer.id, updateData)
  }

  getAccessionTotalCost() : number {
    
    if (this.commoditiesOfferList.length == 0) {
      return 0;
    }

    let productsCost = this.commoditiesOfferList
      .filter((v) => v.product != null)
      .map(o => {
        return o.product.accession
      }).reduce((x, y) => x + y, 0);

    let servicesCost = this.commoditiesOfferList
        .filter((v) => v.service != null)
        .map(o => {
          return o.service.accession
        }).reduce((x, y) => x + y, 0);        
        
    return productsCost + servicesCost;
  }

  getRecurrenteTotalCost() : number {
    
    if (this.commoditiesOfferList.length == 0) {
      return 0;
    }

    let productsCost = this.commoditiesOfferList
      .filter((v) => v.product != null)
      .map(o => {
        return o.product.recurrence 
      }).reduce((x, y) => x + y, 0);

    let servicesCost = this.commoditiesOfferList
        .filter((v) => v.service != null)
        .map(o => {
          return o.service.recurrence
        }).reduce((x, y) => x + y, 0);        
        
    return productsCost + servicesCost;
  }

  onBrandsSelectionChange(event : any) {
    
    let option = event.option
    let brandId = option.value

    if(this.availableForBrands == undefined || !this.availableForBrands){
      this.availableForBrands = []
    }

    if (option.selected) {
      this.availableForBrands.push(brandId)
    } else {
      let idx = this.availableForBrands.indexOf(brandId)
      if (idx > -1) {
        this.availableForBrands.splice(idx, 1)
      }
    } 

  }

  brandIsSelected(id: number) : Boolean {

    return !this.availableForBrands ? false : this.availableForBrands.indexOf(id) > -1
  }

}


