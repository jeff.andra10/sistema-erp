import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector   : 'app-edit-offer-name',
    templateUrl: './edit-offer-name.component.html',
    styleUrls  : ['./edit-offer-name.component.scss']
})
export class EditOfferNameComponent
{
    formActive = false;
    form: FormGroup;
    @Input() offer;
    @Output() onNameChanged = new EventEmitter();
    @ViewChild('nameInput') nameInputField;

    constructor(
        private formBuilder: FormBuilder
    )
    {
    }

    openForm()
    {
        this.form = this.formBuilder.group({
            name: [this.offer.name]
        });
        this.formActive = true;
        this.focusNameField();
    }

    closeForm()
    {
        this.formActive = false;
    }

    focusNameField()
    {
        setTimeout(() => {
            this.nameInputField.nativeElement.focus();
        });
    }

    onFormSubmit()
    {
        if ( this.form.valid )
        {
            this.offer.name = this.form.getRawValue().name;
            this.onNameChanged.next(this.offer.name);
            this.formActive = false;
        }
    }
}
