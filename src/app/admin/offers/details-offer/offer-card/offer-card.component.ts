import { Component, OnInit, ViewEncapsulation, Input, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-offer-card',
  templateUrl: './offer-card.component.html',
  styleUrls: ['./offer-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OfferCardComponent implements OnInit, AfterViewInit {

  @Input() model;

  constructor() { }

  ngOnInit() {
    
  }

  ngAfterViewInit(){
  }

  changeAccession(accession){
  
    if(this.model.product){
      this.model.product.accession = accession.target.value
    }else if(this.model.service){
      this.model.service.accession = accession.target.value
    }
  }

  changeRecurrence(recurrence){

    if(this.model.product){
      this.model.product.recurrence = recurrence.target.value
    }else if(this.model.service){
      this.model.service.recurrence = recurrence.target.value
    }
  }
}
