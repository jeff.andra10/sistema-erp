import { Component, OnInit, AfterViewInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';
import { Offer } from 'app/core/models/offer.model';
import { OffersSelectedService } from 'app/admin/offers/offers.selected.service';

@Component({
  selector: 'app-select-agreement-type',
  templateUrl: './select-agreement-type.component.html',
  styleUrls: ['./select-agreement-type.component.scss'],
  animations : fuseAnimations
})
export class SelectAgreementTypeComponent implements OnInit, AfterViewInit {

  offer: Offer

  constructor(
    private router: Router,
    private offersSelectedService: OffersSelectedService
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {

    this.offersSelectedService.offerSelected.subscribe(offerSubscribed => {
      
      if (offerSubscribed != null) {
        this.offer = offerSubscribed
      }
    })
  }

  selectType(agreementType: string) {

    this.offer.agreement = agreementType
    this.router.navigate(['admin/offers/new'])
  }
}
