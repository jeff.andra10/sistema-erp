import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectAgreementTypeComponent } from './select-agreement-type.component';

describe('SelectAgreementTypeComponent', () => {
  let component: SelectAgreementTypeComponent;
  let fixture: ComponentFixture<SelectAgreementTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectAgreementTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectAgreementTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
