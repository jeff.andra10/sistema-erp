import { Component, OnInit, AfterViewInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';
import { Offer } from 'app/core/models/offer.model';
import { OffersSelectedService } from 'app/admin/offers/offers.selected.service';
import { AppContext } from '../../../core/appcontext';


@Component({
  selector: 'app-offer-select-business-segment',
  templateUrl: './select-business-segment.component.html',
  styleUrls: ['./select-business-segment.component.scss'],
  animations : fuseAnimations
})
export class OfferSelectBusinessSegmentComponent implements OnInit, AfterViewInit {

  offer: Offer
  isTracknme : boolean = false

  constructor(
    private router: Router,
    private offersSelectedService: OffersSelectedService,
    private appContext: AppContext
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {

    if(this.appContext.loggedUser.brand == this.appContext.brandTracknme){
      this.isTracknme = true
    }

    this.offersSelectedService.offerSelected.subscribe(offerSubscribed => {
      
      if (offerSubscribed != null) {
        this.offer = offerSubscribed
      }
    })
  }

  selectType(value: string) {
    this.offer.segment = value
    this.router.navigate(['admin/offers/new/agreement-type'])
  }

  createNewOffer(value: string) {

    console.log('value:',value)
    this.offer.segment = value
    this.offer.agreement = 'SALE'
    this.router.navigate(['admin/offers/new'])
  }
  
}
