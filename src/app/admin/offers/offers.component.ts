import { Component, OnInit, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Subscription, Subject } from 'rxjs';
import { catchError } from "rxjs/operators";
import { of } from "rxjs/observable/of";
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators';
import { MatPaginator, MatSelect } from '@angular/material'
import { FuseConfigService } from '@fuse/services/config.service';

import { OffersService } from '../../core/services/offers.service';
import { BaseComponent } from '../../_base/base-component.component';
import { Offer } from '../../core/models/offer.model';
import { OffersSelectedService } from './offers.selected.service';
import { Brand } from '../../core/models/brand.model';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss'],
  animations : fuseAnimations
})
export class OffersComponent extends BaseComponent implements OnInit, AfterViewInit {

  offers: any[];
  offersByBrand: any[];
  brandsList: Array<Brand>
  segmentList: Array<any>
  agreementList: Array<any>
  selectForm: FormGroup
  resultLength: number = 0
  pageSize: number = 10
  searchSubject = new Subject()
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  @ViewChild(MatPaginator) paginator: MatPaginator
 // @ViewChild(MatSelect) brandsSelect: MatSelect;

  constructor(
    private injector: Injector,
    private router: Router,
    private formBuilder: FormBuilder,
    private offersService: OffersService,
    private offersSelected: OffersSelectedService
  ) { 
    super(injector);
    this.offersSelected.setSelected(null);
  }

  ngOnInit() {

    this.segmentList = [{'name':'Clientes', 'id' :'CUSTOMERS'},{'name':'Empresas', 'id' :'BUSINESS'},{'name':'Rastreador', 'id' :'DEVICE'}]
    this.agreementList = [{'name':'Comodato', 'id' :'LEASE'},{'name':'Venda', 'id':'SALE'},{'name':'Venda sem cobrança', 'id' :'FREE'},{'name':'Aluguel', 'id' :'RENT'},{'name':'Seguro', 'id' :'INSURANCE'}]
    localStorage.setItem('hiddenLoading','false');

    this.selectForm = this.formBuilder.group({
      brand: this.formBuilder.control(''),
      segment: this.formBuilder.control(''),
      agreement: this.formBuilder.control('')
    });

    this.searchSubject
      .debounceTime(500)
      .subscribe((val : string) => {
        this.load(val);
      });

    this.brandsList = this.utilsService.sortByField(this.appContext.brands, 'name');

    this.load();
  }

  ngAfterViewInit() {

    merge(this.paginator.page)
    .pipe(
      tap(() => {
          this.load()
        })
    )
    .subscribe()
  }

  load(filter? :string) {

    this.offersService.findOffers(
        filter, 
        this.selectForm.value.brand,
        this.selectForm.value.segment,
        this.selectForm.value.agreement,
        'name',
        'asc',
        this.paginator.pageIndex, 
        this.paginator.pageSize == null ? this.pageSize : this.paginator.pageSize).pipe(
            catchError(() => of([]))
        )
        .subscribe(result => {
          
            this.offers = result.content;
            this.resultLength = result.totalElements;
        });
  }

  newOffer() {
    
    this.offersSelected.setSelected(new Offer())
    this.router.navigate(['admin/offers/new/business-segment'])
  }

  edit(offer: Offer) {

    this.offersSelected.setSelected(offer)
    this.router.navigate(["admin/offers/", offer.id])
  }

  brandName(id: number) : string {

    let brand = this.brandsList.find(item=> item.id == id)

    return brand ? brand.name : ''
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  onListValueChanged() {
   this.load()
  }
}
