import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {catchError, finalize} from "rxjs/operators";
import {of} from "rxjs/observable/of";
import { BrandsService } from "../../core/services/brands.service";

export class BrandsDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private totalElementsSubject = new BehaviorSubject<number>(0)

    public totalElements$ = this.totalElementsSubject.asObservable()

    public data$ = this.dataSubject.asObservable()

    constructor(
        private brandsService: BrandsService) {
    }

    loadBrands(filter:string,
                parent:number,
                sortDirection:string,
                pageIndex:number,
                pageSize:number) {

                if(filter == undefined){
                    filter = ''
                }

                if(parent == undefined){
                    parent = 0
                }

        this.brandsService.findBrands(filter, parent, sortDirection,
            pageIndex, pageSize).pipe(
                catchError(() => of([]))
            )
            .subscribe(brands => {
                this.totalElementsSubject.next(brands.totalElements)
                this.dataSubject.next(brands.content)
            })
    }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable()
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete()
        this.totalElementsSubject.complete()
    }
}