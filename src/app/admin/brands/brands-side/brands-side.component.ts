import { Component, Output, EventEmitter, } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrandsService } from "../../../core/services/brands.service";
import {MatDialog,MatSnackBar} from '@angular/material';
import { NewBrandComponent } from "../new-brand/new-brand.component";

@Component({
    selector   : 'brands-sidenav',
    templateUrl: './brands-side.component.html',
    styleUrls  : ['./brands-side.component.scss']
})

export class BrandsSidenavComponent{

    @Output() onFilterBrand: EventEmitter<any> = new EventEmitter();
    @Output() onFilterPermission: EventEmitter<any> = new EventEmitter();

    idBy: string;
    brandChildrenList : Array<any>
    brandParentName : string = '';
    brandParentId: string = '';
    secondColorBrand : string = localStorage.getItem('secondColorBrand');

    constructor(
        private activatedRoute: ActivatedRoute,
        private brandsService: BrandsService,
        private dialog: MatDialog,
        private snackbar: MatSnackBar
      ) {}

    ngOnInit() {
        this.idBy = this.activatedRoute.snapshot.params['id'] 
        this.loadBrands()
    }

    loadBrands(){
        this.brandsService.getBrandsTreeById(this.activatedRoute.snapshot.params['id']).subscribe(brands => {
            this.brandParentName =  brands.content[0].name
            this.brandParentId =  brands.content[0].id
            this.brandChildrenList = brands.content[0].brandChildren
        })
    }

    orderByName(list){
        if(list && list.length > 0){
            return list.sort(function(a,b) {
                return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
            });
        }else{
            return list
        }
    }
    
    changeBrand(id)
    {
        this.idBy = id;
        this.onFilterBrand.emit(id)
    }

    showPermission(){
        this.onFilterPermission.emit(true)
    }

    changeBrandParent()
    {
        this.idBy = this.activatedRoute.snapshot.params['id'] 
        this.onFilterBrand.emit(this.brandParentId)
    }

    newBrandDialog(){
        
        this.dialog
        .open(NewBrandComponent, {
          width: '1000px',
          data: this.idBy
        })
        .afterClosed().subscribe(() => this.loadBrands())
      }

}
