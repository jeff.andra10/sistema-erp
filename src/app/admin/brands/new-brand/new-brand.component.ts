import { Component, OnInit, Injector, Inject, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms/src/model';
import { FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MatDialog, MAT_DIALOG_DATA, MatStepper} from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { BrandsService } from "../../../core/services/brands.service";
import { MatColors } from '@fuse/mat-colors';
import { Router } from '@angular/router';
import { CustomersService } from '../../../core/services/customers.service';
import { MaskUtils } from '../../../core/mask-utils';
import { UtilsService } from '../../../core/utils.service';
import { StateAddress } from '../../../core/common-data-domain';
import { FuseSplashScreenService } from "@fuse/services/splash-screen.service";
import { Subject } from 'rxjs';

@Component({
  selector: 'tnm-new-brand',
  templateUrl: './new-brand.component.html',
  styleUrls: ['../brands.component.scss'],
})



export class NewBrandComponent extends BaseComponent implements OnInit {

  @ViewChild('stepper') 
  private stepper: MatStepper;

  public existEmail: boolean = false

  brandDataForm: FormGroup
  imageForm: FormGroup
  customerDataForm: FormGroup
  colorDataForm: FormGroup
  resumeForm: FormGroup
  brandList: Array<object>
  fileToUpload: File = null
  cnpjMask = MaskUtils.cnpjMask
  cpfMask = MaskUtils.cpfMask

  addressForm: FormGroup
  stateAddressList: any
  postalCodeMask =  MaskUtils.postalCodeMask

  url : string = 'assets/images/ecommerce/product-image-placeholder.png'
  parentName : string;
  brandId : number
  alias : string = ''
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  colors: {};
  selectedColor: string;
  selectedColorDefaultValue: string;
  

  searchSubject = new Subject()

  constructor(
    private router: Router,
    private injector: Injector,
    private formBuilder: FormBuilder,
    public  dialog: MatDialog,
    private customersService: CustomersService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<NewBrandComponent>,
    private snackbar: MatSnackBar,
    private brandsService: BrandsService,
    private utils: UtilsService,
    private loadingService: FuseSplashScreenService
  ) { 
    super(injector)
    this.colors = MatColors.all;
    this.updateSelectedColor('primary');
  }

  selectColor(selected)
    {
      this.updateSelectedColor(selected.value);
    }

    private updateSelectedColor(colorName)
    {
        this.selectedColor = colorName;
        this.selectedColorDefaultValue = MatColors.getColor(this.selectedColor)[500];
    }

  
  ngOnInit() {

    localStorage.setItem('hiddenLoading','true');

    this.brandId = parseInt(this.data)

    this.searchSubject
      .debounceTime(2000)
      .subscribe((val : string) => {
          this.loadCustomers(val)
    })

    this.loadDomainListData()

    this.brandsService.getBrandsTreeById(this.brandId).subscribe(brands => {
      this.brandList = brands.content
      this.parentName = brands.content[0].name
  })
    
    this.brandDataForm = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required]),
      parent: this.formBuilder.control({value : this.brandId, disabled: true}),
      corporateName:this.formBuilder.control('',[Validators.required]),
      alias: this.formBuilder.control('', [Validators.required]),
      registrationNumber: this.formBuilder.control('', [Validators.required, validCNPJ]),
    })

    this.colorDataForm = this.formBuilder.group({
      palette: this.formBuilder.control('',[Validators.required])
    })

    this.customerDataForm = this.formBuilder.group({
      name: this.formBuilder.control('',[Validators.required]),
      cpfCnpj: this.formBuilder.control('',[Validators.required, validCPF]),
      email: this.formBuilder.control('',[Validators.required, Validators.email]),
      password: this.formBuilder.control('',[Validators.required,Validators.minLength(8)]),
      passwordConfirm: this.formBuilder.control('',[Validators.required, confirmPassword])
    })
    
    this.imageForm = this.formBuilder.group({
      image: this.formBuilder.control(''),
    })


    this.resumeForm = this.formBuilder.group({
    
    })

    this.addressForm = this.formBuilder.group({
      business: this.formBuilder.control('', []),
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', [])
    })

  }

  
private loadDomainListData() {
  this.stateAddressList = this.utilsService.stringEnumToKeyValue(StateAddress)
}


  convertAlias( name : string ) {
    
    var mapHex 	= {
      a : /[\xE0-\xE6]/g,
      e : /[\xE8-\xEB]/g,
      i : /[\xEC-\xEF]/g,
      o : /[\xF2-\xF6]/g,
      u : /[\xF9-\xFC]/g,
      c : /\xE7/g,
      n : /\xF1/g
    };
  
    for ( var value in mapHex ) {
      var regularExpression = mapHex[value];
      name = name.replace( regularExpression, value );
    }
  
    this.alias =  name.toLowerCase().replace(/ +/g,'');

    return this.alias
  }

  handleFileInput(files: FileList) {
    
    this.fileToUpload = files.item(0);

    var reader = new FileReader();
        reader.readAsDataURL(files.item(0));
        reader.onload = (event) => { 
          this.url = event['target']['result'];
        }
  }


  createBrand() {

    if(this.existEmail){
      this.snackbar.open('O email informado já está em uso, informe outro e-mial', '', { duration: 5000 });
      return false;
    }

    localStorage.setItem('hiddenLoading','true');
    this.loadingService.show()

    let admin = {
      user:{
        brand:'',
        name: this.customerDataForm.value.name,
        login: this.customerDataForm.value.email,
        email: this.customerDataForm.value.email,
        password: this.customerDataForm.value.password,
        confirmPassword: this.customerDataForm.value.passwordConfirm,
        profile:"ADMINISTRATOR",
        status:"ACTIVE",
        cpfCnpj:this.utils.stringOnlyDigits(this.customerDataForm.value.cpfCnpj)
      },
      origin:"ADMIN"
  }

    let body ={
      'name': this.brandDataForm.value.name,
      'parent': this.brandId,
      'palette': this.colorDataForm.value.palette,
      'alias': this.convertAlias(this.alias),
      'corporateName':this.brandDataForm.value.corporateName,
      'registrationNumber': this.utils.numberOnlyDigits(this.brandDataForm.value.registrationNumber),
      'logo':''
    }

    this.brandsService.create(body)
      .subscribe(
        brand => {
          admin.user.brand = brand.id

          if(this.fileToUpload){

              const formData = new FormData();
                    formData.append('file',this.fileToUpload);
                    formData.append('type', this.fileToUpload.type);
                    formData.append('name', this.fileToUpload.name);
              
              this.brandsService.setPictureBrand(formData,brand.id).subscribe(
                picture => { })
          }

          let address ={
              "business": brand.id,
              "postalCode":this.addressForm.value.postalCode,
              "state": this.addressForm.value.state,
              "city": this.addressForm.value.city,
              "district": this.addressForm.value.district,
              "street": this.addressForm.value.street,
              "number": this.addressForm.value.number,
              "complement": this.addressForm.value.complement,
          }

          this.customersService.createCustomerAddress(address).subscribe()
          
          this.customersService.createAdmin(admin).subscribe( admin => {
            
            let brandUpdate = [{ "op": "add", "path": "/administrator", "value": admin.user['id'] }]

            this.brandsService.update(brand.id, brandUpdate)
              .subscribe(brand => {
                
                this.snackbar.open('Brand criada com sucesso', '', {
                  duration: 5000
                })
                this.dialogRef.close()

                this.loadingService.hide()
                localStorage.setItem('hiddenLoading','false');
                
              
              }, error => {
                console.log('Error:',error)
                this.loadingService.hide()
                localStorage.setItem('hiddenLoading','false');
                this.snackbar.open(error.error ? error.error.message : error.message, '', { duration: 5000 });
              })
          })
          
        }
        , error => {
          this.loadingService.hide()
          localStorage.setItem('hiddenLoading','false');
          this.snackbar.open(error.error ? error.error.message : error.message, '', { duration: 5000 });
     }
    )
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  loadCustomers(filter? : string) {

    this.customersService.getCustomersByEmail(
      filter).subscribe(result  => {
          if (result.content && result.content.length == 1) {
            this.snackbar.open('O e-mail informado já está em uso, informe outro e-mial', '', { duration: 5000 });
            this.existEmail = true;
          }else{
            this.existEmail = false;
          }
      })
  }

}

function validCPF(control: AbstractControl) {

    if ( !control.parent || !control )
      {
          return;
      }

    const cpfCnpj = control.parent.get('cpfCnpj');

    let c = cpfCnpj.value.replace(/\D+/g, '')

    if (!/[0-9]{11}/.test(c)){
        return {
          invalidCPF: true
      };
    } 

    var r;
    var s = 0;   
    
    for (var i=1; i<=9; i++)
      s = s + parseInt(c[i-1]) * (11 - i); 

    r = (s * 10) % 11;

    if ((r == 10) || (r == 11)) 
      r = 0;

    if (r != parseInt(c[9])){
      return {
        invalidCPF: true
      };
    }


    s = 0;

    for (i = 1; i <= 10; i++)
      s = s + parseInt(c[i-1]) * (12 - i);

    r = (s * 10) % 11;

    if ((r == 10) || (r == 11)) 
      r = 0;

    if (r != parseInt(c[10])){
      return {
        invalidCPF: true
      };
    }
    
    return
}

function validCNPJ(control: AbstractControl) {

  if ( !control.parent || !control )
    {
        return;
    }

  const registrationNumber = control.parent.get('registrationNumber');

  let cnpj = registrationNumber.value.replace(/\D+/g, '')

  var b = [6,5,4,3,2,9,8,7,6,5,4,3,2];

    if((cnpj = cnpj.replace(/[^\d]/g,"")).length != 14){
      return {
          invalidCNPJ: true
      }
    }
      

    if(/0{14}/.test(cnpj)){
      return {
          invalidCNPJ: true
      }
    }
    

    for (var i = 0, n = 0; i < 12; n += cnpj[i] * b[++i]);
    if(cnpj[12] != (((n %= 11) < 2) ? 0 : 11 - n)){
      return {
          invalidCNPJ: true
      }
    }

    for (var i = 0, n = 0; i <= 12; n += cnpj[i] * b[i++]);
    if(cnpj[13] != (((n %= 11) < 2) ? 0 : 11 - n))
    {
      return {
          invalidCNPJ: true
      }
    }
         
  return 

}

function confirmPassword(control: AbstractControl)
{
    if ( !control.parent || !control )
    {
        return;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return;
    }

    if ( passwordConfirm.value === '' )
    {
        return;
    }

    if ( password.value !== passwordConfirm.value )
    {
        return {
            passwordsNotMatch: true
        };
    }
}




