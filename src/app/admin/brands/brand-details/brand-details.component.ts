import { Component, ViewChild } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatTableDataSource, MatSnackBar, MatStepper, MatExpansionPanel } from '@angular/material'
import { fuseAnimations } from '@fuse/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { NewBrandComponent } from "../new-brand/new-brand.component";
import { DeleteBrandComponent } from '../delete-brand/delete-brand.component';
import { BrandsService } from "../../../core/services/brands.service";
import { MatColors } from '@fuse/mat-colors';
import { FormGroup } from '@angular/forms/src/model';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';
import { PalletColors } from '../../../core/models/pallet.model';
import { AppContext } from "../../../core/appcontext";
import { MaskUtils } from '../../../core/mask-utils';
import { UtilsService } from '../../../core/utils.service';
import { FuseConfig } from '@fuse/types';
import { StateAddress } from '../../../core/common-data-domain';
import { Address } from '../../../core/models/address.model';
import { NewCreditCardComponent } from "../../../payments/new-credit-card/new-credit-card.component";
import { PaymentsService } from '../../../core/services/payments.service';
import { PaymentType } from '../../../core/models/payment.model';
import { Identifiers } from '@angular/compiler';



@Component({
  selector: 'tnm-brand-details',
  templateUrl: './brand-details.component.html',
  styleUrls: ['./brand-details.component.scss'],
  animations: fuseAnimations
})
export class BrandDetailsComponent {

  @ViewChild('stepper')
  private stepper: MatStepper;

  brandName: string
  brandParent: number
  parentName: string
  resumeForm: FormGroup
  brandDataForm: FormGroup
  imageForm: FormGroup
  customerDataForm: FormGroup
  colorDataForm: FormGroup
  paymentDataForm: FormGroup
  brandSelected: object
  cnpjMask = MaskUtils.cnpjMask
  secondColorBrand: string = localStorage.getItem('secondColorBrand');
  businessAddress: Address
  isShowPermission: boolean = false
  addressForm: FormGroup
  stateAddressList: any
  postalCodeMask = MaskUtils.postalCodeMask
  permissionForm: FormGroup
  payments: Array<any> = []
  paymentDataSource: MatTableDataSource<any>
  paymentDisplayedColumns: Array<string>
  paymentTypeList: any
  fileToUpload: File = null
  url: string = 'assets/images/ecommerce/product-image-placeholder.png'
  brandId: number
  brandList: Array<object>
  subBrand: string;
  removeBrand: boolean = false
  colors: {};
  selectedColor: string;
  selectedColorDefaultValue: string;
  primaryColor: string
  alias = '';

  constructor(
    
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router,
    private brandsService: BrandsService,
    private snackbar: MatSnackBar,
    private appContext: AppContext,
    private utils: UtilsService,
    private paymentsService: PaymentsService,
    private fuseConfig: FuseConfigService
  ) {
    this.colors = MatColors.all;
    this.updateSelectedColor('primary');
  }


  ngOnInit() {

    this.loadDomainListData()

    this.brandsService.getBrands().subscribe(brands => {

      this.brandList = brands.content

      this.brandsService.getBrandsTreeById(this.brandId).subscribe(brands => {
        this.brandName = brands.content[0].name
        this.brandParent = brands.content[0].parent
        this.subBrand = brands.content[0].name
        this.loadForms(brands.content[0])
      })
    })

    this.brandId = parseInt(this.activatedRoute.snapshot.params['id'])
    this.initForms()

    this.loadAdress()
    this.loadPayments()
  }

  loadAdress() {

    let id = this.brandSelected && this.brandSelected['id'] ? this.brandSelected['id'] : this.brandId

    this.brandsService.getBrandAddress(id)
      .subscribe(addresses => {

        if (addresses.content != null && addresses.content.length > 0) {

          this.businessAddress = addresses.content[0]

          this.addressForm.setValue({
            business: this.businessAddress['business'] || null,
            street: this.businessAddress.street || null,
            number: this.businessAddress.number || null,
            complement: this.businessAddress.complement || null,
            district: this.businessAddress.district || null,
            city: this.businessAddress.city || null,
            state: this.businessAddress.state || null,
            postalCode: this.businessAddress.postalCode || null,
            reference: this.businessAddress.reference || null
          })
        }
      })

  }

  newBrandDialog() {
    this.dialog
      .open(NewBrandComponent, {
        width: '1000px',
        data: this.brandId
      })
      .afterClosed().subscribe(() => this.router.navigate(['admin/brands/']))
  }

  deleteBrandDialog() {
    this.dialog
      .open(DeleteBrandComponent, {
        width: '600px',
        data: this.brandSelected
      })
      .afterClosed().subscribe(() => this.router.navigate(['admin/brands/', this.brandId]))
  }

  loadForms(data) {

    this.brandDataForm = this.formBuilder.group({
      name: this.formBuilder.control(data.name, [Validators.required]),
      parent: this.formBuilder.control(data.parent, [Validators.required]),
      corporateName: this.formBuilder.control(data.corporateName, [Validators.required]),
      alias: this.formBuilder.control(data.alias ? data.alias : '', [Validators.required]),
      registrationNumber: this.formBuilder.control(data.registrationNumber ? data.registrationNumber : '', [Validators.required])
    })

    this.brandDataForm.controls.parent.disable({ onlySelf: true });

    if (data && data.alias) {
      this.brandDataForm.controls.alias.disable({ onlySelf: true });
    }
    if (data && data.registrationNumber) {
      this.brandDataForm.controls.registrationNumber.disable({ onlySelf: false });
    }

    if (data.alias) {
      this.alias = data.alias
    }

    if (data.palette) {
      let id = this.brandSelected && this.brandSelected['id'] ? this.brandSelected['id'] : this.brandId

      if (id == this.appContext.loggedUser.brand) {
        this.updateSelectedColor(data.palette);
      }
      this.selectedColor = data.palette
      this.colorDataForm = this.formBuilder.group({
        palette: this.formBuilder.control(data.palette)
      })
    }

    if (data.logo) {
      this.url = data.logo
      this.imageForm = this.formBuilder.group({
        image: data.logo
      })
    } else {
      this.url = 'assets/images/ecommerce/product-image-placeholder.png'
    }

    if (this.brandList && this.brandList.length > 0) {
      this.parentName = this.getParentName(data.parent)
    }

  }

  private loadDomainListData() {
    this.stateAddressList = this.utils.stringEnumToKeyValue(StateAddress)
  }

  convertAlias(name: string) {

    var mapHex = {
      a: /[\xE0-\xE6]/g,
      e: /[\xE8-\xEB]/g,
      i: /[\xEC-\xEF]/g,
      o: /[\xF2-\xF6]/g,
      u: /[\xF9-\xFC]/g,
      c: /\xE7/g,
      n: /\xF1/g
    };

    for (var value in mapHex) {
      var regularExpression = mapHex[value];
      name = name.replace(regularExpression, value);
    }

    this.alias = name.toLowerCase().replace(/ +/g, '');

    return this.alias
  }

  backMenuBrands() {
    this.router.navigate(['admin/brands'])
  }

  getParentName(id) {
    let name: ''

    this.brandList.forEach(brand => {
      if (brand['id'] == id) {
        name = brand['name']
      }
    });

    return name
  }

  initForms() {

    this.brandDataForm = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required]),
      parent: this.formBuilder.control(this.brandId, [Validators.required]),
      alias: this.formBuilder.control('', [Validators.required]),
      corporateName: this.formBuilder.control('', [Validators.required]),
      registrationNumber: this.formBuilder.control('', [Validators.required])
    })

    this.brandDataForm.controls.parent.disable({ onlySelf: true });

    this.colorDataForm = this.formBuilder.group({
      palette: this.formBuilder.control('')
    })

    this.imageForm = this.formBuilder.group({
      image: this.formBuilder.control(''),
    })

    this.resumeForm = this.formBuilder.group({})

    this.paymentDataForm = this.formBuilder.group({})

    this.addressForm = this.formBuilder.group({
      business: this.formBuilder.control('', []),
      street: this.formBuilder.control('', [Validators.required]),
      number: this.formBuilder.control('', [Validators.required]),
      complement: this.formBuilder.control('', []),
      district: this.formBuilder.control('', [Validators.required]),
      city: this.formBuilder.control('', [Validators.required]),
      state: this.formBuilder.control('', [Validators.required]),
      postalCode: this.formBuilder.control('', [Validators.required, Validators.minLength(8)]),
      reference: this.formBuilder.control('', [])
    })

    this.paymentDisplayedColumns = ['type', 'creditCardBrand', 'creditCardNumber', 'delete'];
    this.paymentDataSource = new MatTableDataSource<any>(this.payments);
  }

  onFilterPermission(status: boolean) {
    
    this.isShowPermission = status
  }


  parentBrand(){
    this.onFilterBrand(this.brandId.toString())
  }

  

  onFilterBrand(id: string) {

    this.brandsService.getBrandsTreeById(id).subscribe(brands => {

      this.brandSelected = brands.content[0]

      if (this.brandName != brands.content[0].name) {
        this.subBrand = this.brandName + " / " + brands.content[0].name
        this.removeBrand = true
      } else {
        this.subBrand = this.brandName
        this.removeBrand = false
      }

      this.loadForms(brands.content[0])
      this.loadAdress()
      this.loadPayments()

      this.stepper.selectedIndex = 0

    })

  }

  handleFileInput(files: FileList) {

    this.fileToUpload = files.item(0);

    var reader = new FileReader();

    reader.readAsDataURL(files.item(0));

    reader.onload = (event) => {
      this.url = event['target']['result'];
    }
  }

  updateBrand() {

    let editData = []

    for (let item in this.brandDataForm.value) {
      let _value = this.brandDataForm.value[item]

      if (item == 'alias') {
        _value = this.convertAlias(this.brandDataForm.value[item])
      } else if (item == 'registrationNumber') {
        _value = this.utils.numberOnlyDigits(this.brandDataForm.value[item])
      }

      let editItem = { 'op': 'add', 'path': '/' + item, 'value': _value };
      editData.push(editItem);
    }

    for (let item in this.colorDataForm.value) {
      let editItem = { 'op': 'add', 'path': '/' + item, 'value': this.colorDataForm.value[item] };
      editData.push(editItem);
    }

    let id = this.brandSelected && this.brandSelected['id'] ? this.brandSelected['id'] : this.brandId

    let address = {
      "business": id,
      "postalCode": this.addressForm.value.postalCode,
      "state": this.addressForm.value.state,
      "city": this.addressForm.value.city,
      "district": this.addressForm.value.district,
      "street": this.addressForm.value.street,
      "number": this.addressForm.value.number,
      "complement": this.addressForm.value.complement,
    }


    if (!this.businessAddress) {
      this.brandsService.createBrandAddress(address).subscribe()
    } else {
      let editAdress = []

      this.addressForm.patchValue({
        business: id
      })

      for (let item in this.addressForm.value) {
        let editItem = { 'op': 'add', 'path': '/' + item, 'value': this.addressForm.value[item] };
        editAdress.push(editItem);
      }
      this.brandsService.updateBrandAddress(this.businessAddress.id, editAdress).subscribe()
    }

    if (this.fileToUpload) {
      const formData = new FormData();
      formData.append('file', this.fileToUpload);
      formData.append('type', this.fileToUpload.type);
      formData.append('name', this.fileToUpload.name);

      this.brandsService.setPictureBrand(formData, id).subscribe(
        picture => { })
    }


    this.brandsService.update(id, editData)
      .subscribe(brand => {
        this.snackbar.open('Marca atualizada com sucesso', '', {
          duration: 5000
        })
        this.router.navigate(['admin/brands'])

      }, error => {
        this.snackbar.open(error.error ? error.error.message : error.message, '', { duration: 5000 });
      })

  }

  selectColor(selected) {
    let id = this.brandSelected && this.brandSelected['id'] ? this.brandSelected['id'] : this.brandId

    if (id == this.appContext.loggedUser.brand) {

      this.primaryColor = PalletColors[selected.value] + '-A700-bg'
      this.secondColorBrand = PalletColors[selected.value] + '-A400-bg'

      localStorage.setItem('primaryColorBrand', this.primaryColor);
      localStorage.setItem('secondColorBrand', this.secondColorBrand);
      this.setColorBrand()

    }

    this.updateSelectedColor(selected.value);
  }

  private updateSelectedColor(colorName) {
    this.selectedColor = colorName;
    this.selectedColorDefaultValue = MatColors.getColor(this.selectedColor)[500];
  }

  setColorBrand() {

    let fuse: FuseConfig = {
      layout: {
        style: 'vertical-layout-1',
        width: 'fullwidth',
        navbar: {
          hidden: false,
          position: 'left',
          folded: false,
          background: this.primaryColor
        },
        toolbar: {
          hidden: false,
          position: 'below-static',
          background: this.primaryColor
        },
        footer: {
          hidden: false,
          position: 'below-static',
          background: this.primaryColor
        }
      },
      customScrollbars: true
    };

    this.fuseConfig.setConfig(fuse)
    this.fuseConfig.setDefault(fuse)
  }

  loadPayments() {
    let id = this.brandSelected && this.brandSelected['id'] ? this.brandSelected['id'] : this.brandId

    this.paymentsService.getPaymentsByBrand(id)
      .subscribe(payments => {
        if (payments.content != null) {
          this.payments = payments.content
          this.paymentDataSource = new MatTableDataSource<any>(this.payments);
        }
      })
  }

  addNewPayment(type) {

    let id = this.brandSelected && this.brandSelected['id'] ? this.brandSelected['id'] : this.brandId

    this.dialog
      .open(NewCreditCardComponent, { width: '1100px', height: '550px', data: { business: id, type: type } })
      .afterClosed()
      .subscribe(response => {
        this.loadPayments()
      })
  }

  deletePayment(payment) {
    let idx = this.payments.indexOf(payment)
    if (idx != -1) {

      this.paymentsService.delete(payment.id)
        .subscribe(result => {
          this.payments.splice(idx, 1)
          this.paymentDataSource = new MatTableDataSource<any>(this.payments)
        })
    }
  }

  paymentTypeEnumToString(value): string {
    return PaymentType[value]
  }

  expandPanel(matExpansionPanel: MatExpansionPanel, brand: Object, id: string) {
    event.stopPropagation();

    if(brand['children'] && brand['children'].length == 0){
      matExpansionPanel.close();
    }
    
    if(id == '2' && brand['children'] && brand['children'].length > 0){
      matExpansionPanel.close();
      //this.loadBrands(brand['name'],brand['parent'])
    }

  }

  onPermissionChangeDefault(index){
    // if(this.permissionList[index].status){
    //   this.permissionList[index].status = false
    // }else{
    //   this.permissionList[index].status = true
    // }
  }

  orderByName(list){
    if(list && list.length > 0){
        return list.sort(function(a,b) {
            return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });
    }else{
        return list
    }
  }

  loadBrandDialog(matExpansionPanel: MatExpansionPanel,id){
    event.stopPropagation();
    
    if(matExpansionPanel.expanded){
      matExpansionPanel.open();
    }else{
      matExpansionPanel.close();
    }
    
    // this.dialog
    // .open(NewBrandComponent, {
    //   width: '1000px',
    //   data: id
    // })
    // .afterClosed().subscribe(() => this.loadBrands('',0))
  }

  toggleRow(id) {
    this.router.navigate(['admin/brands/', id])
  }


}