import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { BrandsService } from "../../../core/services/brands.service";

@Component({
  selector: 'tnm-delete-brand',
  templateUrl: './delete-brand.component.html',
  styleUrls: ['./delete-brand.component.scss']
})
export class DeleteBrandComponent implements OnInit {

  brandSelected : object
  secondColorBrand : string = localStorage.getItem('secondColorBrand');

  constructor(
    private dialogRef: MatDialogRef<DeleteBrandComponent>,
    private brandsService: BrandsService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private router: Router,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit() {
    this.brandSelected = this.data
  }

  deleteDevice(){
    this.brandsService.delete(this.brandSelected['id'])
      .subscribe(response => {
        this.dialogRef.close()
        this.snackbar.open('Marca excluído com sucesso', '', {
          duration: 5000
        })
        this.router.navigate(['admin/brands'])
      })
  }


}

