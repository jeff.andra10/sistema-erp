import { NgModule, ModuleWithProviders } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { TextMaskModule } from 'angular2-text-mask';
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../../angular-material/material.module";
import { FuseSharedModule } from "@fuse/shared.module";
import { BrandsComponent } from "./brands.component";
import { BrandsSidenavComponent } from './brands-side/brands-side.component';
import { BrandDetailsComponent } from "./brand-details/brand-details.component";
import { NewBrandComponent } from "./new-brand/new-brand.component";
import { DeleteBrandComponent } from './delete-brand/delete-brand.component';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatTreeModule } from '@angular/material/tree';



@NgModule({
    declarations:[
        BrandsComponent,
        BrandsSidenavComponent,
        BrandDetailsComponent,
        NewBrandComponent,
        DeleteBrandComponent
    ],
    imports: [
        FlexLayoutModule,
        TextMaskModule,
        CommonModule,
        ReactiveFormsModule,
        MaterialModule,
        FuseSharedModule,
        CdkTreeModule,
        MatTreeModule
        
    ],
    exports: [
        NewBrandComponent,
        DeleteBrandComponent
    ],
    entryComponents: [
        NewBrandComponent,
        DeleteBrandComponent
    ]
})
export class BrandsModule {}