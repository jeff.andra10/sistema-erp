import { Component, OnInit, ViewChild, Input } from '@angular/core';
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/switchMap'
import { MatPaginator, MatSort, MatExpansionPanel,MatDialog,MatSnackBar} from '@angular/material';
import { BrandsService } from "../../core/services/brands.service";
import { Subject } from 'rxjs';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { AppContext } from "../../core/appcontext";
import { BaseComponent } from '../../_base/base-component.component';
import 'rxjs/add/observable/of';
import { Router } from '@angular/router';
import {MatAccordion} from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { NewBrandComponent } from "./new-brand/new-brand.component";



@Component({
  selector: 'tnm-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.scss'],
  animations: fuseAnimations
 
})

export class BrandsComponent implements OnInit {

  @ViewChild(MatAccordion) accordion: MatAccordion;

  brandsList : Array<any>
  brandChildrenList : Array<any>
  
  resultLength: number = 0
  searchSubject = new Subject()
  
  searchForm: FormGroup
  searchControl: FormControl 
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
 

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private brandsService: BrandsService,
    private appContext: AppContext,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
   }


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  expandPanel(matExpansionPanel: MatExpansionPanel, brand: Object, id: string) {
    event.stopPropagation();

    if(brand['children'] && brand['children'].length == 0){
      matExpansionPanel.close();
    }
    
    if(id == '2' && brand['children'] && brand['children'].length > 0){
      matExpansionPanel.close();
      this.loadBrands(brand['name'],brand['parent'])
    }

  }
  
  ngOnInit() {
    localStorage.setItem('hiddenLoading','false');
    this.initForms()
    this.loadBrands('',this.appContext.loggedUser.brand)
  }

  loadBrands(filter : string,parent: number){
    this.brandsService.getBrandsByTree(filter,parent).subscribe(brands => {
      this.brandsList = brands.content   
      this.brandChildrenList = brands.content[0].brandChildren
    })
  }

  orderByName(list){
    if(list && list.length > 0){
        return list.sort(function(a,b) {
            return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
        });
    }else{
        return list
    }
}

  toggleRow(id) {
      this.router.navigate(['admin/brands/', id])
   }

  initForms() {

    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadBrands(val,this.appContext.loggedUser.brand)
    })

    this.searchForm = this.formBuilder.group({
      searchControl: this.searchControl,
      parent: this.formBuilder.control('')
    });
  }

  searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }  

  newBrandDialog(matExpansionPanel: MatExpansionPanel,id){
    event.stopPropagation();
    
    if(matExpansionPanel.expanded){
      matExpansionPanel.open();
    }else{
      matExpansionPanel.close();
    }
    
    this.dialog
    .open(NewBrandComponent, {
      width: '1000px',
      data: id
    })
    .afterClosed().subscribe(() => this.loadBrands('',0))
  }

}

