import { NgModule, ModuleWithProviders } from "@angular/core";
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import { MaterialModule } from "../../angular-material/material.module";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { NewSupplierComponent } from "./new-supplier/new-supplier.component";
import { EditSupplierComponent } from "./edit-supplier/edit-supplier.component";
import { DeleteSupplierComponent } from "./delete-supplier/delete-supplier.component";
import { SuppliersComponent } from "./suppliers.component";
import { FuseSharedModule } from "@fuse/shared.module";

@NgModule({
    declarations:[
        SuppliersComponent,
        NewSupplierComponent,
        EditSupplierComponent,
        DeleteSupplierComponent
    ],
    imports: [
        TextMaskModule,
        FlexLayoutModule,
        CommonModule,
        ReactiveFormsModule,
        MaterialModule,
        FuseSharedModule
    ],
    exports: [
        NewSupplierComponent,
        EditSupplierComponent,
        DeleteSupplierComponent
    ],
    entryComponents: [
        NewSupplierComponent,
        EditSupplierComponent,
        DeleteSupplierComponent
    ]
})
export class SuppliersModule {}