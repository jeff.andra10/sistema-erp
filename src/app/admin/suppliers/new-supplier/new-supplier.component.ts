import { Component, OnInit, Injector } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { SuppliersService } from '../../../core/services/suppliers.service';
import { Supplier } from '../../../core/models/supplier.model';
import { MaskUtils } from '../../../core/mask-utils';
import { DataDomain } from '../../../core/common-data-domain';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-new-supplier',
  templateUrl: './new-supplier.component.html',
})
export class NewSupplierComponent extends BaseComponent implements OnInit {
  
  supplierForm: FormGroup
  brandList: Array<any>
  bankList: any

  phoneMask = MaskUtils.phoneMask
  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(
    private injector: Injector,
    private suppliersService: SuppliersService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<NewSupplierComponent>,
    private snackbar: MatSnackBar
  ) { 
    super(injector)
  }

  ngOnInit() {
    this.createForm()
    this.brandList = this.appContext.brands
    this.bankList = DataDomain.getBanks()
  }

  createForm() {
    this.supplierForm = this.formBuilder.group({
      personType: this.formBuilder.control('COMPANY', [Validators.required]),
      registrationNumber: this.formBuilder.control('', [Validators.required]),
      companyName: this.formBuilder.control('', [Validators.required]),
      tradingName: this.formBuilder.control('', [Validators.required]),
      contactPerson: this.formBuilder.control('', [Validators.required]),
      email: this.formBuilder.control('', [Validators.required]),
      phone: this.formBuilder.control('', [Validators.required]),
      address: this.formBuilder.control('', [Validators.required]),
      bankCode: this.formBuilder.control('', []),
      bankBranch: this.formBuilder.control('', []),
      bankAccount: this.formBuilder.control('', []),
      merchantId: this.formBuilder.control('', []),
      merchantKey: this.formBuilder.control('', []),
    })

    this.supplierForm.valueChanges.subscribe((newForm) => {
      
      if(newForm.personType == 'PERSON'){
        this.supplierForm.controls.tradingName.setValidators(null)
      }else{
        this.supplierForm.controls.tradingName.setValidators([Validators.required])
      } 
    });
  }

  createSupplier(body) {
    
    let supplier = new Supplier()
    supplier.brand = this.appContext.session.user.brand
    supplier.companyName = body.companyName
    supplier.tradingName = body.tradingName
    supplier.personType = body.personType
    supplier.registrationNumber = this.utilsService.stringOnlyDigits(body.registrationNumber)
    supplier.contactPerson = body.contactPerson
    supplier.email = body.email
    supplier.phone = this.utilsService.stringOnlyDigits(body.phone)
    supplier.address = body.address
    supplier.bankCode = body.bankCode
    supplier.bankBranch = body.bankBranch
    supplier.bankAccount = body.bankAccount
    supplier.merchantId = body.merchantId
    supplier.merchantKey = body.merchantKey

    this.suppliersService.create(supplier)
      .subscribe(suppliers => {
        this.snackbar.open('Fornecedor criado com sucesso', '', {
          duration: 8000
        })
        this.dialogRef.close()
      })
  }

}
