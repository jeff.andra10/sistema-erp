import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Supplier } from '../../../core/models/supplier.model';
import { SuppliersService } from '../../../core/services/suppliers.service';

@Component({
  selector: 'tnm-delete-supplier',
  templateUrl: './delete-supplier.component.html',
  styleUrls: ['./delete-supplier.component.scss']
})
export class DeleteSupplierComponent implements OnInit {

  supplier: Supplier

  constructor(
    private dialogRef: MatDialogRef<DeleteSupplierComponent>,
    private supplierService: SuppliersService,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackbar: MatSnackBar

  ) { }

  ngOnInit() {
    this.supplier = this.data
  }

  deleteSupplier(){
    this.supplierService.delete(this.supplier.id)
      .subscribe(suppliers => {
        this.dialogRef.close()
        this.snackbar.open('Fornecedor excluído com sucesso', '', {
          duration: 8000
        })

      })
  }


}
