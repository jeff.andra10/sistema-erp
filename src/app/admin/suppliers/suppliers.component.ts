import { Component, OnInit, Injector, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { merge } from "rxjs/observable/merge";
import { tap } from 'rxjs/operators'
import { Subject } from 'rxjs';

import { EditSupplierComponent } from './edit-supplier/edit-supplier.component';
import { NewSupplierComponent } from './new-supplier/new-supplier.component';
import { DeleteSupplierComponent } from './delete-supplier/delete-supplier.component';
import { SuppliersService } from '../../core/services/suppliers.service';
import { UtilsService } from '../../core/utils.service';
import { BaseComponent } from '../../_base/base-component.component';
import { fuseAnimations } from '@fuse/animations';
import { SuppliersDataSource } from './suppliers.datasource';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss'],
  animations : fuseAnimations
})
export class SuppliersComponent extends BaseComponent implements OnInit, AfterViewInit {

  brandList: Array<any>
  displayedColumns = ['companyName', 'email', 'registrationNumber', 'edit', 'delete'];
  dataSource: SuppliersDataSource
  
  resultLength: number = 0
  searchSubject = new Subject()
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  primaryColorBrand  : string = localStorage.getItem('primaryColorBrand');
  
  

  @ViewChild(MatPaginator) paginator: MatPaginator
  @ViewChild(MatSort) sort: MatSort

  constructor(
    private injector: Injector,
    private suppliersService: SuppliersService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private utils: UtilsService
  ) {
    super(injector)
   }

   searchOnChange(event){
    this.searchSubject.next(event.currentTarget.value);
  }

  ngOnInit() {
    localStorage.setItem('hiddenLoading','false');
    this.brandList = this.appContext.brands
    
    this.searchSubject
    .debounceTime(500)
    .subscribe((val : string) => {
      this.loadSuppliers(val)
    })

    this.dataSource = new SuppliersDataSource(this.suppliersService)
    this.dataSource.load('', 'companyName', 'asc', 0, 10)
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0)

    if(this.dataSource && this.dataSource.totalElements$){
      this.dataSource.totalElements$.subscribe( value => {
        this.resultLength = value
      })
    }
    
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
        tap(() => this.loadSuppliers())
    )
    .subscribe()
  }

  loadSuppliers(filter? : string) {

    this.dataSource.load(
      filter, 
      this.sort.active,
      this.sort.direction, 
      this.paginator.pageIndex, 
      this.paginator.pageSize)
  } 

  editSupplierDialog(row) {
    this.dialog
      .open(EditSupplierComponent, {
        data: row,
        width: '800px',
      })
      .afterClosed()
        .subscribe(() => this.loadSuppliers())
  }

  newSupplierDialog() {
    this.dialog
      .open(NewSupplierComponent, {
        width: '800px',
      })
      .afterClosed()
        .subscribe(() => this.loadSuppliers())
  }

  deleteSupplierDialog(row){
    this.dialog
      .open(DeleteSupplierComponent, {
        data: row,
        width: '500px',
      })
      .afterClosed()
        .subscribe(() => this.loadSuppliers())
  }

}
