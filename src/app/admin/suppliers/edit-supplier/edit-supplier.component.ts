import { Component, OnInit, Inject, Injector} from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import { BaseComponent } from '../../../_base/base-component.component';
import { SuppliersService } from '../../../core/services/suppliers.service';
import { Supplier } from '../../../core/models/supplier.model';
import { DataDomain } from '../../../core/common-data-domain';
import { MaskUtils } from '../../../core/mask-utils';
import { Subscription } from 'rxjs/Subscription';
import { FuseConfigService } from '@fuse/services/config.service';

@Component({
  selector: 'tnm-edit-supplier',
  templateUrl: './edit-supplier.component.html'
})
export class EditSupplierComponent extends BaseComponent implements OnInit {
 
  supplierForm: FormGroup
  brandList: Array<any>
  submitted: boolean
  currentId: number
  bankList: any

  phoneMask = MaskUtils.phoneMask
  cpfMask = MaskUtils.cpfMask
  cnpjMask = MaskUtils.cnpjMask
  secondColorBrand : string = localStorage.getItem('secondColorBrand');
  
  

  constructor(
    private injector: Injector,
    private dialogRef: MatDialogRef<EditSupplierComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private suppliersService: SuppliersService,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar
  ) { 
    super(injector)
  }

  ngOnInit() {
    this.brandList = this.appContext.brands
    this.bankList = DataDomain.getBanks()
    this.createForm(this.data)
  }

  createForm(data: Supplier){
    this.currentId = data.id

    this.supplierForm = this.formBuilder.group({
      personType: this.formBuilder.control(data.personType, [Validators.required]),
      registrationNumber: this.formBuilder.control(data.registrationNumber, [Validators.required]),
      companyName: this.formBuilder.control(data.companyName, [Validators.required]),
      tradingName: this.formBuilder.control(data.tradingName, [Validators.required]),
      contactPerson: this.formBuilder.control(data.contactPerson, [Validators.required]),
      email: this.formBuilder.control(data.email, [Validators.required]),
      phone: this.formBuilder.control(data.phone, [Validators.required]),
      address: this.formBuilder.control(data.address, [Validators.required]),
      bankCode: this.formBuilder.control(data.bankCode, []),
      bankBranch: this.formBuilder.control(data.bankBranch, []),
      bankAccount: this.formBuilder.control(data.bankAccount, []),
      merchantId: this.formBuilder.control(data.merchantId, []),
      merchantKey: this.formBuilder.control(data.merchantKey, []),
    })

    this.supplierForm.valueChanges.subscribe((newForm) => {
      
      if(newForm.personType == 'PERSON'){
        this.supplierForm.controls.tradingName.setValidators(null)
      }else{
        this.supplierForm.controls.tradingName.setValidators([Validators.required])
      } 
    });
  }

  updateSupplier(editForm) {
    let supplierEditData = []
    for(let item in editForm){
      let editItem = {'op': 'add', 'path': '/'+item, 'value': editForm[item]};
      supplierEditData.push(editItem);
    }
    
    this.suppliersService.update(this.currentId, supplierEditData)
      .subscribe(supplier => {
        this.snackbar.open('Fornecedor atualizado com sucesso', '', {
          duration: 8000
        })
        this.dialogRef.close()
      })
  }

}
